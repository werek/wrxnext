<?php

/**
 * klasa glowna przechowujaca konfiguracje aplikacji
 *
 */
class wrxNext
{

    /**
     * zamienia biale znaki na podkreslenia, opcjonalnie
     * nadpisujac wykryte kodowanie podanego stringa
     * zadana wartoscia
     *
     * @param string $string
     * @param string $sourceEncoding
     * @return string
     */
    public static function prepareForUrl($string, $sourceEncoding = null)
    {
        // return preg_replace('/\s/','_',$string);
        // konwersja znaków utf do znaków podstawowych;
        $filter = new wrxNext_Filter_UrlText($sourceEncoding);
        return $filter->filter($string);
    }

    /**
     * przekierowuje do podanego adresu
     *
     * @param string $url
     */
    /*
     * public static function redirect($url,$internal=true){ if ($internal) {
     * $url=trim($url,'/'); $url=wrx::config()->dir->base.'/'.$url; }
     * header('Location: '.$url,true,302); exit(); }
     */
    /**
     * usuwa tagi html i php z danych
     *
     * @param string|array $array
     * @return string array
     */
    public static function stripTags(&$array)
    {
        if (is_array($array)) {
            array_walk_recursive($array, array(
                self,
                _stripTags
            ));
        } else {
            $array = strip_tags($array);
        }
        return $array;
    }

    /**
     * konwertuje znaki specjalne na entytki
     *
     * @param string|array $array
     * @return string array
     */
    public static function htmlEntities(&$array)
    {
        if (is_array($array)) {
            array_walk_recursive($array, array(
                self,
                '_htmlEntities'
            ));
        } else {
            $array = strip_tags($array);
        }
        return $array;
    }

    /**
     * konwertuje znaki specjalne i narodowe na entytki
     *
     * @param string|array $array
     * @return string array
     */
    public static function htmlEntitiesMB(&$array)
    {
        if (is_array($array)) {
            array_walk_recursive($array, array(
                self,
                '_htmlEntitiesMB'
            ));
        } else {
            $array = strip_tags($array);
        }
        return $array;
    }

    /**
     * usuwa escape'y z danych/stringa tabeli
     *
     * @var array string
     * @return array string
     */
    public static function stripSlashes(&$array)
    {
        if (is_array($array)) {
            array_walk_recursive($array, array(
                self,
                '_stripSlashes'
            ));
        } else {
            $array = stripslashes($array);
        }
        return $array;
    }

    /**
     * Smarty escape modifier plugin
     *
     * Type: modifier<br>
     * Name: escape<br>
     * Purpose: Escape the string according to escapement type
     *
     * @link http://smarty.php.net/manual/en/language.modifier.escape.php
     *       escape (Smarty online manual)
     * @author Monte Ohrt <monte at ohrt dot com>
     * @param string $string
     * @param string $esc_type html|htmlall|url|quotes|hex|hexentity|javascript
     * @param string $char_set
     * @return string
     */
    public static function escape($string, $esc_type = 'html',
                                  $char_set = 'ISO-8859-1')
    {
        $filter = new wrxNext_Filter_Escape();
        return $filter->setEscapeType($esc_type)
            ->setEncodingType($char_set)
            ->filter($string);
    }

    /**
     * zapewnia obsluge rekurencji dla array_walk_recursive
     *
     * @param mixed $value
     * @param mixed $key
     */
    private static function _stripTags(&$value, $key)
    {
        $value = strip_tags($value);
    }

    /**
     * zapewnia obsluge rekurencji dla array_walk_recursive
     *
     * @param mixed $value
     * @param mixed $key
     */
    private static function _htmlEntities(&$value, $key)
    {
        $value = htmlentities($value);
    }

    /**
     * zapewnia obsluge rekurencji dla array_walk_recursive
     *
     * @param mixed $value
     * @param mixed $key
     */
    private static function _htmlEntitiesMB(&$value, $key)
    {
        $value = mb_convert_encoding($value, 'HTML-ENTITIES');
    }

    /**
     * zwraca status debug'owania
     *
     * @return bool
     */
    /*
     * public static function debug($ommitAjax=false){ if
     * (wrx_Ajax_Prototype::isXMLHTTPRequest() && !$ommitAjax) { return false; }
     * if (self::config()->debug=="true") { return true; } return false; }
     */

    /**
     * zapewnia obsluge rekurencji dla array_walk_recursive
     *
     * @param mixed $value
     * @param mixed $key
     */
    private static function _stripSlashes(&$value, $key)
    {
        $value = stripslashes($value);
    }
}

?>
