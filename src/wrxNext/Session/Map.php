<?php

class wrxNext_Session_Map implements ArrayAccess, Countable
{

    /**
     * przechwouje nazwe mapowania
     *
     * @var string
     */
    private $_mapName;

    /**
     * tworzy warstwe posrednictwa wobec mapowania o podanej nazwie
     *
     * @param string $mapName
     */
    public function __construct($mapName)
    {
        $this->_mapName = $mapName;
    }

    /**
     * implementacja magicznej metody do pobierania danych
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $_SESSION[__CLASS__][$this->_mapName][$name];
    }

    /**
     * implementacja magicznej metody do ustawiania danych
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $_SESSION[__CLASS__][$this->_mapName][$name] = $value;
    }

    /**
     * inicjalizje zmienna jezeli nie zostala stworzona
     *
     * @param string $varName
     * @param mixed $value
     */
    public function initializeVar($varName, $value = null)
    {
        if (!isset($_SESSION[__CLASS__][$this->_mapName][$varName])) {
            $_SESSION[__CLASS__][$this->_mapName][$varName] = $value;
        }
    }

    /**
     * zwraca tablice wszystkich zmiennych
     *
     * @return array
     */
    public function toArray()
    {
        return $_SESSION[__CLASS__][$this->_mapName];
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return mixed
     */
    public function offsetGet($name)
    {
        return $_SESSION[__CLASS__][$this->_mapName][$name];
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @param mixed $value
     */
    public function offsetSet($name, $value)
    {
        $_SESSION[__CLASS__][$this->_mapName][$name] = $value;
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return bool
     */
    public function offsetExists($name)
    {
        return isset($_SESSION[__CLASS__][$this->_mapName][$name]);
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     */
    public function offsetUnset($name)
    {
        unset($_SESSION[__CLASS__][$this->_mapName][$name]);
    }

    /**
     * implementacja Countable
     *
     * @return int
     */
    public function count()
    {
        return count($_SESSION[__CLASS__][$this->_mapName][$name]);
    }
}
