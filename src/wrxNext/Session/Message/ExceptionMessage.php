<?php

class wrxNext_Session_Message_ExceptionMessage implements
    wrxNext_Session_Message_MessageInterface
{

    /**
     * holds exception
     *
     * @var \Exception
     */
    protected $_exception = null;

    /**
     * constructor
     *
     * @param \Exception $exception
     */
    public function __construct(\Exception $exception = null)
    {
        if (!is_null($exception)) {
            $this->setException($exception);
        }
    }

    /**
     * gets exception
     *
     * @return \Exception
     */
    public function getException()
    {
        return $this->_exception;
    }

    /**
     * sets exception
     *
     * @param \Exception $exception
     * @return wrxNext_Session_Message_ExceptionMessage
     */
    public function setException(\Exception $exception)
    {
        $this->_exception = $exception;
        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Session_Message_MessageInterface::getValue()
     */
    public function getValue()
    {
        return get_class($this->_exception) . '(' . $this->_exception->getCode() .
        '): ' . $this->_exception->getMessage();
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Session_Message_MessageInterface::getType()
     */
    public function getType()
    {
        return wrxNext_Session_Message_Text::ERROR;
    }
}
