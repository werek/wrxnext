<?php

/**
 * session message interface
 *
 * @author werek
 *
 */
interface wrxNext_Session_Message_MessageInterface
{

    /**
     * returns value to display
     *
     * @return mixed
     */
    public function getValue();

    /**
     * returns type of message, according to @see wrxNext_Session_Message_Text
     *
     * @return string
     */
    public function getType();
}
