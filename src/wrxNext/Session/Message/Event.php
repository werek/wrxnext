<?php

class wrxNext_Session_Message_Event implements wrxNext_Session_Message_MessageInterface
{

    /**
     * holds type of event
     *
     * @var string
     */
    protected $_type = null;

    /**
     * holds event name
     *
     * @var string
     */
    protected $_value = null;

    /**
     * constructor, takes event name and type
     *
     * @param string $eventName
     * @param string $eventType
     */
    public function __construct($eventName, $eventType)
    {
        $this->_value = $eventName;
        $this->_type = $eventType;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Session_Message_MessageInterface::getValue()
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Session_Message_MessageInterface::getType()
     */
    public function getType()
    {
        return $this->_type;
    }
}
