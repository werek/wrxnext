<?php

class wrxNext_Session_Message_Text implements wrxNext_Session_Message_MessageInterface
{

    const INFO = 'info';

    const SUCCESS = 'success';

    const WARNING = 'warning';

    const ERROR = 'error';

    /**
     * holds value
     *
     * @var mixed
     */
    protected $_value = '';

    /**
     * holds type
     *
     * @var string
     */
    protected $_type = '';

    /**
     * constructor recieves message to display and message type
     *
     * @param mixed $value
     * @param string $type
     */
    public function __construct($value, $type = self::INFO)
    {
        $this->setValue($value);
        $this->setType($type);
    }

    /**
     * gets value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * sets value
     *
     * @param string $mixed
     * @return wrxNext_Session_Message_Text
     */
    public function setValue($mixed)
    {
        $this->_value = $mixed;
        return $this;
    }

    /**
     * gets message type
     *
     * @return string
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * sets message type
     *
     * @param string $type
     * @return wrxNext_Session_Message_Text
     */
    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }
}
