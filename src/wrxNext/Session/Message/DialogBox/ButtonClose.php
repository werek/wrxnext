<?php

class wrxNext_Session_Message_DialogBox_ButtonClose implements
    wrxNext_Session_Message_DialogBox_ButtonInterface
{

    /**
     * holds label
     *
     * @var unknown
     */
    protected $label = '';

    /**
     * constructor takes label to display
     *
     * @param string $label
     */
    public function __construct($label)
    {
        $this->setLabel($label);
    }

    /**
     * gets label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * sets label
     *
     * @param string $text
     * @return wrxNext_Session_Message_DialogBox_ButtonClose
     */
    public function setLabel($text)
    {
        $this->label = $text;
        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Session_Message_DialogBox_ButtonInterface::getAction()
     */
    public function getAction()
    {
        return 'function(){$(this).dialog(\'close\');}';
    }
}
