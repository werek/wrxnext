<?php

class wrxNext_Session_Message_DialogBox_ButtonJs implements
    wrxNext_Session_Message_DialogBox_ButtonInterface
{

    /**
     * holds button label
     *
     * @var string
     */
    protected $label = '';

    /**
     * holds jscode
     *
     * @var string
     */
    protected $jsCode = '';

    public function __construct($label, $code)
    {
        $this->setLabel($label);
        $this->setJsCode($code);
    }

    /**
     * gets label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * sets label
     *
     * @param string $label
     * @return wrxNext_Session_Message_DialogBox_ButtonJs
     */
    public function setLabel($label)
    {
        $this->label = $label;
        return $this;
    }

    public function getAction()
    {
        return sprintf('function(){%s}', $this->getJsCode());
    }

    /**
     * gets Javascript Code to execute on button click
     *
     * @return string
     */
    public function getJsCode()
    {
        return $this->jsCode;
    }

    /*
     * (non-PHPdoc) @see
     * wrxNext_Session_Message_DialogBox_ButtonInterface::getAction()
     */

    /**
     * sets Javascript Code to execute on button click
     *
     * @param string $code
     * @return wrxNext_Session_Message_DialogBox_ButtonJs
     */
    public function setJsCode($code)
    {
        $this->jsCode = $code;
        return $this;
    }
}
