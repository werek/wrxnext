<?php

class wrxNext_Session_Message_DialogBox_ButtonUrl implements
    wrxNext_Session_Message_DialogBox_ButtonInterface
{

    /**
     * holds label
     *
     * @var string
     */
    protected $label = '';

    /**
     * holds url address
     *
     * @var string
     */
    protected $url = '';

    /**
     * construktor takes label of button and url
     *
     * @param string $label
     * @param string $url
     */
    public function __construct($label, $url)
    {
        $this->setLabel($label);
        $this->setUrl($url);
    }

    /*
     * (non-PHPdoc) @see
     * wrxNext_Session_Message_DialogBox_ButtonInterface::getLabel()
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * sets label
     *
     * @param string $text
     * @return wrxNext_Session_Message_DialogBox_ButtonUrl
     */
    public function setLabel($text)
    {
        $this->label = $text;
        return $this;
    }

    public function getAction()
    {
        return sprintf('function(){window.location=%s;}',
            Zend_Json::encode((string)$this->getUrl()));
    }

    /**
     * gets url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /*
     * (non-PHPdoc) @see
     * wrxNext_Session_Message_DialogBox_ButtonInterface::getAction()
     */

    /**
     * sets url
     *
     * @param string $url
     * @return wrxNext_Session_Message_DialogBox_ButtonUrl
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }
}
