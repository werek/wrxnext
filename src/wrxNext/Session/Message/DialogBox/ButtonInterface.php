<?php

interface wrxNext_Session_Message_DialogBox_ButtonInterface
{

    /**
     * returns text to put on button
     *
     * @return string
     */
    public function getLabel();

    /**
     * returns code to execute on button click (need to be wrapped in function)
     *
     * @return string
     */
    public function getAction();
}
