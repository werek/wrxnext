<?php

class wrxNext_Session_Message_DialogBox extends wrxNext_Session_Message_Text
{
    /**
     * holds button collection
     *
     * @var wrxNext_Session_Message_DialogBox_ButtonInterface[]
     */
    protected $buttons = array();

    /**
     * adds button to message
     *
     * @param wrxNext_Session_Message_DialogBox_ButtonInterface $button
     */
    public function addButton(
        wrxNext_Session_Message_DialogBox_ButtonInterface $button)
    {
        $this->buttons[] = $button;
    }

    /**
     * returns buttons
     *
     * @return wrxNext_Session_Message_DialogBox_ButtonInterface[]
     */
    public function getButtons()
    {
        return $this->buttons;
    }
}
