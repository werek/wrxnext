<?php

/**
 * exception for Session related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Session_Exception extends wrxNext_Exception
{
}
