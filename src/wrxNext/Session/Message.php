<?php

/**
 * class for session persistent messages
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Session_Message
{

    const NAME_SPACE = 'wrxNext_Session_Message';

    /**
     * holds name of namespace for storage object
     *
     * @var string
     */
    protected $_sessionNamespace = self::NAME_SPACE;

    /**
     * holds storage
     *
     * @var Zend_Session_Namespace
     */
    protected $_storage = null;

    public function __construct($namespace = null)
    {
        if (!is_null($namespace)) {
            $this->setNamespace($namespace);
        }
    }

    /**
     * sets namespace name
     *
     * @param string $namespace
     * @return wrxNext_Session_Message
     */
    public function setNamespace($namespace)
    {
        $this->_sessionNamespace = $namespace;
        return $this;
    }

    /**
     * clears all messages
     *
     * @return wrxNext_Session_Message
     */
    public function clear()
    {
        $this->getStorage()->messages = array();
        return $this;
    }

    /**
     * gets session storage
     *
     * @return Zend_Session_Namespace
     */
    public function getStorage()
    {
        if (is_null($this->_storage)) {
            $this->_storage = new Zend_Session_Namespace($this->getNamespace());
        }
        return $this->_storage;
    }

    /**
     * sets session storage
     *
     * @param Zend_Session_Namespace $sessionNamespace
     * @return wrxNext_Session_Message
     */
    public function setStorage($sessionNamespace)
    {
        $this->_storage = $sessionNamespace;
        return $this;
    }

    /**
     * gets namespace name
     *
     * @return string
     */
    public function getNamespace()
    {
        return $this->_sessionNamespace;
    }

    /**
     * returns messages count
     *
     * @return int
     */
    public function count()
    {
        return count($this->getMessages());
    }

    /**
     * returns messages array
     *
     * @return wrxNext_Session_Message_MessageInterface[]
     */
    public function getMessages()
    {
        return (array)$this->getStorage()->messages;
    }

    /**
     * registers info message
     *
     * @param string $message
     * @return wrxNext_Session_Message
     */
    public function info($message)
    {
        $this->addMessage($message, wrxNext_Session_Message_Text::INFO);
        return $this;
    }

    /**
     * adds message
     *
     * @param string|wrxNext_Session_Message_MessageInterface $message
     * @param string $type
     *            according to @see wrxNext_Session_Message_Text
     */
    public function addMessage($message, $type = 'info')
    {
        if (!$message instanceof wrxNext_Session_Message_MessageInterface) {
            $message = new wrxNext_Session_Message_Text($message, $type);
        }
        $this->getStorage()->messages[] = $message;
    }

    /**
     * registers success message
     *
     * @param string $message
     * @return wrxNext_Session_Message
     */
    public function success($message)
    {
        $this->addMessage($message, wrxNext_Session_Message_Text::SUCCESS);
        return $this;
    }

    /**
     * registers warning message
     *
     * @param string $message
     * @return wrxNext_Session_Message
     */
    public function warning($message)
    {
        $this->addMessage($message, wrxNext_Session_Message_Text::WARNING);
        return $this;
    }

    /**
     * registers error message
     *
     * @param string $message
     * @return wrxNext_Session_Message
     */
    public function error($message)
    {
        $this->addMessage($message, wrxNext_Session_Message_Text::ERROR);
        return $this;
    }

    /**
     * register exception as error message
     *
     * @param \Exception $exception
     * @return wrxNext_Session_Message
     */
    public function exception(\Exception $exception)
    {
        $this->addMessage(new wrxNext_Session_Message_ExceptionMessage($exception));
        return $this;
    }
}
