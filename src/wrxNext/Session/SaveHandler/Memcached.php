<?php

/**
 * Class wrxNext_Session_SaveHandler_Memcached
 *
 * Available options:
 * servers => array(host, port, persistent, weight, timeout, retry_interval, status, failure_callback),
 * compression, compatibility, lifetime=1800 sec, prefix=memcached_session
 * @see http://framework.zend.com/manual/1.12/en/zend.cache.backends.html
 */
class wrxNext_Session_SaveHandler_Memcached extends wrxNext_Session_SaveHandler_Cache
{

    const DEFAULT_HOST = '127.0.0.1';

    const DEFAULT_PORT = 11211;

    const DEFAULT_PERSISTENT = true;

    const DEFAULT_WEIGHT = 1;

    const DEFAULT_TIMEOUT = 1;

    const DEFAULT_RETRY_INTERVAL = 15;

    const DEFAULT_STATUS = true;

    const DEFAULT_FAILURE_CALLBACK = null;

    /**
     *
     * @var Callable callback
     */
    protected $failureCallback = self::DEFAULT_FAILURE_CALLBACK;

    /**
     *
     * @var array
     */
    protected $options = array(
        'servers' => array(
            array(
                'host' => self::DEFAULT_HOST,
                'port' => self::DEFAULT_PORT,
                'persistent' => self::DEFAULT_PERSISTENT,
                'weight' => self::DEFAULT_WEIGHT,
                'timeout' => self::DEFAULT_TIMEOUT,
                'retry_interval' => self::DEFAULT_RETRY_INTERVAL,
                'status' => self::DEFAULT_STATUS,
                'failure_callback' => self::DEFAULT_FAILURE_CALLBACK
            )
        ),
        'cache_id_prefix' => __CLASS__,
        'lifetime' => 1800,
        'compression' => false,
        'compatibility' => false
    );

    /**
     *
     * @param array $options
     * @throws wrxNext_Session_SaveHandler_Exception If memcached is not available
     */
    public function __construct(array $options)
    {
        if (!extension_loaded('memcache')) {
            throw new wrxNext_Session_SaveHandler_Exception(
                sprintf(
                    'Memcached extension is required to use this save handler: %s',
                    __CLASS__));
        }

        $defaults = new Zend_Config($this->options, true);
        $options = $defaults->merge(new Zend_Config($options));

        $cache = Zend_Cache::factory('Core', 'Memcached',
            array(
                'automatic_serialization' => true,
                'lifetime' => $options->get('lifetime')
            ),
            $options->toArray());
        parent::__construct($cache);
    }
}
