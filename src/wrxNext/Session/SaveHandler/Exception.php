<?php

/**
 * esxception for Session save handler related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Session_SaveHandler_Exception extends wrxNext_Session_Exception
{
}
