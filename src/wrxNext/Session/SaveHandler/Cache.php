<?php

/**
 * Class wrxNext_Session_SaveHandler_Cache
 */
class wrxNext_Session_SaveHandler_Cache implements
    Zend_Session_SaveHandler_Interface
{
    use wrxNext_Trait_RegistryLog;

    /**
     *
     * @var Zend_Cache_Core
     */
    protected $cache;

    /**
     *
     * @param Zend_Cache_Core $cache
     */
    function __construct(Zend_Cache_Core $cache)
    {
        $this->setCache($cache);
    }

    /**
     * Get Cache
     * -
     *
     * @return Zend_Cache_Core
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     *
     * @param Zend_Cache_Core $cache
     * @return $this
     */
    public function setCache(Zend_Cache_Core $cache)
    {
        $this->cache = $cache;
        return $this;
    }

    /**
     * Dummy
     *
     * @param string $save_path
     * @param string $name
     * @return boolean
     */
    public function open($save_path, $name)
    {
        return true;
    }

    /**
     * Dummy
     *
     * @return boolean
     */
    public function close()
    {
        return true;
    }

    /**
     *
     * @param string $id
     * @return string
     */
    public function read($id)
    {
        if (empty($id))
            return null;
        $data = $this->cache->load($id);
        return $data != false ? $data : null;
    }

    /**
     *
     * @param string $id
     * @param string $data
     * @return boolean
     */
    public function write($id, $data)
    {
        if (empty($id))
            return false;
        return $this->cache->save($data, $id);
    }

    /**
     * Destroy session
     *
     * @param string $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->cache->remove($id);
    }

    /**
     * Garbage Collection
     *
     * @param int $maxlifetime
     * @return void
     */
    public function gc($maxlifetime)
    {
    }
}
