<?php

/**
 * acts as repository of Zend_Db_Table classes for certain tables
 *
 * assumes that table names are lower case with underscores (requirement),
 * also support overriding returned table class with its specialised version.
 *
 * acces to those tables is available through method calls. ie.
 *
 * table "users" can be retrieved through method "getUsers", while table
 * named "users_other_data" can be retrieved through method named "getUsersOtherData".
 * naturally instances of table object are same only for given class instance.
 * those are not shared through all instances of this class.
 *
 * @author bweres01
 *
 */
class wrxNext_Service_TablesRepository
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds table mapping
     *
     * @var array
     */
    protected $_tableMapping = array();

    /**
     * holds instantiated object of tables
     *
     * @var ArrayObject
     */
    protected $_tableRepository = null;

    /**
     * holds filter
     *
     * @var Zend_Filter_Word_UnderscoreToCamelCase
     */
    protected $_filterUnderscoreToCamelCase = null;

    /**
     * holds filter
     *
     * @var Zend_Filter_Word_CamelCaseToUnderscore
     */
    protected $_filterCamelCaseToUnderscore = null;

    /**
     * constructor, takes table names mapping to table classes
     *
     * @param array $tableMappings
     */
    public function __construct(array $tableMappings = null)
    {
        $this->_tableRepository = new ArrayObject(array());
        $this->_filterUnderscoreToCamelCase = new Zend_Filter_Word_UnderscoreToCamelCase();
        $this->_filterCamelCaseToUnderscore = new Zend_Filter_Word_CamelCaseToUnderscore();
        if (!is_null($tableMappings)) {
            $this->_tableMapping = $tableMappings;
        }
    }

    /**
     * adds table mapping
     *
     * @param string $name
     * @param string $tableClass
     * @return wrxNext_Service_TablesRepository
     */
    public function setTableMapping($name, $tableClass)
    {
        $this->_tableMapping[$name] = $tableClass;
        return $this;
    }

    /**
     * magic method for retrieving instances of tables
     *
     * @param string $name
     * @param array $arguments
     */
    public function __call($name, $arguments)
    {
        if (!$this->_tableRepository->offsetExists($name)) {
            $camelCaseName = preg_replace('#^get#', '', $name);
            $tableName = strtolower(
                $this->_filterCamelCaseToUnderscore->filter($camelCaseName));
            return $this->_inflateTable($tableName, $camelCaseName);
        }
        return $this->_tableRepository->offsetGet($name);
    }

    /**
     * inflates table object according to tablemapping/name and returns object
     * instance, also saves table into repository
     *
     * @param string $tableName
     * @param null|string $camelCaseName
     * @return Zend_Db_Table
     */
    protected function _inflateTable($tableName, $camelCaseName = null)
    {
        if (is_null($camelCaseName)) {
            $camelCaseName = $this->_filterUnderscoreToCamelCase->filter(
                strtolower($tableName));;
        }
        $repositoryKey = 'get' . $camelCaseName;
        if (isset($this->_tableMapping[$tableName])) {
            $tableClass = $this->_tableMapping[$tableName];
            $table = new $tableClass();
        } else {
            $table = new Zend_Db_Table(
                array(
                    Zend_Db_Table::NAME => $tableName
                ));
        }
        $this->_tableRepository->offsetSet($repositoryKey, $table);
        return $table;
    }

    public function beginTransaction()
    {
        $this->_log('----------------starting transaction------------------');
        Zend_Db_Table_Abstract::getDefaultAdapter()->beginTransaction();
        return $this;
    }

    public function commit()
    {
        $this->_log('----------------commiting transaction-----------------');
        Zend_Db_Table_Abstract::getDefaultAdapter()->commit();
        return $this;
    }

    public function rollback()
    {
        $this->_log('----------------rollbacking transaction---------------');
        Zend_Db_Table_Abstract::getDefaultAdapter()->rollBack();
        return $this;
    }
}
