<?php

/**
 * sub service for handling table objects
 *
 * @author bweres01
 *
 */
class wrxNext_Service_Tables extends wrxNext_Service
{
    /**
     * adds table to repo
     *
     * @param string $tableName
     * @return wrxNext_Service_Tables
     */
    public function addTable($tableName)
    {
        $tableNameFiltered = $this->_filter()->filter($tableName);
        $this->offsetSet($tableNameFiltered,
            $this->share(
                function () use ($tableName) {
                    return new Zend_Db_Table(
                        array(
                            Zend_Db_Table::NAME => $tableName
                        ));
                }));
        return $this;
    }

    /**
     * adds all table classes
     *
     * @param array $tableClasses
     * @return wrxNext_Service_Tables
     */
    public function addTableClasses($tableClasses)
    {
        foreach ($tableClasses as $tableName => $className) {
            $this->addTableClass($tableName, $className);
        }
        return $this;
    }

    /**
     * adds table class definition
     *
     * @param unknown $tableName
     * @param unknown $className
     * @return wrxNext_Service_Tables
     */
    public function addTableClass($tableName, $className)
    {
        $tableName = $this->_filter()->filter($tableName);
        $this->offsetSet($tableName,
            $this->share(
                function () use ($className) {
                    return new $className();
                }));
        return $this;
    }
}
