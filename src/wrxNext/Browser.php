<?php

/**
 * klasa zapewniajaca obsluge rozpoznawania przegladarki
 *
 */
class wrxNext_Browser
{

    const BROWSER_IE = "Internet Explorer";

    const BROWSER_FIREFOX = "Mozilla Firefox";

    const BROWSER_NETSCAPE = "Netscape";

    const BROWSER_SAFARI = "Safari";

    const BROWSER_GALEON = "Galeon";

    const BROWSER_KONQUEROR = "Konqueror";

    const BROWSER_OPERA = "Opera";

    const BROWSER_LYNX = "Lynx";

    const BROWSER_GECKO = "Gecko";

    const BROWSER_GECKO_UNKNOWN = "Gecko based";

    const BROWSER_UNKNOWN = false;

    const OS_XP = "Windows XP";

    const OS_98 = "Windows 98";

    const OS_2000 = "Windows 2000";

    const OS_2003s = "Windows 2003 Server";

    const OS_VISTA = "Windows Vista";

    const OS_NT = "Windows NT";

    const OS_ME = "Windows NT";

    const OS_CE = "Windows CE";

    const OS_MACOSX = "Mac OS X";

    const OS_MAC = "Macintosh";

    const OS_LINUX = "Linux";

    const OS_FREEBSD = "Free BSD";

    const OS_SYMBIAN = "Symbian";

    const OS_UNKNOWN = false;

    /**
     * Get browsername and version
     *
     * @param
     *            string user agent
     * @return string browser name and version or false if unrecognized
     * @static
     *
     * @access public
     */
    public static function get_browser($useragent)
    {
        $browser['ver'] = 0;
        $browser['name'] = '';
        // check for most popular browsers first
        // unfortunately that's ie. We also ignore opera and netscape 8
        // because they sometimes send msie agent
        if (strpos($useragent, "MSIE") !== false &&
            strpos($useragent, "Opera") === false &&
            strpos($useragent, "Netscape") === false
        ) {
            // deal with IE
            $browser['name'] = wrxNext_Browser::BROWSER_IE;
            $found = preg_match("/MSIE ([0-9]{1}\.[0-9]{1,2})/", $useragent,
                $mathes);
            if ($found) {
                $browser['ver'] = $mathes[1];
                return $browser;
            }
        } elseif (strpos($useragent, wrxNext_Browser::BROWSER_GECKO)) {
            // deal with Gecko based

            // if firefox
            $found = preg_match("/Firefox\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_FIREFOX;
                $browser['ver'] = $mathes[1];
                return $browser;
            }

            // if Netscape (based on gecko)
            $found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_NETSCAPE;
                $browser['ver'] = $mathes[1];
                return $browser;
            }

            // if Safari (based on gecko)
            $found = preg_match("/Safari\/([0-9]{2,3}(\.[0-9])?)/", $useragent,
                $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_SAFARI;
                $browser['ver'] = $mathes[1];
                return $browser;
            }

            // if Galeon (based on gecko)
            $found = preg_match("/Galeon\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_GALEON;
                $browser['ver'] = $mathes[1];
                return $browser;
            }

            // if Konqueror (based on gecko)
            $found = preg_match("/Konqueror\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_KONQUEROR;
                $browser['ver'] = $mathes[1];
                return $browser;
            }

            // no specific Gecko found
            // return generic Gecko
            return "Gecko based";
        } elseif (strpos($useragent, "Opera") !== false) {
            // deal with Opera
            $found = preg_match("/Opera[\/ ]([0-9]{1}\.[0-9]{1}([0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_OPERA;
                $browser['ver'] = $mathes[1];
                return $browser;
            }
        } elseif (strpos($useragent, "Lynx") !== false) {
            // deal with Lynx
            $found = preg_match("/Lynx\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_LYNX;
                $browser['ver'] = $mathes[1];
                return $browser;
            }
        } elseif (strpos($useragent, "Netscape") !== false) {
            // NN8 with IE string
            $found = preg_match("/Netscape\/([0-9]{1}\.[0-9]{1}(\.[0-9])?)/",
                $useragent, $mathes);
            if ($found) {
                $browser['name'] = wrxNext_Browser::BROWSER_NETSCAPE;
                $browser['ver'] = $mathes[1];
                return $browser;
            }
        } else {
            // unrecognized, this should be less than 1% of browsers (not
            // counting bots like google etc)!
            return wrxNext_Browser::BROWSER_UNKNOWN;
        }
    }

    /**
     * Get browsername and version
     *
     * @param
     *            string user agent
     * @return string os name and version or false in unrecognized os
     * @static
     *
     * @access public
     */
    public static function get_os($useragent)
    {
        $useragent = strtolower($useragent);

        // check for (aaargh) most popular first
        // winxp
        if (strpos("$useragent", "windows nt 5.1") !== false) {
            return wrxNext_Browser::OS_XP;
        } elseif (strpos("$useragent", "windows 98") !== false) {
            return wrxNext_Browser::OS_98;
        } elseif (strpos("$useragent", "windows nt 5.0") !== false) {
            return wrxNext_Browser::OS_2000;
        } elseif (strpos("$useragent", "windows nt 5.2") !== false) {
            return wrxNext_Browser::OS_2003s;
        } elseif (strpos("$useragent", "windows nt 6.0") !== false) {
            return wrxNext_Browser::OS_VISTA;
        } elseif (strpos("$useragent", "windows nt") !== false) {
            return wrxNext_Browser::OS_NT;
        } elseif (strpos("$useragent", "win 9x 4.90") !== false &&
            strpos("$useragent", "win me")
        ) {
            return wrxNext_Browser::OS_ME;
        } elseif (strpos("$useragent", "win ce") !== false) {
            return wrxNext_Browser::OS_CE;
        } elseif (strpos("$useragent", "win 9x 4.90") !== false) {
            return wrxNext_Browser::OS_ME;
        } elseif (strpos("$useragent", "mac os x") !== false) {
            return wrxNext_Browser::OS_MACOSX;
        } elseif (strpos("$useragent", "macintosh") !== false) {
            return wrxNext_Browser::OS_MAC;
        } elseif (strpos("$useragent", "linux") !== false) {
            return wrxNext_Browser::OS_LINUX;
        } elseif (strpos("$useragent", "freebsd") !== false) {
            return wrxNext_Browser::OS_FREEBSD;
        } elseif (strpos("$useragent", "symbian") !== false) {
            return wrxNext_Browser::OS_SYMBIAN;
        } else {
            return wrxNext_Browser::OS_UNKNOWN;
        }
    }
}
