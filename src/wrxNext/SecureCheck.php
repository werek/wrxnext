<?php

/**
 * Utility class for checking status of secure http connection and retrieving user ip address
 *
 * @author Bartlomiej Wereszczynski <bartlomiej.wereszczynski@isobar.com>
 *
 */
class wrxNext_SecureCheck
{

    /**
     * checks if current request is secure, recognises also testing enviroment
     *
     * @return boolean
     */
    public static function isSecure()
    {
        return ((array_key_exists('HTTPS', $_SERVER) &&
                $_SERVER['HTTPS'] === 'on') || (array_key_exists('HTTP_SSL',
                    $_SERVER) && $_SERVER['HTTP_SSL'] == 1)) ? true : false;
    }

    /**
     * returns client ip according to specific production/testing enviroment
     *
     * @return string
     */
    public static function remoteAddress()
    {
        $address = $_SERVER['REMOTE_ADDR'];
        if (array_key_exists('HTTP_X_REAL_IP', $_SERVER)) {
            $address = $_SERVER['HTTP_X_REAL_IP'];
        } elseif (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $address = trim(
                array_reverse(
                    explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']))[0]);
        }
        return $address;
    }
}
