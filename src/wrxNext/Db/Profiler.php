<?php

class wrxNext_Db_Profiler extends Zend_Db_Profiler
{

    /**
     * definiuje czy natychmiast drukowac zapytania
     *
     * @var bool
     */
    private $_echoQueries = true;

    /**
     * Starts a query.
     * Creates a new query profile object (Zend_Db_Profiler_Query)
     * and returns the "query profiler handle". Run the query, then call
     * queryEnd() and pass it this handle to make the query as ended and
     * record the time. If the profiler is not enabled, this takes no
     * action and immediately returns null.
     *
     * @param string $queryText
     *            SQL statement
     * @param integer $queryType
     *            OPTIONAL Type of query, one of the Zend_Db_Profiler::*
     *            constants
     * @return integer null
     */
    public function queryStart($queryText, $queryType = null)
    {
        if ($this->_echoQueries) {
            echo "<code>" . htmlentities($queryText) . "</code></br>";
        }
        return parent::queryStart($queryText, $queryType);
    }

    /**
     * zwraca status natychmiastowego wyswietlania zapytania
     *
     * @return bool
     */
    public function getEchoQueries()
    {
        return $this->_echoQueries;
    }

    /**
     * ustala czy natychmiast wyswietlac zapytania
     *
     * @param bool $value
     */
    public function setEchoQueries($value = true)
    {
        if (is_bool($value)) {
            $this->_echoQueries = $value;
        }
    }

    /**
     * obsluguje magiczne echo'wanie obiektu
     */
    public function __toString()
    {
        $template = "(%s, exec time: %ss) %s<br/>\n";
        $string = "";
        $queryType = array(
            1 => 'CONNECT',
            2 => 'QUERY',
            4 => 'INSERT',
            8 => 'UPDATE',
            16 => 'DELETE',
            32 => 'SELECT',
            64 => 'TRANSACTION'
        );
        foreach ($this->_queryProfiles as $v) {
            $string .= sprintf($template, $queryType[$v->getQueryType()],
                $v->getElapsedSecs(), htmlentities($v->getQuery()));
        }
        return "<code>" . $string . "</code>";
    }
}
