<?php

class wrxNext_Db_Profiler_Log extends Zend_Db_Profiler
{

    protected $_logLevel = Zend_Log::DEBUG;
    use wrxNext_Trait_RegistryLog;

    public function __construct($logLevel = Zend_Log::DEBUG)
    {
        $this->setLogLevel($logLevel);
    }

    /**
     * gets log level
     *
     * @return int
     */
    public function getLogLevel()
    {
        return $this->_logLevel;
    }

    /**
     * sets log level
     *
     * @param int $level
     * @return wrxNext_Db_Profiler_Log
     */
    public function setLogLevel($level)
    {
        $this->_logLevel = $level;
        return $this;
    }

    public function queryStart($queryText)
    {
        $this->_log($queryText, $this->_logLevel);
    }
}
