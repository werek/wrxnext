<?php

class wrxNext_Db_Expr
{

    /**
     * holds registered expressions
     *
     * @var Zend_Db_Expr
     */
    protected static $_expressions = array();

    /**
     * retrieves and caches Zend_Db_Expr
     *
     * @param string $expression
     * @return Zend_Db_Expr
     */
    public static function get($expression)
    {
        if (!array_key_exists($expression, self::$_expressions)) {
            self::$_expressions[$expression] = new Zend_Db_Expr($expression);
        }
        return self::$_expressions[$expression];
    }
}
