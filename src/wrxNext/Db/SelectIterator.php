<?php

class wrxNext_Db_SelectIterator implements \Iterator
{
    /** @var \Zend_Db_Select */
    private $select;
    /** @var int */
    private $fetchMode;
    /** @var  \PDOStatement|\Zend_Db_Statement */
    private $stmt;
    /** @var  mixed */
    private $current;
    /** @var int */
    private $key = -1;

    public function __construct(Zend_Db_Select $select, $fetchMode = Zend_Db::FETCH_ASSOC)
    {
        $this->select    = $select;
        $this->fetchMode = $fetchMode;
    }

    /**
     * Return the current element
     *
     * @link  http://php.net/manual/en/iterator.current.php
     * @return mixed Can return any type.
     * @since 5.0.0
     */
    public function current()
    {
        if ($this->key < 0) {
            $this->next();
        }
        return $this->current;
    }

    /**
     * Move forward to next element
     *
     * @link  http://php.net/manual/en/iterator.next.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function next()
    {
        $this->current = $this->stmt->fetch();
        $this->key++;
    }

    /**
     * Return the key of the current element
     *
     * @link  http://php.net/manual/en/iterator.key.php
     * @return mixed scalar on success, or null on failure.
     * @since 5.0.0
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Checks if current position is valid
     *
     * @link  http://php.net/manual/en/iterator.valid.php
     * @return boolean The return value will be casted to boolean and then evaluated.
     * Returns true on success or false on failure.
     * @since 5.0.0
     */
    public function valid()
    {
        return !is_null($this->current);
    }

    /**
     * Rewind the Iterator to the first element
     *
     * @link  http://php.net/manual/en/iterator.rewind.php
     * @return void Any returned value is ignored.
     * @since 5.0.0
     */
    public function rewind()
    {
        $this->stmt    = $this->select->query($this->fetchMode);
        $this->current = null;
        $this->key     = -1;
    }

    /**
     * The __toString method allows a class to decide how it will react when it is converted to a string.
     *
     * @return string
     * @link http://php.net/manual/en/language.oop5.magic.php#language.oop5.magic.tostring
     */
    function __toString()
    {
        return $this->select->assemble();
    }
}
