<?php

/**
 * proxy for quick access to cachemanager's cache instances
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Cache
{

    /**
     * Zend_Cache_Manager instance
     *
     * @var Zend_Cache_Manager
     */
    protected static $_instance = null;

    /**
     * returns given cache instance
     *
     * @param string $cacheInstance
     * @return Zend_Cache_Core
     */
    public static function get($cacheInstance)
    {
        return self::manager()->getCache($cacheInstance);
    }

    /**
     * returns cache manager instance
     *
     * @return Zend_Cache_Manager
     */
    protected static function manager()
    {
        if (!self::$_instance instanceof Zend_Cache_Manager) {
            self::$_instance = Zend_Controller_Front::getInstance()->getParam(
                'bootstrap')
                ->getPluginResource('cachemanager')
                ->getCacheManager();
        }
        return self::$_instance;
    }

    /**
     * filters key so it will be compatible with cache backend
     *
     * @param string $value
     * @return string
     */
    public static function keyFilter($value)
    {
        return preg_replace('@[^a-zA-Z0-9_]@', '_', $value);
    }
}
