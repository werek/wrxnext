<?php

class wrxNext_Info_Image extends wrxNext_Info_Abstract
{

    public function init()
    {
        if (isset($this->_params['path']) && !empty($this->_params['path']) &&
            file_exists($this->_params['path'])
        ) {
            $img = new wrxNext_Image($this->_params['path']);
            $this->_info = $img->toArray();
        }
    }
}
