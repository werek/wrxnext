<?php

class wrxNext_Info_Youtube extends wrxNext_Info_Abstract
{

    public function init()
    {
        if (isset($this->_params['link']) &&
            preg_match('@http://.*?youtube\.com/watch@i',
                $this->_params['link'])
        ) {
            $this->_info['original_link'] = $this->_params['link'];
            $this->_info['link_info'] = parse_url($this->_params['link']);
            preg_match('@v=([a-zA-Z0-9_\-]+)@i',
                $this->_info['link_info']['query'], $wyniki);
            if (isset($wyniki[1]) && !empty($wyniki[1])) {
                $this->_info['video_id'] = $wyniki[1];
                $this->_info['swf_link'] = 'http://www.youtube.com/v/' . $movieID;
                $imgTemplate = 'http://img.youtube.com/vi/%s/%s.jpg';
                $this->_info['img_link_default'] = sprintf($imgTemplate,
                    $this->_info['video_id'], 'default');
                $this->_info['img_link_1'] = sprintf($imgTemplate,
                    $this->_info['video_id'], '1');
                $this->_info['img_link_2'] = sprintf($imgTemplate,
                    $this->_info['video_id'], '2');
                $this->_info['img_link_3'] = sprintf($imgTemplate,
                    $this->_info['video_id'], '3');
            }
        } else {
            throw new wrxNext_Exception('parametr "link" jest wymagany');
        }
    }
}
