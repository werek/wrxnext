<?php

abstract class wrxNext_Info_Abstract implements wrxNext_Info_Interface
{

    /**
     * przechowuje zdefiniowane parametry
     *
     * @var array
     */
    protected $_params = array();

    /**
     * przechowuje zebrane informacje na podstawie parametrow
     *
     * @var array
     */
    protected $_info = array();

    /**
     * konstruktor jako parametr przyjmuje zbior opcji w formie tablicy
     *
     * @param array $params
     */
    public function __construct($params = array())
    {
        $params = (array)$params;
        $this->setParams($params);
    }

    /**
     * funkcja uruchamiana po ustaleniu parametrow w celu zebrania informacji
     */
    public function init()
    {
    }

    /**
     * zwraca ustalone na starcie parametry
     *
     * @return array
     */
    public function getParams()
    {
        return $this->_params;
    }

    /**
     * ustala parametry dla klasy
     *
     * @param array $params
     */
    public function setParams(array $params)
    {
        $this->_params = array_merge($this->_params, $params);
        $this->init();
    }

    /**
     * zwraca rozpoznane informacje okreslonego typu okreslonego w paramaterze
     *
     * @param string $fieldName
     * @return mixed
     */
    public function getInfo($fieldName)
    {
        if (isset($this->_info[$fieldName])) {
            return $this->_info[$fieldName];
        }
    }

    /**
     * zwraca wszystkie informacje w postaci tablicy parametrow
     */
    public function toArray()
    {
        return $this->_info;
    }
}
