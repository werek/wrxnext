<?php

interface wrxNext_Info_Interface
{

    /**
     * ustala parametry dla klasy
     *
     * @param array $params
     */
    public function setParams(array $params);

    /**
     * zwraca ustalone na starcie parametry
     *
     * @return array
     */
    public function getParams();

    /**
     * zwraca rozpoznane informacje okreslonego typu okreslonego w paramaterze
     *
     * @param string $fieldName
     * @return mixed
     */
    public function getInfo($fieldName);

    /**
     * zwraca wszystkie informacje w postaci tablicy parametrow
     */
    public function toArray();
}
