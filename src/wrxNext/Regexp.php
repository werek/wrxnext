<?php

/**
 * klasa utylitarna przechowujaca przydatne regexp'y
 *
 */
class wrxNext_Regexp
{

    /**
     * regexp znajdujacy linki ed2k
     */
    const regexpEd2k = 'ed2k:\/\/\|file\|.+\|[0-9]+\|[[:alnum:]]{32,32}\|\/';

    /**
     * regexp znajdujacy e-mail
     */
    const regexpEmail = '[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}';

    /**
     * regexp znajdujacy date w formacie
     * 'Mon Mar 27 11:12:33 2006'
     */
    const regexpEzmlmDate = '[a-zA-Z]{3,3} [a-zA-Z]{3,3} [0-9]{2,2} [0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2} [0-9]{4,4}';

    /**
     * regexp znajdujacy parametry z raportu ezmlm
     */
    const regexpEzmlmParam = '[-\+]\w*';

    /**
     * regexp znajdujacy date w formacie sql
     * 'RRRR-MM-DD'
     */
    const regexpSqlDate = '[0-9]{4,4}-[0-9]{2,2}-[0-9]{2,2}';

    /**
     * regexp znajdujacy date w formacie czytelnym dla ludzi
     * 'DD-MM-RRRR'
     */
    const regexpHumanDate = '[0-9]{2,2}-[0-9]{2,2}-[0-9]{4,4}';

    /**
     * regexp znajdujacy czas w formacie
     * 'HH:MM:SS'
     */
    const regexpTime = '[0-9]{2,2}:[0-9]{2,2}:[0-9]{2,2}';

    /**
     * metoda zwracaja pierwszy napotkany email w $string
     *
     * @param string $string
     * @return string
     */
    public static function getSingleEmail($string)
    {
        return self::getEmail($string);
    }

    /**
     * metoda wybierajaca adresy email ze $string,
     * zwraca string ($multiple=false) z 1 adresem email,
     * badz array ($multiple=true) ze wszystkimi znalezionymi adresami
     *
     * @param string $string
     * @param bool $multiple
     * @return mixed
     */
    public static function getEmail($string, $multiple = false)
    {
        preg_match_all('/(' . self::regexpEmail . ')/i', $string, $wyniki);
        if ($multiple) {
            return $wyniki[1];
        } else {
            return $wyniki[1][0];
        }
    }

    /**
     * parsuje naglowek (sekcja from:) do html'a
     *
     * @param string $string
     * @return string
     */
    public static function parseHeaderFrom($string)
    {
        preg_match_all('/\"([\w\s]+)\"/i', $string, $wyniki);
        return '<a href="mailto:' . self::getEmail($string) . '">' .
        $wyniki[1][0] . '</a>';
    }

    /**
     * parsuje nag�owek (sekcja to:) do html'a
     *
     * @param string $string
     * @return string
     */
    public static function parseHeaderTo($string)
    {
        $t = self::getEmail($string, true);
        foreach ($t as $k => $v) {
            $t[$k] = '<a href="mailto:' . $v . '">' . $v . '</a>';
        }
        return implode('<br />', $t);
    }

    /**
     * parsuje tresc raportu ezmlm zwracajac tablice
     * w formie [n]=>array('date','param','email')
     *
     * @param string $string
     * @return array
     */
    public static function parseEzmlmReport2Array($string)
    {
        preg_match_all(
            '/(' . self::regexpEzmlmDate . ') (' . self::regexpEzmlmParam .
            ') (' . self::regexpEmail . ')/i', $string, $wyniki);
        $t = array();
        foreach ($wyniki[0] as $key => $value) {
            $t[$key]['date'] = $wyniki[1][$key];
            $t[$key]['param'] = $wyniki[2][$key];
            $t[$key]['email'] = $wyniki[3][$key];
        }
        return $t;
    }

    /**
     * zwraca adres pierwszego obrazka (img tag) z podanego kodu
     * w przeciwnym wypadku pusty string
     *
     * @param string $code
     * @return string
     */
    public static function getFirstImageSrc($code)
    {
        $wzorzec = '@(<img.+?src=["\'](.+?)["\'])@is';
        $adres = '';
        if (preg_match($wzorzec, $code, $wyniki)) {
            $adres = $wyniki[2];
        }
        return $adres;
    }
}

?>
