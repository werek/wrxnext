<?php

class wrxNext_FtpCrawler_FilenameFilter
{

    protected $_matchExclude = false;

    protected $_filePaterns = array();

    public function __construct($options = array())
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions($options)
    {
        $options = (array)$options;
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    /**
     * gets wheter to matched elements should be excluded
     *
     * @return boolean
     */
    public function getMatchExclude()
    {
        return $this->_matchExclude;
    }

    /**
     * sets wheter to matched elements should be excluded
     *
     * @param boolean $bool
     * @return wrxNext_FtpCrawler_FilenameFilter
     */
    public function setMatchExclude($bool)
    {
        $this->_matchExclude = $bool;
        return $this;
    }

    /**
     * checks file name against patterns and returns wheter to process file or
     * not
     *
     * @param unknown $fileName
     * @return boolean
     */
    public function processFile($fileName)
    {
        $match = false;
        foreach ($this->getFilePaterns() as $pattern) {
            if (preg_match($pattern, $fileName)) {
                $match = true;
            }
        }
        return $this->_matchExclude ? !$match : $match;
    }

    /**
     * gets file patterns to match
     *
     * @return array
     */
    public function getFilePaterns()
    {
        return $this->_filePaterns;
    }

    /**
     * sets file patterns to match
     *
     * @param array $patterns
     * @return wrxNext_FtpCrawler_FilenameFilter
     */
    public function setFilePaterns($patterns)
    {
        $this->_filePaterns = $patterns;
        return $this;
    }
}
