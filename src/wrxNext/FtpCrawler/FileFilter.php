<?php

class wrxNext_FtpCrawler_FileFilter
{

    public function __construct($options = array())
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions($options)
    {
        $options = (array)$options;
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    public function processBody($body)
    {
        return $body;
    }
}
