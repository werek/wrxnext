<?php

class wrxNext_FtpCrawler_FileFilterRegexpReplace extends wrxNext_FtpCrawler_FileFilter
{

    protected $_searchReplace = array();

    /**
     * sets search replace sets
     *
     * @param array $sets
     * @return wrxNext_FtpCrawler_FileFilterRegexpReplace
     */
    public function setSearchReplaceSets($sets)
    {
        $sets = (array)$sets;
        foreach ($sets as $set) {
            if (isset($set['search']) && isset($set['replace'])) {
                $this->addSearchReplace($set['search'], $set['replace']);
            }
        }
        return $this;
    }

    /**
     * adds new search replace regexp pattern
     *
     * @param string $search
     * @param string $replace
     * @return wrxNext_FtpCrawler_FileFilterRegexpReplace
     */
    public function addSearchReplace($search, $replace)
    {
        $this->_searchReplace[] = array(
            'search' => $search,
            'replace' => $replace
        );
        return $this;
    }

    public function processBody($body)
    {
        foreach ($this->getSearchReplaceSets() as $set) {
            $body = preg_replace($set['search'], $set['replace'], $body);
        }
        return $body;
    }

    /*
     * (non-PHPdoc) @see wrxNext_FtpCrawler_FileFilter::processBody()
     */

    /**
     * gets search replace sets
     *
     * @return array
     */
    public function getSearchReplaceSets()
    {
        return $this->_searchReplace;
    }
}
