<?php

/**
 * logic class for handling color operations
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 */
class wrxNext_Color
{
    const RED = 'red';
    const GREEN = 'green';
    const BLUE = 'blue';
    const ALPHA = 'alpha';

    /**
     * przekształca kolorowa wartosc koloru w hex na
     * skale szarosci za pomoca algorytmu luminosity
     *
     * @param string $hex
     * @return string
     */
    public static function hexToGrayscale($hex)
    {
        $rgb = self::hexToRgb($hex);
        $lum = self::rgbToGrayscale($rgb[self::RED], $rgb[self::GREEN], $rgb[self::BLUE]);
        $lum = ($lum > 15) ? dechex($lum) : '0' . dechex($lum);
        return str_repeat($lum, 3);
    }

    /**
     * przyjmuje wartosc hex coloru (#xxxxxx lub #xxx) i zamienia ja na wartosc decymalna
     * rgb zwracajac tablice z wartosciami
     *
     * @param string $hex
     * @return array
     */
    public static function hexToRgb($hex)
    {
        $hex = trim($hex, '#');
        if (strlen($hex) == 3) {
            $hex = substr($hex, 0, 1) . substr($hex, 0, 1) . substr($hex, 1, 1) . substr($hex, 1, 1) . substr($hex, 2, 1) . substr($hex, 0, 1);
        }
        return array(self::RED => (int)hexdec(substr($hex, 0, 2)), self::GREEN => (int)hexdec(substr($hex, 2, 2)), self::BLUE => (int)hexdec(substr($hex, 4, 2)));
    }

    /**
     * zamienia wartosc koloru na wartosc jasności przy uzyciu algorytmu
     * luminosity który uwzględnia percepcje oka
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return int
     */
    public static function rgbToGrayscale($red, $green, $blue)
    {
        return (int)round(((0.21 * $red) + (0.71 * $green) + (0.07 * $blue)) * (255 / 252), 0);
    }

    /**
     * zamienia wartosci rgb na ciag hex
     *
     * @param int $red
     * @param int $green
     * @param int $blue
     * @return string
     */
    public static function rgbToHex($red, $green, $blue)
    {
        $rgb = array(self::RED => $red, self::GREEN => $green, self::BLUE => $blue);
        foreach ($rgb as $color => $value) {
            $c = (string)dechex($value);
            $rgb[$color] = (strlen($c) == 1) ? '0' . $c : $c;
        }
        return implode('', $rgb);
    }
}
