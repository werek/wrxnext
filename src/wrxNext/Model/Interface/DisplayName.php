<?php

/**
 * interface for models which allows different display
 *
 * @author werek
 *
 */
interface wrxNext_Model_Interface_DisplayName
{

    /**
     * returns display name for given entity
     *
     * @return string
     */
    public function displayName();
}
