<?php

/**
 * gives object ability to describe if it should be displayed
 *
 * @author werek
 *
 */
interface wrxNext_Model_Interface_IsVisible
{

    /**
     * returns true if item should be displayed, false otherwise
     *
     * @return boolean
     */
    public function isVisible();
}
