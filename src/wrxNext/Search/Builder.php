<?php

/**
 * type class, used for holding search related constants
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Search_Builder
{

    const FIELD_KEYWORD = 'keyword';

    const FIELD_UNINDEXED = 'unindexed';

    const FIELD_BINARY = 'binary';

    const FIELD_TEXT = 'text';

    const FIELD_UNSTORED = 'unstored';
}
