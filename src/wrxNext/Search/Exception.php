<?php

/**
 * exception for Search related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Search_Exception extends wrxNext_Exception
{
}
