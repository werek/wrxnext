<?php

/**
 * interface for search index builders for passing iterators with
 * Zend_Search_Lucene documents to add to specific locale index
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Search_Builder_Interface
{
    /**
     * returns iterator for given locale which in turn returns Zend_Search_Lucene_Document
     *
     * @param string $locale
     * @return Iterator
     */
    public function getIterator($locale);
}
