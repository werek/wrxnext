<?php

/**
 * abstract class for search builder
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
abstract class wrxNext_Search_Builder_Iterator_Abstract
{

    /**
     * holds encoding
     *
     * @var string
     */
    protected $_encoding = 'UTF-8';

    /**
     * holds information wheter to index all fields or only
     * those stated in fieldIndexes
     *
     * @var unknown
     */
    protected $_indexAllFields = true;

    /**
     * holds default index type if none is provided in columnIndex
     *
     * @var mixed
     */
    protected $_defaultIndexType = null;

    /**
     * holds specific index type per column
     *
     * @var unknown
     */
    protected $_fieldIndexes = array();

    /**
     * setups given options
     *
     * @param array $options
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    /**
     * returns status of indexing unspecified fields
     *
     * @return boolean
     */
    public function getIndexAllFields()
    {
        return $this->_indexAllFields;
    }

    /**
     * sets wheter to index unspecified fields
     *
     * @param boolean $bool
     * @return wrxNext_Search_Builder_Iterator_Abstract
     */
    public function setIndexAllFields($bool = true)
    {
        $this->_indexAllFields = (bool)$bool;
        return $this;
    }

    /**
     * returns array of field index types
     *
     * @return array
     */
    public function getFieldIndexes()
    {
        return $this->_fieldIndexes;
    }

    /**
     * sets array of fields index types
     *
     * @param array $fieldIndexes
     * @return wrxNext_Search_Builder_Iterator_Abstract
     */
    public function setFieldIndexes($fieldIndexes = array())
    {
        $this->_fieldIndexes = (array)$fieldIndexes;
        return $this;
    }

    /**
     * sets index type for given field
     *
     * @param string $fieldName
     * @param int $fieldIndex
     * @return wrxNext_Search_Builder_Iterator_Abstract
     */
    public function setFieldIndex($fieldName, $fieldIndex)
    {
        $this->_fieldIndexes[$fieldName] = $fieldIndex;
        return $this;
    }

    /**
     * gets encoding
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     * sets encoding
     *
     * @param string $encoding
     * @return wrxNext_Search_Builder_Iterator_Abstract
     */
    public function setEncoding($encoding)
    {
        $this->_encoding = (string)$encoding;
        return $this;
    }

    /**
     * build document from given $array of data
     *
     * @param array $fieldArray
     * @return Zend_Search_Lucene_Document
     */
    protected function _buildDocument($fieldArray)
    {
        $array = (array)$fieldArray;
        $document = new Zend_Search_Lucene_Document();
        foreach ($array as $fieldName => $fieldValue) {
            $this->_indexField($document, $fieldName, $fieldValue);
        }
        return $document;
    }

    /**
     * indexes field in given document, acording to field name index type,
     * in case of composite field value (array/Iterator) traverses through
     * entire set and adds
     * all values to given field name
     *
     * @param Zend_Search_Lucene_Document $document
     * @param string $fieldName
     * @param mixed $fieldValue
     */
    protected function _indexField(Zend_Search_Lucene_Document $document,
                                   $fieldName, $fieldValue)
    {
        if (is_array($fieldValue) || $fieldValue instanceof Iterator) {
            foreach ($fieldValue as $value) {
                $this->_indexField($document, $fieldName, $value);
            }
        } else {
            $indexType = $this->_fieldIndexesType($fieldName);
            // var_dump($indexType);
            switch ($indexType) {
                case null:
                    break;
                case wrxNext_Search_Builder::FIELD_BINARY:
                    $document->addField(
                        Zend_Search_Lucene_Field::binary($fieldName,
                            $fieldValue));
                    break;
                case wrxNext_Search_Builder::FIELD_KEYWORD:
                    $document->addField(
                        Zend_Search_Lucene_Field::keyword($fieldName,
                            $fieldValue, $this->_encoding));
                    break;
                case wrxNext_Search_Builder::FIELD_UNINDEXED:
                    $document->addField(
                        Zend_Search_Lucene_Field::unIndexed($fieldName,
                            $fieldValue, $this->_encoding));
                    break;
                case wrxNext_Search_Builder::FIELD_UNSTORED:
                    $document->addField(
                        Zend_Search_Lucene_Field::unStored($fieldName,
                            $fieldValue, $this->_encoding));
                    break;
                case wrxNext_Search_Builder::FIELD_TEXT:
                default:
                    // var_dump($indexType.'-'.$fieldName.': '.$fieldValue);
                    $document->addField(
                        Zend_Search_Lucene_Field::text($fieldName,
                            $fieldValue, $this->_encoding));
                    break;
            }
        }
    }

    /**
     * returns index type to be used for given field name, null if given field
     * should not be indexed
     *
     * @param string $fieldName
     * @return int
     */
    protected function _fieldIndexesType($fieldName)
    {
        if (isset($this->_fieldIndexes[$fieldName])) {
            return $this->_fieldIndexes[$fieldName];
        } elseif (!$this->_indexAllFields) {
            return null;
        }
        return $this->getDefaultIndexType();
    }

    /**
     * returns default index type
     *
     * @return int
     */
    public function getDefaultIndexType()
    {
        if (is_null($this->_defaultIndexType)) {
            return wrxNext_Search_Builder::TEXT;
        }
        return $this->_defaultIndexType;
    }

    /**
     * sets default index type for unspecified fields
     *
     * @param int $indexType
     * @return wrxNext_Search_Builder_Iterator_Abstract
     */
    public function setDefaultIndexType($indexType)
    {
        $this->_defaultIndexType = (int)$indexType;
        return $this;
    }
}
