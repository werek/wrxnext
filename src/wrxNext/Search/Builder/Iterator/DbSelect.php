<?php

/**
 * Zend_Db_Select search builder iterator
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Search_Builder_Iterator_DbSelect extends wrxNext_Search_Builder_Iterator_Abstract implements Iterator
{
    /**
     * holds select object
     *
     * @var Zend_Db_Select
     */
    protected $_select = null;
    /**
     * holds queried statement object
     *
     * @var Zend_Db_Statement
     */
    protected $_stmt = null;
    /**
     * holds data from current row;
     *
     * @var array
     */
    protected $_currentRow = null;
    /**
     * holds current position number, iterated through whole query
     *
     * @var int
     */
    protected $_position = 0;

    /**
     * constuctor
     *
     * @param Zend_Db_Select $select
     */
    public function __construct($select = NULL)
    {
        if (!is_null($select)) {
            $this->setSelect($select);
        }

        $this->init();
    }

    /**
     * dummy method to be used if extending clas should need additional configuration
     * during construction of object
     */
    public function init()
    {
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::rewind()
     */
    public function rewind()
    {
        $this->_runQuery();
    }

    /**
     * resets statement
     */
    protected function _runQuery()
    {
        $this->_position = 0;
        $select = $this->getSelect();
        if (!$select instanceof Zend_Db_Select) {
            throw new Exception('select is not Zend_Db_Select object');
        } else {
            Zend_Registry::get('log')->debug($select->assemble());
        }
        $this->_stmt = $select->query(Zend_Db::FETCH_ASSOC);
        $this->_fetchData();
    }

    /**
     * returns select object
     *
     * @return Zend_Db_Select
     */
    public function getSelect()
    {
        return $this->_select;
    }

    /**
     * sets select object
     *
     * @param Zend_Db_Select $select
     * @return wrxNext_Search_Builder_Iterator_DbSelect
     */
    public function setSelect(Zend_Db_Select $select)
    {
        $this->_select = $select;
        return $this;
    }

    /**
     * fetches new data into memory
     */
    protected function _fetchData()
    {
        $this->_currentRow = $this->_stmt->fetch();
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::current()
     */
    public function current()
    {
        return $this->_combineData();
    }

    /**
     * combines data and returns Zend_Search_Lucene_Document
     *
     * @return Zend_Search_Lucene_Document
     */
    protected function _combineData()
    {
        return $this->_buildDocument($this->_currentRow);
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::key()
     */
    public function key()
    {
        return $this->_position;
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::next()
     */
    public function next()
    {
        $this->_fetchData();
        ++$this->_position;
    }

    /**
     * (non-PHPdoc)
     * @see Iterator::valid()
     */
    public function valid()
    {
        $status = false;
        if (is_array($this->_currentRow)) {
            $status = true;
        }
        return $status;
    }
}
