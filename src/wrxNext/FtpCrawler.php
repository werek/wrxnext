<?php

class wrxNext_FtpCrawler
{

    /**
     * holds ftp connection resource
     *
     * @var resource
     */
    protected $_ftpConnection = null;

    protected $_ftpHost = null;

    protected $_ftpPort = 21;

    protected $_ftpUser = null;

    protected $_ftpPassword = null;

    protected $_ftpDirectory = null;

    protected $_filenameFilter = null;

    protected $_fileFilter = null;

    protected $_log = null;

    public function __construct($options = array())
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function setOptions($options)
    {
        $options = (array)$options;
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    public function crawl()
    {
        $log = $this->getLog();
        $filnameFilter = $this->getFilenameFilter();
        $fileFilter = $this->getFileFilter();

        // opening connection
        $log->info('---- otwieram polaczenie z hostem ' . $this->_ftpHost .
            ' ----');
        $this->_openConnectionAndAuthorize();
        if (!is_resource($this->_ftpConnection)) {
            $log->info(' blad polaczenia kończe prace');
            return;
        } else {
            $log->info('---- polaczono zaczynam przetwarzanie ----');
            $baseDir = $this->_changeDirectory();
            if ($baseDir === false) {
                $log->info(' blad podczas zmiany katalogu, kończe prace');
                return;
            }
        }
    }

    /**
     * gets log object
     *
     * @return Zend_Log
     */
    public function getLog()
    {
        return $this->_log;
    }

    /**
     * sets log object
     *
     * @param Zend_Log $log
     * @return wrxNext_FtpCrawler
     */
    public function setLog($log)
    {
        $this->_log = $log;
        return $this;
    }

    /**
     * gets filename filter
     *
     * @return wrxNext_FtpCrawler_FilenameFilter
     */
    public function getFilenameFilter()
    {
        return $this->_filenameFilter;
    }

    /**
     * sets filename filter
     *
     * @param wrxNext_FtpCrawler_FilenameFilter $filenameFilter
     * @return wrxNext_FtpCrawler
     */
    public function setFilenameFilter($filenameFilter)
    {
        $this->_filenameFilter = $filenameFilter;
        return $this;
    }

    /**
     * gets file body filter
     *
     * @return wrxNext_FtpCrawler_FileFilter
     */
    public function getFileFilter()
    {
        return $this->_fileFilter;
    }

    /**
     * sets file body filter
     *
     * @param wrxNext_FtpCrawler_FileFilter $fileFilter
     * @return wrxNext_FtpCrawler
     */
    public function setFileFilter($fileFilter)
    {
        $this->_fileFilter = $fileFilter;
        return $this;
    }

    protected function _openConnectionAndAuthorize()
    {
        $this->_ftpConnection = ftp_connect($this->getHost(), $this->getPort());
        if (is_resource($this->_ftpConnection)) {
            ftp_login($this->_ftpConnection, $this->getUser(),
                $this->getPassword());
        }
    }

    /**
     * gets hosts
     *
     * @return string
     */
    public function getHost()
    {
        return $this->_ftpHost;
    }

    /**
     * gets ftp port
     *
     * @return int
     */
    public function getPort()
    {
        return $this->_ftpPort;
    }

    /**
     * gets ftp username
     *
     * @return string
     */
    public function getUser()
    {
        return $this->_ftpUser;
    }

    /**
     * gets ftp password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->_ftpPassword;
    }

    protected function _changeDirectory()
    {
        if (!is_null($this->_ftpDirectory)) {
            if (ftp_chdir($this->_ftpConnection, $this->_ftpDirectory) === false) {
                return false;
            }
        }
        return ftp_pwd($this->_ftpConnection);
    }

    /**
     * sets connection resource
     *
     * @param resource $resource
     * @return wrxNext_FtpCrawler
     */
    public function setConnection($resource)
    {
        $this->_ftpConnection = $resource;
        return $this;
    }

    /**
     * gets connection resource
     *
     * @return resource
     */
    public function getConnection()
    {
        return $this->_ftpConnection;
    }

    /**
     * sets hosts
     *
     * @param string $host
     * @return wrxNext_FtpCrawler
     */
    public function setHost($host)
    {
        $this->_ftpHost = $host;
        return $this;
    }

    /**
     * sets ftp port
     *
     * @param int $port
     * @return wrxNext_FtpCrawler
     */
    public function setPort($port)
    {
        $this->_ftpPort = $port;
        return $this;
    }

    /**
     * sets ftp username
     *
     * @param string $username
     * @return wrxNext_FtpCrawler
     */
    public function setUser($username)
    {
        $this->_ftpUser = $username;
        return $this;
    }

    /**
     * sets ftp password
     *
     * @param string $password
     * @return wrxNext_FtpCrawler
     */
    public function setPassword($password)
    {
        $this->_ftpPassword = $password;
        return $this;
    }

    /**
     * sets starting directory
     *
     * @param string $path
     * @return wrxNext_FtpCrawler
     */
    public function setPath($path)
    {
        $this->_ftpDirectory = $path;
        return $this;
    }

    /**
     * gets starting directory
     *
     * @return string
     */
    public function getPath()
    {
        return $this->_ftpDirectory;
    }

    protected function _processDirectory()
    {
    }
}
