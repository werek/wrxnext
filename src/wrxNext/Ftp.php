<?php

/**
 * klasa do obslugi polaczenia ftp (work in progress)
 *
 */
class wrxNext_Ftp
{

    private $_ftp_host = "";

    private $_ftp_user = "";

    private $_ftp_pass = "";

    private $_ftp_pwd = "";

    private $_ftp_connection = null;

    private $_ftp_params = array();

    public function __construct($params, $dir = ".")
    {
        foreach ($params as $k => $v) {
            switch ($k) {
                case "host":
                    $this->_ftp_host = $v;
                    unset($params[$k]);
                    break;
                case "user":
                    $this->_ftp_user = $v;
                    unset($params[$k]);
                    break;
                case "pass":
                    $this->_ftp_pass = $v;
                    unset($params[$k]);
                    break;
            }
        }
        $this->_ftp_pwd = $dir;
    }

    /**
     * otwiera polaczenie z hostem
     *
     * @return wrxNext_Ftp
     */
    public function connect()
    {
        $res = preg_match('@(.*?):(\d*?$)@', $this->_ftp_host, $wyniki);
        $this->_ftp_connection = ftp_connect(
            $res ? $wyniki[1] : $this->_ftp_host, $res ? $wyniki[2] : null);
        if ($this->_ftp_connection === false) {
            throw new wrxNext_Ftp_Exception(
                "error during connecting to host: '" . $this->_ftp_host . "'",
                1);
        }
        return $this;
    }

    /**
     * wykonuje procedure logowania
     *
     * @return wrxNext_Ftp
     */
    public function login()
    {
        if (is_null($this->_ftp_connection) or
            !is_resource($this->_ftp_connection)
        ) {
            throw new wrxNext_Ftp_Exception(
                "there is no active ftp connection to host, you must first be connected to ftp server in order to log in",
                2);
        }
        $res = @ftp_login($this->_ftp_connection, $this->_ftp_user,
            $this->_ftp_pass);
        if ($res === false) {
            throw new wrxNext_Ftp_Exception(
                "failed to log in using credentials, login: " .
                $this->_ftp_user . " , pass:" . $this->_ftp_pass, 3);
        } else {
            $this->cd($this->_ftp_pwd);
        }
        return $this;
    }

    /**
     * zmienia katalog na podany
     *
     * @param string $dir
     * @return wrxNext_Ftp
     */
    public function cd($dir)
    {
        $res = @ftp_chdir($this->_ftp_connection, $dir);
        if ($res === false) {
            throw new wrxNext_Ftp_Exception(
                "error during changing directory to: '" . $dir . "'", 4);
        }
        $this->_ftp_pwd = ftp_pwd($this->_ftp_connection);
        if ($this->_ftp_pwd === false) {
            throw new wrxNext_Ftp_Exception(
                "error during reading of current directory", 5);
        }
        return $this;
    }

    /**
     * uploaduje plik na serwer
     *
     * @param string $filepath
     * @param string $destDir
     * @return wrxNext_Ftp
     */
    public function put($filepath, $destDir = null)
    {
        if (file_exists($filepath)) {
            $file = basename($filepath);
            if (is_null($destDir)) {
                $destDir = $this->pwd();
            }
            $res = ftp_put($this->_ftp_connection, $destDir . '/' . $file,
                $filepath, FTP_BINARY);
            if ($res === false) {
                throw new wrxNext_Ftp_Exception('FTP upload has failed');
            }
        } else {
            throw new wrxNext_Ftp_Exception('Source file does\'nt exist');
        }
        return $this;
    }

    /**
     * zwraca aktualny katalog zdalny
     *
     * @return string
     */
    public function pwd()
    {
        return $this->_ftp_pwd;
    }
}
