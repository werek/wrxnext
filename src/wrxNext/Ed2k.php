<?php

/**
 * klasa zapewnijaca obsluge linkow ED2K
 *
 */
class wrxNext_Ed2k
{

    /**
     * przechowuje dane linku ed2k
     *
     * @var array
     */
    private $data = array();

    /**
     * konstruktor klasy, moze za parametr przyjac link ed2k na
     * ktorym ma pracowac klasa
     *
     * @param string $ed2k
     */
    public function __construct($ed2k = null)
    {
        if (!is_null($ed2k)) {
            $this->parse($ed2k);
        }
    }

    /**
     * wewnetrzna funkcja rozbijajaca link w formie string na tablice
     * za parametrami linku.
     * zwraca prawde przy poprawnym przetworzeniu i falsz przy bledzie
     *
     * @param string $ed2k
     * @return boolean
     */
    private function parse($ed2k)
    {
        if (preg_match('/ed2k:\/\/\|file/', $ed2k)) {
            $ed2k = trim($ed2k);
            preg_match(
                '/^ed2k://\|file\|(.+)\|([0-9]+)\|([[:alnum:]]{32,32})\|\//is',
                $ed2k, $elem);
            $this->data['oryg'] = trim($ed2k);
            $this->data['name'] = $elem[1];
            $this->data['size'] = $elem[2];
            $this->data['hash'] = $elem[3];
            return true;
        } else {
            return false;
        }
    }

    /**
     * parsuje podany w parametrze $ed2k link
     *
     * @param string $ed2k
     */
    public function link($ed2k)
    {
        $this->parse($ed2k);
    }

    /**
     * podaje rozmiar pliku do ktorego odnosi sie link
     *
     * @param boolean $humanReadable
     * @return int
     */
    public function getSize($humanReadable = false)
    {
        if ($humanReadable) {
            $rozmiar = $this->data['size'];
            $znak = 0;
            while ($rozmiar > 999 && $znak < count($nazwy)) {
                $rozmiar = $rozmiar / 1024;
                $znak++;
            }
            $nazwy = array(
                'B',
                'kB',
                'MB',
                'GB',
                'TB',
                'PB',
                'EB'
            );
            $rozmiar = round($rozmiar, 2);
            return $rozmiar . ' ' . $nazwy[$znak];
        } else {
            return $this->data['size'];
        }
    }

    /**
     * zwraca hash pliku z linku
     *
     * @return string
     */
    public function getHash()
    {
        return $this->data['hash'];
    }

    /**
     * zwraca nazwe pliku z linku
     *
     * @return string
     */
    public function getName()
    {
        return $this->data['name'];
    }

    /**
     * zwraca link przygotowany na podstawie wewnetrznych danych
     *
     * @return string
     */
    public function getLink()
    {
        return 'ed2k://|file|' . $this->data['name'] . '|' . $this->data['size'] .
        '|' . $this->data['hash'] . '|/';
    }

    /**
     * ustawia nowa nazwe pliku
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->data['name'] = preg_replace('/\s{1,}/', '.', trim($name));
    }

    /**
     * wstawia do pliku suffix do nazwy pliku
     * tuz przed rozszerzeniem
     *
     * @param string $sufix
     * @return string
     */
    public function setFilenameSufix($sufix)
    {
        return $this->data['name'] = preg_replace('/(.*)(\..*?)$/is',
            '\\1.' . $sufix . '\\2', $this->data['name']);
    }
}

?>
