<?php

class wrxNext_Slot
{
    /**
     * checks given class implementation compatibility againsts slot requirements
     *
     * @param string $adapterName
     * @throws wrxNext_Slot_Exception
     * @return boolean
     */
    public static function checkCompatibility($adapterName)
    {
        $adapter = self::factory($adapterName);
        if (!$adapter->getFrontend() instanceof wrxNext_Slot_Frontend_Interface) {
            throw new wrxNext_Slot_Exception($adapterName . "'s returned frontend object does not implement frontend interface.");
        }
        if (!$adapter->getBackend() instanceof wrxNext_Slot_Backend_Interface) {
            throw new wrxNext_Slot_Exception($adapterName . "'s returned backend object does not implement backend interface.");
        }
        return true;
    }

    /**
     * creates adapter object based on given params
     *
     * @param string $adapter
     * @param string $data
     * @param string $size
     * @throws wrxNext_Slot_Exception
     * @return wrxNext_Slot_Adapter_Interface
     */
    public static function factory($adapter, $data = null, $size = null)
    {
        if (!class_exists($adapter, true)) {
            throw new wrxNext_Slot_Exception("given class does not exists: " . $adapter);
        }
        $reflection = new ReflectionClass($adapter);
        if ($reflection->implementsInterface('wrxNext_Slot_Adapter_Interface')) {
            return $reflection->newInstance($data, $size);
        } else {
            throw new wrxNext_Slot_Exception("given class does not implement adapter interface: " . $adapter);
        }
    }
}
