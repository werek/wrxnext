<?php

/**
 * class for generating signature for given data
 *
 * @author bweres01
 *
 */
class wrxNext_Hmac
{

    /**
     * holds key
     *
     * @var string
     */
    protected $_key = null;

    /**
     * gets hmac key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * sets hmac key
     *
     * @param string $key
     * @return wrxNext_Hmac
     */
    public function setKey($key)
    {
        $this->_key = $key;
        return $this;
    }

    /**
     * generates hmac-sha1 hash
     *
     * @param string $data
     * @return string
     */
    public function sha1($data)
    {
        return $this->_generateHash($data, 'sha1');
    }

    /**
     * generates hmac hash
     *
     * @param mixed $data
     * @param string $hashfunc
     * @return string
     */
    protected function _generateHash($data, $hashfunc = 'sha1')
    {
        $blocksize = 64;
        $hashfunc = 'sha1';
        if (strlen($this->_key) > $blocksize)
            $key = pack('H*', $hashfunc($key));
        $key = str_pad($this->_key, $blocksize, chr(0x00));
        $ipad = str_repeat(chr(0x36), $blocksize);
        $opad = str_repeat(chr(0x5c), $blocksize);
        $hmac = pack('H*',
            $hashfunc(
                ($key ^ $opad) . pack('H*',
                    $hashfunc(($key ^ $ipad) . $data))));
        return $hmac;
    }
}
