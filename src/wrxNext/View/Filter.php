<?php

class wrxNext_View_Filter
{

    /**
     * setups paths to filters
     *
     * @param Zend_View $view
     */
    public static function setUpViewFilters(Zend_View $view)
    {
        $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'Filter');
        $view->addFilterPath($path, __CLASS__);
    }
}
