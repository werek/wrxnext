function uploadError(file, errorCode, message) {
    try {
        switch (errorCode) {
            case SWFUpload.UPLOAD_ERROR.IO_ERROR:
                alert("I/O Error During transmission, typicaly to long transmission");
                break;
            case SWFUpload.UPLOAD_ERROR.FILE_CANCELLED:
                alert("File cancelled");
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_STOPPED:
                alert("upload stopped");
                break;
            case SWFUpload.UPLOAD_ERROR.UPLOAD_LIMIT_EXCEEDED:
                alert("Upload limit Exceeded")
                break;
            default:
                alert(message);
                break;
        }
    } catch (ex3) {
        this.debug(ex3);
    }

}
