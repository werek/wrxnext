var buttonObject = document.getElementById('<{$button_id}>');
var formObject = document.getElementById('<{$form_id}>');

formObject.style.display = 'none';
formObject.style.position = 'absolute';
formObject.style.zIndex = 100;
formObject.style.left = buttonObject.offsetLeft + 'px';
var bottom = buttonObject.offsetTop + buttonObject.offsetHeight;
formObject.style.top = bottom + 'px';
document.getElementById('<{$form_id}>_cancelupload').disabled = true;

buttonObject.onclick = function () {
    document.getElementById('<{$form_id}>').style.display = 'block';
    document.getElementById('<{$button_id}>').disabled = 'disabled';
}

document.getElementById('<{$form_id}>_closeform').onclick = function () {
    document.getElementById('<{$form_id}>').style.display = 'none';
    document.getElementById('<{$button_id}>').disabled = null;
}

//funkcja do aktualizacji pozycji formy
function updateFormPosition() {
    var buttonObject = document.getElementById('<{$button_id}>');
    var formObject = document.getElementById('<{$form_id}>');

    formObject.style.left = buttonObject.offsetLeft + 'px';
    var bottom = buttonObject.offsetTop + buttonObject.offsetHeight;
    formObject.style.top = bottom + 'px';
}

//funkcja do obslugi informacji o upload
function uploadProgress(file, bytesLoaded) {
    var filenameSpan = document.getElementById('<{$form_id}>_queue_current_filename');
    var progressSpan = document.getElementById('<{$form_id}>_queue_current_progress');
    var progressBarDiv = document.getElementById('<{$form_id}>_queue_current_progressbar');
    try {
        var percent = Math.ceil((bytesLoaded / file.size) * 100);
        filenameSpan.innerHTML = file.name + ' (' + SWFUpload.speed.formatBytes(file.size) + ')';
        progressSpan.innerHTML = SWFUpload.speed.formatBytes(file.sizeUploaded) + ' (current speed: ' + SWFUpload.speed.formatBytes(file.currentSpeed / 8) + '/s, average speed: ' + SWFUpload.speed.formatBytes(file.averageSpeed / 8) + '/s)';
        progressBarDiv.style.width = file.percentUploaded + '%';
    } catch (ex) {
        this.debug(ex);
    }
}

//funkcja obslugujaca wysylanie danych po wybraniu plikow
function fileDialogComplete(numOfSelectedFiles, numOfFilesQueued, totalNumOfFilesInQueued) {
    if (numOfFilesQueued > 0) {
        document.getElementById('<{$form_id}>_queue').style.display = 'block';
        document.getElementById(this.customSettings.cancelButtonId).disabled = false;
    }
    this.startUpload();
}

//obsluga dodanego pliku
function fileQueued(file) {
    var listContainer = document.getElementById('<{$form_id}>_queue_list');
    listContainer.style.display = 'block';
    var fileContainer = document.createElement('div');
    fileContainer.id = file.id;
    fileContainer.className = 'fileContainer';
    fileContainer.innerHTML = file.name + ' (' + file.type + ')';
    listContainer.appendChild(fileContainer);
}

//obsluguje zakonczenie wysylania pliku
function uploadComplete(file) {
    //document.getElementById(file.id).style.display='none';
    document.getElementById('<{$form_id}>_queue_current').style.display = 'none';
    if (this.getStats().files_queued === 0) {
        //this.queueSettings.queue_cancelled_flag=false;
        document.getElementById(this.customSettings.cancelButtonId).disabled = true;
        document.getElementById('<{$form_id}>_closeform').disabled = false;
        document.getElementById('<{$form_id}>_selectfiles').disabled = false;
        //document.getElementById('<{$form_id}>_queue').style.display='none';
    }
}

//obsluguje start pobierania pliku
function uploadStart(file) {
    document.getElementById('<{$form_id}>_queue_current').style.display = 'block';
    document.getElementById(this.customSettings.cancelButtonId).disabled = null;
    document.getElementById('<{$form_id}>_closeform').disabled = true;
    document.getElementById('<{$form_id}>_selectfiles').disabled = true;
    return true;
}

//obsluguje zdarzenie poprawnego wyslania pliku
function uploadSuccess(file, serverData, response) {
    var fileDiv = document.getElementById(file.id);
    fileDiv.className = 'upload_success';
    fileDiv.innerHTML += '<br />completed succesfully';
    console.log(response);
    console.log(serverData);
    console.log(file);
    reloadGearImage();
}

//obsluguje bledy zwiazane z uploadem
function uploadError(file, errorCode, message) {
    if (this.debug) {
        alert('error code: ' + errorCode + "\n\n" + message);
    }
    var fileDiv = document.getElementById(file.id);
    fileDiv.className = 'upload_error';
    fileDiv.innerHTML += '<br />upload error';
}

var swfPlaceholder = document.getElementById('<{$form_id}>_placeholder');
swfPlaceholder.style.position = 'absolute';
swfPlaceholder.style.zIndex = 101;
swfPlaceholder.style.width = document.getElementById('<{$form_id}>_selectfiles').style.width;
swfPlaceholder.style.height = document.getElementById('<{$form_id}>_selectfiles').style.height;
swfPlaceholder.style.left = document.getElementById('<{$form_id}>_selectfiles').offsetLeft + 'px';
swfPlaceholder.style.top = document.getElementById('<{$form_id}>_selectfiles').offsetTop + 'px';
