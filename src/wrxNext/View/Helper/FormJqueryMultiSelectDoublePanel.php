<?php

class wrxNext_View_Helper_FormJqueryMultiSelectDoublePanel extends Zend_View_Helper_FormSelect
{

    /**
     * renders multiselect and uses given id to bind jquery function to create
     * double panel select
     *
     * @param unknown $name
     * @param string $value
     * @param string $attribs
     * @param string $options
     * @param string $listsep
     */
    public function formJqueryMultiSelectDoublePanel($name, $value = null,
                                                     $attribs = null, $options = null, $listsep = "<br />\n")
    {
        if (!array_key_exists('id', $attribs)) {
            $attribs['id'] = uniqid('JqueryMultiSelectDoublePanel');
        }
        $html = $this->formSelect($name, $value, $attribs, $options, $listsep);
        $this->_addInitScript($attribs['id']);
        return $html;
    }

    /**
     * adds init script
     *
     * @param string $elementID
     */
    protected function _addInitScript($elementID)
    {
        // main logic behind binding
        $scriptPath = __DIR__ . DIRECTORY_SEPARATOR .
            array_reverse(explode('_', __CLASS__))[0] . '.js';
        $template = new wrxNext_StringBuilder_Template(
            file_get_contents($scriptPath));
        $this->view->headScript()->appendScript(
            $template->render(array(
                'id' => $elementID
            )));
    }
}
