<?php

class wrxNext_View_Helper_SwfObject extends Zend_View_Helper_Abstract
{

    /**
     * zwraca szablon do obslugi plikow flash, opcjonalnie dajac dodatkowe
     * parametry
     *
     * @param string $swfPath
     * @param string $swfName
     * @param string $width
     * @param string $height
     * @param string $flashVer
     * @param array $flashVars
     * @param array $params
     * @return string
     */
    public function swfObject($swfPath, $swfName, $width, $height, $flashVer,
                              $flashVars = array(), $params = array())
    {
        $div = '<div id="' . $swfName .
            '">SimpleViewer requires Adobe Flash. <a href="http://www.macromedia.com/go/getflashplayer/">Get Adobe Flash.</a> If you have Flash installed, <a href="?detectflash=false">click to view gallery</a>.</div>' .
            "\n";
        $jsBaseCode = '<script type="text/javascript">
			var ##var###vars={};
			##vars###
			var ##var###params={};
			##params###
            swfobject.embedSWF("' .
            $swfPath . '", "##var###", "' . $width . '", "' . $height .
            '", "' . $flashVer . '",false,##var###vars,##var###params);
        </script>';

        $jsBaseCode = str_replace("##var###", $swfName, $jsBaseCode);
        $vars = array();
        $varsFormat = "##var###vars.%s=\"%s\";";
        $varsFormat = str_replace("##var###", $swfName, $varsFormat);
        foreach ($flashVars as $k => $v) {
            $tmpCode = sprintf($varsFormat, $k, $v);
            if (is_bool($v)) {
                $tmpCode = preg_replace('@".*?"@i', ($v) ? 'true' : 'false',
                    $tmpCode);
            }
            $vars[] = $tmpCode;
        }
        $param = array();
        $paramFormat = "##var###params.%s=\"%s\";";
        $paramFormat = str_replace("##var###", $swfName, $paramFormat);
        foreach ($params as $k => $v) {
            $tmpCode = sprintf($paramFormat, $k, $v);
            if (is_bool($v)) {
                $tmpCode = preg_replace('@".*?"@i', ($v) ? 'true' : 'false',
                    $tmpCode);
            }
            $param[] = $tmpCode;
        }
        $jsBaseCode = str_replace('##vars###', implode("\n", $vars),
            $jsBaseCode);
        $jsBaseCode = str_replace('##params###', implode("\n", $param),
            $jsBaseCode);
        $code = $jsBaseCode;
        return $div . $code . "\n";
    }
}
