<?php

/**
 * helper class for generating html5 specific elements
 *
 * @author bweres01
 *
 */
class wrxNext_View_Helper_Html5 extends Zend_View_Helper_Abstract
{

    /**
     * holds types for source links based on extension
     *
     * @var array
     */
    protected $_mediaExtensionsType = array(
        'audio' => array(
            'mp3' => 'audio/mpeg',
            'm4a' => 'audio/mp4',
            'webm' => 'audio/webm',
            'ogg' => 'audio/ogg',
            'mp4' => 'audio/mp4'
        ),
        'video' => array(
            'mp3' => 'audio/mpeg',
            'm4a' => 'audio/mp4',
            'webm' => 'video/webm',
            'ogg' => 'video/ogg',
            'mp4' => 'video/mp4'
        )
    );

    /**
     * fluent interface for accessing internall method
     *
     * @return wrxNext_View_Helper_Html5
     */
    public function html5()
    {
        return $this;
    }

    /**
     * override to limit unwanted echo on this helper
     *
     * @return string
     */
    public function __toString()
    {
        return '';
    }

    /**
     * renders audio tag for given sources
     *
     * @param string|array $source
     * @param array $options
     */
    public function audio($source, array $options = null)
    {
        return $this->media('audio', $source, $options)->render();
    }

    /**
     * constructs html5 media element object
     *
     * @param string $elementType
     * @param string|array $source
     * @param array $options
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function media($elementType, $source, array $options = null)
    {
        $element = new wrxNext_View_Helper_Html5_Element($elementType, $options);
        if (is_array($source)) {
            $context = strtolower($elementType);
            foreach ($source as $type => $src) {
                $config = array(
                    'src' => $src,
                    'element' => 'source'
                );
                if (intval($type) === $type) {
                    $type = $this->getMediaTypeFromPath($src, $context);
                }
                if (!empty($type)) {
                    $config['type'] = $type;
                }
                $element->addElement($config);
            }
        } else {
            $element->set('src', (string)$source);
        }
        return $element;
    }

    /**
     * returns media type according to file extension, to be used in <source>
     * element
     *
     * @param string $path
     * @param string $context
     * @throws wrxNext_Exception
     * @return string null if type is not found
     */
    public function getMediaTypeFromPath($path, $context = 'video')
    {
        if (!array_key_exists($context, $this->_mediaExtensionsType)) {
            throw new wrxNext_Exception(
                'given context "' . $context . '" is not recognised');
        }
        $fileNameParts = array_reverse(explode('.', basename($path)));
        $extension = $fileNameParts[0];
        if (isset($this->_mediaExtensionsType[$context][$extension])) {
            return $this->_mediaExtensionsType[$context][$extension];
        }
    }

    /**
     * renders video tag for given sources
     *
     * @param string|array $source
     * @param array $options
     */
    public function video($source, array $options = null)
    {
        return $this->media('video', $source, $options)->render();
    }
}
