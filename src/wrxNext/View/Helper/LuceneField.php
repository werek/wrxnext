<?php

class wrxNext_View_Helper_LuceneField extends Zend_View_Helper_Abstract
{

    /**
     * extracts given field value, if field does not exists the returns default
     * value
     *
     * @param Zend_Search_Lucene_Document $luceneDoc
     * @param string $fieldName
     * @param string $defaultValue
     * @return string
     */
    public function luceneField($luceneDoc, $fieldName, $defaultValue = null)
    {
        try {
            return $luceneDoc->getFieldValue($fieldName);
        } catch (Zend_Search_Lucene_Exception $e) {
            return $defaultValue;
        }
    }
}
