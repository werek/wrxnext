<?php

class wrxNext_View_Helper_SwfUpload extends Zend_View_Helper_Abstract
{

    /**
     * przechwouje sciezke skryptu
     *
     * @var string
     */
    protected $_scriptPath;

    /**
     * przechwouje instancje swfupload
     *
     * @var wrxNext_SwfUpload
     */
    protected $_swfUpload = null;

    /**
     * zwraca szablon do obslugi plikow flash, opcjonalnie dajac dodatkowe
     * parametry
     *
     * @param wrxNext_SwfUpload $swfUpload
     * @return string
     */
    public function swfUpload($swfUpload = null)
    {
        if ($swfUpload instanceof wrxNext_SwfUpload) {
            $this->_swfUpload = $swfUpload;
        }
        return $this;
    }

    /**
     * dodaje sciezke do skryptu swf upload
     *
     * @param string $path
     * @return $this
     */
    public function setJsPath($path)
    {
        $this->view->headScript()->appendFile($path);
        $this->_scriptPath = $path;
        return $this;
    }

    /**
     * zwraca aktualna sciezke
     *
     * @return string
     */
    public function getJsPath()
    {
        return $this->_scriptPath;
    }

    /**
     * ustala sciezke do pliku swf kontrolujacego wysylanie
     *
     * @param string $path
     * @return $this
     */
    public function setSwfPath($path)
    {
        $this->_swfUpload->flash_url = $path;
        return $this;
    }

    /**
     * zwraca sciezke do pliku swf kontrolujacego wysylanie
     *
     * @return string
     */
    public function getSwfPath()
    {
        return $this->_swfUpload->flash_url;
    }

    /**
     * ustala sciezke na ktora ma isc wysylanie kazdego pliku z kolejki
     *
     * @param string $path
     * @return $this
     */
    public function setUploadPath($path)
    {
        $this->_swfUpload->upload_url = $path;
        return $this;
    }

    /**
     * zwraca sciezke na ktora ma isc wysylanie kazdego pliku z kolejki
     *
     * @return string
     */
    public function getUploadPath()
    {
        return $this->_swfUpload->upload_url;
    }

    /**
     * zwraca nazwe instancji swfupload
     *
     * @return string
     */
    public function getInstanceName()
    {
        return $this->_swfUpload->instanceName;
    }

    /**
     * magiczna metoda na echo
     *
     * @return string
     */
    public function __toString()
    {
        return $this->buildCodeBlock();
    }

    /**
     * buduje gotowy blok kodu z obiektu
     *
     * @return string
     */
    public function buildCodeBlock()
    {
        $code = "<script type=\"text/javascript\">\n%s\n</script>";
        return sprintf($code,
            wrxNext_SwfUpload_Js::indentCode($this->_swfUpload->getCode()));
    }
}
