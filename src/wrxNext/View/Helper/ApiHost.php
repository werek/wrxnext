<?php

class wrxNext_View_Helper_ApiHost extends Zend_View_Helper_Abstract
{

    protected static $_useBaseUrl = false;

    protected static $_address = null;

    /**
     * sets whete to use baseUrl helper or not
     *
     * @param boolean $flag
     */
    public static function useBaseUrl($flag = false)
    {
        self::$_useBaseUrl = $flag;
    }

    /**
     * sets base address
     *
     * @param string $uri
     */
    public static function address($uri)
    {
        self::$_address = (string)$uri;
    }

    /**
     * creates link for external elements
     *
     * @param string $relativeUrl
     * @return string
     */
    public function apiHost($relativeUrl = null)
    {
        if (!self::$_useBaseUrl) {
            if (preg_match('#^http://.*$#', $relativeUrl)) {
                return $relativeUrl;
            }

            $relativeUrl = self::$_address . ltrim($relativeUrl, '/');

            return $relativeUrl;
        } else {
            return $this->view->baseUrl($relativeUrl);
        }
    }
}
