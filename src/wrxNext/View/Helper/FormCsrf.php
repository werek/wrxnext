<?php

class wrxNext_View_Helper_FormCsrf extends Zend_View_Helper_FormHidden
{
    /**
     * generates hidden input with random csrfValue
     *
     * @param string $name
     * @param mixed $value
     * @param array $attribs
     */
    public function formCsrf($name, $value = null, array $attribs = null)
    {
        $validator = new wrxNext_Validate_CsrfValue($attribs['csrfName']);
        $value = $validator->registerValue();
        unset($attribs['csrfName']);
        return $this->formHidden($name, $value, $attribs);
    }
}
