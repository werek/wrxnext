<?php

class wrxNext_View_Helper_FlashMessage extends Zend_View_Helper_Abstract
{

    const MESSAGE_INFO = 'alert_info';

    const MESSAGE_SUCCESS = 'alert_success';

    const MESSAGE_WARNING = 'alert_warning';

    const MESSAGE_ERROR = 'alert_error';

    /**
     * holds renderer instance
     *
     * @var wrxNext_View_Helper_FlashMessage_RenderInterface
     */
    protected $_renderer = null;

    /**
     * sets renderer class for messages
     *
     * @param wrxNext_View_Helper_FlashMessage_RenderInterface $renderer
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function setRenderer(wrxNext_View_Helper_FlashMessage_RenderInterface $renderer)
    {
        $this->_renderer = $renderer;
        return $this;
    }

    /**
     * returns renderer class for messages
     *
     * @return wrxNext_View_Helper_FlashMessage_RenderInterface
     */
    public function getRenderer()
    {
        if ( !$this->_renderer instanceof wrxNext_View_Helper_FlashMessage_RenderInterface) {
            $this->_renderer = new wrxNext_View_Helper_FlashMessage_BootstrapRender();
        }
        return $this->_renderer;
    }


    /**
     * returns store
     *
     * @return ArrayObject
     */
    protected function _getArray()
    {
        if ( !Zend_Registry::isRegistered(__CLASS__)) {
            Zend_Registry::set(
                __CLASS__,
                new ArrayObject(array(), ArrayObject::STD_PROP_LIST)
            );
            Zend_Registry::get(__CLASS__)->messages = array();
        }
        return Zend_Registry::get(__CLASS__);
    }

    /**
     *
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function flashMessage()
    {
        return $this;
    }

    /**
     * adds message
     *
     * @param string $message
     * @param string $type
     */
    protected function _addMessage($message, $type)
    {
        $this->_getArray()->messages[] = array(
            'message' => $message,
            'type'    => $type
        );
    }

    /**
     * renders all messages to string
     *
     * @return string
     */
    public function render()
    {
        return $this->getRenderer()->render($this->_getArray()->messages);
    }

    /**
     * echoes all stored messages
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * processes messages from form;
     *
     * @param Zend_Form $form
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function form(Zend_Form $form)
    {
        foreach ($form->getErrorMessages() as $message) {
            $this->_addMessage($message, self::MESSAGE_ERROR);
        }
        foreach ($form->getMessages() as $key => $messages) {
            $message = intval($key) ? '' : $this->view->translate($key) . ': ';
            if (is_array($messages)) {
                $message .= implode(' / ', $messages);
            } else {
                $message .= (string)$messages;
            }
            $this->_addMessage($message, self::MESSAGE_ERROR);
        }
        return $this;
    }

    /**
     * processes exception formating
     *
     * @param Exception $e
     * @param boolean   $includeTrace
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function exception(Exception $e, $includeTrace = false)
    {
        $message = get_class($e) . '(code:' . $e->getCode() . '): ' .
            $e->getMessage();
        $this->error($message);
        if ($includeTrace) {
            $this->error($e->getTraceAsString());
        }
        return $this;
    }

    /**
     * processes messages from session storage
     *
     * @param wrxNext_Session_Message $sessionMessages
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function session(wrxNext_Session_Message $sessionMessages)
    {
        $mapping = array(
            wrxNext_Session_Message_Text::INFO    => self::MESSAGE_INFO,
            wrxNext_Session_Message_Text::ERROR   => self::MESSAGE_ERROR,
            wrxNext_Session_Message_Text::WARNING => self::MESSAGE_WARNING,
            wrxNext_Session_Message_Text::SUCCESS => self::MESSAGE_SUCCESS
        );

        foreach ($sessionMessages->getMessages() as $message) {
            $type = self::MESSAGE_INFO;
            if (array_key_exists($message->getType(), $mapping)) {
                $type = $mapping[ $message->getType() ];
            }
            $this->_addMessage((string)$message->getValue(), $type);
            if ($message instanceof wrxNext_Session_Message_ExceptionMessage) {
                $this->_addMessage(
                    '<pre>' . $message->getException()
                                      ->getTraceAsString() . '</pre>',
                    $type
                );
            }
        }
        return $this;
    }

    /**
     * adds multiple messages
     *
     * @param array $messages
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function addMessages($messages)
    {
        foreach ($messages as $item) {
            $this->_addMessage($item['message'], $item['type']);
        }
        return $this;
    }

    /**
     *
     * @param string $message
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function info($message)
    {
        $this->_addMessage($message, self::MESSAGE_INFO);
        return $this;
    }

    /**
     *
     * @param string $message
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function success($message)
    {
        $this->_addMessage($message, self::MESSAGE_SUCCESS);
        return $this;
    }

    /**
     *
     * @param string $message
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function warning($message)
    {
        $this->_addMessage($message, self::MESSAGE_WARNING);
        return $this;
    }

    /**
     *
     * @param string $message
     * @return wrxNext_View_Helper_FlashMessage
     */
    public function error($message)
    {
        $this->_addMessage($message, self::MESSAGE_ERROR);
        return $this;
    }
}
