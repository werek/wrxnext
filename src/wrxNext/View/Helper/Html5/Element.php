<?php

class wrxNext_View_Helper_Html5_Element
{
    use wrxNext_Trait_Object_Options;

    /**
     * holds tag name
     *
     * @var string
     */
    protected $_type = null;

    /**
     * holds flag if given element contains other elements
     *
     * @var boolean
     */
    protected $_open = false;

    /**
     * holds additional tag parameters to set
     *
     * @var array
     */
    protected $_options = array();

    /**
     * holds children if any
     *
     * @var wrxNext_View_Helper_Html5_Element[]
     */
    protected $_childrens = array();

    /**
     * constructor
     *
     * @param string $element
     * @param array $options
     */
    public function __construct($element, array $options = null)
    {
        $this->_type = $element;
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @return Object
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                $this->set($key, $value);
            }
        }
        return $this;
    }

    /**
     * sets tag option
     *
     * @param string $optionName
     * @param mixed $value
     * @throws wrxNext_Exception
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function set($optionName, $value)
    {
        if ($optionName == 'element') {
            throw new wrxNext_Exception(
                'setting element type after construction is forbidden');
        } else {
            $this->_options[$optionName] = $value;
        }
        return $this;
    }

    /**
     * sets elements
     *
     * @param array $list
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function setElements($list)
    {
        $list = (array)$list;
        $this->clearElements()->addElements($list);
        return $this;
    }

    /**
     * adds given elements
     *
     * @param array $list
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function addElements($list)
    {
        foreach ($list as $element) {
            $this->addElement($element);
        }
        return $this;
    }

    /**
     * adds single element to children
     *
     * in case $element is provided with array or element instance, then
     * $options are discarded
     *
     * @param string|array|wrxNext_View_Helper_Html5_Element $element
     * @param array $options
     * @throws wrxNext_Exception
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function addElement($element, array $options = null)
    {
        $tag = null;
        if ($element instanceof wrxNext_View_Helper_Html5_Element) {
            $tag = $element;
        } elseif (is_array($element)) {
            if (!array_key_exists('element', $element)) {
                throw new wrxNext_Exception(
                    'Array description of element should containt "type" key');
            }
            $options = $element;
            $element = $element['element'];
            unset($options['element']);
            $tag = new self($element, $options);
        } else {
            $tag = new self((string)$element, $options);
        }
        $this->_childrens[] = $tag;
        $this->_open = true;
        return $this;
    }

    /**
     * clears all elements
     *
     * @return wrxNext_View_Helper_Html5_Element
     */
    public function clearElements()
    {
        $this->_childrens = array();
        $this->_open = false;
        return $this;
    }

    /**
     * gets elements
     *
     * @return array
     */
    public function getElements()
    {
        return $this->_childrens;
    }

    /**
     * magic method
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * magic method
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->set($name, $value);
    }

    /**
     * gets tag param
     *
     * @param string $optionName
     * @return mixed
     */
    public function get($optionName)
    {
        if ($optionName == 'element') {
            return $this->_type;
        } else {
            return $this->_options[$optionName];
        }
    }

    /**
     * magic method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders html tag
     *
     * @return string
     */
    public function render()
    {
        $html = '<' . $this->_type;
        if (count($this->_options)) {
            foreach ($this->_options as $key => $value) {
                if (!empty($value)) {
                    $html .= ' ' . $key . '="' . htmlentities((string)$value) .
                        '"';
                }
            }
        }
        if ($this->_open) {
            $html .= '>';
            foreach ($this->_childrens as $element) {
                $html .= $element->render();
            }
            $html .= '</' . $this->_type . '>';
        } else {
            $html .= ' />';
        }
        return $html;
    }
}
