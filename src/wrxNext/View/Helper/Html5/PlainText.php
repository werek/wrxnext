<?php

class wrxNext_View_Helper_Html5_PlainText extends wrxNext_View_Helper_Html5_Element
{

    /**
     * holds text
     *
     * @var string
     */
    protected $_text = '';

    /**
     * constructor
     *
     * @param string $text
     */
    public function __construct($text = null)
    {
        $this->setText($text);
    }

    /**
     * gets text
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * sets text
     *
     * @param string $text
     * @return wrxNext_View_Helper_Html5_PlainText
     */
    public function setText($text)
    {
        $this->_text = $text;
        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_View_Helper_Html5_Element::render()
     */
    public function render()
    {
        return (string)$this->_text;
    }
}
