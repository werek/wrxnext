<?php

class wrxNext_View_Helper_CleanUrl extends Zend_View_Helper_Abstract
{
    /**
     * converts value to clean version
     *
     * @param string $value
     * @return string
     */
    public function cleanUrl($value)
    {
        $filter = new wrxNext_Filter_UrlName();
        return $filter->filter($value);
    }
}
