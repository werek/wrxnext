<?php

class wrxNext_View_Helper_StripComment extends Zend_View_Helper_Abstract
{

    /**
     * Purpose: Truncate a string to a certain length if necessary,
     * optionally splitting in the middle of a word, and
     * appending the $etc string or inserting $etc into the middle.
     *
     * @param
     *            string
     * @param
     *            integer
     * @param
     *            string
     * @param
     *            boolean
     * @param
     *            boolean
     * @return string
     */
    public function stripComment($string)
    {
        return preg_replace('@<!--.*?-->@is', '', $string);
    }
}
