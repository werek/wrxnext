<?php

class wrxNext_View_Helper_ReadableBytes extends Zend_View_Helper_Abstract
{

    /**
     * suffix for given number
     *
     * @var array
     */
    protected $_suffix = array(
        'B',
        'kB',
        'MB',
        'GB',
        'TB'
    );

    /**
     * converts number of bytes to short name
     *
     * @param int $bytes
     * @return string
     */
    public function readableBytes($bytes)
    {
        $i = 0;
        $size = intval($bytes);
        while ($size >= 1024) {
            $size = round($size / 1024, 1);
            $i++;
        }
        return $size . ' ' . $this->_suffix[$i];
    }
}
