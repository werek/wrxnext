<?php

class wrxNext_View_Helper_FormJqueryUIAutocomplete extends Zend_View_Helper_FormElement
{

    /**
     * holds js template for jquery UI autocomplete feature
     *
     * @var string
     */
    protected $_jsTemplate = <<<JSTEMPLATE
$(function(){
    $('input#%fieldIdentifier%').data('currentname',$('input#%fieldIdentifier%').val());
    $('input#%fieldIdentifier%').on('keyup',function(){
        var input = $(this);
        if (input.data('currentname')!= input.val()){
            $(this).addClass('not-set');
        } else {
            $(this).removeClass('not-set');
        }
    });
	$('input#%fieldIdentifier%').autocomplete({
		source: function(request,response){
			$.ajax({
				url:%url%,
				dataType:"json",
				data:{%queryFieldName%:request.term },
				success: function(data){
					response($.map(data.items, function(item){
						return {
							label: item[%nameField%],
							value: item[%nameField%],
                            id: item[%keyField%]
						};
					}));
				}
			});},
		minLength: 2,
		select: function(event, ui){
            var hidden=$('input#%hidden%');
            hidden.val(ui.item.id);
            $(this).removeClass('not-set');
            $(this).data('currentname',ui.item.value)
		}
	});
    
});
JSTEMPLATE;

    public function formJqueryUIAutocomplete($name, $value = null,
                                             array $attribs = null, $options = null)
    {
        $info = $this->_getInfo($name, $value, $attribs, $options);
        $inputHiddenAttribs = array();
        if (isset($info['id'])) {
            if (isset($info['attribs']) && is_array($info['attribs'])) {
                foreach ($info['attribs'] as $key => $attrib) {
                    if (!in_array($key,
                        array(
                            'routeParams',
                            'routeName',
                            'url',
                            'routeAutocompleteField',
                            'responseKeyField',
                            'responseNameField',
                            'displayCallback'
                        ))
                    ) {
                        $inputHiddenAttribs[$key] = $attrib;
                    }
                }
                $inputHiddenAttribs['id'] = $info['id'];
            } else {
                $inputHiddenAttribs = array(
                    'id' => $info['id']
                );
            }
        }
        // creating input hidden tag
        $inputHidden = $this->_hidden($info['name'], $info['value'],
            $inputHiddenAttribs);
        //
        $inputId = uniqid('autocomplete');
        $inputValue = null;

        if (is_callable($attribs['displayCallback'])) {
            $inputValue = call_user_func_array($attribs['displayCallback'],
                array(
                    $value
                ));
        }

        // creating input text tag
        $inputText = '<input type="text"' . ' id="' .
            $this->view->escape($inputId) . '"' . ' value="' .
            $this->view->escape($inputValue) . '"' . ' data-for="' .
            $this->view->escape($inputHiddenAttribs['id']) . '"' .
            $this->getClosingBracket();
        // creating init script
        $this->_createInitScript($inputId, $attribs);
        // sending html to
        return $inputHidden . $inputText;
    }

    protected function _createInitScript($inputId, $attribs)
    {
        if (isset($attribs['url']) && !empty($attribs['url'])) {
            $url = $attribs['url'];
        } else {
            $url = $this->view->url((array)$attribs['routeParams'],
                (string)$attribs['routeName'], true);
        }
        $template = new wrxNext_StringBuilder_Template($this->_jsTemplate);
        $this->view->headScript()->appendScript(
            $template->render(
                array(
                    'queryFieldName' => Zend_Json::encode(
                        $attribs['routeAutocompleteField']),
                    'url' => Zend_Json::encode($url),
                    'fieldIdentifier' => $inputId,
                    'nameField' => Zend_Json::encode(
                        $attribs['responseNameField']),
                    'keyField' => Zend_Json::encode(
                        $attribs['responseKeyField']),
                    'hidden' => $attribs['id']
                )));
    }
}
