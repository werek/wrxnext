<?php

class wrxNext_View_Helper_Head extends Zend_View_Helper_Placeholder_Container_Standalone
{

    protected $_regKey = 'wrxNext_View_Helper_Head';

    protected $_autoEscape = false;

    /**
     * allows to directly add content with current capture type
     *
     * @param string $content
     * @param null|string $setType
     * @return wrxNext_View_Helper_Head
     */
    public function head($content = null, $setType = null)
    {
        $content = (string)$content;
        if ($content !== '') {
            if ($setType == Zend_View_Helper_Placeholder_Container_Abstract::SET) {
                $this->set($content);
            } elseif ($setType == Zend_View_Helper_Placeholder_Container_Abstract::PREPEND) {
                $this->prepend($content);
            } else {
                $this->append($content);
            }
        }
        return $this;
    }
}
