<?php

class wrxNext_View_Helper_FormSwfupload extends Zend_View_Helper_FormButton
{

    /**
     * przechowuje obiekt ktory jest configiem dla kontrolki swfUpload
     *
     * @var wrxNext_SwfUpload
     */
    protected $_swfupload;

    protected $_cdn = array(
        'script_url' => 'http://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/swfupload.js',
        'script_url_swfobject' => 'http://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/plugins/swfupload.swfobject.js',
        'script_url_speed' => 'http://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/plugins/swfupload.speed.js',
        'script_url_queue' => 'http://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/plugins/swfupload.queue.js',
        'flash_url' => 'http://swfupload.googlecode.com/svn/swfupload/tags/swfupload_v2.2.0_core/Flash/swfupload.swf'
    );

    /**
     * id formularza (tworzone dynamicznie)
     *
     * @var string
     */
    protected $_formID = null;

    /**
     * przechowuje id przycisku
     *
     * @var string
     */
    protected $_buttonID = null;

    /**
     * przechowuje atrybuty
     *
     * @var array
     */
    protected $_attribs = null;

    public function formSwfupload($name, $value = null, $attribs = null)
    {
        $this->_buttonID = $name;
        $this->_formID = $name . "_form";

        foreach ($attribs as $key => $value) {
            switch ($key) {
                case 'swfupload':
                    if ($value instanceof wrxNext_SwfUpload) {
                        $this->_swfupload = $value;
                    }
                    unset($attribs[$key]);
                    break;
                case 'onclick':
                    unset($attribs['key']);
                    break;
            }
        }

        // zapisanie przetworzonych wstepnie atrybutow dla pozniejszego
        // wykorzystania
        $this->_attribs = $attribs;

        // budujemy button do inicjalizacji
        $returnCode[] = $this->formButton($name, $value,
            array(
                'content' => $attribs['content']
            ));

        // budujemy kod formularza
        $returnCode[] = $this->buildUploadForm();

        return implode("\n", $returnCode);
    }

    /**
     * tworzy kod formy i instancji swfupload
     *
     * @return string
     */
    protected function buildUploadForm()
    {
        // ustalenie placeholder'a
        $this->_swfupload->instanceName = $this->_formID . '_placeholder';

        // obsluga ew. include'a skryptow Javascript
        if (isset($this->_attribs['script_url']) &&
            !empty($this->_attribs['script_url'])
        ) {
            if (is_array($this->_attribs['script_url'])) {
                foreach ($this->_attribs['script_url'] as $value) {
                    $this->view->headScript()->appendFile($value);
                }
            } else {
                $this->view->headScript()->appendFile(
                    $this->_attribs['script_url']);
            }
        } elseif ($this->_attribs['useDefaultRepository']) {
            $this->view->headScript()->appendFile($this->_cdn['script_url']);
            $this->view->headScript()->appendFile(
                $this->_cdn['script_url_swfobject']);
            $this->view->headScript()->appendFile(
                $this->_cdn['script_url_speed']);
            $this->view->headScript()->appendFile(
                $this->_cdn['script_url_queue']);
        }

        // parametry http
        $sHttp = new wrxNext_SwfUpload_Config_Http();
        if (isset($this->_attribs['upload_url']) &&
            !empty($this->_attribs['upload_url'])
        ) {
            if (is_array($this->_attribs['upload_url'])) {
                $sHttp->setUploadUrl(
                    Zend_Controller_Front::getInstance()->getRouter()
                        ->assemble($this->_attribs['upload_url'], 'default',
                            true));
            } else {
                $sHttp->setUploadUrl($this->_attribs['upload_url']);
            }
        }
        if (isset($this->_attribs['flash_url']) &&
            !empty($this->_attribs['flash_url'])
        ) {
            $sHttp->setFlashUrl($this->_attribs['flash_url']);
        } elseif ($this->_attribs['useDefaultRepository']) {
            $sHttp->setFlashUrl($this->_cdn['flash_url']);
        }
        $sHttp->setPostName($this->_buttonID);
        $sHttp->setFileUploadName($this->_buttonID);

        // dodanie parametrow do obiektu swfupload
        $this->_swfupload->mergeConfig($sHttp);

        // obsluga dodatkowych pol $_POST
        $session[session_name()] = session_id();
        if (!is_array($this->_attribs['post_params'])) {
            $this->_attribs['post_params'] = array();
        }
        $this->_attribs['post_params'] = array_merge_recursive(
            $this->_attribs['post_params'], $session);
        if (isset($this->_attribs['post_params']) &&
            !empty($this->_attribs['post_params'])
        ) {
            $sPostParams = new wrxNext_SwfUpload_Config_PostParams();
            $sPostParams->addPostParams($this->_attribs['post_params']);
            $this->_swfupload->mergeConfig($sPostParams);
        }

        // obsluga handlerow eventow swfupload
        $handlers = new wrxNext_SwfUpload_Config_Handlers();
        $handlers->setUseDefaultHandlers(false);
        $handlers->setFileQueuedHandler(new wrxNext_SwfUpload_Js('fileQueued'));
        $handlers->setUploadStartHandler(
            new wrxNext_SwfUpload_Js('uploadStart'));
        $handlers->setUploadProgressHandler(
            new wrxNext_SwfUpload_Js('uploadProgress'));
        $handlers->setUploadCompleteHandler(
            new wrxNext_SwfUpload_Js('uploadComplete'));
        $handlers->setUploadSuccessHandler(
            new wrxNext_SwfUpload_Js('uploadSuccess'));
        $handlers->setFileDialogCompleteHandler(
            new wrxNext_SwfUpload_Js('fileDialogComplete'));
        $handlers->setUploadErrorHandler(
            new wrxNext_SwfUpload_Js('uploadError'));
        $this->_swfupload->mergeConfig($handlers);

        // dodatkowa konfiguracja
        $sPlain = new wrxNext_SwfUpload_Config_Plain();
        $sPlain->button_width = 120;
        $sPlain->button_height = 30;
        $sPlain->button_window_mode = new wrxNext_SwfUpload_Js(
            "SWFUpload.WINDOW_MODE.TRANSPARENT");
        $sPlain->button_cursor = new wrxNext_SwfUpload_Js(
            "SWFUpload.CURSOR.HAND");
        // $sPlain->debug=true;
        $customSettings['cancelButtonId'] = $this->_formID . "_cancelupload";
        $sPlain->custom_settings = $customSettings;

        // merge ostatnich ustawien do swfupload
        $this->_swfupload->mergeConfig($sPlain);

        // include plikow form i plikow akcji javascript
        $addonsDir = realpath(
            dirname(__FILE__) . DIRECTORY_SEPARATOR . 'FormSwfupload');
        $script = file_get_contents($addonsDir . DIRECTORY_SEPARATOR . 'form.js');
        $form = file_get_contents($addonsDir . DIRECTORY_SEPARATOR . 'form.html');

        // przemapowanie nazw glownych elementow form i buttona
        $script = str_replace('<{$form_id}>', $this->_formID, $script);
        $script = str_replace('<{$button_id}>', $this->_buttonID, $script);
        $form = str_replace('<{$form_id}>', $this->_formID, $form);
        $form = str_replace('<{$button_id}>', $this->_buttonID, $form);
        $script = sprintf("<script type=\"text/javascript\">\n%s\n</script>",
            $script);
        $swfUpload = sprintf("<script type=\"text/javascript\">\n%s\n</script>",
            wrxNext_SwfUpload_Js::indentCode($this->_swfupload->getCode()));

        // dodanie formy i skryptu do zwracanego kodu strony
        $code = $form . $script . $swfUpload;

        return $code;
    }
}
