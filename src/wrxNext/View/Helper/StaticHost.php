<?php

class wrxNext_View_Helper_StaticHost extends Zend_View_Helper_Abstract
{

    /**
     * flag for override behavior to baseUrl
     *
     * @var bool
     */
    protected static $_useBaseUrl = false;

    /**
     * holds prepared prefix address
     *
     * @var string
     */
    protected static $_address = null;

    /**
     * sets whete to use baseUrl helper or not
     *
     * @param boolean $flag
     */
    public static function useBaseUrl($flag = false)
    {
        self::$_useBaseUrl = $flag;
    }

    /**
     * sets base address
     *
     * @param string $uri
     */
    public static function address($uri)
    {
        self::$_address = (string)$uri;
    }

    /**
     * creates link for external elements
     *
     * @param string $relativeUrl
     * @return string
     */
    public function staticHost($relativeUrl = null)
    {
        if (!self::$_useBaseUrl) {
            if (preg_match('#^(http|https)://.*$#i', $relativeUrl)) {
                return $relativeUrl;
            }

            $relativeUrl = self::$_address . trim($relativeUrl, '/');

            return $relativeUrl;
        } else {
            return $this->view->baseUrl($relativeUrl);
        }
    }
}
