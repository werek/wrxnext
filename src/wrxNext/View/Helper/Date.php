<?php

class wrxNext_View_Helper_Date extends Zend_View_Helper_Abstract
{

    const MYSQL_DATE = 'YYYY-MM-dd';

    const MYSQL_DATETIME = 'YYYY-MM-dd HH:mm:ss';

    const HUMAN_DAY_MONTH = 'd MMMM';

    /**
     * Zend_Date instance
     *
     * @var Zend_Date
     */
    protected $_date = null;

    /**
     * constructor
     */
    public function __construct()
    {
        $this->reset();
    }

    /**
     * resets internal date object
     *
     * @return wrxNext_View_Helper_Date
     */
    public function reset()
    {
        $this->_date = new Zend_Date();
        return $this;
    }

    /**
     *
     * @param mixed $timestamp
     * @return wrxNext_View_Helper_Date
     */
    public function Date($timestamp = null)
    {
        if (!is_null($timestamp)) {
            if (!is_int($timestamp)) {
                $timestamp = strtotime($timestamp);
            }
            $this->_date->set($timestamp, Zend_Date::TIMESTAMP);
        }
        return $this;
    }

    /**
     * return date in <day> <month_literal>
     *
     * @return string
     */
    public function dayAndFullMonth()
    {
        return $this->_date->toString(self::HUMAN_DAY_MONTH);
    }

    /**
     * returns date in form of database format YYYY-MM-DD
     *
     * @return string
     */
    public function getDbDate()
    {
        return $this->_date->toString(self::MYSQL_DATE);
    }

    /**
     * returns date in form of database format YYYY-MM-DD HH:MM:SS
     *
     * @return string
     */
    public function getDbDateTime()
    {
        return $this->_date->toString(self::MYSQL_DATETIME);
    }

    /**
     * returns true if currently set date is in past
     *
     * @return boolean
     */
    public function isPast()
    {
        return $this->_date->getTimestamp() < time();
    }

    /**
     * checks if current date is beetwen given mysql datetime dates,
     * both dates are optional
     *
     * @param string $from
     * @param string $to
     * @return boolean
     */
    public function isBeetwen($from = null, $to = null)
    {
        $return = true;
        if (!empty($from) && $this->_date->getTimestamp() <= strtotime($from)) {
            $return = false;
        }
        if ($return) {
            if (!empty($to) && $this->_date->getTimestamp() >= strtotime($to)) {
                $return = false;
            }
        }
        return $return;
    }

    /**
     * trap for methods not implemented in current class
     *
     * @param string $method
     * @param mixed $options
     * @return mixed
     */
    public function __call($method, $options)
    {
        if (method_exists($this->_date, $method)) {
            return call_user_func_array(
                array(
                    $this->_date,
                    $method
                ), $options);
        }
    }
}
