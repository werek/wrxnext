<?php

class wrxNext_View_Helper_FlashMessage_BootstrapRender implements wrxNext_View_Helper_FlashMessage_RenderInterface
{
    /**
     * hold class map for flashmessage info levels
     *
     * @var array
     */
    protected $classMap = array(
        wrxNext_View_Helper_FlashMessage::MESSAGE_ERROR   => 'alert alert-danger',
        wrxNext_View_Helper_FlashMessage::MESSAGE_WARNING => 'alert alert-warning',
        wrxNext_View_Helper_FlashMessage::MESSAGE_INFO    => 'alert alert-info',
        wrxNext_View_Helper_FlashMessage::MESSAGE_SUCCESS => 'alert alert-success',
    );

    /**
     * @param array $messages
     * @return string
     */
    public function render(array $messages)
    {
        $messages = array_map(
            function ($message) {
                return '<div class="' . $this->classMap[ $message['type'] ] . '">' . $message['message'] . '</div>';
            },
            $messages
        );
        return implode("\n", $messages);
    }

}
