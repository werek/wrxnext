<?php

interface wrxNext_View_Helper_FlashMessage_RenderInterface
{
    /**
     * @param array $messages
     * @return string
     */
    public function render(array $messages);
}
