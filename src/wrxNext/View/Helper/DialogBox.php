<?php

class wrxNext_View_Helper_DialogBox
{
    use wrxNext_Trait_Template, wrxNext_Trait_RegistryTranslate;

    /**
     * holds template for dialogbox display command
     *
     * @var string
     */
    protected $_dialogTemplate = '$("<div>%content%</div>").dialog({modal:true,width:"auto",draggable:false,resizable:false,buttons:{%buttons%}});';

    /**
     * template for dialogbox button
     *
     * @var string
     */
    protected $_buttonTemplate = '%name%:{click:%action%,text:%label%}';

    /**
     * holds prefix
     *
     * @var string
     */
    protected $_prefix = "<script type=\"text/javascript\">$(function(){\n";

    /**
     * holds postfix
     *
     * @var string
     */
    protected $_postfix = "\n});</script>";

    /**
     * adds dialogbox content to display
     *
     * @param null|string|wrxNext_Session_Message_MessageInterface $message
     * @return wrxNext_View_Helper_DialogBox
     */
    public function dialogBox($message = null)
    {
        if (!is_null($message)) {
            if (!$message instanceof wrxNext_Session_Message_MessageInterface) {
                $message = new wrxNext_Session_Message_DialogBox($message);
                $message->addButton(
                    new wrxNext_Session_Message_DialogBox_ButtonClose('close'));
            }
            $this->append($message);
        }
        return $this;
    }

    /**
     * appends message to storage
     *
     * @param wrxNext_Session_Message_MessageInterface $message
     * @return qr_View_Helper_DialogBox
     */
    public function append(wrxNext_Session_Message_MessageInterface $message)
    {
        $this->_getArray()->messages[] = $message;
        return $this;
    }

    /**
     * returns store
     *
     * @return ArrayObject
     */
    protected function _getArray()
    {
        if (!Zend_Registry::isRegistered(__CLASS__)) {
            Zend_Registry::set(__CLASS__,
                new ArrayObject(array(), ArrayObject::STD_PROP_LIST));
            Zend_Registry::get(__CLASS__)->messages = array();
        }
        return Zend_Registry::get(__CLASS__);
    }

    /**
     * magic method for string conversion
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders whole dialogbox init
     *
     * @return string
     */
    public function render()
    {
        $dialogBoxes = array();
        $return = '';
        foreach ($this->_getArray()->messages as $message) {
            $dialogBoxes[] = $this->renderDialogBoxInit($message);
        }
        if (count($dialogBoxes)) {
            $return = sprintf('%s%s%s', $this->_prefix,
                implode("\n", $dialogBoxes), $this->_postfix);
        }
        return $return;
    }

    /**
     * renders message
     *
     * @param wrxNext_Session_Message_MessageInterface $message
     * @return string
     */
    protected function renderDialogBoxInit(
        wrxNext_Session_Message_MessageInterface $message)
    {
        $data['content'] = $this->_escapeJs(
            $this->_translate($message->getValue()));
        if ($message instanceof wrxNext_Session_Message_DialogBox) {
            $data['buttons'] = $this->renderDialogBoxButtons($message);
        }
        return $this->_buildTemplate($this->_dialogTemplate, $data);
    }

    /**
     * escapes string
     *
     * @param string $string
     * @return string
     */
    protected function _escapeJs($string)
    {
        return trim(Zend_Json::encode((string)$string), '"');
    }

    /**
     * render buttons from DialogBox message
     *
     * @param wrxNext_Session_Message_DialogBox $message
     * @return string
     */
    protected function renderDialogBoxButtons(
        wrxNext_Session_Message_DialogBox $message)
    {
        $buttons = array();
        foreach ($message->getButtons() as $button) {
            $buttons[] = $this->renderDialogBoxButton($button);
        }
        return implode(',', $buttons);
    }

    /**
     * render single instance of button
     *
     * @param wrxNext_Session_Message_DialogBox_ButtonInterface $button
     * @return string
     */
    protected function renderDialogBoxButton(
        wrxNext_Session_Message_DialogBox_ButtonInterface $button)
    {
        $data['name'] = uniqid('dialogbox') . mt_rand(0, PHP_INT_MAX);
        $data['action'] = $button->getAction();
        $data['label'] = Zend_Json::encode(
            $this->_translate($button->getLabel()));
        return $this->_buildTemplate($this->_buttonTemplate, $data);
    }

    /**
     * import messages from session message
     *
     * @param wrxNext_Session_Message $sessionMessage
     * @return qr_View_Helper_DialogBox
     */
    public function session(wrxNext_Session_Message $sessionMessage)
    {
        foreach ($sessionMessage->getMessages() as $message) {
            $this->append($message);
        }
        return $this;
    }
}
