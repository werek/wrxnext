<?php

class wrxNext_View_Helper_Template extends Zend_View_Helper_Abstract
{
    use wrxNext_Trait_Template;

    /**
     * converts string template vars inf form %var% to values from array
     * existing under 'var' key
     *
     * @param string $template
     * @param array $vars
     * @return string
     */
    public function template($template, $vars = array())
    {
        $vars = (array)$vars;
        return $this->_buildTemplate($template, $vars);
    }
}
