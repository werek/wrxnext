/**
 *
 */
$(function () {
    var multiselect = $('#%id%');
    var parent = multiselect.parent();
    var parentWidth = parent.innerWidth();
    var parentHeight = parent.innerHeight();
    var availableSelect = multiselect.clone();
    var filterSelect = multiselect.clone();
    multiselect.hide();
    var btnSelect = $('<input type="button" value="&gt;" />');
    var btnDeselect = $('<input type="button" value="&lt;" />');
    var ops = $('<div></div>');
    ops.append(btnSelect);
    ops.append(btnDeselect);
    filterSelect.removeAttr('id');
    filterSelect.removeAttr('name');
    filterSelect.find('option').remove();
    availableSelect.removeAttr('id');
    availableSelect.removeAttr('name');
    availableSelect.find('option').remove();
    parent.append(availableSelect);
    parent.append(ops);
    ops.width(btnSelect.outerWidth());
    var selectWidth = parentWidth / 2 - ops.width() - 20;
    parent.append(filterSelect);
    parent.append($('<div style="clear:left;"></div>'));
    availableSelect.width(selectWidth);
    filterSelect.width(selectWidth);
    parent.css({
        'postion': 'relative'
    });
    availableSelect.css({
        float: 'left'
    });
    filterSelect.css({
        float: 'left'
    });
    ops.css({
        float: 'left'
    });
    var sortOptions = function (select) {
        var compare = function (a, b) {
            var at = $(a).text().toLowerCase(), bt = $(b).text().toLowerCase();
            return (at > bt) ? 1 : ((at < bt) ? -1 : 0);
        };
        var opt = $(select).find('option');
        opt.sort(compare);
        $(select).html('').append(opt);
    };
    var propagateSelected = function () {
        var values = [];
        filterSelect.find('option').each(function (index, opt) {
            values.push($(opt).val());
        });
        multiselect.val(values);
    };
    // populating selected options
    availableSelect.append(multiselect.find('option').clone());
    filterSelect.append(availableSelect.find('option:selected')
        .removeAttr('selected'));
    sortOptions(availableSelect);
    sortOptions(filterSelect);
    btnSelect.on('click', function (e) {
        e.preventDefault();
        var move = availableSelect.find('option:selected');
        filterSelect.append(move.removeAttr('selected'));
        sortOptions(filterSelect);
        propagateSelected();
    });
    btnDeselect.on('click', function (e) {
        e.preventDefault();
        var move = filterSelect.find('option:selected');
        availableSelect.append(move.removeAttr('selected'));
        sortOptions(availableSelect);
        propagateSelected();
    });

});
