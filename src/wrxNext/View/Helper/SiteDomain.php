<?php

class wrxNext_View_Helper_SiteDomain extends Zend_View_Helper_Abstract
{
    /**
     * holds set domain
     *
     * @var string
     */
    protected static $_domain = null;

    /**
     * prefixes given string with domain from configuration
     *
     * @param string $url
     * @return string
     */
    public function siteDomain($url = null)
    {
        // Remove trailing slashes
        if (null !== $url) {
            $url = ltrim($url, '/\\');
        }

        return rtrim(self::getDomain(), '/') . '/' . $url;
    }

    /**
     * returns currently set domain
     *
     * @return string
     */
    public static function getDomain()
    {
        if (is_null(self::$_domain)) {
            self::$_domain = (new wrxNext_Url())->getHost();
        }
        return self::$_domain;
    }

    /**
     * sets domain
     *
     * @param string $domain
     */
    public static function setDomain($domain)
    {
        self::$_domain = $domain;
    }
}
