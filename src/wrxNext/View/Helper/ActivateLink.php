<?php

class wrxNext_View_Helper_ActivateLink extends Zend_View_Helper_Abstract
{

    /**
     * szerokosc odtwarzacza
     *
     * @var int
     */
    protected $_playerWidth = 640;

    /**
     * wysokosc odtwarzacza
     *
     * @var int
     */
    protected $_playerHeight = 385;

    /**
     * okresla czy do emebedowania flash'a uzywac swfobject
     *
     * @var bool
     */
    protected $_useSwfObject = false;

    /**
     * okresla czy parsowac istniejacy w tekscie bbcode
     *
     * @var bool
     */
    protected $_useBBcode = false;

    /**
     * okresla czy tworzyc odtwarzacze multimedialne dla plikow multimedialnych
     * (jezeli takie zostana zauwazone), zastepuje to tworzenie linkow
     *
     * @var bool
     */
    protected $_createMediaPlayers = false;

    /**
     * SEO, okresla czy dodawac do linkow atrybut nofollow
     * aby nie przenosic wartosci strony do zewnetrznych linkow
     *
     * @var bool
     */
    protected $_relNofollow = true;

    /**
     * okresla czy używać dekoracji przy generowaniu odtwarzaczy
     * m.in.
     * widoczny link do tresci
     *
     * @var bool
     */
    protected $_useDecorative = false;

    /**
     * okresla czy osadzac skrypt w miejscu linka czy tez za pomoca headScript
     * w nagłówku strony
     *
     * @var bool
     */
    protected $_useInlineScript = true;

    /**
     * okresla zwrot na generowanie standardowego linku jezeli filtr specjalny
     * nie rozpozna wewnetrznie
     * linku jako poprawny i nie zwroci zmienionej tresaci
     *
     * @var bool
     */
    protected $_fallbackToLink = false;

    protected $_parseSpecialELements = true;

    private $_matchSpecial = array(
        'youtube' => '@http://.*?youtube\.com/watch@i',
        'dailymotion_video' => '@http://.*?dailymotion\.[a-zA-Z]{2,4}/video/@i',
        'vimeo' => '@http://vimeo\.com/\d+?@i',
        'wrzuta_film' => '@wrzuta\.pl/film@i',
        'wrzuta_audio' => '@wrzuta\.pl/audio@i',
        'quicktime' => '@\.(mov|hdmov)@i'
    );

    private $_matchLink = '@(>| )([a-z]+?://.*?)( |<)@i';

    /**
     * głowny akcesor do pomocnika
     *
     * @param string|null $text
     * @param array $params
     * @return string wrxNext_View_Helper_ActivateLink
     */
    public function activateLink($text = null, $params = array())
    {
        if (is_null($text)) {
            return $this;
        } else {
            return $this->parse($text);
        }
    }

    /**
     * przetwarza tekst pod katem linkow ktore mozna zamienic na aktywna tresc
     *
     * @param string $text
     * @return string
     */
    public function parse($text)
    {
        preg_match_all($this->_matchLink, $text, $wyniki);
        $replacePair = array();
        for ($i = 0; $i < count($wyniki[2]); $i++) {
            $tmpLink = $this->checkSpecial($wyniki[2][$i]);
            if ($tmpLink != $wyniki[2][$i]) {
                $replacePair[$wyniki[2][$i]] = $tmpLink;
            } else {
                $tmpLink = $this->createLink($wyniki[2][$i]);
                if ($tmpLink != $wyniki[2][$i]) {
                    $replacePair[$wyniki[2][$i]] = $tmpLink;
                }
            }
        }
        $text = strtr($text, $replacePair);
        if ($this->_useBBcode) {
            $text = $this->parseBBcode($text);
        }
        return $text;
    }

    /**
     * rozwiazuje metode jak ma uzyc do parsowania stringa
     * i wykonuje ja na danym linku
     *
     * @param string $link
     * @return string
     */
    protected function checkSpecial($link)
    {
        $useLink = true;
        foreach ($this->_matchSpecial as $type => $pattern) {
            if (preg_match($pattern, $link)) {
                $funcName = 'create' . strtoupper($type{0});
                $funcName .= substr($type, 1);
                if (method_exists($this, $funcName)) {
                    $useLink = false;
                    $link = $this->{$funcName}($link);
                }
            }
        }
        if ($useLink && $this->_fallbackToLink) {
            $this->createLink($link);
        }
        return $link;
    }

    /**
     * zamienia adres na link
     *
     * @param string $link
     * @return string
     */
    protected function createLink($link)
    {
        if (preg_match('@[a-z]+?://[^\s]+?\.[a-z]{2,4}.*?@i', $link)) {
            $format = '<a href="%s"%s>%s</a>';
            $link = sprintf($format, $link,
                ($this->_relNofollow) ? ' rel="nofollow"' : '', $link);
        }
        return $link;
    }

    /**
     * parsuje kod BBCode
     *
     * @param string $text
     * @return string
     */
    protected function parseBBcode($text)
    {
        return $text;
    }

    /**
     * obsluga echo'wania helpera bez podania tekstu
     *
     * @return string
     */
    public function __toString()
    {
        return "Body text is null";
    }

    /**
     * ustala wymiary dla filmu
     *
     * @param int $width
     * @param int $height
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function playerDimensions($width, $height)
    {
        $this->_playerWidth = $width;
        $this->_playerHeight = $height;
        return $this;
    }

    /**
     * definiuje wykorzystanie skladni dla SWFObject
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function useSwfObject($status)
    {
        $this->_useSwfObject = $status;
        return $this;
    }

    /**
     * definiuje parsowanie tresci pod katem obecnosci BBCode
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function useBBCode($status)
    {
        $this->_useBBcode = $status;
        return $this;
    }

    /**
     * definiuje czy tworzyc odtwarzacze dla znalezionych linkow do mediow
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function createMediaPlayers($status)
    {
        $this->_createMediaPlayers = $status;
        return $this;
    }

    /**
     * Definiuje czy tworzyc dekoracyjny widok dla odtwarzaczy z dodatkowym
     * podpisem i linkiem
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function useDecorative($status)
    {
        $this->_useDecorative = $status;
        return $this;
    }

    /**
     * definuje czy dodatkowe skrypty dodawac w kodzie czy tez
     * wykorzystac plugin headScript()
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function useInlineScript($status)
    {
        $this->_useInlineScript = $status;
        return $this;
    }

    /**
     * definiuje czy w przypadku braku zmian (po przetworzeniu linkow
     * specjalnych)
     * powrocic i przetworzyc ponownie skrypt za pomoca zwyklego linku
     *
     * @param bool $status
     * @return wrxNext_View_Helper_ActivateLink
     */
    public function fallbackToLink($status)
    {
        $this->_fallbackToLink = $status;
        return $this;
    }

    /**
     * tworzy player dla video z dailymotion
     *
     * @param string $link
     * @return string
     */
    protected function createDailymotion_video($link)
    {
        preg_match('@//(.*?)/video/([a-zA-Z0-9]*?)_@i', $link, $wyniki);
        if (isset($wyniki[2]) && !empty($wyniki[2])) {
            $pureLink = $link;
            $movieID = $wyniki[2];
            $swf = 'http://' . $wyniki[1] . '/swf/video/' . $movieID;
            $link = $this->generateFlashPlayerCode($swf, $pureLink,
                'dailymotion_' . $movieID);
            if ($this->_useDecorative) {
                // $decoration="<div style=\"text-align:center;margin: 10px
                // 0px\">\n\n%s\n\n<div
                // style=\"font-weight:bold;\">\n%s\n</div>\n</div>\n\n";
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }

    /**
     * generuje kod wspolny dla wiekszosci odtwrzaczy sieciowych
     * (wykorzystywany m.in.
     * w youtube, vimeo)
     *
     * @param string $swf
     * @param string $pureLink
     * @param string $movieID
     * @return string
     */
    protected function generateFlashPlayerCode($swf, $pureLink, $movieID = null)
    {
        if ($this->_useSwfObject) {
            $player = '<div id="' . $movieID .
                '">You need Javascript support to view this movie, if you don\'t have it activated you can still view this movie on ' .
                $this->createLink($pureLink) . '</div>';
            $jscode = 'swfobject.embedSWF( "' . $swf . '", "' . $movieID . '", "' .
                $this->_playerWidth . '", "' . $this->_playerHeight .
                '", "9.0.0", "", {allowscriptaccess:"always",allowFullScreen:true}, {wmode : "transparent",allowscriptaccess:"always",allowFullScreen:true} );';
            if ($this->_useInlineScript) {
                $link = $player . "\n\n<script type='text/javascript'>" . $jscode .
                    '</script>' . "\n\n\n";
            } else {
                $link = $player;
                $this->view->headScript()->appendScript($jscode);
            }
        } else {
            $format = '<object width="%s" height="%s"><param name="movie" value="%s"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><embed src="%s" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="%s" height="%s"></embed></object>';
            $link = sprintf($format, $this->_playerWidth, $this->_playerHeight,
                $swf, $swf, $this->_playerWidth, $this->_playerHeight);
        }
        return $link;
    }

    /**
     * tworzy kod odtwarzacza dla podanego adrssu pliku mov
     *
     * @param string $link
     * @return string
     */
    protected function createQuicktime($link)
    {
        extract(parse_url($link));
        preg_match('@(.+?\.mov)@i', $path, $wyniki);
        if (isset($wyniki[1]) && !empty($wyniki[1]) &&
            $this->_createMediaPlayers
        ) {
            $link = $this->view->htmlQuicktime($link,
                array(
                    "width" => $this->_playerWidth,
                    "height" => $this->_playerHeight
                ), array(
                    'type' => 'video/quicktime'
                ));
            if ($this->_useDecorative) {
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }

    /**
     * zamienia czysty link do youtube na odtwarzacz
     *
     * @param string $link
     * @return string
     */
    protected function createYoutube($link)
    {
        extract(parse_url($link));
        preg_match('@v=([a-zA-Z0-9_\-]+)@i', $query, $wyniki);
        if (isset($wyniki[1]) && !empty($wyniki[1])) {
            $pureLink = $link;
            $movieID = $wyniki[1];
            $swf = 'http://www.youtube.com/v/' . $movieID;
            $link = $this->generateFlashPlayerCode($swf, $pureLink,
                'youtube_' . $movieID);
            if ($this->_useDecorative) {
                // $decoration="<div style=\"text-align:center;margin: 10px
                // 0px\">\n\n%s\n\n<div
                // style=\"font-weight:bold;\">\n%s\n</div>\n</div>\n\n";
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }

    /**
     * zamienia czytsy link do filmu na vimeo na odtwarzacz
     *
     * @param string $link
     * @return string
     */
    protected function createVimeo($link)
    {
        extract(parse_url($link));
        preg_match('@/(\d+?)$@i', $link, $wyniki);
        if (isset($wyniki[1]) && !empty($wyniki[1])) {
            $pureLink = $link;
            $movieID = $wyniki[1];
            $swf = 'http://vimeo.com/moogaloop.swf?clip_id=' . $movieID .
                '&amp;server=vimeo.com&amp;show_title=1&amp;show_byline=1&amp;show_portrait=0&amp;color=&amp;fullscreen=1';
            $link = $this->generateFlashPlayerCode($swf, $pureLink,
                'youtube_' . $movieID);
            if ($this->_useDecorative) {
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }

    /**
     * tworzy player'a video z linku do wrzuty
     *
     * @param string $link
     * @return string
     */
    protected function createWrzuta_film($link)
    {
        extract(parse_url($link));
        preg_match('@//([^\s]+?)\.wrzuta\.pl/film/([a-zA-Z0-9]+?)/@i', $link,
            $wyniki);
        if (isset($wyniki[2]) && !empty($wyniki[2])) {
            $pureLink = $link;
            $movieID = $wyniki[2];
            $login = $wyniki[1];
            $embed = '<script type="text/javascript" src="http://www.wrzuta.pl/embed_video.js?key=' .
                $movieID . '&login=' . $login . '&width=' .
                $this->_playerWidth . '&height=' . $this->_playerHeight .
                '&bg=ffffff"></script>';
            $link = $embed;
            if ($this->_useDecorative) {
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }

    /**
     * tworzy player'a audio z linku do wrzuty
     *
     * @param string $link
     * @return string
     */
    protected function createWrzuta_audio($link)
    {
        extract(parse_url($link));
        preg_match('@//([^\s]+?)\.wrzuta\.pl/audio/([a-zA-Z0-9]+?)/@i', $link,
            $wyniki);
        if (isset($wyniki[2]) && !empty($wyniki[2])) {
            $pureLink = $link;
            $movieID = $wyniki[2];
            $login = $wyniki[1];
            $embed = '<script type="text/javascript" src="http://www.wrzuta.pl/embed_audio.js?key=' .
                $movieID . '&login=' . $login . '&width=' .
                $this->_playerWidth . '&bg=ffffff"></script>';
            $link = $embed;
            if ($this->_useDecorative) {
                $decoration = "\n\n%s\n\n<p style=\"font-weight:bold;margin:10px 0px;text-align:center;\">\n%s\n</p>\n\n";
                $link = sprintf($decoration, $link,
                    $this->createLink($pureLink));
            }
        }
        return $link;
    }
}
