<?php

/**
 *
 * @author krzysiek
 *
 */
class wrxNext_View_Helper_FormCKEditor extends Zend_View_Helper_FormElement
{
    static protected $_fileManagerDialogActive = false; // we need filemanager dialog only once

    /**
     *
     * @param mixed $name
     * @param mixed $value
     * @param mixed $attribs
     * @param mixed $options
     */
    public function formCKEditor($name, $value = null, $attribs = null, $options = null)
    {
        $this->view->headScript()->appendFile($this->view->staticHost('js/ckeditor/ckeditor.js'));
        $info = $this->_getInfo($name, $value, $attribs, $options);

        $plugins = empty($info['options']['ckePlugins']) ? "extraPlugins: ''" : "extraPlugins: '" . $info['options']['ckePlugins'] . "'";
        $skin = "skin: '" . $info['options']['ckeSkin'] . "'";
        $toolbar = "toolbar: " . $info['options']['ckeToolbar'] . "";

        $textareaXhtml = $this->view->formTextarea($name, $value, $attribs);

        $script = "
$(function() {
	CKEDITOR.replace('" . $info['id'] . "', {
		" . implode(', ', array($plugins, $skin, $toolbar)) . "
	});
	
});
";
        $this->view->headScript()->appendScript($script);

        // append filemanager dialog when needed
        if (!empty($info['options']['ckePlugins']) && in_array('filemanager', explode(',', $info['options']['ckePlugins'])) && !wrxNext_View_Helper_FormCKEditor::$_fileManagerDialogActive) {
            wrxNext_View_Helper_FormCKEditor::$_fileManagerDialogActive = true;
            $textareaXhtml .= $this->view->partial('filemanager/dialog.phtml', array('class' => 'ckeditorFileManager'));
        }

        return $textareaXhtml;
    }
}
