<?php

class wrxNext_View_Helper_FormTinymce extends Zend_View_Helper_FormTextarea
{

    public function formTinymce($name, $value, $attribs)
    {
        if (!isset($attribs['class'])) {
            $attribs['class'] = $name . "_class";
        }

        // wlasciwe zbudowanie kodu Tinymce
        $config = array(
            'mode' => 'textareas',
            'theme' => 'advanced',
            'plugins' => "safari,spellchecker,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,preview,media,searchreplace,print,contextmenu,paste,visualchars",
            // 'theme_advanced_buttons1'=>"bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,undo,redo,|,link,unlink,anchor,image,code",
            'theme_advanced_buttons1' => "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
            'theme_advanced_buttons2' => "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
            'theme_advanced_buttons3' => "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
            'theme_advanced_buttons4' => "insertlayer,moveforward,movebackward,absolute,|,styleprops,spellchecker,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,blockquote,pagebreak,|,insertfile,insertimage",
            'theme_advanced_toolbar_location' => "top",
            'theme_advanced_toolbar_align' => "left",
            'theme_advanced_statusbar_location' => "bottom",
            'editor_selector' => $attribs['class']
        );

        if (isset($attribs['file_browser_url'])) {
            $fbscript = file_get_contents(
                dirname(__FILE__) . DIRECTORY_SEPARATOR . 'FormTinymce' .
                DIRECTORY_SEPARATOR . 'FileBrowserHelper.js');
            $fbscript = str_replace('___CTRL_NAME___', $name, $fbscript);
            $fbscript = str_replace('_____FILEBROWSER__URL__PREFIX____',
                $attribs['file_browser_url'], $fbscript);
            $this->view->headScript()->appendScript($fbscript);
            $config['file_browser_callback'] = new wrxNext_SwfUpload_Js(
                $name . 'myFileBrowser');
            unset($attribs['file_browser_url']);
        }
        $jsWrapper = new wrxNext_SwfUpload_Js_Array($config);
        $jsWrapper->setEncodeKeys(false);
        $script = sprintf('tinyMCE.init(%s);', $jsWrapper->getCode());
        $this->view->headScript()->appendScript($script);

        return $this->formTextarea($name, $value, $attribs);
    }
}
