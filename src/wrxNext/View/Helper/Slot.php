<?php

class wrxNext_View_Helper_Slot extends Zend_View_Helper_Abstract
{

    /**
     * holds slots data
     *
     * @var array
     */
    protected $_rowset = array();

    /**
     * main entry method, optionally takes array of slot data
     *
     * @param array|null $repository
     * @return wrxNext_View_Helper_Slot
     */
    public function slot($repository = null)
    {
        if (is_array($repository)) {
            $this->setRepository($repository);
        }
        return $this;
    }

    /**
     * parses rowset with slot data
     *
     * @param array $rowset
     * @return wrxNext_View_Helper_Slot
     */
    public function setRepository($rowset)
    {
        $this->reset();
        foreach ($rowset as $row) {
            $this->_rowset[$row['slot_order']] = $row;
        }
        return $this;
    }

    /**
     * resets slot data stack
     *
     * @return wrxNext_View_Helper_Slot
     */
    public function reset()
    {
        $this->_rowset = array();
        return $this;
    }

    /**
     * returns frontend class of given slot adapter, if slot is not found
     * then empty string is returned
     *
     * @param int $slotNumber
     * @return wrxNext_Slot_Frontend_Interface|string
     */
    public function getFrontend($slotNumber)
    {
        if (isset($this->_rowset[$slotNumber])) {
            $adapter = wrxNext_Slot::factory(
                $this->_rowset[$slotNumber]['slot_class'],
                $this->_rowset[$slotNumber]['slot_content'],
                $this->_rowset[$slotNumber]['slot_size']);
            $frontend = $adapter->getFrontend();
            $frontend->setView($this->view);
            return $frontend;
        } else {
            return '';
        }
    }
}
