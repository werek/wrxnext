<?php

/**
 * class generating link to cdn resources
 *
 * @author bweres01
 *
 */
class wrxNext_View_Helper_Cdn extends Zend_View_Helper_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds photo manipulation object
     *
     * @var wrxNext_Cdn_Photo
     */
    protected $_photo = null;

    /**
     * composes link, if no param provided also provides additional methods
     * for photo resize
     *
     * @param string $relativeUrl
     * @return string
     */
    public function cdn($relativeUrl = null)
    {
        if (!is_null($relativeUrl)) {
            return $this->link($relativeUrl);
        }
        return $this;
    }

    /**
     * composes file address for given cdn resource
     *
     * @param string|wrxNext_Cdn $relativeUrl
     * @return string
     */
    public function link($relativeUrl)
    {
        if ($relativeUrl instanceof wrxNext_Cdn) {
            $relativeUrl = $relativeUrl->getLocation();
        }
        if (preg_match('#^htt(p|ps)://.*$#', $relativeUrl)) {
            return $relativeUrl;
        }

        $relativeUrl = str_replace('\\', '/', $relativeUrl);
        $relativeUrl = preg_replace('@/+@', '/', $relativeUrl);
        $pathParts = explode('/', $relativeUrl);
        for ($i = 0; $i < count($pathParts); $i++) {
            $pathParts[$i] = rawurlencode($pathParts[$i]);
        }
        $relativeUrl = implode('/', $pathParts);
        if (defined('APPLICATION_CDN_URI')) {
            $relativeUrl = APPLICATION_CDN_URI . trim($relativeUrl, '/');
        }
        return $relativeUrl;
    }

    /**
     * helper for resizing cdn photos inside view, returns full url to cdn
     *
     * @param string|wrxNext_Cdn $photo
     * @param int $width
     * @param int $height
     * @param boolean $keepAspectRatio
     * @param boolean $crop
     * @throws wrxNext_Image_Exception
     * @return string
     */
    public function photo($photo, $width, $height, $keepAspectRatio = true,
                          $crop = true, $force = false)
    {
        if ($photo instanceof wrxNext_Cdn) {
            $photo = $photo->getLocation();
        }
        $newPhotoUrl = '';
        $photoMapper = $this->_photo();
        try {
            if (($newPhotoUrl = $photoMapper->thumbnailExists($photo, $width,
                    $height, $keepAspectRatio, $crop)) === false || $force
            ) {
                $newPhotoUrl = $photoMapper->resizeCdnPhoto($photo, $width,
                    $height, $keepAspectRatio, $crop);
            }
        } catch (wrxNext_Image_Exception $e) {
            switch ($e->getCode()) {
                case 1:
                case 2:
                case 101:
                case 102:
                    $newPhotoUrl = $photo;
                    $this->_log(
                        'problem while resizing photo in view: ' .
                        $e->getCode() . '-' . $e->getMessage() .
                        $e->getTraceAsString(), Zend_Log::WARN);
                    break;
                default:
                    throw $e;
                    break;
            }
        }
        return $this->link($newPhotoUrl);
    }

    /**
     * returns cdn photo instance
     *
     * @return wrxNext_Cdn_Photo
     */
    protected function _photo()
    {
        if (!$this->_photo instanceof wrxNext_Cdn_Photo) {
            $this->_photo = new wrxNext_Cdn_Photo();
        }
        return $this->_photo;
    }

    /**
     * provides failsafe behavior
     *
     * @return string
     */
    public function __toString()
    {
        return $this->link('');
    }
}
