<?php

/**
 * toolset class for setting up view extensions
 *
 * @author werek
 *
 */
class wrxNext_View_Helper
{

    /**
     * set ups paths for view helper to given Zend_View instance
     *
     * @param Zend_View $view
     */
    public static function setUpViewHelpers(Zend_View $view)
    {
        $path = realpath(__DIR__ . DIRECTORY_SEPARATOR . 'Helper');
        $view->addHelperPath($path, __CLASS__);
    }
}
