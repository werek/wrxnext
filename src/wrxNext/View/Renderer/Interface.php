<?php

/**
 * interface for renderers of html
 *
 * @author bweres01
 *
 */
interface wrxNext_View_Renderer_Interface
{

    /**
     * renders html
     *
     * @return string
     */
    public function render();
}
