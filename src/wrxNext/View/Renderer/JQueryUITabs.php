<?php

class wrxNext_View_Renderer_JQueryUITabs implements wrxNext_View_Renderer_Interface
{
    use wrxNext_Trait_Object_ViewSet;

    /**
     * holds object id
     *
     * @var string
     */
    protected $_id = 'tabs';

    /**
     * holds class for placeholder
     *
     * @var string
     */
    protected $_class = null;

    /**
     * wheter to create init script or not
     *
     * @var boolean
     */
    protected $_createInitScript = true;
    /**
     * holds tabs
     *
     * @var array
     */
    protected $_tabs = array();

    /**
     * constructor takes tabs id, and flag for generating init script
     *
     * @param string $id
     * @param bool $createInitScript
     */
    public function __construct($id = null, $createInitScript = true)
    {
        if (!is_null($id)) {
            $this->setId($id);
        }
        $this->setCreateInitScript($createInitScript);
    }

    /**
     * gets id for placeholder
     *
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * sets id for placeholder
     *
     * @param string $id
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * gets class for placeholder
     *
     * @return string
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * sets class for placeholder
     *
     * @param string $class
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function setClass($class)
    {
        $this->_class = $class;
        return $this;
    }

    /**
     * gets flag for creating init script
     *
     * @return bolean
     */
    public function getCreateInitScript()
    {
        return $this->_createInitScript;
    }

    /**
     * sets flag for creating init script
     *
     * @param bolean $bool
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function setCreateInitScript($bool)
    {
        $this->_createInitScript = $bool;
        return $this;
    }

    /**
     * adds tab to stack
     *
     * @param string $displayName
     * @param string $content
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function addTab($displayName, $content)
    {
        $this->_tabs[] = array(
            'name' => $displayName,
            'content' => $content
        );
        return $this;
    }

    /**
     * magic method to string
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * rendrs tabs html and adds init js to headScript helper
     *
     * @return string
     */
    public function render()
    {
        $idfilter = new wrxNext_Filter_UrlName();
        $titles = array();
        $tabs = array();
        $prefix = $idfilter->filter($this->_id);
        foreach ($this->_tabs as $key => $value) {
            $tabKey = $prefix . '-tab-' . $key;
            $titles[] = '<a href="#' . $tabKey . '">' .
                $this->_view->escape($value['name']) . '</a>';
            $tabs[] = '<div id="' . $tabKey . '" class="single-tab">' .
                $value['content'] . '</div>';
        }

        $titlesHtml = "<ul>\n<li>" . implode("</li>\n<li>", $titles) .
            "</li>\n</ul>";
        $tabsHtml = implode("\n", $tabs);
        $tabsPlaceholder = new wrxNext_View_Helper_Html5_Element('div',
            array(
                'id' => $this->_id,
                'class' => $this->_class,
                'elements' => array(
                    new wrxNext_View_Helper_Html5_PlainText(
                        $titlesHtml . "\n" . $tabsHtml)
                )
            ));
        if ($this->_createInitScript) {
            $initScript = '$(function(){$(\'#' . $prefix . '\').tabs();});';
            $this->_view->headScript()->appendScript($initScript);
        }
        return $tabsPlaceholder->render();
    }
}
