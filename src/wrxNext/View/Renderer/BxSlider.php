<?php

class wrxNext_View_Renderer_BxSlider implements wrxNext_View_Renderer_Interface
{
    use wrxNext_Trait_Object_ViewSet;

    /**
     * checks if capture has already started
     *
     * @var bool
     */
    protected static $_captureStarted = false;

    /**
     * holds object id
     *
     * @var string
     */
    protected $_id = null;

    /**
     * holds class for given slider
     *
     * @var string
     */
    protected $_class = 'auto-slider';

    /**
     * wheter to create init script or not
     *
     * @var boolean
     */
    protected $_createInitScript = true;

    /**
     * holds slides
     *
     * @var array
     */
    protected $_slides = array();

    /**
     * constructor takes tabs id, and flag for generating init script
     *
     * @param string $id
     * @param bool $createInitScript
     */
    public function __construct($id = null, $createInitScript = true)
    {
        if (!is_null($id)) {
            $this->setId($id);
        }
        $this->setCreateInitScript($createInitScript);
    }

    /**
     * gets id for placeholder
     *
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * sets id for placeholder
     *
     * @param string $id
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function setId($id)
    {
        $this->_id = $id;
        return $this;
    }

    /**
     * gets class to use
     *
     * @return string
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * sets class to use
     *
     * @param string $className
     * @return wrxNext_View_Renderer_BxSlider
     */
    public function setClass($className)
    {
        $this->_class = $className;
        return $this;
    }

    /**
     * gets flag for creating init script
     *
     * @return bolean
     */
    public function getCreateInitScript()
    {
        return $this->_createInitScript;
    }

    /**
     * sets flag for creating init script
     *
     * @param bolean $bool
     * @return wrxNext_View_Renderer_JQueryUITabs
     */
    public function setCreateInitScript($bool)
    {
        $this->_createInitScript = $bool;
        return $this;
    }

    /**
     * starts capturing output
     *
     * @throws BadMethodCallException
     * @return wrxNext_View_Renderer_BxSlider
     */
    public function captureStart()
    {
        if (self::$_captureStarted) {
            throw new BadMethodCallException(
                'output has been already started and did not finished');
        }
        ob_start();
        self::$_captureStarted = true;
        return $this;
    }

    /**
     * ends capturing output
     *
     * @throws BadMethodCallException
     * @return wrxNext_View_Renderer_BxSlider
     */
    public function captureEnd()
    {
        if (!self::$_captureStarted) {
            throw new BadMethodCallException('output capture hasn\'t started');
        }
        $this->addSlide(ob_get_clean());
        self::$_captureStarted = false;
        return $this;
    }

    /**
     * adds slide content
     *
     * @param string $content
     * @return wrxNext_View_Renderer_BxSlider
     */
    public function addSlide($content)
    {
        $this->_slides[] = $content;
        return $this;
    }

    public function __toString()
    {
        return $this->render();
    }

    /**
     * handles rendering of slides
     *
     * @return string
     */
    public function render()
    {
        if (count($this->_slides) < 1) {
            return '';
        } else {
            $uniqID = empty($this->_id) ? uniqid('bxslider') : $this->_id;
            $placeholder = (new wrxNext_View_Helper_Html5_Element('div'))->set('id',
                $uniqID);
            if (!empty($this->_class)) {
                $placeholder->set('class', $this->_class);
            }
            $list = new wrxNext_View_Helper_Html5_Element('ul');
            $placeholder->addElement($list);
            foreach ($this->_slides as $slide) {
                $item = (new wrxNext_View_Helper_Html5_Element('li'))->addElement(
                    new wrxNext_View_Helper_Html5_PlainText(trim($slide)));
                $list->addElement($item);
            }
            // for optional init script
            if ($this->_createInitScript &&
                $this->getView() instanceof Zend_View_Interface
            ) {
                $init = sprintf(
                    '$(function(){$(\'#%s ul\').bxSlider({pager:true});});',
                    $uniqID);
                $this->getView()
                    ->headScript()
                    ->appendScript($init);
            }
            return $placeholder->render();
        }
    }
}
