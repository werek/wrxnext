<?php

/**
 * klasa fabryka do tworzenia i zbierania informacji z wrapperow dostarczajacych informacje o danych
 *
 */
class wrxNext_Info
{

    /**
     * zwraca informacje po przetworzeniu przez obiekt informacyjny
     *
     * @param string $type
     * @param array $params
     * @return array
     */
    public static function getInfo($type, $params = array())
    {
        $obj = self::get($type, $params);
        return $obj->toArray();
    }

    /**
     * zwraca obiekta informacyjny
     *
     * @param string $type
     * @param array $params
     * @return wrxNext_Info_Interface
     */
    public static function get($type, $params = array())
    {
        $standardPrefix = 'wrxNext_Info_';
        $type = trim($type, '_');
        $class = $standardPrefix . $type;

        Zend_Loader::loadClass($standardPrefix . $type);

        $obj = new $class();
        $obj->setParams($params);
        return $obj;
    }
}
