<?php

/**
 * klasa pomagajaca w tworzeniu kontrolki uploadu wielu plikow
 * za pomoca biblioteki SWFupload
 *
 * @todo implementacja pelnej obslugi i renderowania kodu kontrolki
 *
 */
class wrxNext_SwfUpload
{

    /**
     * przechowuje nazwe instancji (id placeholder'a)
     *
     * @var string
     */
    public $instanceName = "";

    /**
     * przechwouje kod ktory ma byc dodany przed stworzeniem
     *
     * @var array
     */
    private $_prependCode = array();

    /**
     * konfiguracja
     *
     * @var Zend_Config
     */
    private $_config;

    /**
     * nazwa parametrow ktore maja byc traktowane jako JS
     *
     * @var array
     */
    private $_threatAsJavascript = array(
        'file_queue_error_handler',
        'file_dialog_complete_handler',
        'file_queued_handler',
        'file_dialog_start_handler',
        'swfupload_loaded_handler',
        'upload_progress_handler',
        'upload_error_handler',
        'upload_success_handler',
        'upload_complete_handler',
        'upload_start_handler',
        'button_window_mode',
        'button_cursor',
        'debug_handler',
        'http_success'
    );

    /**
     * pola ktorych nie wyswietlac w configu
     *
     * @var array
     */
    private $_omitFields = array(
        'misc',
        'prependCode'
    );

    /**
     * za parametr przyjmuje obiekt konfiguracji badz tablice z opcjami
     * konfiguracyjnymi
     *
     * @param mixed $config
     */
    public function __construct($config = null)
    {
        $this->_config = new Zend_Config_Ini(
            dirname(__FILE__) . DIRECTORY_SEPARATOR . 'SwfUpload.ini',
            'swfupload', array(
            'allowModifications' => true
        ));

        $this->_setup();

        $this->mergeConfig($config);
    }

    /**
     * ustawianie po inicjalizacji konfiguracji
     */
    private function _setup()
    {
        if (!isset($this->_config->upload_url)) {
            $this->_config->upload_url = $_SERVER['SCRIPT_URI'];
        }
    }

    /**
     * zczytuje ustawienia i dodaje je do configa
     *
     * @param mixed $config
     */
    public function mergeConfig($config)
    {
        if ($config instanceof wrxNext_SwfUpload_Config_Interface) {
            $this->_config->merge($config->getConfig());
        } elseif ($config instanceof Zend_Config) {
            $this->_config->merge($config);
        } elseif (is_array($config)) {
            $this->_config->merge(new Zend_Config($config));
        }
    }

    /**
     * czysci liste kodu
     */
    public function clearPrependCode()
    {
        $this->_prependCode = array();
    }

    /**
     * zwraca kod html potrzebny do wyswietlenia formularza dodawania pliku
     *
     * @param string $instanceName
     * @return string
     */
    public function getCode($instanceName = null)
    {
        if (is_null($instanceName)) {
            if (!empty($this->instanceName)) {
                $instanceName = $this->instanceName;
            } else {
                $instanceName = get_class($this);
            }
        }

        $tA = $this->_config->toArray();
        $config = new wrxNext_SwfUpload_Js_Array($tA);
        $config->setEncodeKeys(false);

        if (isset($config->prependCode)) {
            if (is_array($config->prependCode)) {
                foreach ($config->prependCode as $v) {
                    $this->addPrependCode($v);
                }
            } else {
                $this->addPrependCode($config->prependCode);
            }
        }

        foreach ($config as $k => $v) {
            if (in_array($k, $this->_omitFields)) {
                unset($config->{$k});
            } elseif (in_array($k, $this->_threatAsJavascript)) {
                if ($v instanceof wrxNext_SwfUpload_Js_Interface) {
                    // do nothing :)
                } else {
                    $config->{$k} = new wrxNext_SwfUpload_Js($v);
                }
            }
        }

        if (!isset($config->file_upload_name)) {
            $config->file_upload_name = $instanceName;
        }
        $this->instanceName = $instanceName;

        if (!isset($config->button_placeholder_id)) {
            $config->button_placeholder_id = $this->instanceName;
        }

        $code = "var " . $instanceName . ";\nvar _swf_f = function () {\n    " .
            $instanceName .
            " = new SWFUpload(%s);\n};\n\njQuery(document).ready(_swf_f);";
        return $this->getPrependCode() . sprintf($code, $config->getCode());
    }

    /**
     * dodaje kawalek kodu ktory zostanie dodany przed kodem instancji
     *
     * @param string $code
     */
    public function addPrependCode($code)
    {
        $this->_prependCode[] = $code;
    }

    /**
     * zwraca caly kod do dodania
     *
     * @return string
     */
    public function getPrependCode()
    {
        return implode("\n\n\n", $this->_prependCode);
    }

    /**
     * obsluguje proces uploadu
     */
    public function handleUpload()
    {
        $POST_MAX_SIZE = ini_get('post_max_size');
        $unit = strtoupper(substr($POST_MAX_SIZE, -1));
        $multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit ==
        'G' ? 1073741824 : 1)));

        if ((int)$_SERVER['CONTENT_LENGTH'] > $multiplier * (int)$POST_MAX_SIZE &&
            $POST_MAX_SIZE
        ) {
            header("HTTP/1.1 500 Internal Server Error"); // This will trigger
            // an uploadError
            // event in SWFUpload
            echo "POST exceeded maximum allowed size.";
            exit(0);
        }

        // Settings
        $save_path = $this->_config->misc->uploadPath; // The path were we will
        // save the file
        // (getcwd() may not be
        // reliable and should be
        // tested in your
        // environment)
        $upload_name = $this->_config->file_post_name;
        $max_file_size_in_bytes = $this->_config->misc->max_file_size_in_bytes; // 2GB
        // in
        // byte
        $valid_chars_regex = $this->_config->misc->valid_chars_regex; // Characters
        // allowed
        // in the
        // file
        // name
        // (in a
        // Regular
        // Expression
        // format)

        // Other variables
        $MAX_FILENAME_LENGTH = 260;
        $uploadErrors = array(
            0 => "There is no error, the file uploaded with success",
            1 => "The uploaded file exceeds the upload_max_filesize directive in php.ini",
            2 => "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
            3 => "The uploaded file was only partially uploaded",
            4 => "No file was uploaded",
            6 => "Missing a temporary folder"
        );

        // Validate the upload
        if (!isset($_FILES[$upload_name])) {
            $this->handleError(
                "No upload found in \$_FILES for " . $upload_name);
            exit(0);
        } else
            if (isset($_FILES[$upload_name]["error"]) &&
                $_FILES[$upload_name]["error"] != 0
            ) {
                $this->handleError(
                    $uploadErrors[$_FILES[$upload_name]["error"]]);
                exit(0);
            } else
                if (!isset($_FILES[$upload_name]["tmp_name"]) ||
                    !@is_uploaded_file($_FILES[$upload_name]["tmp_name"])
                ) {
                    $this->handleError("Upload failed is_uploaded_file test.");
                    exit(0);
                } else
                    if (!isset($_FILES[$upload_name]['name'])) {
                        $this->handleError("File has no name.");
                        exit(0);
                    }

        // Validate the file size (Warning: the largest files supported by this
        // code is 2GB)
        $file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
        if (!$file_size || $file_size > $max_file_size_in_bytes) {
            $this->handleError("File exceeds the maximum allowed size");
            exit(0);
        }

        if ($file_size <= 0) {
            $this->handleError("File size outside allowed lower bound");
            exit(0);
        }

        // Validate file name (for our purposes we'll just remove invalid
        // characters)
        $file_name = preg_replace('/[^' . $valid_chars_regex . ']|\.+$/i', "",
            basename($_FILES[$upload_name]['name']));
        if (strlen($file_name) == 0 || strlen($file_name) > $MAX_FILENAME_LENGTH) {
            $this->handleError("Invalid file name");
            exit(0);
        }

        // Validate that we won't over-write an existing file
        if ($this->_config->misc->overwrite == 0) {
            if (file_exists($save_path . $file_name)) {
                $this->handleError("File with this name already exists");
                exit(0);
            }
        }

        // Validate file extension
        $path_info = pathinfo($_FILES[$upload_name]['name']);
        $file_extension = $path_info["extension"];
        $is_valid_extension = false;
        foreach ($extension_whitelist as $extension) {
            if (strcasecmp($file_extension, $extension) == 0) {
                $is_valid_extension = true;
                break;
            }
        }

        // Validate file contents (extension and mime-type can't be trusted)
        /*
         * Validating the file contents is OS and web server configuration
         * dependant. Also, it may not be reliable. See the comments on this
         * page: http://us2.php.net/fileinfo Also see
         * http://72.14.253.104/search?q=cache:3YGZfcnKDrYJ:www.scanit.be/uploads/php-file-upload.pdf+php+file+command&hl=en&ct=clnk&cd=8&gl=us&client=firefox-a
         * which describes how a PHP script can be embedded within a GIF image
         * file. Therefore, no sample code will be provided here. Research the
         * issue, decide how much security is needed, and implement a solution
         * that meets the needs.
         */

        // Process the file
        /*
         * At this point we are ready to process the valid file. This sample
         * code shows how to save the file. Other tasks could be done such as
         * creating an entry in a database or generating a thumbnail. Depending
         * on your server OS and needs you may need to set the Security
         * Permissions on the file after it has been saved.
         */
        if (!move_uploaded_file($_FILES[$upload_name]["tmp_name"],
            $save_path . $file_name)
        ) {
            $this->handleError(
                "File could not be saved. " . $save_path . $file_name);
            exit(0);
        }
    }

    private function handleError($message)
    {
        echo $message;
    }
}
