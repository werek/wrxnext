<?php

/**
 * filters polish string noted number value to float
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_Float implements Zend_Filter_Interface
{

    /*
     * (non-PHPdoc) @see Zend_Filter_Interface::filter()
     */
    public function filter($value)
    {
        $value = preg_replace('@[^0-9\.,]@', '', $value);
        $value = str_replace(',', '.', $value);
        $value = floatval($value);
        return $value;
    }
}
