<?php

/**
 * filter for non-conflict cache key name
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_CacheKey implements Zend_Filter_Interface
{

    /**
     * filters given key so it will be compatible with every cache backend
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        return preg_replace('@[^a-zA-Z0-9_]@', '_', $value);
    }
}
