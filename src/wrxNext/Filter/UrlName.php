<?php

/**
 * filters string for use in URL
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_UrlName implements Zend_Filter_Interface
{
    /**
     * holds encoding of incoming data
     *
     * @var string
     */
    protected $_encoding = 'UTF-8';

    /**
     * converts any non ascii characters to it's ascii counterparts
     * and strip all non alphanumeric characters, left spaces are converted to
     * dashes
     * @param mixed $value
     * @return mixed|string
     */
    public function filter($value)
    {
        $value = mb_strtolower($value, $this->_encoding);
        $value = wrxNext_URLify::downcode($value);
        $value = preg_replace('@[^a-z0-9]@', ' ', $value);
        $value = preg_replace('@\s+@', '-', trim($value));
        return $value;
    }

    /**
     * gets encoding
     *
     * @return string
     */
    public function getEncoding()
    {
        return $this->_encoding;
    }

    /**
     * sets encoding
     *
     * @param string $encoding
     * @return wrxNext_Filter_UrlName
     */
    public function setEncoding($encoding)
    {
        $this->_encoding = $encoding;
        return $this;
    }
}
