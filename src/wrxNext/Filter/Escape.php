<?php

class wrxNext_Filter_Escape implements Zend_Filter_Interface
{

    /**
     * przechowuje typ rodzaju escape'owania
     *
     * @var string
     */
    protected $_escapeType = 'html';

    /**
     * przechowuje typ kodowania znakow
     *
     * @var string
     */
    protected $_encodingType = 'ISO-8859-1';

    /**
     * ustala parametry filtra
     *
     * @param string|array|Zend_Config $escapeType
     * @param string $encodingType
     */
    public function __construct($escapeType = 'html', $encodingType = 'ISO-8859-1')
    {
        if ($escapeType instanceof Zend_Config) {
            $options = $escapeType->toArray();
        } else
            if (!is_array($escapeType)) {
                $options = func_get_args();
                $temp['escapeType'] = array_shift($options);
                $temp['encodingType'] = array_shift($options);
                $options = $temp;
            } else {
                $options = $escapeType;
            }

        if (array_key_exists('escapeType', $options)) {
            $this->setEscapeType($options['escapeType']);
        }
        if (array_key_exists('encodingType', $options)) {
            $this->setEncodingType($options['encodingType']);
        }
    }

    /**
     * filtruje tresc escape'ujac wedlug ustawionego typu
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        switch ($this->_escapeType) {
            case 'html':
                return htmlspecialchars($value, ENT_QUOTES,
                    $this->_encodingType);

            case 'htmlall':
                return htmlentities($value, ENT_QUOTES, $this->_encodingType);

            case 'url':
                return rawurlencode($value);

            case 'urlpathinfo':
                return str_replace('%2F', '/', rawurlencode($value));

            case 'quotes':
                // escape unescaped single quotes
                return preg_replace("%(?<!\\\\)'%", "\\'", $value);

            case 'hex':
                // escape every character into hex
                $return = '';
                for ($x = 0; $x < strlen($value); $x++) {
                    $return .= '%' . bin2hex($value[$x]);
                }
                return $return;

            case 'hexentity':
                $return = '';
                for ($x = 0; $x < strlen($value); $x++) {
                    $return .= '&#x' . bin2hex($value[$x]) . ';';
                }
                return $return;

            case 'decentity':
                $return = '';
                for ($x = 0; $x < strlen($value); $x++) {
                    $return .= '&#' . ord($value[$x]) . ';';
                }
                return $return;

            case 'javascript':
                // escape quotes and backslashes, newlines, etc.
                return strtr($value,
                    array(
                        '\\' => '\\\\',
                        "'" => "\\'",
                        '"' => '\\"',
                        "\r" => '\\r',
                        "\n" => '\\n',
                        '</' => '<\/'
                    ));

            case 'mail':
                // safe way to display e-mail address on a web page
                return str_replace(array(
                    '@',
                    '.'
                ), array(
                    ' [AT] ',
                    ' [DOT] '
                ), $value);

            case 'nonstd':
                // escape non-standard chars, such as ms document quotes
                $_res = '';
                for ($_i = 0, $_len = strlen($value); $_i < $_len; $_i++) {
                    $_ord = ord(substr($value, $_i, 1));
                    // non-standard char, escape it
                    if ($_ord >= 126) {
                        $_res .= '&#' . $_ord . ';';
                    } else {
                        $_res .= substr($value, $_i, 1);
                    }
                }
                return $_res;

            default:
                return $value;
        }
    }

    /**
     * zwraca rodzaj escape'owania
     *
     * @return string
     */
    public function getEscapeType()
    {
        return $this->_escapeType;
    }

    /**
     * ustala rodzaj escape'owania
     *
     * @param string $value
     * @return wrxNext_Filter_Escape
     */
    public function setEscapeType($value)
    {
        $this->_escapeType = $value;
        return $this;
    }

    /**
     * zwraca typ kodowania znakow
     *
     * @return string
     */
    public function getEncodingType()
    {
        return $this->_encodingType;
    }

    /**
     * ustala typ kodowania znakow
     *
     * @param string $value
     * @return wrxNext_Filter_Escape
     */
    public function setEncodingType($value)
    {
        $this->_encodingType = $value;
        return $this;
    }
}
