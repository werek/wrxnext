<?php

class wrxNext_Filter_Stack implements Zend_Filter_Interface
{

    /**
     * holds filters
     *
     * @var Zend_Filter_Interface[]
     */
    protected $_filters = array();

    /**
     * gets filters
     *
     * @return Zend_Filter_Interface
     */
    public function getFilters()
    {
        return $this->_filters;
    }

    /**
     * sets filters
     *
     * @param Zend_Filter_Interface $filters
     * @return wrxNext_Filter_Stack
     */
    public function setFilters($filters)
    {
        $this->_filters = array();
        foreach ($filters as $filter) {
            $this->addFilter($filter);
        }
        return $this;
    }

    /**
     * adds filter to stack
     *
     * @param Zend_Filter_Interface $filter
     * @return wrxNext_Filter_Stack
     */
    public function addFilter(Zend_Filter_Interface $filter)
    {
        $this->_filters[] = $filter;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function filter($value)
    {
        foreach ($this->_filters as $filter) {
            $value = $filter->filter($value);
        }
        return $value;
    }
}
