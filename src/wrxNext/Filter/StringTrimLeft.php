<?php

/**
 * trims string from left side, also removing given characters
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_StringTrimLeft extends Zend_Filter_StringTrim
{

    /**
     * Unicode aware trim left method
     * Fixes a PHP problem
     *
     * @param string $value
     * @param string $charlist
     * @return string
     */
    protected function _unicodeTrim($value, $charlist = '\\\\s')
    {
        $chars = preg_replace(array(
            '/[\^\-\]\\\]/S',
            '/\\\{4}/S',
            '/\//'
        ), array(
            '\\\\\\0',
            '\\',
            '\/'
        ), $charlist);

        $pattern = '^[' . $chars . ']*';
        return preg_replace("/$pattern/sSD", '', $value);
    }
}
