<?php

/**
 * filter for escaping special Perl Regexp characters
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_EscapeRegexpCharacters implements Zend_Filter_Interface
{

    protected $_specialCharEscape = array(
        '^' => '\^',
        '$' => '\$',
        '.' => '\.',
        '*' => '\*',
        '+' => '\+',
        '?' => '\?',
        '|' => '\|',
        '(' => '\(',
        ')' => '\)',
        '[' => '\[',
        ']' => '\]',
        '{' => '\{',
        '}' => '\}',
        '\\' => '\\\\'
    );

    /**
     * holds pattern enclosing char
     *
     * @var string
     */
    protected $_perlEnclosingChar = '/';

    /*
     * (non-PHPdoc) @see Zend_Filter_Interface::filter()
     */
    public function filter($value)
    {
        // TODO Auto-generated method stub
        $charactersReplace = $this->_specialCharEscape;
        $charactersReplace[$this->_perlEnclosingChar] = '\\' .
            $this->_perlEnclosingChar;
        return strtr($value, $charactersReplace);
    }

    /**
     * sets pattern enclosing char
     *
     * @param string $char
     * @return wrxNext_Filter_EscapeRegexpCharacters
     */
    public function setEnclosingChar($char)
    {
        $this->_perlEnclosingChar = $char;
        return $this;
    }

    /**
     * gets pattern enclosing char
     *
     * @return string
     */
    public function getEnclosingChar()
    {
        return $this->_perlEnclosingChar;
    }
}
