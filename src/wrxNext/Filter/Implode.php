<?php

/**
 * changes array to concatenated by separator string
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_Implode implements Zend_Filter_Interface
{

    /**
     * holds glue for implode function
     *
     * @var string
     */
    protected $_glue = ", ";

    /*
     * (non-PHPdoc) @see Zend_Filter_Interface::filter()
     */
    public function filter($value)
    {
        $value = (array)$value;
        return implode($this->getGlue(), $value);
    }

    /**
     * gets glue
     *
     * @return string
     */
    public function getGlue()
    {
        return $this->_glue;
    }

    /**
     * sets glue
     *
     * @param string $glue
     * @return wrxNext_Filter_Implode
     */
    public function setGlue($glue)
    {
        $this->_glue = $glue;
        return $this;
    }
}
