<?php

class wrxNext_Filter_UrlText implements Zend_Filter_Interface
{

    /**
     * określa źródłowe kodowanie tekstu
     *
     * @var string
     */
    protected $_sourceEncoding = null;

    /**
     * Sets filter options
     *
     * @param string|array|Zend_Config $sourceEncoding
     */
    public function __construct($sourceEncoding = null)
    {
        if ($sourceEncoding instanceof Zend_Config) {
            $options = $sourceEncoding->toArray();
        } else
            if (!is_array($sourceEncoding)) {
                $options = func_get_args();
                $temp['sourceEncoding'] = array_shift($options);
                $options = $temp;
            } else {
                $options = $sourceEncoding;
            }

        if (array_key_exists('sourceEncoding', $options)) {
            $this->setSourceEncoding($options['sourceEncoding']);
        }
    }

    /**
     * filtruje tekst na potrzeby wyswietlenia w linku
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if (is_null($this->_sourceEncoding)) {
            $encoding = mb_detect_encoding($value);
        } else {
            $encoding = $this->_sourceEncoding;
        }
        $str = iconv($encoding, 'ASCII//TRANSLIT', trim($value));
        return ereg_replace(' +', '_',
            trim(preg_replace('/[^a-zA-Z0-9\s\(\)-]/', '',
                strtolower($str))));
    }

    /**
     * zwraca ustalone kodowanie źródłowe
     *
     * @return string null
     */
    public function getSourceEncoding()
    {
        return $this->_sourceEncoding;
    }

    /**
     * ustala kodowanie źródłowe
     *
     * @param string|null $value
     * @return wrxNext_Filter_UrlText
     */
    public function setSourceEncoding($value)
    {
        $this->_sourceEncoding = $value;
        return $this;
    }
}
