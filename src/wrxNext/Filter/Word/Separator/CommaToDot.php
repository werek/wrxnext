<?php

/**
 * filter to replace comma to dot
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Filter_Word_Separator_CommaToDot extends Zend_Filter_Word_SeparatorToSeparator
{

    public function __construct()
    {
        parent::__construct(',', '.');
    }
} 
