<?php

class wrxNext_Filter_Word_MergeMeasureUnitUsingNbsp implements Zend_Filter_Interface
{

    /**
     * holds units to consider
     *
     * @var array
     */
    protected $_units = array(
        'szt',
        'dkg',
        'dag',
        'kg',
        'cm',
        'ml',
        'dg',
        'l',
        'g'
    );

    /**
     * @inheritdoc
     */
    public function filter($value)
    {
        foreach ($this->_units as $con) {
            $pattern = '@([^>])(\d[\.,\d]?)(\s+)(' . preg_quote($con) .
                '\.?)([^[:alpha:]\.]?)@iu';
            $value = preg_replace($pattern, '\1\2&nbsp;\4\5', $value);
        }
        return $value;
    }
}
