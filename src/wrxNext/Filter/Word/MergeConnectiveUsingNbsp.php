<?php

/**
 * filter for making connective words stick to next one using &nbsp;
 *
 * @author bweres01
 *
 */
class wrxNext_Filter_Word_MergeConnectiveUsingNbsp implements Zend_Filter_Interface
{

    /**
     * holds list of connectives which should be escaped
     *
     * @var array
     */
    protected $_connectives = array(
        'aby',
        'ale',
        'bo',
        'bowiem',
        'chociaż',
        'choć',
        'gdyż',
        'jednak',
        'jeśli',
        'jeżeli',
        'lecz',
        'natomiast',
        'ponieważ',
        'żeby',
        'toteż',
        'oraz',
        'ale',
        'lecz',
        'z',
        'w',
        'o',
        'i',
        'a'
    );

    /**
     * @inheritdoc
     */
    public function filter($value)
    {
        foreach ($this->_connectives as $con) {
            $pattern = '@([^[:alnum:];></])(' . preg_quote($con) .
                ')[^[:alnum:]\.><a-z/&]+?([[:alnum:]])@iu';
            $value = preg_replace($pattern, '\1\2&nbsp;\3', $value);
        }
        return $value;
    }
}
