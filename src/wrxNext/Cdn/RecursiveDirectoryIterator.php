<?php

class wrxNext_Cdn_RecursiveDirectoryIterator extends RecursiveIteratorIterator
{

    /**
     * creates CDN recursive directory iterator, mode/flags are the same as
     *
     * @link RecursiveIteratorIterator
     *
     * @param wrxNext_Cdn $cdn
     * @param int $mode
     * @param int $flags
     * @throws wrxNext_Cdn_Exception
     */
    public function __construct(wrxNext_Cdn $cdn, $mode = self::LEAVES_ONLY, $flags = 0)
    {
        if (!$cdn->isDirectory()) {
            throw new wrxNext_Cdn_Exception(
                'given location "' . $cdn->getLocation() .
                '" is not a directory');
        }
        parent::__construct(
            new RecursiveDirectoryIterator($cdn->getFullPath(),
                FilesystemIterator::FOLLOW_SYMLINKS), $mode, $flags);
    }

    /**
     * returns cdn file entry
     *
     * @return wrxNext_Cdn
     */
    public function current()
    {
        return $this->_splFileInfoToCdn(parent::current());
    }

    /**
     * converts SplFileInfo object to wrxNext_Cdn
     *
     * @param \SplFileInfo $info
     * @throws wrxNext_Cdn_Exception
     * @return wrxNext_Cdn
     */
    protected function _splFileInfoToCdn(\SplFileInfo $info)
    {
        $fullPath = $info->getPath() . DIRECTORY_SEPARATOR . $info->getFilename();
        if (strpos($fullPath, wrxNext_Cdn::getRootDirectory()) !== 0) {
            throw new wrxNext_Cdn_Exception(
                'given file is not located in current cdn directory, file in question: ' .
                $fullPath);
        }
        $location = str_replace(wrxNext_Cdn::getRootDirectory(), '', $fullPath);
        return new wrxNext_Cdn($location);
    }
}
