<?php

/**
 * ZF1 bootstrap resource class for handling cdn settings
 *
 * @author bweres01
 *
 */
class wrxNext_Cdn_Resource extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * resolves passed parameters and sets constants for use later
     *
     * (non-PHPdoc)
     * @see Zend_Application_Resource_Resource::init()
     */
    public function init()
    {
        if (isset ($this->_options ['localPath']) && !empty ($this->_options ['localPath'])) {
            $realPath = realpath($this->_options ['localPath']);
        } else {
            $realPath = realpath(APPLICATION_PATH . '../public');
        }

        define('APPLICATION_CDN_DIRECTORY', $realPath);

        $baseUrl = "";
        if ($this->_options ['discoverBaseUrl']) {
            $baseUrl = Zend_Controller_Front::getInstance()->getBaseUrl();
        }

        $urlParts = parse_url($this->_options ['address']);

        $cdnUrl = '';

        // online protocol discovery based on current request
        if ($this->_options ['discoverProtocol']) {
            if (wrxNext_SecureCheck::isSecure()) {
                $cdnUrl .= 'https://';
            } else {
                $cdnUrl .= 'http://';
            }
        } else {
            $cdnUrl .= (isset ($urlParts ['scheme'])) ? $urlParts ['scheme'] . '://' : 'http://';
        }

        // host
        $cdnUrl .= (isset ($urlParts ['host'])) ? $urlParts ['host'] : $_SERVER ['HTTP_HOST'];
        // optional auethentication
        if (isset ($urlParts ['user'])) {
            if (isset ($urlParts ['pass'])) {
                $cdnUrl .= $urlParts ['user'] . ':' . $urlParts . '@';
            } else {
                $cdnUrl .= $urlParts ['user'] . '@';
            }
        }
        // optional port
        $cdnUrl .= (isset ($urlParts ['port'])) ? ':' . $urlParts ['port'] : '';

        // if host is present baseUrl is disabled by default
        if (!empty ($baseUrl) && !isset ($urlParts ['host'])) {
            $cdnUrl .= (isset ($urlParts ['path'])) ? $baseUrl . trim($urlParts ['path'], '/') : $baseUrl;
        } else {
            $cdnUrl .= (isset ($urlParts ['path'])) ? $urlParts ['path'] : '/';
        }
        // addditional url validation
        if (substr($cdnUrl, -1) != '/') {
            $cdnUrl .= '/';
        }

        define('APPLICATION_CDN_URI', $cdnUrl);
    }
}
