<?php

/**
 * service with usefull methods for managing cdn entries
 *
 * @author bweres01
 *
 */
class wrxNext_Cdn_Service
{

    /**
     * returns list of files location recursivelly, statring with given
     * directory
     *
     * @param wrxNext_Cdn $directory
     * @return array
     */
    public function getFilesLocationRecursivelly(wrxNext_Cdn $directory)
    {
        $data = array();
        foreach ($this->recursiveDirectoryIterator($directory) as $cdnEntry) {
            if ($cdnEntry->isFile()) {
                $data[] = $cdnEntry->getLocation();
            };
        }
        return $data;
    }

    /**
     * returns directory iterator for given path which will return file info
     * objects
     *
     * @param wrxNext_Cdn $cdnPath
     * @return \wrxNext_Cdn_RecursiveDirectoryIterator
     */
    public function recursiveDirectoryIterator(wrxNext_Cdn $cdnPath)
    {
        return new \wrxNext_Cdn_RecursiveDirectoryIterator($cdnPath);
    }
}
