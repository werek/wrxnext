<?php

interface wrxNext_Cdn_Interface_Location
{

    /**
     * returns cdn location
     *
     * @return string
     */
    public function getLocation();
}
