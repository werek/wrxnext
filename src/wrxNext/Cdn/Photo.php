<?php

/**
 * class for handling image operations on cdn repository
 *
 * @author bweres01
 *
 */
class wrxNext_Cdn_Photo
{
    use wrxNext_Trait_RegistryLog;

    /**
     * resizes given photo Uri relative to cdn, returning new file cdn Uri
     *
     * @param string $cdnUri
     * @param int $width
     * @param int $height
     * @param bool $keepAspectRatio
     * @param bool $crop
     * @throws wrxNext_Image_Exception
     * @return string
     */
    public function resizeCdnPhoto($cdnUri, $width, $height,
                                   $keepAspectRatio = true, $crop = true)
    {
        if (!$cdnUri instanceof wrxNext_Cdn) {
            $cdnSource = new wrxNext_Cdn($cdnUri);
        } else {
            $cdnSource = $cdnUri;
        }
        if (!$cdnSource->exists() || !$cdnSource->readable()) {
            throw new wrxNext_Image_Exception(
                'file is not readable under cdn path:' .
                $cdnSource->getFullPath(), 101);
        }
        $image = new wrxNext_Image($cdnSource->getFullPath(), true);
        $image->setQuality(85);
        $image->resize($width, $height, $keepAspectRatio, $crop);
        $thumbnailPath = $this->thumbnailPath($cdnUri, $width, $height,
            $keepAspectRatio, $crop);
        $cdnThumbnail = new wrxNext_Cdn($thumbnailPath);
        $cdnThumbnailDirectory = $cdnThumbnail->parentLocation();
        $this->_log(
            'directory to save resized photo: ' .
            $cdnThumbnailDirectory->getFullPath());
        if (!$cdnThumbnailDirectory->exists()) {
            $this->_log(
                'directory doesn\'t exists, attempting to create directory: ' .
                $cdnThumbnailDirectory->getFullPath());
            if (!$cdnThumbnailDirectory->createDirectory()) {
                throw new wrxNext_Image_Exception(
                    'couldn\'t create directory for path: ' .
                    $cdnThumbnailDirectory->getFullPath(), 102);
            }
        }
        $image->save(null, $cdnThumbnail->getFullPath());
        return str_replace('\\', '/', $cdnThumbnail->getLocation());
    }

    /**
     * generates thumbnail path based on resize parameters, relative to cdn base
     * directory
     *
     * @param string $cdnUri
     * @param int $width
     * @param int $height
     * @param boolean $keepAspectRatio
     * @param boolean $crop
     * @return string
     */
    public function thumbnailPath($cdnUri, $width, $height,
                                  $keepAspectRatio = true, $crop = true)
    {
        if ($cdnUri instanceof wrxNext_Cdn) {
            $cdnUri = $cdnUri->getLocation();
        }
        $modeDir = '';
        if ($crop === false) {
            $modeDir = 'scaled/';
        } elseif ($crop === true && $keepAspectRatio === true) {
            $modeDir = 'cropped/';
        } elseif ($crop === true && $keepAspectRatio === false) {
            $modeDir = 'bordered/';
        }
        $thumbnailPath = 'thumbnail/' . $modeDir . intval($width) . 'x' .
            intval($height) . '/' . ltrim($cdnUri, '\\/');
        return $thumbnailPath;
    }

    /**
     * checks if thumbnail of given sizes exists for given cdn uri
     *
     * @param string $cdnUri
     * @param int $width
     * @param int $height
     * @param bool $keepAspectRatio
     * @param bool $crop
     * @return bool
     */
    public function thumbnailExists($cdnUri, $width, $height,
                                    $keepAspectRatio = true, $crop = true)
    {
        $cdn = new wrxNext_Cdn(
            $this->thumbnailPath($cdnUri, $width, $height, $keepAspectRatio,
                $crop));
        return $cdn->exists() ? $cdn->getLocation() : false;
    }
}
