<?php

/**
 * exception for CDN oprations
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Cdn_Exception extends wrxNext_Exception
{
}
