<?php

class wrxNext_Akismet
{

    private $_api_key = 'ed9a445088eb'; // klucz dla maila u22982@wp.pl
    private $_host;

    private $_path = '/';

    private $_wp_version = '2.92';

    private $_akismet_host;

    private $_akismet_port = 80;

    private $_blog = "wrxnext";

    public function __construct($api_key = null)
    {
        $this->_api_key = $api_key;
        $this->_akismet_host = $this->_api_key . '.rest.akismet.com';
    }

    public function checkComment($comment)
    {
        $akismet_api_host = $this->_akismet_host;
        $akismet_api_port = $this->_akismet_port;

        $comment['user_ip'] = preg_replace('/[^0-9., ]/', '',
            $_SERVER['REMOTE_ADDR']);
        $comment['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
        $comment['referrer'] = $_SERVER['HTTP_REFERER'];
        $comment['blog'] = $this->_blog;

        $ignore = array(
            'HTTP_COOKIE'
        );

        foreach ($_SERVER as $key => $value)
            if (!in_array($key, $ignore))
                $comment["$key"] = $value;

        $query_string = '';
        foreach ($comment as $key => $data)
            $query_string .= $key . '=' . urlencode(stripslashes($data)) . '&';

        $response = $this->akismet_http_post($query_string, $akismet_api_host,
            '/1.1/comment-check', $akismet_api_port);
        if ('true' == $response[1]) {
            return true;
        } else {
            return false;
        }
    }

    protected function akismet_http_post($request, $host, $path, $port = 80)
    {
        $wp_version = $this->_wp_version;

        $http_request = "POST $path HTTP/1.0\r\n";
        $http_request .= "Host: $host\r\n";
        $http_request .= "Content-Type: application/x-www-form-urlencoded; charset=" .
            get_option('blog_charset') . "\r\n";
        $http_request .= "Content-Length: " . strlen($request) . "\r\n";
        $http_request .= "User-Agent: WordPress/$wp_version | Akismet/2.0\r\n";
        $http_request .= "\r\n";
        $http_request .= $request;

        $response = '';
        if (false != ($fs = @fsockopen($host, $port, $errno, $errstr, 10))) {
            fwrite($fs, $http_request);

            while (!feof($fs))
                $response .= fgets($fs, 1160); // One TCP-IP packet
            fclose($fs);
            $response = explode("\r\n\r\n", $response, 2);
        }
        return $response;
    }
}
