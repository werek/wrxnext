<?php

/**
 * Main Image handling class for common image operations
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 */
class wrxNext_Image
{

    const GIF = 1;

    const JPEG = 2;

    const PNG = 3;

    const ORIENTATION_PORTRAIT = 0;

    const ORIENTATION_LANDSCAPE = 1;
    /**
     * przechowuje zasob GD reprezentujacy obraz
     *
     * @var GD Resource
     */
    public $_imageGD;
    /**
     * przechowuje stos dla efektow
     *
     * @var wrxNext_Image_Effect
     */
    private $_effect;
    /**
     * przechowuje informacje typu exif
     *
     * @var wrxNext_Exif
     */
    private $_exif;
    /**
     * przechowuje podstawowe informacje o pliku zwrocone przez getimagesize()
     *
     * @var array
     */
    private $_type;
    /**
     * przechowuje sciezke pliku
     *
     * @var string
     */
    private $_filePath;

    /**
     * przechowuje procentową wartośi jakośi zapisu dla stratnych formatów
     * kompresji
     *
     * @var int
     */
    private $_quality = 75;

    /**
     * czy przechowywać przeźroczystoć
     *
     * @var boolean
     */
    private $_alpha = false;

    /**
     * konstruktor, jako parametr przyjmuje sciezke do pliku graficznego
     *
     * @param string $filePath
     * @param bool $saveAlpha
     * @throws wrxNext_Image_Exception
     */
    function __construct($filePath = null, $saveAlpha = false)
    {
        if (!is_null($filePath)) {
            $this->readFile($filePath, $saveAlpha);
        }
        $this->_effect = new wrxNext_Image_Effect();
    }

    /**
     * zczytuje plik graficzny z podanej sciezki
     *
     * @param string $filePath
     * @param bool $saveAlpha
     * @return wrxNext_Image
     * @throws wrxNext_Image_Exception
     */
    public function readFile($filePath, $saveAlpha = false)
    {
        if (!is_readable($filePath) || is_dir($filePath)) {
            throw new wrxNext_Image_Exception(
                'File is not readable under path: ' . $filePath, 2);
        }
        $this->_alpha = $saveAlpha;
        $this->_type = @getimagesize($filePath);
        $this->_exif = new wrxNext_Exif($filePath);
        if (is_array($this->_type)) {
            switch ($this->_type[2]) {
                case self::GIF:
                    $this->_imageGD = imagecreatefromgif($filePath);
                    break;
                case self::JPEG:
                    $this->_imageGD = imagecreatefromjpeg($filePath);
                    break;
                case self::PNG:
                    $this->_imageGD = imagecreatefrompng($filePath);
                    break;
                default:
                    throw new wrxNext_Image_Exception(
                        "Unsuported image type for file: " . $filePath, 1);
                    break;
            }
            if ($this->_alpha) {
                imagealphablending($this->_imageGD, false);
                imagesavealpha($this->_imageGD, true);
            }
            $this->_filePath = $filePath;
        } else {
            throw new wrxNext_Image_Exception(
                "Unsuported image type for file: " . $filePath, 1);
        }
        return $this;
    }

    /**
     * tworzy czysty obrazek
     *
     * @param int $width
     * @param int $height
     * @param string $color
     * @param bool $withAlpha
     * @return wrxNext_Image
     */
    public static function createBlank($width, $height, $color = '#ffffff',
                                       $withAlpha = true)
    {
        $img = imagecreatetruecolor($width, $height);
        if ($withAlpha) {
            imagesavealpha($img, true);
        }
        if (!is_null($color)) {
            $color = wrxNext_Color::hexToRgb($color);
            imagefill($img, 0, 0,
                imagecolorallocate($img, $color[wrxNext_Color::RED],
                    $color[wrxNext_Color::GREEN], $color[wrxNext_Color::BLUE]));
        } else {
            imagefill($img, 0, 0, imagecolorallocatealpha($img, 0, 0, 0, 127));
        }
        $image = new wrxNext_Image(null, $withAlpha);
        $image->setImageGD($img);
        return $image;
    }

    /**
     * zapisuje obraz w formacie okreslonym przez $as binarnie,
     * w sciezce okreslonej przez $path, zwraca wynik poprawnosci zapisu
     * true - udalo sie, false - nie udalo sie
     *
     * @param int $as
     * @param string $path
     * @return bool
     */
    public function save($as = null, $path = null)
    {
        if (is_null($as)) {
            $as = $this->_type[2];
        }
        if (is_null($path)) {
            $path = $this->_filePath;
        }
        $success = false;
        switch ($as) {
            case self::GIF:
                $success = imagegif($this->_imageGD, $path);
                break;
            case self::JPEG:
                $success = imagejpeg($this->_imageGD, $path,
                    $this->getQuality());
                break;
            case self::PNG:
                $success = imagepng($this->_imageGD, $path,
                    $this->getQuality(true));
                break;
        }
        return $success;
    }

    /**
     * gets procentowa wartosc dla zapisu stratnych formatow kompresji
     *
     * @param bool $decimal
     * @return int
     */
    public function getQuality($decimal = false)
    {
        return ($decimal) ? intval($this->_quality / 10) : $this->_quality;
    }

    /**
     * sets procentowa wartosc dla zapisu stratnych formatow kompresji
     *
     * @param int $quality
     * @return wrxNext_Image
     */
    public function setQuality($quality)
    {
        $this->_quality = $quality;
        return $this;
    }

    /**
     * zmienia rozmiar obrazu do podanych wartosci
     *
     * @param int $destWidth
     * @param int $destHeight
     * @param bool $keepAspectRatio
     * @param bool $crop
     * @return wrxNext_Image
     */
    public function resize($destWidth, $destHeight, $keepAspectRatio = false,
                           $crop = true)
    {
        $this->changePercentageToReal($destWidth, $destHeight);
        $srcWidth = $this->_type[0];
        $srcHeight = $this->_type[1];
        $sourceOffsetX = 0;
        $sourceOffsetY = 0;
        if ($keepAspectRatio) {
            $aspect = $srcWidth / $srcHeight;
            $destAspect = $destWidth / $destHeight;
            if (floor($aspect * 1000) != floor($destAspect * 1000)) {
                if (!$crop) {
                    if ($destAspect > $aspect) {
                        $destWidth = floor(
                            ($destHeight / $srcHeight) * $srcWidth);
                    } else {
                        $destHeight = floor(
                            ($destWidth / $srcWidth) * $srcHeight);
                    }
                } else {
                    if ($destAspect > $aspect) {
                        // wycinamy panorame z portretu, szerokosc pozostaje ta
                        // sama, obliczamy wysokosc i offset wysokosci
                        $sourceOffsetY = floor(
                            ($srcHeight - ($srcWidth / $destAspect)) / 2);
                        $srcHeight = floor($srcWidth / $destAspect);
                    } else {
                        // wycinamy portret z panoramy, wysokosc pozostaje ta
                        // sama, obliczamy szerokosc ze zrodla i offset
                        // szerokosci
                        $sourceOffsetX = floor(
                            ($srcWidth - ($srcHeight * $destAspect)) / 2);
                        $srcWidth = floor($srcHeight * $destAspect);
                    }
                }
            }
        }
        $tmpimage = $this->_blank($destWidth, $destHeight);
        imagecopyresampled($tmpimage, $this->_imageGD, 0, 0, $sourceOffsetX,
            $sourceOffsetY, $destWidth, $destHeight, $srcWidth, $srcHeight);
        imagedestroy($this->_imageGD);
        $this->_imageGD = $tmpimage;
        $this->_type[0] = (int)$destWidth;
        $this->_type[1] = (int)$destHeight;
        return $this;
    }

    /**
     * zamienia wartosci procentowe okreslajace wymiary na wartosci realne
     * wzgledem aktualnych wymiarow obrazu
     *
     * @param string $x
     * @param string $y
     */
    private function changePercentageToReal(&$x, &$y)
    {
        $tmp = "";
        if (preg_match('/.*%$/i', $x)) {
            $tmp = substr($x, 0, strlen($x) - 1);
            $x = floor(($tmp / 100) * $this->_type[0]);
        }
        if (preg_match('/.*%$/i', $y)) {
            $tmp = substr($y, 0, strlen($y) - 1);
            $x = floor(($tmp / 100) * $this->_type[1]);
        }
    }

    /**
     * zwraca resource pustego obrazu o wybranych wymiarach, dodajac kanał alpha
     * jezeli obiekt był tworzony z takim
     *
     * @param int $width
     * @param int $height
     * @return resource
     */
    protected function _blank($width, $height)
    {
        $tmpimage = imagecreatetruecolor($width, $height);
        if ($this->_alpha) {
            imagealphablending($tmpimage, false);
            imagesavealpha($tmpimage, true);
        }
        return $tmpimage;
    }

    /**
     * przycina obraz do podanych wymiarow, ustawiajac poczatek wycinania w
     * podanym punkcie
     *
     * @param int $destWidth
     * @param int $destHeight
     * @param int $sourceOffsetX
     * @param int $sourceOffsetY
     * @return wrxNext_Image
     */
    public function crop($destWidth, $destHeight, $sourceOffsetX = null,
                         $sourceOffsetY = null)
    {
        $this->changePercentageToReal($destWidth, $destHeight);
        if (is_null($sourceOffsetX)) {
            $sourceOffsetX = floor(($this->_type[0] - $destWidth) / 2);
        }
        if (is_null($sourceOffsetY)) {
            $sourceOffsetY = floor(($this->_type[1] - $destHeight) / 2);
        }
        $tmpimage = $this->_blank($destWidth, $destHeight);
        imagecopyresampled($tmpimage, $this->_imageGD, 0, 0, $sourceOffsetX,
            $sourceOffsetY, $destWidth, $destHeight, $destWidth, $destHeight);
        imagedestroy($this->_imageGD);
        $this->_imageGD = $tmpimage;
        $this->_type[0] = $destWidth;
        $this->_type[1] = $destHeight;
        return $this;
    }

    /**
     * zwraca identyfikator zasobu obrazu GD
     *
     * @return GDResource
     */
    public function getImageGD()
    {
        return $this->_imageGD;
    }

    /**
     * ustawia identyfikator zasobu obrazu GD
     *
     * @param GDResource $imageGD
     * @return wrxNext_Image
     */
    public function setImageGD($imageGD)
    {
        $this->_imageGD = $imageGD;
        $this->_type[0] = imagesx($this->_imageGD);
        $this->_type[1] = imagesy($this->_imageGD);
        return $this;
    }

    /**
     *
     * @link printImage
     *
     */
    public function __toString()
    {
        $this->printImage();
        return "";
    }

    /**
     * wysyla obraz do przegladarki korzystajac z kompresji okreslonej
     * przez $as (domyslnie rodzaj pierwotny), i zwraca status powodzenia
     * operacji
     *
     * @param int $as
     * @param bool $exitOnCompletion
     * @return bool
     */
    public function printImage($as = null, $exitOnCompletion = false)
    {
        if (is_null($as)) {
            $as = $this->_type[2];
        }
        $succes = false;
        switch ($as) {
            case self::GIF:
                header("Content-Type: image/gif");
                $succes = imagegif($this->_imageGD);
                break;
            case self::PNG:
                header("Content-Type: image/png");
                $succes = imagepng($this->_imageGD);
                break;
            case self::JPEG:
            default:
                header("Content-Type: image/jpeg");
                $succes = imagejpeg($this->_imageGD);
                break;
        }
        if ($succes && $exitOnCompletion) {
            exit();
        }
        return $succes;
    }

    /**
     * zwraca dane obrazu jako tablica
     *
     * @return array
     */
    public function toArray()
    {
        $t = $this->_type;
        $t['exif'] = $this->_exif->toArray();
        return $t;
    }

    /**
     * zwraca typ obrazu w numeracji zgodnej z wynikiem getimagesize
     *
     * @return int
     */
    public function getImageType()
    {
        if (is_array($this->_type) && isset($this->_type[2])) {
            return $this->_type[2];
        }
        return null;
    }

    /**
     * zwraca orientacje obazu
     *
     * @return int mozeliwe opcje to PORTRET = 0 lub LANDSCAPE = 1
     */
    public function getOrientation()
    {
        if ($this->getWidth() >= $this->getHeight()) {
            $orientation = self::ORIENTATION_LANDSCAPE;
        } else {
            $orientation = self::ORIENTATION_PORTRAIT;
        }
        return $orientation;
    }

    /**
     * zwraca szerokosc obrazu
     *
     * @return int
     */
    public function getWidth()
    {
        return imagesx($this->_imageGD);
    }

    /**
     * zwraca wysokosc obrazu
     *
     * @return int
     */
    public function getHeight()
    {
        return imagesy($this->_imageGD);
    }

    /**
     * sprawdza czy obraz posiada dane exif
     *
     * @return bool
     */
    public function hasExifData()
    {
        return $this->_exif->hasExifHeader();
    }

    /**
     * returns exif object
     *
     * @return wrxNext_Exif
     */
    public function exif()
    {
        return $this->_exif;
    }

    /**
     * ustawia stos dla efektow
     *
     * @param wrxNext_Image_Effect $effectHandler
     * @return wrxNext_Image
     */
    public function setEffectHandler($effectHandler)
    {
        $this->_effect = $effectHandler;
        return $this;
    }

    /**
     * aplikuje efekty do obrazu
     *
     * @return wrxNext_Image
     *
     */
    public function applyEffects()
    {
        $this->getEffectHandler()->applyToImage($this);
        return $this;
    }

    /**
     * zwraca stos efektow
     *
     * @return wrxNext_Image_Effect
     */
    public function getEffectHandler()
    {
        return $this->_effect;
    }

    /**
     * zwraca dane EXIF ze zdjecia
     *
     * @return array
     */
    public function getExifData()
    {
        return $this->_exif->getData();
    }

    /**
     * zwraca hash tresci obrazu
     *
     * @param int|number $hashWidth
     * @param int|number $hashHeight
     * @param int|number $reduceBitDepthTo
     * @param bool $hashToMD5
     * @return string
     */
    public function contentHash($hashWidth = 16, $hashHeight = 16,
                                $reduceBitDepthTo = 8, $hashToMD5 = true)
    {
        $hashImage = imagecreatetruecolor($hashWidth, $hashHeight);
        imagecopyresampled($hashImage, $this->_imageGD, 0, 0, 0, 0, $hashWidth,
            $hashHeight, $this->getWidth(), $this->getHeight());
        $hash = array();
        for ($y = 0; $y < $hashHeight; $y++) {
            for ($x = 0; $x < $hashWidth; $x++) {
                $hash[] = $this->_greyscaleAt($hashImage, $x, $y,
                    $reduceBitDepthTo);
            }
        }
        $hashString = implode(',', $hash);
        return ($hashToMD5) ? md5($hashString) : $hashString;
    }

    /**
     * zwraca wartoś luminancji zgodnie z modelem percepcji oka
     *
     * @param resource $resource
     * @param int $x
     * @param int $y
     * @param int $bitDepth number of bits to use, default is 8
     * @return int
     */
    protected function _greyscaleAt($resource, $x, $y, $bitDepth = null)
    {
        $colors = $this->_colorAt($resource, $x, $y);
        $luminance = wrxNext_Color::rgbToGrayscale($colors[wrxNext_Color::RED],
            $colors[wrxNext_Color::GREEN], $colors[wrxNext_Color::BLUE]);
        if (!is_null($bitDepth) && $bitDepth != 8) {
            // changing int value for given destination bit precision
            $luminance = round(
                $luminance * ((pow(2, intval($bitDepth)) - 1) / 255), 0);
        }
        return intval($luminance);
    }

    /**
     * zwraca tablice z wartościami kolorów dla wybranego obrazu
     *
     * zwracana tablica posiada 4 indeksy: red, green, blue, alpha
     *
     * @param resource $resource
     * @param int $x
     * @param int $y
     * @return array
     */
    protected function _colorAt($resource, $x, $y)
    {
        $rgb = imagecolorat($resource, $x, $y);
        return imagecolorsforindex($resource, $rgb);
    }

    /**
     * orients image by exif orientation value
     *
     * @return wrxNext_Image
     */
    public function orientByExif()
    {
        if ($this->_exif->hasExifHeader()) {
            $deg = 0;
            $flip = false;
            switch ($this->_exif->getRawData('Orientation')) {
                case 2:
                    $flip = IMG_FLIP_VERTICAL;
                    break;
                case 3:
                    $deg = 180;
                    break;
                case 4:
                    $flip = IMG_FLIP_HORIZONTAL;
                    break;
                case 5:
                    $deg = 90;
                    $flip = IMG_FLIP_VERTICAL;
                    break;
                case 6:
                    $deg = 270;
                    break;
                case 7:
                    $deg = 270;
                    $flip = IMG_FLIP_VERTICAL;
                case 8:
                    $deg = 90;
                    break;
            }
            if ($deg > 0) {
                if (function_exists('imagerotate')) {
                    $this->_imageGD = imagerotate($this->_imageGD, $deg,
                        imagecolorallocatealpha($this->_imageGD, 0, 0, 0,
                            127), 0);
                    if ($this->_alpha) {
                        imagealphablending($this->_imageGD, false);
                        imagesavealpha($this->_imageGD, true);
                    }
                } else {
                    $this->_failoverRotate($deg);
                }
            }
            if ($flip !== false) {
                imageflip($this->_imageGD, $flip);
            }
        }
        return $this;
    }

    /**
     * funkcja obracajaca zdjecie, jeżeli nie zostanie znaleziona funkcja
     * natywna
     *
     * @param int $angle
     * @return number|GD|wrxNext_Image
     */
    protected function _failoverRotate($angle)
    {
        $bgcolor = imagecolorallocatealpha($this->_imageGD, 0, 0, 0, 127);

        function rotateX($x, $y, $theta)
        {
            return $x * cos($theta) - $y * sin($theta);
        }

        function rotateY($x, $y, $theta)
        {
            return $x * sin($theta) + $y * cos($theta);
        }

        $srcw = imagesx($this->_imageGD);
        $srch = imagesy($this->_imageGD);

        if ($angle == 0)
            return $this->_imageGD;

        // Convert the angle to radians
        $theta = deg2rad($angle);

        // Calculate the width of the destination image.
        $temp = array(
            rotateX(0, 0, 0 - $theta),
            rotateX($srcw, 0, 0 - $theta),
            rotateX(0, $srch, 0 - $theta),
            rotateX($srcw, $srch, 0 - $theta)
        );
        $minX = floor(min($temp));
        $maxX = ceil(max($temp));
        $width = $maxX - $minX;

        // Calculate the height of the destination image.
        $temp = array(
            rotateY(0, 0, 0 - $theta),
            rotateY($srcw, 0, 0 - $theta),
            rotateY(0, $srch, 0 - $theta),
            rotateY($srcw, $srch, 0 - $theta)
        );
        $minY = floor(min($temp));
        $maxY = ceil(max($temp));
        $height = $maxY - $minY;

        $destimg = imagecreatetruecolor($width, $height);
        imagefill($destimg, 0, 0,
            imagecolorallocatealpha($destimg, 0, 0, 0, 127));

        // sets all pixels in the new image
        for ($x = $minX; $x < $maxX; $x++) {
            for ($y = $minY; $y < $maxY; $y++) {
                // fetch corresponding pixel from the source image
                $srcX = round(rotateX($x, $y, $theta));
                $srcY = round(rotateY($x, $y, $theta));
                if ($srcX >= 0 && $srcX < $srcw && $srcY >= 0 && $srcY < $srch) {
                    $color = imagecolorat($this->_imageGD, $srcX, $srcY);
                } else {
                    $color = $bgcolor;
                }
                imagesetpixel($destimg, $x - $minX, $y - $minY, $color);
            }
        }
        $this->_imageGD = $destimg;
        return $this;
    }
}
