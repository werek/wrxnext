<?php

interface wrxNext_Shortcodes_Parser_Interface
{

    /**
     * returns string for which this parser will be used
     *
     * @return string
     */
    public function name();

    /**
     * this method generates replacement for given shortcode
     * based on any optional parameters, and if present text
     * encapsulated by shortdcode tags
     *
     * @param array $params
     * @param string $text
     * @return string
     */
    public function parse($params = array(), $text = null);
}
