<?php

abstract class wrxNext_Shortcodes_Parser_Abstract implements
    wrxNext_Shortcodes_Parser_Interface
{
    /**
     * @inheritdoc
     */
    public function name()
    {
        throw new wrxNext_Shortcodes_Exception(
            'Unimplemented method name() in class ' . get_class($this),
            10000);
    }

    /**
     * @inheritdoc
     */
    public function parse($params = array(), $text = null)
    {
        throw new wrxNext_Shortcodes_Exception(
            'Unimplemented method parse() in class ' . get_class($this),
            10000);
    }
}
