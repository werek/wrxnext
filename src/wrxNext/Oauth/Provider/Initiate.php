<?php

class wrxNext_Oauth_Provider_Initiate extends wrxNext_Oauth_Provider_Request_Abstract
{
    protected $_oauthParamsRequired = array(
        'consumer_key',
        'signature_method',
        'timestamp',
        'nonce',
        'callback',
        'signature'
    );
    protected $_consumerStorage = null;

    public function handle(Zend_Controller_Request_Http $request, Zend_Controller_Response_Http $response)
    {
        $this->_verifySignature();
        if (!empty($this->_oauthRequestParams['consumer_key'])) {
            $consumer = $this->getConsumerStorage()->getByConsumerKey($this->_oauthRequestParams['consumer_key']);
            if ($consumer instanceof Zend_Db_Table_Row_Abstract) {
                $auth = new wrxNext_Authentication();
                $tempKey = $auth->randomAlphaNumeric(16);
                $tempSecret = $auth->randomAlphaNumeric(32);
                $callback = $this->_oauthRequestParams['callback'];

                //saving temporary credentials
                $tempCredentials = array(
                    'token' => $tempKey,
                    'token_secret' => $tempSecret,
                    'callback' => $callback,
                    'oauth_consumer_id' => $consumer->id
                );
                $this->getTempCredentialStorage()->insert($tempCredentials);

                //building response
                $response->setHeader('Content-Type', 'application/x-www-form-urlencoded');
                $responseArray = array(
                    'oauth_token' => $tempKey,
                    'oauth_token_secret' => $tempSecret,
                    'oauth_callback_confirmed' => 'true'
                );
                $response->setBody($this->_buildBodyFromArray($responseArray));
                echo $response;
            } else {
                throw new wrxNext_Oauth_Provider_Exception('consumer unknown');
            }
        }
    }

    /**
     *
     * @return wrxNext_Oauth_Storage_ConsumerDbTable
     */
    public function getConsumerStorage()
    {
        if (is_null($this->_consumerStorage)) {
            $this->_consumerStorage = new wrxNext_Oauth_Storage_ConsumerDbTable();
        }
        return $this->_consumerStorage;
    }

    /**
     *
     * @param wrxNext_Oauth_Storage_ConsumerDbTable $storage
     * @return wrxNext_Oauth_Provider_Initiate
     */
    public function setConsumerStorage($storage)
    {
        $this->_consumerStorage = $storage;
        return $this;
    }
}
