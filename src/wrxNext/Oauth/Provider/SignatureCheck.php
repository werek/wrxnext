<?php

class wrxNext_Oauth_Provider_SignatureCheck
{
    use wrxNext_Trait_RegistryLog;

    /**
     *
     * @var Zend_Oauth_Http_Utility
     */
    protected $_httpUtility = null;

    /**
     *
     * @var Zend_Controller_Request_Http
     */
    protected $_httpRequest = null;

    /**
     *
     * @var array
     */
    protected $_oauthRequestParams = array();

    /**
     *
     * @var array
     */
    protected $_otherParams = array();

    /**
     * verifies given request
     *
     * @param string $consumerSecret
     * @param string $tokenSecret
     * @return boolean
     */
    public function verifyRequest($consumerSecret, $tokenSecret = null)
    {
        $baseUri = $this->_buildBaseUri();
        $oauthParams = $this->_oauthRequestParams;
        $params = $this->_otherParams;
        $requestSignature = $oauthParams['oauth_signature'];
        unset($oauthParams['oauth_signature']);

        $params = array_merge($oauthParams, $params);

        $signature = $this->getHttpUtility()->sign($params,
            $oauthParams['oauth_signature_method'], $consumerSecret,
            $tokenSecret, $_SERVER['REQUEST_METHOD'], $baseUri);

        $this->_log('request signature: ' . $requestSignature);
        $this->_log('calculated signature: ' . $signature);
        return ($signature == $requestSignature);
    }

    protected function _buildBaseUri()
    {
        $this->_setupParams();
        // checking for allowed scheme
        $httpRequest = $this->getHttpRequest();
        $scheme = $httpRequest->getScheme();
        $host = $httpRequest->getHttpHost();
        $requestUriParts = explode('?', $httpRequest->getRequestUri());
        $requestUri = $requestUriParts[0];

        $baseUri = $scheme . '://' . $host . $requestUri;
        $this->_log('request uri: ' . $httpRequest->getRequestUri());
        $this->_log('rebuild request ouath base string uri: ' . $baseUri);
        return $baseUri;
    }

    protected function _setupParams()
    {
        // setups all oauth_params to internal property, also saves nonce
        // request to protect multiple requests
        foreach ($_GET as $key => $param) {
            if (preg_match('@oauth_(.*)@i', $key, $wyn)) {
                $this->_oauthRequestParams[$key] = $param;
            } else {
                $this->_otherParams[$key] = $param;
            }
        }
    }

    /**
     *
     * @return Zend_Controller_Request_Http
     */
    public function getHttpRequest()
    {
        if (is_null($this->_httpRequest)) {
            $this->_httpRequest = new Zend_Controller_Request_Http();
        }
        return $this->_httpRequest;
    }

    /**
     *
     * @return Zend_Oauth_Http_Utility
     */
    public function getHttpUtility()
    {
        if (is_null($this->_httpUtility)) {
            $this->_httpUtility = new Zend_Oauth_Http_Utility();
        }
        return $this->_httpUtility;
    }

    public function setupParams()
    {
        $this->_setupParams();
        return $this;
    }

    public function getOauthParams()
    {
        return $this->_oauthRequestParams;
    }

    public function getNonceUniqueSet()
    {
        return array(
            'key' => $this->_oauthRequestParams['oauth_consumer_key'],
            'nonce' => $this->_oauthRequestParams['oauth_nonce'],
            'timestamp' => $this->_oauthRequestParams['oauth_timestamp']
        );
    }
}
