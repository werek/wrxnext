<?php

abstract class wrxNext_Oauth_Provider_Request_Abstract implements wrxNext_Oauth_Provider_Request_Interface
{
    /**
     *
     * @var wrxNext_Oauth_Storage_TempCredentialsDbTable
     */
    protected $_tempCredentialStorage = null;
    protected $_oauthParamsRequired = array();
    protected $_nonceStorage = null;
    /**
     *
     * @var Zend_Oauth_Http_Utility
     */
    protected $_httpUtility = null;
    protected $_oauthRequestParams = array();
    protected $_otherParams = array();

    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
        $this->init();
    }

    /**
     * handles setting up options
     *
     * @param array $options
     * @return wrxNext_Oauth_Provider_Request_Abstract
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                $this->_options[$key] = $value;
            }
        }
        return $this;
    }

    public function init()
    {

    }

    /**
     * @return wrxNext_Oauth_Storage_TempCredentialsDbTable
     */
    public function getTempCredentialStorage()
    {
        if (is_null($this->_tempCredentialStorage)) {
            $this->_tempCredentialStorage = new wrxNext_Oauth_Storage_TempCredentialsDbTable();
        }
        return $this->_tempCredentialStorage;
    }

    /**
     *
     * @param wrxNext_Oauth_Storage_TempCredentialsDbTable $storage
     * @return wrxNext_Oauth_Provider_Request_Abstract
     */
    public function setTempCredentialStorage($storage)
    {
        $this->_tempCredentialStorage = $storage;
        return $this;
    }

    public function __get($name)
    {
        return isset($this->_oauthRequestParams[$name]) ? $this->_oauthRequestParams[$name] : null;
    }

    public function handle(Zend_Controller_Request_Http $request, Zend_Controller_Response_Http $response)
    {
        throw new wrxNext_Oauth_Provider_Exception('method handle need to have proper implementation');
    }

    protected function _verifySignature()
    {
        Zend_Registry::get('log')->debug('request uri: ' . $_SERVER['REQUEST_URI']);
        $baseUri = $this->_buildBaseUri();
        return true;
    }

    protected function _buildBaseUri()
    {
        $this->_setupParams();
        $httpRequest = new Zend_Controller_Request_Http();

    }

    protected function _setupParams()
    {
        //setups all oauth_params to internal property, also saves nonce request to protect multiple requests
        foreach ($_GET as $key => $param) {
            if (preg_match('@oauth_(.*)@i', $key, $wyn)) {
                $this->_oauthRequestParams[$wyn[1]] = $param;
            } else {
                $this->_otherParams[$key] = $param;
            }
        }
        $this->_saveNonce();
    }

    protected function _saveNonce()
    {
        $this->getNonceStorage()
            ->insert(array(
                'key' => $this->_oauthRequestParams['consumer_key'],
                'nonce' => $this->_oauthRequestParams['nonce'],
                'timestamp' => $this->_oauthRequestParams['timestamp']
            ));
    }

    public function getNonceStorage()
    {
        if (is_null($this->_nonceStorage)) {
            $this->_nonceStorage = new wrxNext_Oauth_Storage_NonceDbTable();
        }
        return $this->_nonceStorage;
    }

    public function setNonceStorage($storage)
    {
        $this->_nonceStorage = $storage;
        return $this;
    }

    protected function _buildBodyFromArray($array)
    {
        $this->getHttpUtility()->toEncodedQueryString($params);
    }

    /**
     *
     * @return Zend_Oauth_Http_Utility
     */
    public function getHttpUtility()
    {
        if (is_null($this->_httpUtility)) {
            $this->_httpUtility = new Zend_Oauth_Http_Utility();
        }
        return $this->_httpUtility;
    }
}
