<?php

interface wrxNext_Oauth_Provider_Request_Interface
{
    public function __construct($options = null);

    public function handle(Zend_Controller_Request_Http $request, Zend_Controller_Response_Http $response);
}
