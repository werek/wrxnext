<?php

class wrxNext_Oauth_Storage_ConsumerDbTable extends Zend_Db_Table
{
    protected $_name = 'oauth_consumer';

    public function getByConsumerKey($consumerKey)
    {
        return $this->fetchRow(array('consumer_key = ?' => $consumerKey));
    }
}
