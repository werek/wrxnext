<?php

/**
 * import result class
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Result
{

    /**
     * holds errors
     *
     * @var array
     */
    protected $_errors = array();

    /**
     * holds status of import
     *
     * @var boolean
     */
    protected $_result = TRUE;

    /**
     * adds error to stack
     *
     * @param mixed $message
     * @return wrxNext_Import_Result
     */
    public function addError($message)
    {
        $this->_result = false;
        $this->_errors[] = $message;
        return $this;
    }

    /**
     * return status of import
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->_result;
    }

    /**
     * returns errors array
     *
     * @return array
     */
    public function getErrors()
    {
        return $this->_errors;
    }

    /**
     * magic method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toString();
    }

    /**
     * renders whole message stack
     *
     * @return string
     */
    public function toString()
    {
        $string = ($this->_result) ? 'Operation Successful' : 'Operation failed';
        $string .= "\n\nerror messages:\n\n";
        foreach ($this->_errors as $value) {
            if ($value instanceof Exception) {
                $string .= $value->getFile() . ', line: ' . $value->getLine() .
                    '. message: ' . $value->getMessage() . ". trace: " .
                    $value->getTraceAsString() . "\n\n";
            } elseif (is_string($value)) {
                $string .= $value . "\n\n";
            } else {
                ob_start();
                var_dump($value);
                $string .= ob_end_clean() . "\n\n";
            }
        }
        return $string;
    }
}
