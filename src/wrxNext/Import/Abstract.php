<?php

/**
 * abstract class implementing Import interface
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
abstract class wrxNext_Import_Abstract implements wrxNext_Import_Interface
{

    /**
     * stores class name for write adapter
     *
     * @var string
     */
    protected $_writeAdapterClass = NULL;

    /**
     * stores class name for read adapter
     *
     * @var string
     */
    protected $_readAdapterClass = NULL;

    /**
     * write adapter instance
     *
     * @var wrxNext_Import_Writer_Interface
     */
    protected $_writeAdapter = null;

    /**
     * read adapter instance
     *
     * @var wrxNext_Import_Reader_Interface
     */
    protected $_readAdapter = null;

    /**
     * Contains options given to importer
     *
     * @var array
     */
    protected $_options = array();

    /**
     * basic constructor handles setting options
     *
     * @see wrxNext_Import_Writer_Interface::__construct()
     * @param array $options
     */
    public function __construct($options = NULL)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
        $this->init();
    }

    /**
     * setups options from Zend_Config object
     *
     * @param Zend_Config $config
     * @return wrxNext_Import_Abstract
     */
    public function setConfig(Zend_Config $config)
    {
        return $this->setOptions($config->toArray());
    }

    /**
     * basic setup method after instantiation
     */
    public function init()
    {
    }

    /**
     * returns array of options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * setups Options
     *
     * @param array $array
     * @return wrxNext_Import_Abstract
     */
    public function setOptions($array)
    {
        $array = (array)$array;
        foreach ($array as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                if (is_array($this->_options[$key])) {
                    $this->_options[$key] = array_merge_recursive(
                        $this->_options[$key], (array)$value);
                } else {
                    $this->_options[$key] = $value;
                }
            }
        }

        return $this;
    }

    /**
     * handles import
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Interface::import()
     * @return wrxNext_Import_Result
     */
    public function import()
    {
        $result = new wrxNext_Import_Result();
        try {
            while ($array = $this->getReadAdapter()->read()) {
                if (false === $this->getWriteAdapter()->write($array)) {
                    $result->addError(
                        'there was problem with writing data: ' .
                        Zend_Debug::dump($array, null, false));
                }
            }
        } catch (wrxNext_Import_Exception $e) {
            $result->addError($e);
        }
        return $result;
    }

    /**
     * returns current read adapter
     *
     * @return wrxNext_Import_Reader_Interface
     */
    public function getReadAdapter()
    {
        if (!$this->_readAdapter instanceof wrxNext_Import_Reader_Interface) {
            if (empty($this->_readAdapterClass)) {
                throw new wrxNext_Import_Exception(
                    'class for read adapter has not been defined', 3);
            }
            $readReflection = new ReflectionClass($this->_readAdapterClass);
            if (!$readReflection->implementsInterface(
                'wrxNext_Import_Reader_Interface')
            ) {
                throw new wrxNext_Import_Exception(
                    'defined reader class does not implements wrxNext_Import_Reader_Interface',
                    3);
            }
            $this->_readAdapter = $readReflection->newInstance(
                $this->getOption('reader'));
        }
        return $this->_readAdapter;
    }

    /**
     * sets read adapter
     *
     * @param wrxNext_Import_Reader_Interface $adapter
     * @return wrxNext_Import_Abstract
     */
    public function setReadAdapter(wrxNext_Import_Reader_Interface $adapter)
    {
        $this->_readAdapter = $adapter;
        return $this;
    }

    /**
     * returns current write adapter
     *
     * @return wrxNext_Import_Write_Interface
     */
    public function getWriteAdapter()
    {
        if (!$this->_writeAdapter instanceof wrxNext_Import_Writer_Interface) {
            if (empty($this->_writeAdapterClass)) {
                throw new wrxNext_Import_Exception(
                    'class for write adapter has not been defined', 1);
            }
            $writeReflection = new ReflectionClass($this->_writeAdapterClass);
            if (!$writeReflection->implementsInterface(
                'wrxNext_Import_Writer_Interface')
            ) {
                throw new wrxNext_Import_Exception(
                    'defined writer class does not implements wrxNext_Import_Writer_Interface',
                    2);
            }
            $this->_writeAdapter = $writeReflection->newInstance(
                $this->getOption('writer'));
        }
        return $this->_writeAdapter;
    }

    /**
     * sets write adapter
     *
     * @param wrxNext_Import_Writer_Interface $adapter
     * @return wrxNext_Import_Abstract
     */
    public function setWriteAdapter(wrxNext_Import_Writer_Interface $adapter)
    {
        $this->_writeAdapter = $adapter;
        return $this;
    }

    /**
     * instantiates adapters
     */
    protected function setupAdapters()
    {
        $readReflection = new ReflectionClass($this->_readAdapterClass);
        $this->_readAdapter = $readReflection->newInstance(
            $this->getOption('reader'));
        $writeReflection = new ReflectionClass($this->_writeAdapterClass);
        $this->_writeAdapter = $writeReflection->newInstance(
            $this->getOption('writer'));
    }

    /**
     * returns value of single option
     *
     * @param string $name
     * @return mixed
     */
    public function getOption($name)
    {
        if (isset($this->_options[$name]))
            return $this->_options[$name];
        return null;
    }
}
