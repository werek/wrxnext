<?php

class wrxNext_Import_Reader_PHPExcelReader extends wrxNext_Import_Reader_Abstract
{

    /**
     * holds reader class
     *
     * @var PHPExcel
     */
    protected $_reader = null;

    /**
     * index of sheet to use
     *
     * @var int
     */
    protected $_sheet = 0;

    /**
     * holds number of last read row, 0 for none
     *
     * @var int
     */
    protected $_lastReadRow = 0;

    /**
     * stores filepath for read
     *
     * @var string
     */
    protected $_filePath = null;

    /**
     * count read lines
     *
     * @var int
     */
    protected $_readLines = 0;

    /**
     * keeps mapping of field name to return position
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * flag for auto discovery of field names, works only when
     * ommit header is set to true
     *
     * @var boolean
     */
    protected $_autoDiscover = TRUE;

    /**
     * flag for ommiting first line in file
     *
     * @var boolean
     */
    protected $_ommitHeader = false;

    /**
     * holds status of ommited header
     *
     * @var boolean
     */
    protected $_headerOmmited = false;

    /**
     * keeps csv file internal encoding
     *
     * @var string
     */
    protected $_inEncoding = 'cp1250';

    /**
     * keeps output encoding
     *
     * @var string
     */
    protected $_outEncoding = 'utf-8';

    /**
     * status wheter or not convert encoding
     *
     * @var boolean
     */
    protected $_convertEncoding = false;

    /**
     * resets reader
     *
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function reset()
    {
        $this->_reader = null;
        $this->_lastReadRow = 0;
        return $this;
    }

    /**
     * sets flag to skip first line
     *
     * @param boolean $skip
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setSkipHeader($skip)
    {
        $this->_ommitHeader = $skip;
        return $this;
    }

    /**
     * gets flag to skip first line
     *
     * @return boolean
     */
    public function getSkipHeader()
    {
        return $this->_ommitHeader;
    }

    /**
     * gets filepath to read
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filePath;
    }

    /**
     * sets filepath to read
     *
     * @param string $filePath
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setFilePath($filePath)
    {
        $this->_filePath = $filePath;
        return $this;
    }

    /**
     * returns array of currently used field names
     *
     * @return array
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * sets field names for maping recognized fields in csv
     *
     * @param array $array
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setFields($array)
    {
        $array = (array)$array;
        $this->_fields = array();
        foreach ($array as $value) {
            $this->_fields[] = $value;
        }
        return $this;
    }

    /**
     * sets sheet index
     *
     * @param int $index
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setSheetIndex($index)
    {
        $this->_sheet = $index;
        return $this;
    }

    /**
     * gets sheet index
     *
     * @return int
     */
    public function getSheetIndex()
    {
        return $this->_sheet;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_Abstract::read()
     */
    public function read()
    {
        if ($this->_ommitHeader && !$this->_headerOmmited) {
            $this->_lastReadRow++;
            $this->_headerOmmited = true;
        }
        if ($this->_lastReadRow >= $this->_sheet()->getHighestRow()) {
            return false;
        }
        $this->_lastReadRow++;
        $range = 'A' . $this->_lastReadRow . ':' .
            $this->_sheet()->getHighestColumn() . $this->_lastReadRow;
        $row = $this->_sheet()->rangeToArray($range, null, true, false)[0];
        $this->_readLines++;
        return $this->_mapToFields($row);
    }

    /**
     * returns sheet to use
     *
     * @return PHPExcel_Worksheet
     */
    protected function _sheet()
    {
        return $this->_reader()->getSheet($this->_sheet);
    }

    /**
     * opens current filepath for reading
     *
     * @return PHPExcel
     */
    protected function _reader()
    {
        if (!$this->_reader instanceof PHPExcel) {
            $this->_reader = PHPExcel_IOFactory::load($this->_filePath);
        }
        return $this->_reader;
    }

    /**
     * maps index values to field names
     *
     * @param array $row
     * @return array
     */
    protected function _mapToFields($row)
    {
        $tmp = array();
        foreach ($row as $key => $value) {
            $value = $this->convertEncoding($value);
            if (isset($this->_fields[$key])) {
                $tmp[$this->_fields[$key]] = $value;
            } else {
                $tmp[$key] = $value;
            }
        }
        return $tmp;
    }

    /**
     * converts encoding
     *
     * @param string $string
     * @return string
     */
    protected function convertEncoding($string)
    {
        if ($this->getConvertEncoding() && function_exists('iconv')) {
            $string = iconv($this->getInEncoding(), $this->getOutEncoding(),
                $string);
        }
        return $string;
    }

    /**
     * returns status of convert encoding
     *
     * @return boolean
     */
    public function getConvertEncoding()
    {
        return $this->_convertEncoding;
    }

    /**
     * sets wheter to convert encoding
     *
     * @param boolean $bool
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setConvertEncoding($bool = false)
    {
        $this->_convertEncoding = $bool;
        return $this;
    }

    /**
     * gets input encoding
     *
     * @return string
     */
    public function getInEncoding()
    {
        return $this->_inEncoding;
    }

    /**
     * sets input encoding
     *
     * @param string $encoding
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setInEncoding($encoding = 'cp1250')
    {
        $this->_inEncoding = $encoding;
        return $this;
    }

    /**
     * gets output encoding
     *
     * @return string
     */
    public function getOutEncoding()
    {
        return $this->_outEncoding;
    }

    /**
     * sets output encoding
     *
     * @param string $encoding
     * @return wrxNext_Import_Reader_PHPExcelReader
     */
    public function setOutEncoding($encoding = 'utf-8')
    {
        $this->_outEncoding = $encoding;
        return $this;
    }

    /**
     * returns number of read rows
     *
     * @return int
     */
    public function getReadRows()
    {
        return $this->_readLines;
    }

    /**
     * handles the auto discovery of field names
     *
     * @param string $string
     */
    protected function discoverFields($string)
    {
        if ($this->_autoDiscover) {
            // @todo convert incoming data into fields for array
        }
    }
}
