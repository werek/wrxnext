<?php

/**
 * exception for Import Reder Related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_Exception extends wrxNext_Import_Exception
{
}
