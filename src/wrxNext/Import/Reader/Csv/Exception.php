<?php

/**
 * exception for import csv reader related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_Csv_Exception extends wrxNext_Import_Reader_File_Exception
{

}
