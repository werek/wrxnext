<?php

/**
 * exception for import reader file related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_File_Exception extends wrxNext_Import_Reader_Exception
{
}
