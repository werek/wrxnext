<?php

/**
 * csv reader, does not strips enclosure
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_CsvWithoutEnclosure extends wrxNext_Import_Reader_Csv
{
    /**
     * holds encosure mark
     *
     * @var string
     */
    protected $_enclosure = '';

    /**
     * @inheritdoc
     */
    protected function isValid($string)
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected function quoteDelimiter($string)
    {
        return $string;
    }

    /**
     * @inheritdoc
     */
    protected function unquoteDelimiter($string)
    {
        return $string;
    }

    /**
     * @inheritdoc
     */
    protected function failsafeRead()
    {
        if (!is_resource($this->_fp))
            $this->fopen();
        if (!feof($this->_fp)) {
            $string = fgetc($this->_fp, 4096);
            if ($this->getConvertEncoding() && function_exists('iconv')) {
                $string = iconv($this->getInEncoding(), $this->getOutEncoding(), $string);
            }
            return $string;
        }
        return false;
    }
}
