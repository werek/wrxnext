<?php

/**
 * Zend_Db_Select reader for import
 *
 * @author bweres01
 *
 */
class wrxNext_Import_Reader_DbSelect extends wrxNext_Import_Reader_Abstract
{

    /**
     * holds query statement
     *
     * @var Zend_Db_Statement
     */
    protected $_stmt = null;

    /**
     * holds select object
     *
     * @var Zend_Db_Select
     */
    protected $_select = null;

    /**
     * reader constructor, accept select object directly or set of options
     *
     * @param array|Zend_Config|Zend_Db_Select $options
     */
    public function __construct($options = NULL)
    {
        if ($options instanceof Zend_Db_Select) {
            $optArray['select'] = $options;
            $options = $optArray;
        }
        parent::__construct($options);
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_Abstract::read()
     */
    public function read()
    {
        $this->_query();
        return $this->_stmt->fetch();
    }

    /**
     * queries current select object and sets return statement
     */
    protected function _query()
    {
        if (is_null($this->_stmt)) {
            $this->_stmt = $this->getSelect()->query(Zend_Db::FETCH_ASSOC);
        }
    }

    /**
     * gets select object
     *
     * @return Zend_Db_Select
     */
    public function getSelect()
    {
        return $this->_select;
    }

    /**
     * sets select object
     *
     * @param Zend_Db_Select $select
     * @return wrxNext_Import_Reader_DbSelect
     */
    public function setSelect($select)
    {
        $this->_select = $select;
        return $this;
    }

    /**
     * resets current query
     *
     * @return wrxNext_Import_Reader_DbSelect
     */
    public function reset()
    {
        $this->_stmt = null;
        return $this;
    }
}
