<?php

/**
 * implementation using php fgetcsv
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_CsvNative extends wrxNext_Import_Reader_Csv
{

    /**
     * reads one row of csv file and returns its contents as array
     *
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_File::read()
     */
    public function read()
    {
        $array = $this->failsafeRead();
        if ($this->getOmmitHeader() && !$this->_headerOmmited) {
            $this->discoverFields($array);
            $array = $this->failsafeRead();
            $this->_headerOmmited = true;
        }
        if ($array !== false && is_array($array))
            return $this->parse($array);
        return false;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_Csv::failsafeRead()
     */
    protected function failsafeRead()
    {
        if (!is_resource($this->_fp))
            $this->fopen();
        if (!feof($this->_fp)) {
            $array = fgetcsv($this->_fp, 16384, $this->getDelimiter(),
                $this->getEnclosure());
            if (is_array($array)) {
                $array = array_map(
                    array(
                        $this,
                        'convertEncoding'
                    ), $array);
            }
            return $array;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function discoverFields($array)
    {
        $string = implode($this->getDelimiter(), (array)$array);
        parent::discoverFields($string);
    }

    /**
     * @inheritdoc
     */
    public function parse($array)
    {
        $return = array();
        for ($i = 0, $_i = count($array); $i < $_i; $i++) {
            $key = (isset($this->_fields[$i]) && !empty($this->_fields[$i])) ? $this->_fields[$i] : $i;
            $return[$key] = trim($array[$i]);
        }
        $this->_readLines++;
        return $return;
    }
}
