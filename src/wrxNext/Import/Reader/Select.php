<?php

/**
 * Zend_Db_Select reader
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_Select extends wrxNext_Import_Reader_Abstract
{
    /**
     * holds select object
     *
     * @var Zend_Db_Select
     */
    protected $_dbSelect;
    /**
     * holds executed select statement
     *
     * @var PDOStatement|Zend_Db_Statement
     */
    protected $_stmt;

    /**
     * overriden constructor also accepting direct pass of zend_db_select object
     *
     * @param array|Zend_Db_Select $options
     */
    public function __construct($options)
    {
        if ($options instanceof Zend_Db_Select) {
            $options['dbSelect'] = $options;
        }
        parent::__construct($options);
    }

    /**
     * returns current select object
     *
     * @return Zend_Db_Select
     */
    public function getDbSelect()
    {
        return $this->_dbSelect;
    }

    /**
     * sets select object
     *
     * @param Zend_Db_Select $select
     * @return wrxNext_Import_Reader_Select
     */
    public function setDbSelect(Zend_Db_Select $select)
    {
        $this->_dbSelect = $select;
        return $this;
    }

    /**
     * returns executed statement object
     *
     * @return PDOStatement|Zend_Db_Statement
     */
    public function getStatement()
    {
        return $this->_stmt;
    }

    /**
     * (non-PHPdoc)
     * @see wrxNext_Import_Reader_Abstract::read()
     * @return array
     */
    public function read()
    {
        if (is_null($this->_stmt)) {
            $this->execute();
        }
        return $this->_stmt->fetch(Zend_Db::FETCH_ASSOC);
    }

    /**
     * executes select
     *
     */
    protected function execute()
    {
        $this->_stmt = $this->_dbSelect->query();
    }
}
