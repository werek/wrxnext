<?php

/**
 * file import adapter for reading file line by line
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_File extends wrxNext_Import_Reader_Abstract
{

    /**
     * Keeps filepath
     *
     * @var string
     */
    protected $_filename = null;

    /**
     * Keeps file handle
     *
     * @var resource
     */
    protected $_fp;

    /**
     * constructor, can handle array of options or just string, if string is
     * given
     * it will be treated as path to file
     *
     * @param array|string $options
     */
    public function __construct($options = null)
    {
        if (is_string($options)) {
            $options['filePath'] = $options;
        }
        parent::__construct($options);
    }

    /**
     * set file path
     *
     * @param string $path
     * @return wrxNext_Import_Reader_File
     */
    public function setFilePath($path)
    {
        $this->_filename = $path;
        return $this;
    }

    /**
     * returns file path
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filename;
    }

    /**
     * Main method, reads line and returns it as array with single element
     *
     * @see wrxNext_Import_Reader_Abstract::read()
     * @return array
     */
    public function read()
    {
        if (!is_resource($this->_fp))
            $this->fopen();
        if (!feof($this->_fp))
            return array(
                fgets($this->_fp, 4096)
            );
        return false;
    }

    /**
     * opens file handle
     *
     * @return resource
     */
    protected function fopen()
    {
        $this->_fp = @fopen($this->_filename, 'r');
        if ($this->_fp === false) {
            throw new wrxNext_Import_Reader_File_Exception(
                'there was a problem during fopen of file "' .
                $this->_filename . '"', 2);
        }
        return $this->_fp;
    }

    /**
     * closes filepointer
     *
     * @return wrxNext_Import_Reader_File
     */
    public function reset()
    {
        if (is_resource($this->_fp))
            fclose($this->_fp);
        return $this;
    }

    /**
     * closes file handle
     *
     * @return wrxNext_Import_Reader_File
     */
    protected function fclose()
    {
        fclose($this->_fp);
        return $this;
    }
}
