<?php

/**
 * interface for import reader objects
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Import_Reader_Interface
{
    /**
     * constructor with optional parameters
     *
     * @param array $options
     */
    public function __construct($options = null);

    /**
     * reads on full entity and returns as array
     *
     * @return array
     */
    public function read();
}
