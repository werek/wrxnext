<?php

/**
 * csv reader, with translation
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Reader_Csv extends wrxNext_Import_Reader_File
{

    /**
     * count read lines
     *
     * @var int
     */
    protected $_readLines = 0;

    /**
     * keeps mapping of field name to return position
     *
     * @var array
     */
    protected $_fields = array();

    /**
     * flag for auto discovery of field names, works only when
     * ommit header is set to true
     *
     * @var boolean
     */
    protected $_autoDiscover = TRUE;

    /**
     * flag for ommiting first line in file
     *
     * @var boolean
     */
    protected $_ommitHeader = TRUE;

    /**
     * informs wheter header have been ommited
     *
     * @var boolean
     */
    protected $_headerOmmited = false;

    /**
     * csv field delimiter
     *
     * @var string
     */
    protected $_delimiter = ';';

    /**
     * holda temporary mark for hidden delimiter chars inside enclosed
     * field value
     *
     * @var string
     */
    protected $_quoteDelimiterMark = '__QUOTED_DELIMITER___';

    /**
     * csv field enclosure mark
     *
     * @var string
     */
    protected $_enclosure = '"';

    /**
     * keeps csv file internal encoding
     *
     * @var string
     */
    protected $_inEncoding = 'cp1250';

    /**
     * keeps output encoding
     *
     * @var string
     */
    protected $_outEncoding = 'utf-8';

    /**
     * status wheter or not convert encoding
     *
     * @var boolean
     */
    protected $_convertEncoding = false;

    /**
     * sets wheter to use auto discovery of header field names
     * works in conjunction with ommit header set to true
     *
     * @param boolean $bool
     * @return wrxNext_Import_Reader_Csv
     */
    public function setAutoDiscovery($bool = true)
    {
        $this->_autoDiscover = $bool;
        return $this;
    }

    /**
     * returns auto discovery flag status
     *
     * @return boolean
     */
    public function getAutoDiscovery()
    {
        return $this->_autoDiscover;
    }

    /**
     * returns array of currently used field names
     *
     * @return array
     */
    public function getFields()
    {
        return $this->_fields;
    }

    /**
     * sets field names for maping recognized fields in csv
     *
     * @param array $array
     * @return wrxNext_Import_Reader_Csv
     */
    public function setFields($array)
    {
        $array = (array)$array;
        $this->_fields = array();
        foreach ($array as $value) {
            $this->_fields[] = $value;
        }
        return $this;
    }

    /**
     * returns number of read lines
     *
     * @return int
     */
    public function getReadLines()
    {
        return $this->_readLines;
    }

    /**
     * reads one row of csv file and returns its contents as array
     *
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_File::read()
     */
    public function read()
    {
        $string = trim($this->failsafeRead());
        if ($this->getOmmitHeader() && !$this->_headerOmmited) {
            $this->discoverFields($string);
            $string = $this->failsafeRead();
            $this->_headerOmmited = true;
        }
        if (!empty($string))
            return $this->parse($string);
        return false;
    }

    /**
     * method for reading csv files with new lines in fields
     *
     * @return string
     */
    protected function failsafeRead()
    {
        if (!is_resource($this->_fp))
            $this->fopen();
        if (!feof($this->_fp)) {
            $string = '';
            $inEnclosedValue = false;
            $return = false;
            do {
                $char = fgets($this->_fp, 4096);
                if (substr_count($char, $this->getEnclosure()) % 2 == 1) {
                    $inEnclosedValue = ($inEnclosedValue) ? false : true;
                }
                if (!$inEnclosedValue) {
                    $return = true;
                }
                $string .= $char;
            } while (!$return);
            return $this->convertEncoding($string);
        }
        return false;
    }

    /**
     * returns enclosure character
     *
     * @return string
     */
    public function getEnclosure()
    {
        return $this->_enclosure;
    }

    /**
     * sets enclosure character
     *
     * @param string $enclosure
     * @return wrxNext_Import_Reader_Csv
     */
    public function setEnclosure($enclosure = '"')
    {
        $this->_enclosure = $enclosure;
        return $this;
    }

    /**
     * converts encoding
     *
     * @param string $string
     * @return string
     */
    protected function convertEncoding($string)
    {
        if ($this->getConvertEncoding() && function_exists('iconv')) {
            $string = iconv($this->getInEncoding(), $this->getOutEncoding(),
                $string);
        }
        return $string;
    }

    /**
     * returns status of convert encoding
     *
     * @return boolean
     */
    public function getConvertEncoding()
    {
        return $this->_convertEncoding;
    }

    /**
     * sets wheter to convert encoding
     *
     * @param boolean $bool
     * @return wrxNext_Import_Reader_Csv
     */
    public function setConvertEncoding($bool = false)
    {
        $this->_convertEncoding = $bool;
        return $this;
    }

    /**
     * gets input encoding
     *
     * @return string
     */
    public function getInEncoding()
    {
        return $this->_inEncoding;
    }

    /**
     * sets input encoding
     *
     * @param string $encoding
     * @return wrxNext_Import_Reader_Csv
     */
    public function setInEncoding($encoding = 'cp1250')
    {
        $this->_inEncoding = $encoding;
        return $this;
    }

    /**
     * gets output encoding
     *
     * @return string
     */
    public function getOutEncoding()
    {
        return $this->_outEncoding;
    }

    /**
     * sets output encoding
     *
     * @param string $encoding
     * @return wrxNext_Import_Reader_Csv
     */
    public function setOutEncoding($encoding = 'utf-8')
    {
        $this->_outEncoding = $encoding;
        return $this;
    }

    /**
     * returns ommit headers flag status
     *
     * @return boolean
     */
    public function getOmmitHeader()
    {
        return $this->_ommitHeader;
    }

    /**
     * sets flag to ommit header
     *
     * @param boolean $bool
     * @return wrxNext_Import_Reader_Csv
     */
    public function setOmmitHeader($bool = true)
    {
        $this->_ommitHeader = $bool;
        return $this;
    }

    /**
     * handles the auto discovery of field names
     *
     * @param string $string
     */
    protected function discoverFields($string)
    {
        if ($this->_autoDiscover) {
            $this->_fields = array();
            $t = explode($this->getDelimiter(), $string);
            foreach ($t as $field) {
                $this->_fields[] = $field;
            }
        }
    }

    /**
     * returns current delimiter
     *
     * @return string
     */
    public function getDelimiter()
    {
        return $this->_delimiter;
    }

    /**
     * setups csv field delimiter
     *
     * @param string $delimiter
     * @return wrxNext_Import_Reader_Csv
     */
    public function setDelimiter($delimiter = ';')
    {
        $this->_delimiter = $delimiter;
        return $this;
    }

    /**
     * convert string representation to array notation
     *
     * @param string $string
     * @return array
     */
    protected function parse($string)
    {
        $string = $this->quoteDelimiter($string);
        if (!$this->isValid($string)) {
            throw new wrxNext_Import_Reader_Csv_Exception(
                'line format does not match, expecting delimiter: ' .
                $this->getDelimiter() . ', enclosure:  ' .
                $this->getEnclosure(), 1);
        }
        $temp = explode($this->getDelimiter(), $string);
        $return = array();
        for ($i = 0, $_i = count($temp); $i < $_i; $i++) {
            $key = (isset($this->_fields[$i]) && !empty($this->_fields[$i])) ? $this->_fields[$i] : $i;
            $return[$key] = $this->unquoteDelimiter(
                trim(trim($temp[$i], "\n"), $this->getEnclosure()));
        }
        $this->_readLines++;
        return $return;
    }

    /**
     * quotes any delimiter characters occuring in enclosed values for safety
     * during
     * field extractions
     *
     * @param string $string
     * @return string
     */
    protected function quoteDelimiter($string)
    {
        $haystack = 0;
        $inEnclosedValue = false;
        $tempString = '';
        do {
            $startEnclosure = strpos($string, $this->getEnclosure(), $haystack);
            if (false !== $startEnclosure) {
                $inEnclosedValue = ($inEnclosedValue) ? false : true;
                $endEnclosure = strpos($string, $this->getEnclosure(),
                    $startEnclosure + 1);
                $endEnclosure = ($endEnclosure === false) ? (strlen($string)) : $endEnclosure;
                $enclosedString = substr($string, $startEnclosure,
                    $endEnclosure - $startEnclosure);
                if (strpos($enclosedString, $this->getDelimiter()) !== false &&
                    $inEnclosedValue
                ) {
                    $enclosedString = str_replace($this->getDelimiter(),
                        $this->_quoteDelimiterMark, $enclosedString);
                }
                $tempString .= $enclosedString;
                $haystack = $endEnclosure;
            } else {
                $tempString .= substr($string, $haystack);
                $haystack = strlen($string);
            }
        } while ($haystack < strlen($string));
        $string = $tempString;
        return $string;
    }

    /**
     * checks wheter line have correct numebr of delimiter/enclosure chars
     *
     * @param string $string
     * @return boolean
     */
    protected function isValid($string)
    {
        $enclosureCount = substr_count($string, $this->getEnclosure());
        if ($enclosureCount == 0 || $enclosureCount % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * replaces any quoted delimiter charcters to original state
     *
     * @param string $string
     * @return string
     */
    protected function unquoteDelimiter($string)
    {
        $string = str_replace($this->_quoteDelimiterMark, $this->getDelimiter(),
            $string);
        return $string;
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Import_Reader_File::reset()
     */
    public function reset()
    {
        $this->_headerOmmited = false;
        return parent::reset();
    }
}
