<?php

/**
 * exception for Import Related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Exception extends Exception
{
}
