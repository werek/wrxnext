<?php

/**
 * interface for Import writer
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Import_Writer_Interface
{
    /**
     * basic constructor with optional parameters
     *
     * @param array $options
     */
    public function __construct($options = null);

    /**
     * main method to write data, returns true on success and false on failure
     *
     * @param array $array
     * @return boolean
     */
    public function write($array);
}
