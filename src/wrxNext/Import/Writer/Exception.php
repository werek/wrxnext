<?php

/**
 * exception for Import Writer related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Writer_Exception extends wrxNext_Import_Exception
{
}
