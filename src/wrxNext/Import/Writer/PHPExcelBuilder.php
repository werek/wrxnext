<?php

class wrxNext_Import_Writer_PHPExcelBuilder extends wrxNext_Import_Writer_File
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds exporter name
     *
     * @var string
     */
    protected $_format = 'Excel2007';

    /**
     * holds key=>column mapping
     *
     * @var array
     */
    protected $_columnKeyMapping = array();

    /**
     * holds current line number
     *
     * @var int
     */
    protected $_currentLine = 1;

    /**
     * holds values to write as header
     *
     * @var array
     */
    protected $_header = array();

    /**
     * flag for writing header
     *
     * @var boolean
     */
    protected $_writeHeader = false;

    /**
     * flag for header
     *
     * @var boolean
     */
    protected $_headerWritten = false;

    /**
     * holds field names to write, if empty then assumes all
     *
     * @var array
     */
    protected $_useFields = array();

    /**
     * holds PHPExcel object instance
     *
     * @var PHPExcel
     */
    protected $_excelDoc = null;

    /**
     * sets names of fields to use
     *
     * @param array $names
     * @return wrxNext_Import_Writer_PHPExcelBuilder
     */
    public function setUseFieldNames(array $names)
    {
        $this->_useFields = $names;
        return $this;
    }

    /**
     * gets names of fields to use
     *
     * @return array
     */
    public function getUseFieldNames()
    {
        return $this->_useFields;
    }

    /**
     * gets flag for writing header
     *
     * @return boolean
     */
    public function getWriteHeader()
    {
        return $this->_writeHeader;
    }

    /**
     * sets flag for writing header
     *
     * @param boolean $flag
     * @return wrxNext_Import_Writer_PHPExcelBuilder
     */
    public function setWriteHeader($flag)
    {
        $this->_writeHeader = $flag;
        return $this;
    }

    /**
     * sets header fields values
     *
     * @param array $values
     * @return wrxNext_Import_Writer_PHPExcelBuilder
     */
    public function setHeaderFieldValues($values)
    {
        $this->_header = $values;
        return $this;
    }

    /**
     * gets header fields values
     *
     * @return array
     */
    public function getHeaderFieldValues()
    {
        return $this->_header;
    }

    /**
     * @inheritdoc
     */
    public function write($array)
    {
        if ($this->_writeHeader && !$this->_headerWritten) {
            $this->_writeHeader($array);
        }
        $this->_write($array);
        return true;
    }

    protected function _writeHeader($array)
    {
        if (count($this->_header)) {
            $header = array_values($this->_header);
        } else {
            $header = array_keys($array);
        }
        $this->_write($header, true);
        $this->_headerWritten = true;
    }

    /**
     * writes row to active sheet
     *
     * @param array $array
     * @param bool $ignoreUseFields
     */
    protected function _write($array, $ignoreUseFields = false)
    {
        if (count($this->_useFields) && !$ignoreUseFields) {
            $fieldNames = array_values($this->_useFields);
        } else {
            $fieldNames = array_keys($array);
        }
        foreach ($fieldNames as $index => $name) {
            if (array_key_exists($name, $array)) {
                $address = $this->_columnIndex($index) . $this->_currentLine;
                $this->getExcelDoc()
                    ->getActiveSheet()
                    ->setCellValue($address, $array[$name]);
            }
        }
        $this->_currentLine++;
    }

    /**
     * changes column index from numerical 0-based to literal A-based
     *
     * @param int $index
     * @return string
     */
    protected function _columnIndex($index)
    {
        if (array_key_exists($index, $this->_columnKeyMapping)) {
            $column = $this->_columnKeyMapping[$index];
        } else {
            $workIndex = $index;
            for ($column = ""; $workIndex >= 0; $workIndex = intval(
                    $workIndex / 26) - 1) {
                $column = chr($workIndex % 26 + 0x41) . $column;
            }
            $this->_columnKeyMapping[$index] = $column;
        }
        return $column;
    }

    /**
     * gets Excel document
     *
     * @return PHPExcel
     */
    public function getExcelDoc()
    {
        if (!$this->_excelDoc instanceof PHPExcel) {
            $this->_excelDoc = new PHPExcel();
            $this->_excelDoc->setActiveSheetIndex(0);
            $this->_excelDoc->getProperties()->setCreator('PHPExcelBuilder');
        }
        return $this->_excelDoc;
    }

    /**
     * sets Excel document
     *
     * @param PHPExcel $doc
     * @return wrxNext_Import_Writer_PHPExcelBuilder
     */
    public function setExcelDoc(PHPExcel $doc)
    {
        $this->_excelDoc = $doc;
        return $this;
    }

    /**
     * saves builded sheet
     *
     * @param string $path
     */
    public function save($path = null)
    {
        $writer = PHPExcel_IOFactory::createWriter($this->getExcelDoc(),
            $this->getFormat());
        if (is_null($path)) {
            $path = $this->getFilePath();
        }
        $writer->save($path);
    }

    /**
     * gets writer format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->_format;
    }

    /**
     * sets writer format
     *
     * @param string $writerName
     * @return wrxNext_Import_Writer_PHPExcelBuilder
     */
    public function setFormat($writerName)
    {
        $this->_format = $writerName;
        return $this;
    }
}
