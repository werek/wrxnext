<?php

class wrxNext_Import_Writer_CsvMap extends wrxNext_Import_Writer_Csv
{

    /**
     * holds map of column used for writing
     *
     * @var array
     */
    protected $_columnMap = array();

    /**
     * sets columns used for writing
     *
     * @param array $columns
     * @return wrxNext_Import_Writer_CsvMap
     */
    public function setColumnsMap($columns)
    {
        $this->_columnMap = $columns;
        return $this;
    }

    /**
     * gets columns used for writing
     *
     * @return array
     */
    public function getColumnsMap()
    {
        return $this->_columnMap;
    }

    /**
     * @inheritdoc
     */
    public function parse($array)
    {
        $array = $this->_mapColumns($array);
        return parent::parse($array);
    }

    /**
     * maps $array data to specified columns
     *
     * @param array $array
     * @return array
     */
    protected function _mapColumns($array)
    {
        $data = array();
        foreach ($this->_columnMap as $column) {
            $data[$column] = (isset($array[$column])) ? $array[$column] : null;
        }
        return $data;
    }
}
