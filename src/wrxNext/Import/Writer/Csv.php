<?php

/**
 * csv writer
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Writer_Csv extends wrxNext_Import_Writer_File
{
    /**
     * holds flag wheter or not write header from input array keys
     *
     * @var bool
     */
    protected $_writeHeader = false;
    /**
     * holds flag wheter or not header have been writen
     *
     * @var bool
     */
    protected $_headerWriten = false;
    /**
     * holds enclosure
     *
     * @var string
     */
    protected $_enclosure = '"';
    /**
     * holds field separator
     *
     * @var string
     */
    protected $_separator = ';';
    /**
     * flag for commiting each write with new line
     *
     * @var bool
     */
    protected $_writeNewline = true;

    /**
     * @inheritdoc
     */
    public function parse($array)
    {
        $tmp = array();
        if ($this->getWriteHeader() && !$this->_headerWriten) {
            $this->_headerWriten = true;
            $this->write(array_keys($array));
        }
        foreach ($array as $value) {
            if (is_array($value)) {
                $tmp [] = $this->getEnclosure() . parent::parse($value) . $this->getEnclosure();
            } else {
                $tmp [] = $this->getEnclosure() . ( string )$value . $this->getEnclosure();
            }
        }
        return implode($this->getSeparator(), $tmp);
    }

    /**
     * returns status of writing header
     *
     * @return bool
     */
    public function getWriteHeader()
    {
        return $this->_writeHeader;
    }

    /**
     * sets writing of header from keys
     *
     * @param bool $bool
     * @return wrxNext_Import_Writer_Csv
     */
    public function setWriteHeader($bool)
    {
        $this->_writeHeader = ($bool) ? true : false;
        return $this;
    }

    /**
     * returns current enclosure string
     *
     * @return string
     */
    public function getEnclosure()
    {
        return $this->_enclosure;
    }

    /**
     * sets enclosure string
     *
     * @param string $string
     * @return wrxNext_Import_Writer_Csv
     */
    public function setEnclosure($string)
    {
        $this->_enclosure = ( string )$string;
        return $this;
    }

    /**
     * returns current field separator
     *
     * @return string
     */
    public function getSeparator()
    {
        return $this->_separator;
    }

    /**
     * sets field separator
     *
     * @param string $string
     * @return wrxNext_Import_Writer_Csv
     */
    public function setSeparator($string)
    {
        $this->_separator = ( string )$string;
        return $this;
    }
}
