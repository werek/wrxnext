<?php

class wrxNext_Import_Writer_SessionCollection extends wrxNext_Import_Writer_Abstract
{

    /**
     * holds named session
     *
     * @var string
     */
    protected $_sessionName = __CLASS__;

    /**
     * holds session colection object
     *
     * @var Zend_Session_Namespace
     */
    protected $_sessionCollection = null;

    /**
     * @inheritdoc
     */
    public function write($array)
    {
        $this->getSessionCollection()->data[] = $array;
        return true;
    }

    /**
     * gets session collection
     *
     * @return Zend_Session_Namespace
     */
    public function getSessionCollection()
    {
        if (!$this->_sessionCollection instanceof Zend_Session_Namespace) {
            $this->_sessionCollection = new Zend_Session_Namespace(
                $this->getSessionName());
        }
        return $this->_sessionCollection;
    }

    /**
     * sets session collection
     *
     * @param Zend_Session_Namespace $sessionNamespace
     * @return wrxNext_Import_Writer_SessionCollection
     */
    public function setSessionCollection(
        Zend_Session_Namespace $sessionNamespace)
    {
        $this->_sessionCollection = $sessionNamespace;
        return $this;
    }

    /**
     * gets session name
     *
     * @return string
     */
    public function getSessionName()
    {
        return $this->_sessionName;
    }

    /**
     * sets session name
     *
     * @param string $name
     * @return wrxNext_Import_Writer_SessionCollection
     */
    public function setSessionName($name)
    {
        $this->_sessionName = $name;
        return $this;
    }
}
