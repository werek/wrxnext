<?php

/**
 * import writer class which collects data to write
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Writer_Collector extends wrxNext_Import_Writer_Abstract
{
    /**
     * holds collected data
     *
     * @var mixed
     */
    protected $_collectedData = array();

    /**
     * @inheritdoc
     */
    public function write($array)
    {
        $this->_collectedData[] = $array;
        return true;
    }

    /**
     * returns array of collected data
     */
    public function getCollection()
    {
        return $this->_collectedData;
    }
}
