<?php

/**
 * abstract import writer class implementing import writer interface
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
abstract class wrxNext_Import_Writer_Abstract implements wrxNext_Import_Writer_Interface
{

    /**
     * Contains options given to writer
     *
     * @var array
     */
    protected $_options = array();

    /**
     * basic constructor handles setting options
     *
     * @see wrxNext_Import_Writer_Interface::__construct()
     * @param array $options
     */
    public function __construct($options = NULL)
    {
        if ($options instanceof Zend_Config) {
            $this->setConfig($options);
        } elseif (is_array($options)) {
            $this->setOptions($options);
        }
        $this->init();
    }

    /**
     * setups options from Zend_Config object
     *
     * @param Zend_Config $config
     * @return wrxNext_Import_Writer_Abstract
     */
    public function setConfig(Zend_Config $config)
    {
        return $this->setOptions($config->toArray());
    }

    /**
     * simple method for post-construct actions
     */
    public function init()
    {
    }

    /**
     * returns array of options
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * setups Options
     *
     * @param array $array
     * @return wrxNext_Import_Writer_Abstract
     */
    public function setOptions($array)
    {
        $array = (array)$array;
        foreach ($array as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                if (is_array($this->_options[$key])) {
                    $this->_options[$key] = array_merge_recursive(
                        $this->_options[$key], (array)$value);
                } else {
                    $this->_options[$key] = $value;
                }
            }
        }

        return $this;
    }

    /**
     * returns value of single option
     *
     * @param string $name
     * @return mixed
     */
    public function getOption($name)
    {
        if (isset($this->_options[$name]))
            return $this->_options[$name];
        return null;
    }

    /**
     * @inheritdoc
     */
    abstract public function write($array);
}
