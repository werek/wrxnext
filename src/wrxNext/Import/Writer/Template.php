<?php

class wrxNext_Import_Writer_Template extends wrxNext_Import_Writer_Abstract
{
    use wrxNext_Trait_Template;

    /**
     * holds calback for filter
     *
     * @var callable
     */
    protected $_filterCallback = null;

    /**
     * holds defaults for writen data
     *
     * @var array
     */
    protected $_defaults = array();

    /**
     * holds template for data maping
     *
     * @var string
     */
    protected $_template = null;

    /**
     * holds mode for stream
     *
     * @var string
     */
    protected $_streamMode = 'w';

    /**
     * holds stream uri
     *
     * @var string
     */
    protected $_streamUrl = 'php://output';

    /**
     * holds stream resource
     *
     * @var resource
     */
    protected $_fp = null;

    /**
     * gets template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     * sets template
     *
     * @param string $template
     * @return wrxNext_Import_Writer_Template
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
        return $this;
    }

    /**
     * sets stream mode
     *
     * @param string $mode
     * @return wrxNext_Import_Writer_Template
     */
    public function setMode($mode)
    {
        $this->_streamMode = $mode;
        return $this;
    }

    /**
     * gets stream mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->_streamMode;
    }

    /**
     * sets stream uri
     *
     * @param string $uri
     * @return wrxNext_Import_Writer_Template
     */
    public function setUri($uri)
    {
        $this->_streamUrl = $uri;
        return $this;
    }

    /**
     * gets stream uri
     *
     * @return string
     */
    public function getUri()
    {
        return $this->_streamUrl;
    }

    /**
     * sets default values
     *
     * @param array $default
     * @return wrxNext_Import_Writer_Template
     */
    public function setDefault($default)
    {
        $this->_defaults = $default;
        return $this;
    }

    /**
     * gets default values
     *
     * @return array
     */
    public function getDefault()
    {
        return $this->_defaults;
    }

    /**
     * gets value filter callback
     *
     * @return callable
     */
    public function getFilterCallback()
    {
        return $this->_filterCallback;
    }

    /**
     * sets value filter callback
     *
     * @param callable $callback
     * @return wrxNext_Import_Writer_Template
     */
    public function setFilterCallback($callback)
    {
        $this->_filterCallback = $callback;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function write($array)
    {
        $this->_initStream();
        return $this->_writeTemplate($array);
    }

    /**
     * inits stream
     */
    protected function _initStream()
    {
        if (!is_resource($this->_fp)) {
            $this->_fp = fopen($this->_streamUrl, $this->_streamMode);
        }
    }

    /**
     * writes data to stream after mapping it to given template
     *
     * @param array $array
     * @return boolean
     */
    protected function _writeTemplate($array)
    {
        $array = $this->_filterValues($array);
        $array = array_merge((array)$this->_defaults, $array);
        $string = $this->_buildTemplate($this->_template, $array);
        return !!fwrite($this->_fp, $string);
    }

    /**
     * filters values using callback
     *
     * @param array $array
     * @return array
     */
    protected function _filterValues($array)
    {
        if (!is_null($this->_filterCallback) &&
            is_callable($this->_filterCallback)
        ) {
            foreach ($array as $key => $value) {
                $array[$key] = call_user_func_array($this->_filterCallback,
                    array(
                        $value
                    ));
            }
        }
        return $array;
    }

    /**
     * closes stream
     */
    public function close()
    {
        if (is_resource($this->_fp)) {
            fclose($this->_fp);
        }
        $this->_fp = null;
    }
}
