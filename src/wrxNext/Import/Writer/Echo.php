<?php

/**
 * dummy echo writer for import reader debugging
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Writer_Echo extends wrxNext_Import_Writer_Abstract
{
    /**
     * @inheritdoc
     */
    public function write($array)
    {
        Zend_Debug::dump($array, '<h3>dane do zapisania</h3>');
    }
}
