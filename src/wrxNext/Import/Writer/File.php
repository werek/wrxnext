<?php

/**
 * file writer
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Import_Writer_File extends wrxNext_Import_Writer_Abstract
{
    /**
     * holds file resource
     *
     * @var resource
     */
    protected $_fp = null;
    /**
     * holds file path
     *
     * @var string
     */
    protected $_filePath = null;
    /**
     * holds flag wheter or not to write line end at each write
     *
     * @var bool
     */
    protected $_writeNewline = false;
    /**
     * holds mode for opening the file
     *
     * @var string
     */
    protected $_mode = 'w';

    /**
     * closes file
     *
     */
    public function fclose()
    {
        if (is_resource($this->_fp)) {
            fclose($this->_fp);
        }
    }

    /**
     * @inheritdoc
     */
    public function write($array)
    {
        if (!is_resource($this->_fp)) {
            $this->fopen();
        }
        $string = $this->parse($array);
        if ($this->getWriteNewline()) {
            $string .= PHP_EOL;
        }
        return (fwrite($this->_fp, $string) === false) ? false : true;
    }

    /**
     * opens file
     *
     * @throws wrxNext_Import_Writer_File_Exception
     */
    public function fopen()
    {
        $this->_fp = fopen($this->getFilePath(), $this->getMode());
        if ($this->_fp === FALSE) {
            throw new wrxNext_Import_Writer_File_Exception ('there was a problem during opening of file: "' . $this->getFilePath() . '" , with mode: "' . $this->getMode() . '"', 1);
        }
    }

    /**
     * returns current file path
     *
     * @return string
     */
    public function getFilePath()
    {
        return $this->_filePath;
    }

    /**
     * sets file path
     *
     * @param string $value
     * @return wrxNext_Import_Writer_File
     */
    public function setFilePath($value)
    {
        $this->_filePath = ( string )$value;
        return $this;
    }

    /**
     * returns current file open mode
     *
     * @return string
     */
    public function getMode()
    {
        return $this->_mode;
    }

    /**
     * sets opening mode for given filepath
     *
     * @param string $mode
     * @throws wrxNext_Import_Writer_File_Exception
     * @return wrxNext_Import_Writer_File
     */
    public function setMode($mode)
    {
        if (in_array(substr($mode, -1, 1), array('b', 't'))) {
            $_tmpMode = substr($mode, 0, strlen($mode) - 1);
        } else {
            $_tmpMode = $mode;
        }
        if ($_tmpMode == 'r') {
            throw new wrxNext_Import_Writer_File_Exception('Mode cannot be set to read-only, please set mode that allows writing', 2);
        }
        $this->_mode = ( string )$mode;
        return $this;
    }

    /**
     * converts input array to string
     *
     * @param array $array
     * @return string
     */
    protected function parse($array)
    {
        $string = '';
        foreach ($array as $value) {
            if (!is_array($value)) {
                $string .= ( string )$value;
            } else {
                $string .= $this->parse($value);
            }
        }
        return ( string )$string;
    }

    /**
     * returns status of adding new line at each write
     *
     * @return bool
     */
    public function getWriteNewline()
    {
        return $this->_writeNewline;
    }

    /**
     * sets flag to add line endings on each write
     *
     * @param bool $bool
     * @return wrxNext_Import_Writer_File
     */
    public function setWriteNewline($bool)
    {
        $this->_writeNewline = ($bool) ? true : false;
        return $this;
    }
}
