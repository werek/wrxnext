<?php

/**
 * interface for import object handling import operation
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Import_Interface
{
    /**
     * basic constructor with ability to pass optional parameters
     *
     * @param array $options
     */
    public function __construct($options = null);

    /**
     * runs import, returns boolean status after end
     *
     * @return wrxNext_Import_Result
     */
    public function import();
}
