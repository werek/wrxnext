<?php

class wrxNext_Search
{

    protected static $_instance = null;

    /**
     * holds classes names for creating search documents
     *
     * @var array
     */
    protected $_buildClasses = array();

    /**
     * holds instantiated builder objects
     *
     * @var array
     */
    protected $_builders = array();

    /**
     * holds search directory
     *
     * @var string
     */
    protected $_searchDirectory = '';

    /**
     * holds locale instances of Zend_Search_Lucene
     *
     * @var array
     */
    protected $_luceneLocale = array();

    /**
     * blocked constructor
     */
    protected function __construct()
    {
    }

    /**
     * returns wrxNext_search instance
     *
     * @return wrxNext_Search
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            Zend_Search_Lucene_Analysis_Analyzer::setDefault(
                new Zend_Search_Lucene_Analysis_Analyzer_Common_Utf8Num_CaseInsensitive());
            Zend_Search_Lucene_Search_QueryParser::setDefaultEncoding('UTF-8');
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * handles setting up options
     *
     * @param array $options
     * @return wrxNext_Search
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * returns array of builder classes
     *
     * @return array
     */
    public function getBuildClasses()
    {
        return $this->_buildClasses;
    }

    /**
     * sets array of builder classes, merges them with current ones
     *
     * @param array $classesArray
     * @return wrxNext_Search
     */
    public function setBuildClasses($classesArray)
    {
        $this->_buildClasses = array_merge($this->_buildClasses, $classesArray);
        return $this;
    }

    /**
     * rebuilds index from scratch
     *
     * @throws wrxNext_Search_Exception
     * @return wrxNext_Search
     */
    public function rebuildIndex()
    {
        Zend_Registry::get('log')->info('-----begining index rebuild-----');
        // checking if search directory is writable
        if (!is_writable($this->getSearchDirectory())) {
            throw new wrxNext_Search_Exception(
                $this->getSearchDirectory() . ' is not writable');
        }

        // creating temporary directory for index rebuild
        $tmpSearchDir = $this->getSearchDirectory() . '/tmp/';
        $this->_removeDirectoryRecursive($tmpSearchDir);
        if (!file_exists($tmpSearchDir)) {
            if (!mkdir($tmpSearchDir, 0777, true)) {
                throw new wrxNext_Search_Exception(
                    "couldn't create temporary directory: " . $tmpSearchDir);
            }
        }

        // iterating through available translations to create search for each
        // one
        foreach (Zend_Registry::get('Zend_Translate')->getList() as $locale) {
            Zend_Registry::get('log')->info(
                '-----building index for locale ' . $locale . ' -----');
            // creating index directory for locale
            $localeIndexDir = $tmpSearchDir . $locale . '/';
            if (!mkdir($localeIndexDir, 0777, true)) {
                throw new wrxNext_Search_Exception(
                    "couldn't create temporary directory for locale: " .
                    $localeIndexDir);
            }

            // initializing index for given locale
            $localeIndex = Zend_Search_Lucene::create($localeIndexDir);

            // building index
            foreach ($this->getBuilderNames() as $builderName) {
                Zend_Registry::get('log')->info(
                    '----- running builder: ' . $builderName . ' -----');
                try {
                    $builder = $this->getBuilder($builderName);
                    foreach ($builder->getIterator($locale) as $document) {
                        $localeIndex->addDocument($document);
                    }
                } catch (Exception $e) {
                    Zend_Registry::get('log')->err('-----got exception-----');
                    Zend_Registry::get('log')->err($e->getMessage());
                    Zend_Registry::get('log')->err($e->getTraceAsString());
                }
            }

            // optimizing current locale index
            Zend_Registry::get('log')->info(
                '-----optimizing created index-----');
            $localeIndex->optimize();

            // releaseing index
            unset($localeIndex);

            // moving current index in temporary place
            Zend_Registry::get('log')->info(
                '-----replacing current index with new one-----');
            $currentLocaleIndexDir = $this->getSearchDirectory() . '/' . $locale .
                '/';
            $tmpDir = $this->getSearchDirectory() . '/' . $locale . '_move/';
            if (file_exists($currentLocaleIndexDir)) {
                if (!rename($currentLocaleIndexDir, $tmpDir)) {
                    throw new wrxNext_Search_Exception(
                        "could not move current lucene directory from: " .
                        $currentLocaleIndexDir . " , to: " . $tmpDir);
                } else {
                    chmod($tmpDir, 0777);
                }
            }

            // moving new index in place of current
            if (!rename($localeIndexDir, $currentLocaleIndexDir)) {
                throw new wrxNext_Search_Exception(
                    "could not move new lucene directory from: " .
                    $currentLocaleIndexDir . " , to: " . $tmpDir);
            }

            // deleting old index
            $this->_removeDirectoryRecursive($tmpDir);
            Zend_Registry::get('log')->info(
                '-----index rebuilt for locale: ' . $locale . '-----');

            // opening index for check
            Zend_Registry::get('log')->info(
                '-----opening index for locale: ' . $locale .
                ' to check index in directory: ' .
                $currentLocaleIndexDir . '-----');
            $localeIndex = Zend_Search_Lucene::open($currentLocaleIndexDir);
            // Zend_Registry::get ( 'log' )->debug ( 'indexed terms: ' .
            // Zend_Debug::dump ( $localeIndex->terms (), null, false ) );
            Zend_Registry::get('log')->info(
                '-----releasing index for locale: ' . $locale .
                ' to check index in directory: ' .
                $currentLocaleIndexDir . '-----');
            unset($localeIndex);
        }
        return $this;
    }

    /**
     * returns search directory path
     *
     * @return string
     */
    public function getSearchDirectory()
    {
        return $this->_searchDirectory;
    }

    /**
     * sets search directory
     *
     * @param string $path
     * @return wrxNext_Search
     */
    public function setSearchDirectory($path)
    {
        $this->_searchDirectory = (string)$path;
        return $this;
    }

    /**
     * removes directory recursively
     *
     * @param string $dirPath
     */
    protected function _removeDirectoryRecursive($dirPath)
    {
        if (file_exists($dirPath)) {
            foreach (new RecursiveIteratorIterator(
                         new RecursiveDirectoryIterator($dirPath,
                             FilesystemIterator::SKIP_DOTS),
                         RecursiveIteratorIterator::CHILD_FIRST) as $path) {
                $path->isFile() ? unlink($path->getPathname()) : rmdir(
                    $path->getPathname());
            }
            rmdir($dirPath);
        }
    }

    /**
     * returns array of builder names
     *
     * @return multitype:
     */
    public function getBuilderNames()
    {
        return array_keys($this->_buildClasses);
    }

    /**
     * returns builder or null if not found
     *
     * @param string $builderName
     * @throws wrxNext_Search_Exception
     * @return wrxNext_Search_Builder_Interface
     */
    public function getBuilder($builderName)
    {
        if (isset($this->_buildClasses[$builderName])) {
            $reflection = new ReflectionClass(
                $this->_buildClasses[$builderName]);
            if (!$reflection->implementsInterface(
                'wrxNext_Search_Builder_Interface')
            ) {
                throw new wrxNext_Search_Exception(
                    'class: ' . $this->_buildClasses[$builderName] .
                    ' does not implement required interface wrxNext_Search_Builder_Interface');
            }
            return $reflection->newInstance();
        }
        return null;
    }

    /**
     * returns array of entries for given search query
     *
     * @param string|Zend_Search_Lucene_Search_Query $searchQuery
     * @param string $locale
     * @return array Zend_Search_Lucene_Search_QueryHit
     */
    public function search($searchQuery, $locale = null)
    {
        $locale = is_null($locale) ? Zend_Registry::get('Zend_Translate')->getLocale() : $locale;
        $lucene = $this->getSearch($locale);
        $lucene->find($searchQuery);
    }

    /**
     * returns Zend_Search_Lucene object for given locale,
     * or null if locale dir does not exists
     *
     * @param string $locale
     * @return Zend_Search_Lucene_Interface
     */
    public function getSearch($locale)
    {
        if (!isset($this->_luceneLocale[$locale])) {
            $searchDirectory = realpath(
                $this->getSearchDirectory() . DIRECTORY_SEPARATOR . $locale);
            if (!empty($searchDirectory)) {
                $this->_luceneLocale[$locale] = Zend_Search_Lucene::open(
                    $searchDirectory);
            }
        }
        return $this->_luceneLocale[$locale];
    }

    /**
     * blocked clone operator
     */
    protected function __clone()
    {
    }
}
