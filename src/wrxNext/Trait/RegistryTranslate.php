<?php

/**
 * enables translation mechanism. gives method _translate(4message, $locale=null) for translating any message through Zend_Translate registered in Zend_Registry
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_RegistryTranslate
{

    /**
     * translates message according to current locale or one passed as parameter
     *
     * @param string $message
     * @param string $locale
     * @return string
     */
    public function _translate($message, $locale = null)
    {
        if ($this->_getTranslate() !== false) {
            return $this->_getTranslate()->translate($message, $locale);
        }
        return $message;
    }

    /**
     * returns Zend_Translate object instance from registry
     *
     * @return Zend_Translate
     */
    protected function _getTranslate()
    {
        if (Zend_Registry::isRegistered('Zend_Translate')) {
            if (Zend_Registry::get('Zend_Translate') instanceof Zend_Translate) {
                return Zend_Registry::get('Zend_Translate');
            }
        }
        return false;
    }
}
