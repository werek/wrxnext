<?php

/**
 * adds methods for handling exclusive translation for forms
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Mapper_Locale
{
    /**
     * returns array to put into form populate method including array data
     * of each translation language
     *
     * @param Zend_Db_Table_Row_Abstract $row
     * @return Ambigous <multitype:, array>
     */
    public function getFormDataWithLocale(Zend_Db_Table_Row_Abstract $row)
    {
        $data = $row->toArray();
        if (in_array('getLocaleData', get_class_methods($row))) {
            foreach (Zend_Registry::get('Zend_Translate')->getList() as $locale) {
                $data[$locale] = $row->getLocaleData($locale);
            }
        }
        return $data;
    }

    /**
     * set locale data from form, also puts any form normal data into row
     *
     * @param Zend_Db_Table_Row_Abstract $row
     * @param array $formData
     * @return $this
     */
    public function setFormDataWithLocale(Zend_Db_Table_Row_Abstract $row, $formData)
    {
        $localeData = array();
        if (in_array('setLocaleData', get_class_methods($row))) {
            foreach (Zend_Registry::get('Zend_Translate')->getList() as $locale) {
                $localeData[$locale] = $formData[$locale];
                unset($formData[$locale]);
            }
        }
        foreach ($formData as $key => $value) {
            $row->{$key} = $value;
        }
        $row->save();
        if (in_array('setLocaleData', get_class_methods($row))) {
            foreach ($localeData as $locale => $value) {
                $row->setLocaleData($locale, $value);
            }
        }
        return $this;
    }
}
