<?php

/**
 * adds method for quick creation of paginator from Zend_Db_Select
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Mapper_PaginatorDbSelect
{
    /**
     * creates paginator based on db select
     *
     * @param Zend_Db_Select $select
     * @param int $page
     * @param int $pageSize
     * @return Zend_Paginator
     */
    protected function createPaginator($select, $page, $pageSize)
    {
        $adapter = new Zend_Paginator_Adapter_DbSelect($select);
        $paginator = new Zend_Paginator($adapter);
        $paginator->setCurrentPageNumber($page);
        $paginator->setDefaultItemCountPerPage($pageSize);
        return $paginator;
    }
}
