<?php

/**
 * trait with setOptions method to map passed array to setter methods of object
 *
 * @author bweres01
 *
 */
trait wrxNext_Trait_Object_Options
{

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @return Object
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }
}
