<?php

trait wrxNext_Trait_Object_ViewSetDefaultGlobal
{

    /**
     * holds view instance
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * gets view object
     *
     * @return Zend_View_Interface
     */
    public function getView()
    {
        if (!$this->_view instanceof Zend_View_Interface) {
            $this->_view = Zend_Controller_Front::getInstance()->getParam(
                'bootstrap')->getResource('view');
        }
        return $this->_view;
    }

    /**
     * sets view object
     *
     * @param Zend_View_Interface $view
     * @return Object
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }
}
