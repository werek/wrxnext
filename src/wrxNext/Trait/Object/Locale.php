<?php

trait wrxNext_Trait_Object_Locale
{

    /**
     * if provided locale is null then returns currently used locale
     *
     * @param string $locale
     * @return string
     */
    protected function _locale($locale = null)
    {
        return is_null($locale) ? Zend_Registry::get('Zend_Translate')->getLocale() : $locale;
    }
}
