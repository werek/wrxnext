<?php

trait wrxNext_Trait_Object_ViewSet
{

    /**
     * holds view instance
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * gets view object
     *
     * @return Zend_View_Interface
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * sets view object
     *
     * @param Zend_View_Interface $view
     * @return Object
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }
}
