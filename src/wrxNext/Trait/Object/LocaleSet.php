<?php

trait wrxNext_Trait_Object_LocaleSet
{

    /**
     * holds locale to use
     *
     * @var string
     */
    protected $_locale = null;

    /**
     * gets locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->_locale;
    }

    /**
     * sets locale
     *
     * @param string $locale
     * @return $this
     */
    public function setLocale($locale)
    {
        $this->_locale = $locale;
        return $this;
    }

    /**
     * returns locale
     *
     * order of priority:
     * 1. locale passed as param
     * 2. locale set in property
     * 3. locale from Zend_Translate object stored in Zend_Registry under key
     * Zend_Translate
     *
     * @param string $locale
     * @return string
     */
    protected function _locale($locale = null)
    {
        if (!is_null($locale)) {
            return $locale;
        } elseif (!is_null($this->_locale)) {
            return $this->_locale;
        } else {
            return Zend_Registry::get('Zend_Translate')->getLocale();
        }
    }
}
