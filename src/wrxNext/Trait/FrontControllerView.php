<?php

/**
 * enables access to view object registered in Front Controller
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_FrontControllerView
{

    /**
     * returns registered View object from Front Controller
     *
     * @return Zend_View_Interface
     */
    protected function _view()
    {
        return Zend_Controller_Front::getInstance()->getParam('bootstrap')->getResource(
            'view');
    }
}
