<?php

/**
 * locale exception
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Trait_DbTableRow_Locale_Exception extends wrxNext_Trait_Exception
{
}
