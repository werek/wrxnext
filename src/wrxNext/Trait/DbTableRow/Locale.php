<?php

/**
 * proxy trait for wrxNext_Trait_DbTable_Locale, enabling per row
 * locale extraction modification, relies on properly set table
 * info
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com
 *
 */
trait wrxNext_Trait_DbTableRow_Locale
{

    /**
     * updates locale data for given row
     *
     * @param string $locale
     * @param array $data
     * @throws wrxNext_Trait_DbTableRow_Locale_Exception
     * @return mixed
     */
    public function setLocaleData($locale, $data)
    {
        if (!in_array('setLocaleDataForID',
            get_class_methods($this->getTable()))
        ) {
            throw new wrxNext_Trait_DbTableRow_Locale_Exception(
                'setting locale relies on coresponding table method delivered by wrxNext_Trait_DbTable_Locale, missing method: setLocaleDataForID');
        }
        return $this->getTable()->setLocaleDataForID($this->_getIdentityValue(),
            $locale, $data);
    }

    /**
     * returns identity value commonly know as id
     *
     * @return mixed
     */
    protected function _getIdentityValue()
    {
        foreach ((array)$this->_primary as $key) {
            if ($this->getTable()->isIdentity($key)) {
                return $this->{$key};
            }
        }
    }

    /**
     * returns locale data for given row
     *
     * @param string $locale
     * @throws wrxNext_Trait_DbTableRow_Locale_Exception
     * @return mixed
     */
    public function getLocaleData($locale)
    {
        if (!in_array('getLocaleDataForID',
            get_class_methods($this->getTable()))
        ) {
            throw new wrxNext_Trait_DbTableRow_Locale_Exception(
                'getting locale relies on coresponding table method delivered by wrxNext_Trait_DbTable_Locale, missing method: getLocaleDataForID');
        }
        return $this->getTable()->getLocaleDataForID($this->_getIdentityValue(),
            $locale);
    }

    /**
     * returns all locale data for given row
     *
     * @throws wrxNext_Trait_DbTableRow_Locale_Exception
     * @return array
     */
    public function getAllLocaleData()
    {
        if (!in_array('getLocaleDataForID',
            get_class_methods($this->getTable()))
        ) {
            throw new wrxNext_Trait_DbTableRow_Locale_Exception(
                'getting locale relies on coresponding table method delivered by wrxNext_Trait_DbTable_Locale, missing method: getAllLocaleDataForID');
        }
        return $this->getTable()->getAllLocaleDataForID(
            $this->_getIdentityValue());
    }
}
