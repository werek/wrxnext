<?php

/**
 * exception for Zend_Db_Table linking trait
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Trait_DbTable_LinkingTable_Exception extends wrxNext_Trait_Exception
{
}
