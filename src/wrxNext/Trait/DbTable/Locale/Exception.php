<?php

/**
 * exception for Zend_Db_Table locale operations
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Trait_DbTable_Locale_Exception extends wrxNext_Trait_Exception
{
}
