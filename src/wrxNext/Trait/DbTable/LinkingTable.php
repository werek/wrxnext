<?php

/**
 * adds quick methods to n:m relating tables, enabling to get/set coresponding arrays of ids
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_DbTable_LinkingTable
{

    /**
     * sets linking of one column to multiple set of another
     *
     * @param string $relevantColumnName
     * @param int $relevantColumnValue
     * @param array $corespondingIDs
     * @return wrxNext_Trait_DbTable_LinkingTable
     */
    public function setCorespondingIDs($relevantColumnName,
                                       $relevantColumnValue, $corespondingIDs = array())
    {
        $corespondingIDs = (array)$corespondingIDs;
        $currentIDs = $this->getCorespondingIDs($relevantColumnName,
            $relevantColumnValue);
        $corespondingColumnName = (array_search($relevantColumnName,
                $this->_primary) == 1) ? $this->_primary[2] : $this->_primary[1];

        // extract new binds
        $newIDs = array_diff($corespondingIDs, $currentIDs);

        // extract binds to delete
        $deleteIDs = array_diff($currentIDs, $corespondingIDs);

        // making new connections
        if (count($newIDs) > 0) {
            foreach ($newIDs as $newID) {
                $this->insert(
                    array(
                        $relevantColumnName => $relevantColumnValue,
                        $corespondingColumnName => $newID
                    ));
            }
        }

        // deleting connections
        if (count($deleteIDs) > 0) {
            foreach ($deleteIDs as $deleteID) {
                $this->delete(
                    array(
                        $relevantColumnName . ' = ?' => $relevantColumnValue,
                        $corespondingColumnName . ' = ?' => $deleteID
                    ));
            }
        }
        return $this;
    }

    /**
     * returns array of id keys from two column linking table, according to n:m
     * relation model
     *
     * @param string $relevantColumnName
     * @param mixed $relevantColumnValue
     * @return array
     * @throws wrxNext_Trait_DbTable_LinkingTable_Exception
     */
    public function getCorespondingIDs($relevantColumnName,
                                       $relevantColumnValue)
    {
        $this->_setupPrimaryKey();
        if (!in_array($relevantColumnName, $this->_primary)) {
            throw new wrxNext_Trait_DbTable_LinkingTable_Exception(
                'column "' . $relevantColumnName .
                '" doesn\'t exists in primary index of table ' .
                $this->_name);
        }
        if (count($this->_primary) != 2) {
            throw new wrxNext_Trait_DbTable_LinkingTable_Exception(
                'primary index of table ' . $this->_name .
                ' is not composed of two columns, extraction of coresponding ids is not possible');
        }
        $corespondingColumnName = (array_search($relevantColumnName,
                $this->_primary) == 1) ? $this->_primary[2] : $this->_primary[1];
        $foundKeys = $this->select()
            ->from($this->_name,
                array(
                    $corespondingColumnName
                ))
            ->where($relevantColumnName . ' = ?', $relevantColumnValue)
            ->query()
            ->fetchAll(Zend_Db::FETCH_COLUMN);
        return (array)$foundKeys;
    }
}
