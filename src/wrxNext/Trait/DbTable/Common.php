<?php

/**
 * adds common methods for Zend_Db_Table objects. ie. method for quick retrival rows with given primary key value
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_DbTable_Common
{

    /**
     * returns page row by give id
     *
     * @param int $id
     * @return Zend_Db_Table_Row_Abstract
     */
    public function getByID($id)
    {
        return $this->fetchRow(
            array(
                $this->_getIdentityColumn() . ' = ?' => $id
            ));
    }

    /**
     * returns identity column
     *
     * @return string
     */
    protected function _getIdentityColumn()
    {
        $this->_setupPrimaryKey();
        return (string)$this->_primary[$this->_identity];
    }

    /**
     * returns all columns names
     *
     * @return array
     */
    public function getColumns()
    {
        return $this->_getCols();
    }

    /**
     * checks if given column exists in current table
     *
     * @param string $columnName
     * @return boolean
     */
    public function hasColumn($columnName)
    {
        return in_array($columnName, $this->_getCols());
    }

    /**
     * strips from array keys which do not coresponds to column names
     *
     * @param array $data
     * @return array
     */
    public function stripUnknownColumnKeys($data)
    {
        $columns = $this->_getCols();
        $cleanData = array();
        foreach ($data as $key => $value) {
            if (in_array($key, $columns)) {
                $cleanData[$key] = $value;
            }
        }
        return $cleanData;
    }

    /**
     * fetches all rows as associative array
     *
     * @return array
     */
    public function getAll()
    {
        return $this->select(true)
            ->query(Zend_Db::FETCH_ASSOC)
            ->fetchAll();
    }

    /**
     * returns key=>value array of given table and limited to given where
     * statement(s) and given order
     *
     * @param string $keyColumn
     * @param string $valueColumn
     * @param string|array|Zend_Db_Expr $where
     * @return array
     */
    public function getMap($keyColumn, $valueColumn, $where = array(), $order = null)
    {
        $map = array();
        $select = $this->select(true)->columns(
            array(
                $keyColumn,
                $valueColumn
            ));
        foreach ($where as $column => $value) {
            $select->where($column, $value);
        }
        if (!is_null($order)) {
            $select->order($order);
        }
        $stmt = $select->query(Zend_Db::FETCH_ASSOC);
        while ($row = $stmt->fetch()) {
            $map[$row[$keyColumn]] = $row[$valueColumn];
        }
        return $map;
    }

    /**
     * returns distinct values of given column
     *
     * @param string $column
     * @param string|array|Zend_Db_Expr $where
     * @return array
     */
    public function getDistinct($column, $where = null)
    {
        $select = $this->getAdapter()
            ->select()
            ->from($this->_name,
                new Zend_Db_Expr(
                    'distinct(' . $this->getAdapter()
                        ->quoteIdentifier($column) . ')'));
        if (!is_null($where)) {
            if ($where instanceof Zend_Db_Expr) {
                $select->where($where);
            } elseif (is_array($where)) {
                foreach ($where as $cond => $value) {
                    if ($cond === intval($cond)) {
                        $select->where($value);
                    } else {
                        $select->where($cond, $value);
                    }
                }
            } else {
                $select->where((string)$where);
            }
        }
        return $select->query()->fetchAll(Zend_Db::FETCH_COLUMN);
    }

    /**
     * starts transaction
     *
     * @return wrxNext_Trait_DbTable_Common
     */
    public function beginTransaction()
    {
        $this->getAdapter()->beginTransaction();
        return $this;
    }

    /**
     * commits transaction
     *
     * @return wrxNext_Trait_DbTable_Common
     */
    public function commit()
    {
        $this->getAdapter()->commit();
        return $this;
    }

    /**
     * rollbacks transaction
     *
     * @return wrxNext_Trait_DbTable_Common
     */
    public function rollback()
    {
        $this->getAdapter()->rollback();
        return $this;
    }

    /**
     * returns count of elements matching $where
     *
     * @param null|array $where
     * @return mixed
     * @throws \Zend_Db_Select_Exception
     */
    public function count($where = null)
    {
        $select = $this->select()->from($this->_name, new Zend_Db_Expr('count(1) as count'));
        if ( !is_null($where)) {
            $this->_where($select, $where);
        }
        return intval($select->query()->fetch(Zend_Db::FETCH_COLUMN));
    }
}
