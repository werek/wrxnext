<?php

/**
 * adds exclusive translation specific methods, thus enabling given table object to work with locale aware rows instances.
 *
 * trait requires two properties to be present in table:
 *
 * _localeBind='<locale_table>.<fk_column>';
 * _localeColumn='<locale_column>';
 *
 * _localeBind points to table and column on which join will be created with current table, when
 * _localeColumn points to column on which locale will recognised, for example:
 *
 * _localeBind='product_locale.product_id';
 * _localeColumn='language';
 *
 * if _localeColumn is not defined then 'language' will be used
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com
 *
 */
trait wrxNext_Trait_DbTable_Locale
{

    /**
     * creates select statement with joined locale data
     *
     * @param string $locale
     * @throws wrxNext_Trait_DbTable_Locale_Exception
     * @return Zend_Db_Table_Select
     */
    public function selectWithLocale($locale)
    {
        if (!$this->_localePropertiesValid()) {
            throw new wrxNext_Trait_DbTable_Locale_Exception(
                'locale data for join are missing');
        }
        $select = $this->getAdapter()
            ->select()
            ->from(
                array(
                    't' => $this->_name
                ));
        $select->joinLeft(
            array(
                'l' => $this->_localeGetTable()
            ),
            'l.' . $this->_localeGetIdentityColumn() . '=t.' .
            $this->_getIdentityColumn() . ' AND l.' .
            $this->_localeGetLanguageColumn() . ' = ' . $this->getAdapter()
                ->quote($locale));
        return $select;
    }

    /**
     * checks if properties set in class can be used to bind locale table
     *
     * @return boolean
     */
    protected function _localePropertiesValid()
    {
        if (!isset($this->_localeBind) || empty($this->_localeBind) ||
            !preg_match('@^.+\..+$@', $this->_localeBind)
        ) {
            return false;
        }
        return true;
    }

    /**
     * returns locale table name
     *
     * @return string
     */
    protected function _localeGetTable()
    {
        $identity = explode('.', $this->_localeBind);
        return $identity[0];
    }

    /**
     * returns locale table foreign identity column
     *
     * @return string
     */
    protected function _localeGetIdentityColumn()
    {
        $identity = explode('.', $this->_localeBind);
        return $identity[1];
    }

    /**
     * returns locale table language column
     *
     * @return string
     */
    protected function _localeGetLanguageColumn()
    {
        return isset($this->_localeColumn) ? $this->_localeColumn : 'language';
    }

    /**
     * set locale data coresponding with given this table row id and locale
     *
     * @param int $id
     * @param string $locale
     * @param array $data
     * @return Ambigous <mixed, multitype:>
     */
    public function setLocaleDataForID($id, $locale, $data)
    {
        if (!$this->_localePropertiesValid()) {
            throw new wrxNext_Trait_DbTable_Locale_Exception(
                'locale data for join are missing');
        }
        $table = $this->_localeGetTableInstance();
        $tmpData = $data;
        $data = array();
        foreach ($table->info(Zend_Db_Table_Abstract::COLS) as $column) {
            if (array_key_exists($column, $tmpData)) {
                $data[$column] = $tmpData[$column];
            }
        }
        $row = $table->fetchRow(
            array(
                $this->_localeGetIdentityColumn() . ' = ?' => $id,
                $this->_localeGetLanguageColumn() . ' = ?' => $locale
            ));
        if ($row instanceof Zend_Db_Table_Row_Abstract) {
            foreach ($data as $key => $value) {
                $row->{$key} = $value;
            }
        } else {
            $data[$this->_localeGetLanguageColumn()] = $locale;
            $data[$this->_localeGetIdentityColumn()] = $id;
            $row = $table->createRow($data);
        }
        return $row->save();
    }

    /**
     * returns locale table instance
     *
     * @return Zend_Db_Table
     */
    protected function _localeGetTableInstance()
    {
        $tableName = $this->_localeGetTable();
        if (!Zend_Registry::isRegistered('locale_table_repository')) {
            $repo = new ArrayObject(array());
            Zend_Registry::set('locale_table_repository', $repo);
        } else {
            $repo = Zend_Registry::get('locale_table_repository');
        }
        if (!$repo->offsetExists($tableName)) {
            $repo->offsetSet($tableName, new Zend_Db_Table($tableName));
        }
        return $repo->offsetGet($tableName);
    }

    /**
     * returns locale data coresponding with given this table row id and locale
     *
     * @param int $id
     * @param string $locale
     * @return Ambigous <multitype:, array>|NULL
     */
    public function getLocaleDataForID($id, $locale)
    {
        if (!$this->_localePropertiesValid()) {
            throw new wrxNext_Trait_DbTable_Locale_Exception(
                'locale data for join are missing');
        }
        $table = $this->_localeGetTableInstance();
        $row = $table->fetchRow(
            array(
                $this->_localeGetIdentityColumn() . ' = ?' => $id,
                $this->_localeGetLanguageColumn() . ' = ?' => $locale
            ));
        if ($row instanceof Zend_Db_Table_Row_Abstract) {
            return $row->toArray();
        }
        return null;
    }

    /**
     * returns all locale data as array, keys are retrieved locale names while
     * values contains array with locale data
     *
     * @param int $id
     * @throws wrxNext_Trait_DbTable_Locale_Exception
     * @return array
     */
    public function getAllLocaleDataForID($id)
    {
        if (!$this->_localePropertiesValid()) {
            throw new wrxNext_Trait_DbTable_Locale_Exception(
                'locale data for join are missing');
        }
        $table = $this->_localeGetTableInstance();
        $locale = array();
        $stmt = $table->select(true)
            ->where($this->_localeGetIdentityColumn() . ' = ?', $id)
            ->query(Zend_Db::FETCH_ASSOC);
        while ($row = $stmt->fetch()) {
            $locale[$row[$this->_localeGetLanguageColumn()]] = $row;
        }
        return $locale;
    }
}
