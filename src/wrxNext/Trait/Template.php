<?php

/**
 * adds simple templating capabilities. also support dynamic methods
 *
 * templates use %<var>% syntax which translates to passed array in fashion of
 * $data[<var>]. also support executing any callable functions from params constructed
 * in manner of:
 *
 * %<callName>|<param>(|<param>)%
 *
 * that call can support many params, requirements for callback functions are that first
 * parameter will be params from tag and second would be data passed to buildTemplate function.
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Template
{

    /**
     * holds callable handlers for given calls in params
     *
     * holds them as associative array <call_name> => <call_handler>
     *
     * @var array
     */
    protected $_templateCallHandlers = array();

    /**
     * replaces template placeholders with given data
     *
     * @param string $template
     * @param array $data
     * @return string
     */
    protected function _buildTemplate($template, $data = array())
    {
        $vars = $this->_templateVars($template);
        $replace = array();
        // preparing placeholders data
        foreach ($vars as $placeholder) {
            if (strpos($placeholder, '|') !== false) {
                $placeHolderParts = explode('|', $placeholder);
                $callName = array_shift($placeHolderParts);
                $replace['%' . $placeholder . '%'] = $this->_templateCall(
                    $callName, $placeHolderParts, $data);
            } else {
                $replace['%' . $placeholder . '%'] = (isset($data[$placeholder])) ? $data[$placeholder] : '';
            }
        }

        // replacing placeholders and returning content
        return strtr($template, $replace);
    }

    /**
     * returns recognised vars in template
     *
     * @param string $template
     * @return array
     */
    protected function _templateVars($template)
    {
        preg_match_all('@%([a-z0-9_\|]*?)%@miu', $template, $vars);
        return (is_array($vars[1])) ? $vars[1] : array();
    }

    /**
     * calls given handler name and returns content
     *
     * @param string $callName
     * @param array $args
     * @param array $data
     * @return string
     */
    protected function _templateCall($callName, $args, $data)
    {
        if (!array_key_exists($callName, $this->_templateCallHandlers)) {
            return '';
        }

        return call_user_func($this->_templateCallHandlers[$callName], $args,
            $data);
    }

    /**
     * adds call handler to template stack
     *
     * @param string $callName
     * @param callable $callHandler
     * @return wrxNext_Trait_Template
     */
    protected function _templateCallHandler($callName, $callHandler)
    {
        $this->_templateCallHandlers[$callName] = $callHandler;
        return $this;
    }
}
