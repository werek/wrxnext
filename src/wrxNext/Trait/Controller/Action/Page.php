<?php

/**
 * adds biedronka.pl cms page specific methods for recognition/handling of passed page params. also to process and set up of any grid coresponding to recognised page
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Controller_Action_Page
{
    use wrxNext_Trait_RegistryLog;

    protected $_backgroundOverrideTemplate = "body {background-image: url(%s) !important; background-repeat: no-repeat; background-position: top center;}";

    /**
     * checks wheter to process page, if yes then assigns view with necesary
     * data do display page
     * returns true on success and false if controller shold redirect/forward
     *
     * @return bool
     */
    protected function _setupPage()
    {
        $pageID = intval($this->getParam('page_id'));
        $language = preg_replace('@[^a-z_]@i', '', $this->getParam('language'));

        if (!empty($language)) {
            Zend_Registry::get('Zend_Translate')->setLocale($language);
        }

        $pagesMapper = new Front_Model_Mapper_Pages();

        $processPage = true;

        if (empty($pageID)) {
            $this->_log(
                bd_Router_Page::PAGE_ID .
                ' key was empty, no page loaded');

            $processPage = false;
        } else {
            $page = $pagesMapper->getByID($pageID);
            if ($page instanceof bd_Model_DbTableRow_Pages) {
                $ageVerification = $this->view->page()->param('verify_age');
                if (!empty($ageVerification)) {
                    $this->_log('Page has age verification enabled');
                    if (!$this->verifyAge($ageVerification)) {
                        $this->_log('Detected underage, redirecting');
                        $this->redirect($this->view->page()->hrefByName('personalised_offer'), array('prependBase' => false));
                    }
                }
                if ($page->published == 1 || ($page->published != 1 && Zend_Auth::getInstance()->hasIdentity())) {
                    if ($page->published != 1 && Zend_Auth::getInstance()->hasIdentity()) {
                        $this->_log('Page is not published, but admin is logged in so access is granted');
                    }
                    if ($this->_checkDates($page->date_from, $page->date_to) == 0 || Zend_Auth::getInstance()->hasIdentity()) {
                        if (!empty($page->background_file)) {
                            $this->view->headStyle()->appendStyle(
                                sprintf($this->_backgroundOverrideTemplate,
                                    $this->view->cdn($page->background_file)));
                        }
                        $this->_setupSeo($page);
                        $this->view->page = $page;
                        $this->view->slots = $pagesMapper->getSlotsForPage($page->id);
                    } elseif ($this->_checkDates($page->date_from, $page->date_to) == 1) {
                        // page is after its date_to date
                        $this->_log('Page date_to expired, searching for next page to publish and redirecting');
                        if (($pageID = $pagesMapper->publishNextDelayed($page->guid)) !==
                            false
                        ) {
                            $this->_log('published page: ' . $pageID);
                            $this->redirect(
                                $this->view->url(
                                    array(
                                        bd_Router_Page::PAGE_ID => $pageID
                                    ), 'pages', true),
                                array(
                                    'prependBase' => false
                                ));
                        } else {
                            $this->_log(
                                'did not found next page to be published for guid: ' .
                                $page->guid);
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    $this->_log(
                        'page is not published: ' . $page->name . ' (id:' . $page->id .
                        '; guid:' . $page->guid . ')');
                    $processPage = false;
                }
            } else {
                $this->_log(
                    'page not found by id/language: ' . $pageID . '/' . $language);
                $processPage = false;
            }
        }

        return $processPage;
    }

    /**
     * handles age verification check
     *
     * @param string $adapter name from adapters configuration
     * @return boolean
     */
    public function verifyAge($adapter = 'loginnewsletter')
    {
        $result = $this->_helper->ageVerification()->verifyAge($this->getRequest(), $adapter);
        if ($result->getResult() == wrxNext_Controller_Action_Helper_AgeVerification_Result::UNKNOWN) {
            $this->_helper->ageVerification()->getAdapter($adapter)->redirectToVerification();
        }
        return ($result->getResult() > 0);
    }

    /**
     * returns date comparison with current date, posibble states
     * -1 - given page is before date_from date
     * 0 - given page falls beetwen page date_from/date_to, can be displayed
     * 1 - given page is after date_to date
     *
     * dates should be formated in MySQL DATETIME type
     *
     * @param string $dateFrom
     * @param string $dateTo
     * @return int
     */
    protected function _checkDates($dateFrom = null, $dateTo = null)
    {
        $date = new Zend_Date();
        if (!empty($dateFrom) && $date->isEarlier($dateFrom,
                bd_View_Helper_Date::MYSQL_DATETIME)
        ) {
            return -1;
        }
        if (!empty($dateTo) && $date->isLater($dateTo,
                bd_View_Helper_Date::MYSQL_DATETIME)
        ) {
            return 1;
        }
        return 0;
    }

    /**
     * setups seo information if not empty and page message
     *
     * @param bd_Model_DbTableRow_Pages $page
     */
    protected function _setupSeo(bd_Model_DbTableRow_Pages $page)
    {
        if (!empty($page->seo_title)) {
            $this->view->headTitle($page->seo_title,
                Zend_View_Helper_Placeholder_Container_Abstract::SET);
        }
        if (!empty($page->seo_description)) {
            $this->view->headMeta()->setName('description', $page->seo_description);
        }
        if (!empty($page->seo_head)) {
            $this->view->head($page->seo_head);
        }
        if (!empty($page->page_message)) {
            $this->view->flashMessage()->success($page->page_message);
        }
        if (null !== ($trackingCode = $this->view->page()->param('tracking', null, $page->id))) {
            $this->view->placeholder('body_tracking')->set($trackingCode);
        }
    }

    /**
     * setups page data for given page name, returns false if page was not found
     *
     * @param string $pageName
     * @param boolean $overridePageHelperDefault
     * @return boolean
     */
    protected function _setupPageByName($pageName, $overridePageHelperDefault = false)
    {
        $pagesMapper = new Front_Model_Mapper_Pages();
        $this->_log('pageName: ' . $pageName);
        $page = $pagesMapper->getPageByName($pageName);

        if ($page instanceof bd_Model_DbTableRow_Pages) {
            if ($page->published == 1 || ($page->published != 1 &&
                    Zend_Auth::getInstance()->hasIdentity())
            ) {
                if ($this->_checkDates($page->date_from, $page->date_to) == 0 ||
                    Zend_Auth::getInstance()->hasIdentity()
                ) {

                    if (!empty($page->background_file)) {
                        $this->view->headStyle()->appendStyle(
                            sprintf($this->_backgroundOverrideTemplate,
                                $this->view->cdn($page->background_file)));
                    }
                    if ($overridePageHelperDefault) {
                        $this->view->page()->setDefaultPageID($page->id);
                    }
                    $this->_setupSeo($page);
                    $this->view->page = $page;
                    $this->view->slots = $pagesMapper->getSlotsForPage($page->id);
                } elseif ($this->_checkDates($page->date_from, $page->date_to) == 1) {
                    // page is after its date_to date
                    if ($pagesMapper->publishNextDelayed($page->guid) !== false) {
                        $this->redirect($this->view->url(),
                            array(
                                'prependBase' => false
                            ));
                    } else {
                        $this->_log(
                            'did not found next page to be published for guid: ' .
                            $page->guid);
                        return false;
                    }
                } else {
                    return false;
                }
            } else {
                $this->_log(
                    'page is not published: ' . $page->name . ' (id:' . $page->id . '; guid:' .
                    $page->guid . ')');
                return false;
            }
            return true;
        } else {
            $this->_log('page not found by name: ' . $pageName);
        }
        return false;
    }
}
