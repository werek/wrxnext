<?php

/**
 * adds common filtering/tool methods for Zend_Controller_Action instances
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Controller_Action_Common
{

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to Integer value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return int
     */
    public function getParamInt($paramName, $default = null)
    {
        return intval($this->getParam($paramName, $default));
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to Float value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return float
     */
    public function getParamFloat($paramName, $default = null)
    {
        return floatval($this->getParam($paramName, $default));
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to Double value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return double
     */
    public function getParamDouble($paramName, $default = null)
    {
        return doubleval($this->getParam($paramName, $default));
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to Array value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return array
     */
    public function getParamArray($paramName, $default = null)
    {
        return (array)$this->getParam($paramName, $default);
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to Boolean value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return boolean
     */
    public function getParamBoolean($paramName, $default = null)
    {
        return !!$this->getParam($paramName, $default);
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * removes non alphanumeric ascii values
     *
     * @param string $paramName
     * @param mixed $default
     * @return string
     */
    public function getParamAlphanumeric($paramName, $default = null)
    {
        return $this->getParamRegexpExclude($paramName, '@[^a-z0-9]@i',
            $default);
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * removes matching pattern from given value.
     *
     * for patterns special meaning go to {@link
     * http://pl1.php.net/preg_match#105924 php manual}
     *
     * @param string $paramName
     * @param string $matchingRegexp
     *            regexp matching characters to delete
     * @param mixed $default
     * @return string
     */
    public function getParamRegexpExclude($paramName, $matchingRegexp,
                                          $default = null)
    {
        return preg_replace($matchingRegexp, '',
            $this->getParamString($paramName, $default));
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * filtered to string value.
     *
     * @param string $paramName
     * @param mixed $default
     * @return string
     */
    public function getParamString($paramName, $default = null)
    {
        return strval($this->getParam($paramName, $default));
    }

    /**
     * Gets a parameter from the {@link $_request Request object}.
     * removes non numeric ascii values
     *
     * @param string $paramName
     * @param mixed $default
     * @return string
     */
    public function getParamNumeric($paramName, $default = null)
    {
        return $this->getParamRegexpExclude($paramName, '@[^\d]@i', $default);
    }

    /**
     * redirects to starting url otherwise uses default from params
     *
     * @param string $defaultAction
     * @param string $defaultController
     * @param string $defaultModule
     * @param array $routeParams
     */
    public function redirectToReferer($defaultAction = null,
                                      $defaultController = null, $defaultModule = null, $routeParams = array())
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $current = new wrxNext_Url();
            $referer = new wrxNext_Url($_SERVER['HTTP_REFERER']);
            if ($current->getHost() == $referer->getHost() &&
                $current->getPath() != $referer->getPath()
            ) {
                $referer->redirect();
            } else {
                $this->_helper->redirector($defaultAction, $defaultController,
                    $defaultModule, $routeParams);
            }
        } else {
            $this->_helper->redirector($defaultAction, $defaultController,
                $defaultModule, $routeParams);
        }
    }
}
