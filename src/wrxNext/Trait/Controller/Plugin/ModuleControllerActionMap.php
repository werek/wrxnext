<?php

/**
 * trait for managing tree mappings in plugin configuration
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap
{

    /**
     * holds map tree
     *
     * @var array
     */
    protected static $_mapTree = array();

    /**
     * extracts params from map for given request
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param array $defaults
     * @return array
     */
    protected function _extractParamsFromMapByRequest(
        Zend_Controller_Request_Abstract $request, $defaults = array())
    {
        return $this->_extractParamsFromMap($request->getModuleName(),
            $request->getControllerName(), $request->getActionName(),
            $defaults);
    }

    /**
     * extracts params from map for given module/controller/action set
     *
     * @param array $defaults
     * @param string $module
     * @param string $controller
     * @param string $action
     * @return array
     */
    protected function _extractParamsFromMap($module, $controller, $action,
                                             $defaults = array())
    {
        // getting keys
        $stepsKeys = array(
            $module,
            $controller,
            $action
        );
        return $this->_extractKeyValues($stepsKeys, self::getMapTree(),
            $defaults);
    }

    /**
     * traverses through data tree in search for keys from default,
     * also if found next step in current data keys the digs deeper
     *
     * @param array $stepsKeys
     * @param array $data
     * @param array $defaults
     * @return array
     */
    protected function _extractKeyValues(array $stepsKeys, $data = array(),
                                         $defaults = array())
    {
        // searching for defaults and assigning found values
        foreach ($defaults as $key => $value) {
            if (array_key_exists($key, $data)) {
                $defaults[$key] = $data[$key];
                unset($data[$key]);
            }
        }

        // shifting next step
        if (count($stepsKeys)) {
            $step = array_shift($stepsKeys);
            if (array_key_exists($step, $data)) {
                $defaults = $this->_extractKeyValues($stepsKeys, $data[$step],
                    $defaults);
            }
        }

        // returning found defaults
        return $defaults;
    }

    /**
     * returns map tree
     *
     * @return array
     */
    public static function getMapTree()
    {
        return self::$_mapTree;
    }

    /**
     * sets map tree
     *
     * @param array $array
     */
    public static function setMapTree($array)
    {
        self::$_mapTree = $array;
    }
}
