<?php

/**
 * holds utility methods for Zend_Form derived classes.
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Form
{

    /**
     * clears all elements from their values
     *
     * @param bool $includeSubForms
     * @return Zend_Form
     */
    public function clearValues($includeSubForms = true)
    {
        $this->_clearFormValues($this, $includeSubForms);
        return $this;
    }

    /**
     * clears all elements from their values
     *
     * @param Zend_Form $form
     * @param bool $includeSubForms
     * @return Zend_Form
     */
    protected function _clearFormValues(Zend_Form $form,
                                        $includeSubForms = true)
    {
        // iterating through all elements of given forms
        foreach ($form->getElements() as $element) {
            if ($element instanceof Zend_Form_Element_Button) {
                continue;
            }
            if ($element instanceof Zend_Form_Element_Submit) {
                continue;
            }
            if ($element instanceof Zend_Form_Element) {
                $element->setValue(null);
            }
        }
        if ($includeSubForms) {
            foreach ($form->getSubForms() as $subForm) {
                $this->_clearFormValues($subForm, $includeSubForms);
            }
        }
        return $this;
    }

    /**
     * returns all values as flat key=>value array
     *
     * by default if field name will be repeated then latest field value will
     * replace previous, setting $appendValues to true will append repeting key
     * values into array
     *
     * @param boolean $appendValues
     * @param array $previousValues
     * @return array
     */
    public function getValuesFlat($appendValues = false, $previousValues = array())
    {
        //workaround for File element to trigger recieve event
        $this->getValues();
        return $this->_getValuesFlat($this, $appendValues, $previousValues);
    }

    /**
     * returns all values as flat key=>value array
     *
     * by default if field name will be repeated then latest field value will
     * replace previous, setting $appendValues to true will append repeting key
     * values into array
     *
     * @param Zend_Form $form
     * @param boolean $appendValues
     * @param array $previousValues
     * @return array
     */
    protected function _getValuesFlat(Zend_Form $form, $appendValues = false,
                                      $previousValues = array())
    {
        $values = (array)$previousValues;
        // iterating through all elements of given forms
        foreach ($form->getElements() as $key => $element) {
            if ($element instanceof Zend_Form_Element_Button) {
                continue;
            }
            if ($element instanceof Zend_Form_Element_Submit) {
                continue;
            }
            if ($element instanceof Zend_Form_Element) {
                $value = $element->getValue();
                if ($appendValues && array_key_exists($key, $values)) {
                    if (is_array($values[$key])) {
                        $values[$key][] = $value;
                    } else {
                        $values[$key] = array(
                            $values[$key],
                            $value
                        );
                    }
                } else {
                    $values[$key] = $value;
                }
            }
        }
        foreach ($form->getSubForms() as $subForm) {
            $values = $this->_getValuesFlat($subForm, $appendValues, $values);
        }
        return $values;
    }
}
