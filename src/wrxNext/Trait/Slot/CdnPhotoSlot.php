<?php

/**
 * adds methos which checks if passed key value differs from slot
 * internal data array key. if so given image is the resized
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Slot_CdnPhotoSlot
{

    public function checkImageChange($value = null, $fullField = 'image',
                                     $thumbnailField = 'thumbnail')
    {
        $refreshThumbnail = false;
        if (!isset($this->_data[$fullField])) {
            $this->_data[$fullField] = '';
        }
        if (!empty($value) && $value != $this->_data[$fullField]) {
            // image changed recreating image thumbnail
            $refreshThumbnail = true;
            $this->_data[$fullField] = $value;;
        } elseif ($value == $this->_data[$fullField] &&
            $this->_data[$fullField . '_size'] != $this->_size
        ) {
            // size have changed, need to recreate thumbnail
            $refreshThumbnail = true;
        }

        if ($refreshThumbnail) {
            $photoMapper = new Admin_Model_Mapper_Photos();
            $this->_data[$thumbnailField] = $photoMapper->resizeCdnPhoto(
                $this->_data[$fullField], $this->_getWidth(),
                $this->_getHeight());
            $this->_data[$fullField . '_size'] = $this->_size;
        }
    }
}
