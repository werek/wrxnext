<?php

/**
 * adds capability to generate subform inside given form, with fileManager elements for given sizes.
 *
 * sizes names should be strings in form of <digit>x<digit> so that they conform to grid standards
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Slot_CdnPhotoOverride
{

    /**
     * adds subform with filemanager fields for given sizes
     *
     * @param Zend_Form $form
     * @param string $subKey
     * @param array $sizes
     * @param int $order
     */
    public function addOverrideSubform(Zend_Form $form, $subKey,
                                       $sizes = array(), $order = null)
    {
        $subForm = new Zend_Form_SubForm();

        if (isset($this->_overrideSizes) && is_array($this->_overrideSizes)) {
            foreach ($this->_overrideSizes as $size) {
                if (array_search($size, $sizes) === false) {
                    $sizes[] = $size;
                }
            }
        }

        sort($sizes);

        foreach ($sizes as $size) {
            $subForm->addElement(
                new Admin_Form_Element_FileManager($size,
                    array(
                        'label' => $size,
                        'filters' => array(
                            'StringTrim',
                            'StripTags'
                        )
                    )));
        }

        $form->addSubForm($subForm, $subKey, $order)
            ->getSubForm($subKey)
            ->setLegend('Slot overrride photos');
    }
}
