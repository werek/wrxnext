<?php

/**
 * adds proxy log method with content aware logging capability through Zend_Log registered under 'log' in Zend_Registry
 * . ie. passing exception will produce code, message and stack trace for given exception. for array and objects their
 * whole structure will be dumped through {@link Zend_Debug::dump() dump}
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_RegistryLog
{

    /**
     * logs message to log residing in registry under key 'log'
     *
     * @param string|Exception $message
     * @param int              $priority
     * @throws wrxNext_Trait_Exception
     */
    public function _log($message, $priority = null)
    {
        $pre = get_class($this) . ': ';
        if (is_null($priority)) {
            $priority = Zend_Log::DEBUG;
        }
        $log = null;
        if (Zend_Registry::isRegistered('log')) {
            $log = Zend_Registry::get('log');
            if ( !$log instanceof Zend_Log) {
                throw new wrxNext_Trait_Exception(
                    'service manager "log" does not contain Zend_Log object'
                );
            }
            if ($message instanceof Exception) {
                do {
                    $text = 'exception: ' . get_class($message) . '(' . $message->getCode() . '): ' . $message->getMessage();
                    $text .= "\n\n" . $message->getTraceAsString();
                    $log->log($pre . $text, $priority);
                    $message = $message->getPrevious();
                } while ($message instanceof Exception);
            } elseif ($message instanceof JsonSerializable) {
                $message = get_class($message) . ' JSON: ' .
                    Zend_Json::encode($message);
                $log->log($pre . $message, $priority);
            } elseif (is_array($message) || is_object($message)) {
                $message = Zend_Debug::dump($message, null, false);
                if (PHP_SAPI != 'cli') {
                    $message = html_entity_decode($message);
                }
                $log->log($pre . $message, $priority);
            } elseif (is_null($message)) {
                $message = 'NULL value';
                $log->log($pre . $message, $priority);
            } else {
                $log->log($pre . $message, $priority);
            }
        }
    }
}
