<?php

/**
 * Exception for any trait related functionalities
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Trait_Exception extends wrxNext_Exception
{
}
