<?php

/**
 * this trait overrides translation method of element, in effect disabling ability to translate
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Form_Element_DisableTranslateValue
{

    /**
     * option to disable value translation
     *
     * @param  string $value
     * @return string
     */
    protected function _translateValue($value)
    {
        return $value;
    }
}
