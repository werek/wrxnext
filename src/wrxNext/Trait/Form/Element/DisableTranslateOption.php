<?php

/**
 * this trait overrides default translation check of option in multi elements, in effect disabling translation of values
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
trait wrxNext_Trait_Form_Element_DisableTranslateOption
{
    /**
     * override to disable option translation
     *
     * @param  string $option
     * @param  string $value
     * @return bool
     */
    protected function _translateOption($option, $value)
    {
        return false;
    }
}
