<?php

class wrxNext_Slot_Grid
{

    const COORDINATES_REGEX = '@^(\d*?)x(\d*?)$@i';

    /**
     * holds regex patern for coordinates
     *
     * @var string
     */
    protected static $_coordinatesRegex = '@^(\d*?)x(\d*?)$@i';

    /**
     * holds previously calculated values for given grid sizes
     *
     * @var array
     */
    protected static $_calculatedSizes = array();

    /**
     * holds previously calculated values for given slot position
     *
     * @var unknown
     */
    protected static $_calculatedPosition = array();

    /**
     * holds slot border length
     *
     * @var int
     */
    protected static $_slotSize = null;

    /**
     * holds slot spaceing
     *
     * @var int
     */
    protected static $_slotSpacing = null;

    /**
     * holds grid width
     *
     * @return int
     */
    protected static $_gridWidth = null;

    /**
     * holds number of columns in grid
     *
     * @var int
     */
    protected static $_gridColumns = null;

    /**
     * returns slot border length
     *
     * @return int
     */
    public static function getSlotSize()
    {
        return self::$_slotSize;
    }

    /**
     * sets slot border length
     *
     * @param int $size
     */
    public static function setSlotSize($size)
    {
        self::$_slotSize = (int)$size;
    }

    /**
     * returns slot spacing
     *
     * @return int
     */
    public static function getSlotSpacing()
    {
        return self::$_slotSpacing;
    }

    /**
     * sets slot spacing
     *
     * @param int $size
     */
    public static function setSlotSpacing($size)
    {
        self::$_slotSpacing = (int)$size;
    }

    /**
     * returns grid width
     *
     * @param int $gridColumns
     * @return int
     */
    public static function getGridWidth($gridColumns = null)
    {
        if (!is_null($gridColumns)) {
            return self::getSlotWidth($gridColumns . 'x1');
        }
        return self::$_gridWidth;
    }

    /**
     * sets grid width
     *
     * @param int $size
     */
    public static function setGridWidth($size)
    {
        self::$_gridWidth = (int)$size;
    }

    /**
     * calculates horizontal length for given $gridSize (for example: 4x3)
     *
     * @param string $gridSize
     * @return int null
     */
    public static function getSlotWidth($gridSize)
    {
        if (preg_match(self::COORDINATES_REGEX, $gridSize, $wyn)) {
            $gridLength = $wyn[1];
            if (!isset(self::$_calculatedSizes[$gridLength])) {
                $width = $gridLength * self::$_slotSize;
                if ($gridLength > 1) {
                    $width += ($gridLength - 1) * self::$_slotSpacing;
                }
                self::$_calculatedSizes[$gridLength] = $width;
            }
            return self::$_calculatedSizes[$gridLength];
        }
        return null;
    }

    /**
     * returns grid columns number
     *
     * @return int
     */
    public static function getGridColumns()
    {
        return self::$_gridColumns;
    }

    /**
     * sets grid column number
     *
     * @param int $gridColumns
     */
    public static function setGridColumns($gridColumns)
    {
        self::$_gridColumns = (int)$gridColumns;
    }

    /**
     * calculates vertical length for given $gridSize (for example: 4x3)
     *
     * @param string $gridSize
     * @return int null
     */
    public static function getSlotHeight($gridSize)
    {
        if (preg_match(self::COORDINATES_REGEX, $gridSize, $wyn)) {
            $gridLength = $wyn[2];
            if (!isset(self::$_calculatedSizes[$gridLength])) {
                $height = $gridLength * self::$_slotSize;
                if ($gridLength > 1) {
                    $height += ($gridLength - 1) * self::$_slotSpacing;
                }
                self::$_calculatedSizes[$gridLength] = $height;
            }
            return self::$_calculatedSizes[$gridLength];
        }
        return null;
    }

    /**
     * calculates distance from left border of grid, based on given position
     *
     * @param string $position
     * @return int
     */
    public static function getSlotLeft($position)
    {
        if (preg_match(self::COORDINATES_REGEX, $position, $wyn)) {
            $gridLength = $wyn[2] - 1;
            if (!isset(self::$_calculatedPosition[$gridLength])) {
                $height = $gridLength * self::$_slotSize;
                if ($gridLength > 0) {
                    $height += $gridLength * self::$_slotSpacing;
                }

                self::$_calculatedPosition[$gridLength] = $height;
            }
            return self::$_calculatedPosition[$gridLength];
        }
        return null;
    }

    /**
     * calculates distance from top border of grid, based on given position
     *
     * @param string $position
     * @return int
     */
    public static function getSlotTop($position)
    {
        if (preg_match(self::COORDINATES_REGEX, $position, $wyn)) {
            $gridLength = $wyn[1] - 1;
            if (!isset(self::$_calculatedPosition[$gridLength])) {
                $height = $gridLength * self::$_slotSize;
                if ($gridLength > 0) {
                    $height += $gridLength * self::$_slotSpacing;
                }

                self::$_calculatedPosition[$gridLength] = $height;
            }
            return self::$_calculatedPosition[$gridLength];
        }
        return null;
    }

    /**
     * calculates first available slot position for given slots
     *
     * @param string $size
     * @param array $slotsData
     * @param null|int $gridColumns number of columns
     * @return string false if position could not be found
     */
    public static function findAvailablePositionForSize($size, $slotsData,
                                                        $gridColumns = null)
    {
        $position = false;
        if (preg_match(self::COORDINATES_REGEX, trim($size), $wyn)) {
            if (is_null($gridColumns)) {
                $gridColumns = self::$_gridColumns;
            }

            $gridMap = new wrxNext_Slot_Grid_Map(new wrxNext_Slot_Collection($slotsData),
                array(
                    'columns' => $gridColumns
                ));
            $position = $gridMap->getPositionForSlotSize($size);
        }
        return $position;
    }

    /**
     * splits coordinates
     *
     * @param string $coordinates
     * @return array
     */
    public static function splitCoordinates($coordinates)
    {
        if (preg_match(self::COORDINATES_REGEX, trim($coordinates), $slotPos)) {
            return array(
                $slotPos[1],
                $slotPos[2]
            );
        }
        return false;
    }
}
