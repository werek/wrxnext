<?php

class wrxNext_Slot_Render_Css_Entry
{

    protected $_tag = '';

    protected $_width = 0;

    protected $_height = 0;

    protected $_left = 0;

    protected $_top = 0;

    protected $_item = null;

    public function __construct($selector)
    {
        $this->setSelector($selector);
    }

    /**
     * sets css selector
     *
     * @param string $selector
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setSelector($selector)
    {
        $this->_tag = $selector;
        return $this;
    }

    /**
     * echoes css definition
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render() . "\n";
    }

    /**
     * renders css selector
     *
     * @return string
     */
    public function render()
    {
        return $this->getSelector() . ' {width:' . $this->getWidth() .
        'px; height:' . $this->getHeight() . 'px; left:' .
        $this->getLeft() . 'px; top:' . $this->getTop() . 'px;}';
    }

    /**
     * gets css selector
     *
     * @return string
     */
    public function getSelector()
    {
        return $this->_tag;
    }

    /**
     * gets width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * sets width
     *
     * @param int $width
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setWidth($width)
    {
        $this->_width = $width;
        return $this;
    }

    /**
     * gets height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * sets height
     *
     * @param int $height
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setHeight($height)
    {
        $this->_height = $height;
        return $this;
    }

    /**
     * gets left
     *
     * @return int
     */
    public function getLeft()
    {
        return $this->_left;
    }

    /**
     * sets left
     *
     * @param int $left
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setLeft($left)
    {
        $this->_left = $left;
        return $this;
    }

    /**
     * gets top
     *
     * @return int
     */
    public function getTop()
    {
        return $this->_top;
    }

    /**
     * sets top
     *
     * @param int $top
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setTop($top)
    {
        $this->_top = $top;
        return $this;
    }

    /**
     * gets colection item
     *
     * @return wrxNext_Slot_Collection_Item
     */
    public function getItem()
    {
        return $this->_item;
    }

    /**
     * sets colection item
     *
     * @param wrxNext_Slot_Collection_Item $item
     * @return wrxNext_Slot_Render_Css_Entry
     */
    public function setItem($item)
    {
        $this->_item = $item;
        return $this;
    }
}
