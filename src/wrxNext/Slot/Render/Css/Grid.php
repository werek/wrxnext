<?php

class wrxNext_Slot_Render_Css_Grid
{

    /**
     * holds css selector
     *
     * @var string
     */
    protected $_selector = '';

    /**
     * holds grid width
     *
     * @var int
     */
    protected $_width = 0;

    /**
     * holds grid height
     *
     * @var int
     */
    protected $_height = 0;

    /**
     * accepts css selector as string
     *
     * @param string $selector
     */
    public function __construct($selector)
    {
        $this->setSelector($selector);
    }

    /**
     * magic method for echoing css declaration
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders css code
     *
     * @return string
     */
    public function render()
    {
        return $this->getSelector() . ' {position:relative; width:' .
        $this->getWidth() . 'px; height:' . $this->getHeight() . 'px;}';
    }

    /**
     * gets selector
     *
     * @return string
     */
    public function getSelector()
    {
        return $this->_selector;
    }

    /**
     * sets selector
     *
     * @param string $selector
     * @return wrxNext_Slot_Render_Css_Grid
     */
    public function setSelector($selector)
    {
        $this->_selector = $selector;
        return $this;
    }

    /**
     * gets width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * sets width
     *
     * @param int $width
     * @return wrxNext_Slot_Render_Css_Grid
     */
    public function setWidth($width)
    {
        $this->_width = $width;
        return $this;
    }

    /**
     * gets height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * sets height
     *
     * @param int $height
     * @return wrxNext_Slot_Render_Css_Grid
     */
    public function setHeight($height)
    {
        $this->_height = $height;
        return $this;
    }
}
