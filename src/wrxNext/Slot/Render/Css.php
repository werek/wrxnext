<?php

class wrxNext_Slot_Render_Css extends wrxNext_Slot_Render_Abstract
{
    /**
     * holds heights of custom rows
     *
     * @var array
     */
    protected $_rowHeights = array();

    /**
     * holds all css declarations
     *
     * @var array
     */
    protected $_cssDeclarations = array();

    /**
     * hols slot mappings to selectors
     *
     * @var array
     */
    protected $_slotCssSelectors = array();

    /**
     * holds css entries
     *
     * @var array
     */
    protected $_cssEntries = array();

    /**
     * handles echoing css declarations
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders css
     *
     * @return string
     */
    public function render()
    {
        foreach ($this->_gridCollection->getIterator() as $slotItem) {
            $this->_cssEntry($slotItem);
        }
        $this->_traversMap();
        return implode("\n", $this->_cssDeclarations) . "\n";
    }

    /**
     * builds css entry for given slot item
     *
     * @param wrxNext_Slot_Collection_Item $slotItem
     */
    protected function _cssEntry(wrxNext_Slot_Collection_Item $slotItem)
    {
        $slotData = $this->_buildSlotData($slotItem);
        //defining css selector
        if (array_key_exists($slotItem->slotIdentifier(), $this->_slotCssSelectors)) {
            //given slot has overriden css selector
            $selector = $this->_slotCssSelectors[$slotItem->slotIdentifier()];
        } else {
            $selector = $this->_buildCssSelector($slotData['class_raw']);
        }
        $cssEntry = new wrxNext_Slot_Render_Css_Entry($selector);
        $cssEntry->setItem($slotItem);
        $slot = $this->_getSlotContextObject($slotItem);
        if ($slot instanceof wrxNext_Slot_Interface_Size) {
            $cssEntry->setWidth($slot->getWidth());
            $cssEntry->setHeight($slot->getHeight());
        } else {
            $cssEntry->setWidth(
                wrxNext_Slot_Grid::getSlotWidth($slotItem->getSize()));
            $cssEntry->setHeight(
                wrxNext_Slot_Grid::getSlotHeight($slotItem->getSize()));
        }


        //saving css entry
        $this->_cssEntries[$slotItem->slotIdentifier()] = $cssEntry;
        $columns = $this->_gridCollection->getMaxColumns();
        // recognition of row with custom height
        if (wrxNext_Slot_Grid::getSlotHeight($slotItem->getSize()) !=
            $cssEntry->getHeight() &&
            $slotItem->getGridWidth() >= $this->_gridCollection->getMaxColumns()
        ) {
            $this->_rowHeights[$slotItem->getGridRow()] = $cssEntry->getHeight();
        }
    }

    /**
     * converts html class identifier to css class selector
     *
     * @param string $classRaw
     * @return string
     */
    protected function _buildCssSelector($classRaw)
    {
        return str_replace(' ', '', trim($classRaw));
    }

    /**
     * walks through array of slot positions and calculates necesary position
     * coordinates
     * including difference from custom height rows
     */
    protected function _traversMap()
    {
        ksort($this->_rowHeights);
        $map = $this->_gridCollection->getMapArray();
        for ($row = 1; $row <= $this->_gridCollection->getMaxRows(); $row++) {
            for ($column = 1; $column <= $this->_gridCollection->getMaxColumns(); $column++) {
                if (isset($map[$row][$column])) {
                    if ($map[$row][$column] instanceof wrxNext_Slot_Collection_Item) {
                        $item = $map[$row][$column];
                        $cssEntry = $this->_cssEntries[$item->slotIdentifier()];
                        if (!array_key_exists($cssEntry->getSelector(),
                            $this->_cssDeclarations)
                        ) {
                            $cssEntry->setLeft(
                                wrxNext_Slot_Grid::getSlotLeft(
                                    $row . 'x' . $column));
                            // custom height rows
                            $cssEntry->setTop($this->_calculateSlotTop($row));
                            // rendering css position data
                            if (array_key_exists($item->slotIdentifier(), $this->_slotCssSelectors)) {
                                $selector = $this->_slotCssSelectors[$item->slotIdentifier()];
                            } else {
                                $selector = $cssEntry->getSelector();
                            }
                            $this->_cssDeclarations[$selector] = $cssEntry->render();
                        }
                    }
                }
            }
        }

        // getting grid dimensions
        $grid = new wrxNext_Slot_Render_Css_Grid($this->_buildGridClassRaw());
        $grid->setWidth(
            wrxNext_Slot_Grid::getSlotWidth(
                $this->_gridCollection->getMaxColumns() . 'x' . $this->_gridCollection->getMaxRows()));
        $grid->setHeight($this->_calculateSlotTop($this->_gridCollection->getMaxRows() + 1));
        $this->_cssDeclarations[$grid->getSelector()] = $grid->render();
    }

    /**
     * calculates top pixel position of slot by given row number
     *
     * @param int $row
     * @return int
     */
    protected function _calculateSlotTop($row)
    {
        if (count($this->_rowHeights)) {
            // retrieving rows numbers
            $customRows = array_keys($this->_rowHeights);
            if ($row > $customRows[0]) {
                // getting base for first custom row
                $top = wrxNext_Slot_Grid::getSlotTop($customRows[0] . 'x1');
                $slotStandardHeight = wrxNext_Slot_Grid::getSlotHeight('1x1');
                $slotStandardSpacing = wrxNext_Slot_Grid::getSlotSpacing();
                for ($i = $customRows[0]; $i < $row; $i++) {
                    if (isset($this->_rowHeights[$i])) {
                        // adding custom row height
                        $top += $this->_rowHeights[$i];
                    } else {
                        $top += $slotStandardHeight;
                    }
                    $top += $slotStandardSpacing;
                }
                return $top;
            } else {
                return wrxNext_Slot_Grid::getSlotTop($row . 'x1');
            }
        } else {
            return wrxNext_Slot_Grid::getSlotTop($row . 'x1');
        }
    }

    /**
     * builds grid class selector
     *
     * @return string
     */
    protected function _buildGridClassRaw()
    {
        return '.' . $this->_buildCssSelector($this->getGridClass());
    }

    /**
     * sets slot css selectors Mappings
     *
     * @param array $mappings
     * @return wrxNext_Slot_Render_Rwd_MediaQuery
     */
    public function setCssMappings($mappings)
    {
        $this->_slotCssSelectors = $mappings;
        return $this;
    }

    /**
     * gets slot css selectors Mappings
     *
     * @return array
     */
    public function getCssMappings()
    {
        return $this->_slotCssSelectors;
    }

    /**
     * calculates total grid height
     *
     * @return number
     */
    protected function _calculateGridHeight()
    {
        if (count($this->_rowHeights)) {
            $standardRows = $this->_gridCollection->getMaxRows() - count($this->_rowHeights);
            $standardRowsHeight = wrxNext_Slot_Grid::getSlotHeight('1x' . $standardRows);
            $height = $standardRowsHeight + wrxNext_Slot_Grid::getSlotSpacing();
            $height += array_sum($this->_rowHeights) + ((count($this->_rowHeights) - 1) * wrxNext_Slot_Grid::getSlotSpacing());
        } else {
            $height = wrxNext_Slot_Grid::getSlotHeight('1x' . $this->_gridCollection->getMaxRows());
        }
        return intval($height);
    }

    /**
     * builds slot css class definition
     *
     * @param wrxNext_Slot_Collection_Item $slotItem
     * @return string
     */
    protected function _buildSlotClassRaw(wrxNext_Slot_Collection_Item $slotItem)
    {
        $slotClassRaw[] = '.' . $this->getSlotClass();
        $slotClassRaw[] = '.' . $this->getPositionClass() .
            $slotItem->getPosition();
        $slotClassRaw[] = '.' . $this->getSizeClass() . $slotItem->getSize();
        return implode(' ', $slotClassRaw);
    }

    /**
     * returns css code for slot size
     *
     * @param wrxNext_Slot_Render_Css_Entry $entry
     * @return string
     */
    protected function _buildSize(wrxNext_Slot_Render_Css_Entry $entry)
    {
        return '.' . $this->getSizeClass() . $entry->getItem()->getSize() .
        ' {width: ' . $entry->getWidth() . 'px; height:' .
        $entry->getHeight() . 'px;}';
    }

    /**
     * returns css code for slot position
     *
     * @param wrxNext_Slot_Render_Css_Entry $entry
     * @return string
     */
    protected function _buildPosition(wrxNext_Slot_Render_Css_Entry $entry)
    {
        return '.' . $this->getPositionClass() . $entry->getItem()->getPosition() .
        ' {left:' . $entry->getLeft() . 'px; top:' . $entry->getTop() .
        'px;}';
    }
}
