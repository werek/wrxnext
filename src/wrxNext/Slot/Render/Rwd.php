<?php

class wrxNext_Slot_Render_Rwd extends wrxNext_Slot_Render_Abstract
{
    /**
     * holds slot css query mappings
     *
     * @var array
     */
    protected $_slotSet = array();

    /**
     * holds columns count to which grid should be recalculated
     *
     * @var array
     */
    protected $_columnsStep = array(
        2,
        4,
        6
    );

    /**
     * holds all of query sets for each columns configuration
     *
     * @var wrxNext_Slot_Render_Rwd_MediaQuery[]
     */
    protected $_querySet = array();

    /**
     * magic method for echoing whole code
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders rwd css
     *
     * @return string
     */
    public function render()
    {
        $this->buildMediaQuerySets();
        $css = '';
        foreach (array_reverse($this->getMediaQuerySets()) as $mediaQuery) {
            $css .= $mediaQuery->render();
        }
        return $css;
    }

    /**
     * builds media query set
     *
     * @return wrxNext_Slot_Render_Rwd
     */
    public function buildMediaQuerySets()
    {
        $this->_querySet = array();
        $this->_buildSlotMappings();
        $steps = $this->_getSteps();
        $lowestColumnsNumber = $steps[0];
        foreach ($steps as $columns) {
            if ($columns == $lowestColumnsNumber) {
                // fix for mobile devices so media query matches also lower
                // resolution
                $mediaQuery = $this->_buildMediaQuery($columns,
                    array(
                        'min-width'
                    ));
            } else {
                $mediaQuery = $this->_buildMediaQuery($columns);
            }
            $this->_querySet[] = $mediaQuery;
        }
        return $this;
    }

    /**
     * builds static slot mapping
     */
    protected function _buildSlotMappings()
    {
        foreach ($this->_gridCollection->getIterator() as $item) {
            $slotData = $this->_buildSlotData($item);
            $this->_slotSet[$item->slotIdentifier()] = '.' .
                str_replace(' ', '.', $slotData['class_raw']);
        }
    }

    /**
     * builds lowered columns number sets
     *
     * @return array
     */
    protected function _getSteps()
    {
        $steps = $this->_columnsStep;
        $columns = $this->_gridCollection->getMaxColumns();
        if (count($steps) && $columns <= max($steps)) {
            while (count($steps) && $columns <= max($steps)) {
                array_pop($steps);
            }
        }
        sort($steps);
        return $steps;
    }

    /**
     * builds media query object for given columns, optionally accepts array of
     * media query keys to skip from auto generated queries
     *
     * @param int $columns
     * @param array $removeQueries
     * @return wrxNext_Slot_Render_Rwd_MediaQuery
     */
    protected function _buildMediaQuery($columns, $removeQueries = array())
    {
        $collection = clone $this->_gridCollection;
        $collection->setColumns($columns)->recalculate();
        $queries = array(
            'min-width' => wrxNext_Slot_Grid::getGridWidth($columns) + 20,
            'max-width' => wrxNext_Slot_Grid::getGridWidth($columns + 2) + 19
        );
        if (count($removeQueries)) {
            foreach ($removeQueries as $queryKey) {
                if (array_key_exists($queryKey, $queries)) {
                    unset($queries[$queryKey]);
                };
            };
        }
        $options = array(
            'cssMappings' => $this->_slotSet,
            'queries' => $queries
        );
        $mediaQuery = new wrxNext_Slot_Render_Rwd_MediaQuery($collection, $options);
        $mediaQuery->setView($this->getView());
        return $mediaQuery;
    }

    /**
     * returns media query sets
     *
     * @return wrxNext_Slot_Render_Rwd_MediaQuery[]
     */
    public function getMediaQuerySets()
    {
        return $this->_querySet;
    }

    /**
     * sets columns steps for RWD reflow
     *
     * @param array $columns
     * @return wrxNext_Slot_Render_Rwd
     * @throws wrxNext_Slot_Render_Exception
     */
    public function setColumnsNumbers($columns)
    {
        if (!is_array($columns)) {
            throw new wrxNext_Slot_Render_Exception(
                'columns numbers should be passed as array');
        }
        sort($columns);
        $this->_columnsStep = $columns;
        return $this;
    }

    /**
     * gets columns steps for RWD reflow
     *
     * @return array
     */
    public function getColumnsNumbers()
    {
        return $this->_columnsStep;
    }
}
