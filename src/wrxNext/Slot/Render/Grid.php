<?php

class wrxNext_Slot_Render_Grid extends wrxNext_Slot_Render_Abstract
{

    const TEMPLATE_SLOT = '<div data-row="%row%" data-col="%column%" data-sizex="%width%" data-sizey="%height%" class="%class_raw%" %additional%>%content%</div>';

    const TEMPLATE_GRID = '<div class="%class_raw%">%content%</div>';

    /**
     * holds flag for rendering additional content
     *
     * @var boolean
     */
    protected $_renderAdditional = true;

    /**
     * holds slot html template
     *
     * @var string
     */
    protected $_templateSlot = self::TEMPLATE_SLOT;

    /**
     * holds grid html template
     *
     * @var string
     */
    protected $_templateGrid = self::TEMPLATE_GRID;

    /**
     * renders grid
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * renders grid
     *
     * @throws wrxNext_Slot_Render_Exception
     */
    public function render()
    {
        if (!$this->getView() instanceof Zend_View_Interface) {
            throw new wrxNext_Slot_Render_Exception(
                'view object is not valid zend_view_interface object', 1);
        }
        $html = '';
        foreach ($this->_gridCollection->getIterator() as $slotItem) {
            $slot = $this->_getSlotContextObject($slotItem);

            $slot->setView($this->getView());
            $html .= $this->_buildSlot($slot, $this->_buildSlotData($slotItem));
        }

        return $this->_buildGrid($html);
    }

    /**
     * builds slot html
     *
     * @param wrxNext_Slot_Frontend_Interface|wrxNext_Slot_Backend_Interface $slot
     * @param array $slotData
     * @return string
     */
    protected function _buildSlot($slot, $slotData)
    {
        if ($slot instanceof wrxNext_Slot_Interface_Render) {
            return $slot->render($slotData['size'], $slotData['position']);
        } else {
            return $this->_buildSlotFromTemplate($slot, $slotData);
        }
    }

    /**
     * builds slot html with current template
     *
     * @param wrxNext_Slot_Frontend_Interface|wrxNext_Slot_Backend_Interface $slot
     * @param array $slotData
     * @throws wrxNext_Slot_Render_Exception
     * @return string
     */
    protected function _buildSlotFromTemplate($slot, $slotData)
    {
        // rendering slot content
        ob_start();
        echo $slot;
        $slotData['content'] = ob_get_clean();

        if ($this->getAdditionalParams()) {
            $slotData['additional'] = $this->_buildSlotParams(
                $slotData['params']);
        }

        return $this->_buildTemplate($this->getTemplateSlot(), $slotData);
    }

    /**
     * gets flag for rendering additional content, true by default
     *
     * @return boolean
     */
    public function getAdditionalParams()
    {
        return $this->_renderAdditional;
    }

    /**
     * builds tag params
     *
     * @param string $params
     * @return string
     */
    protected function _buildSlotParams($params)
    {
        $entityParams = array();
        foreach ($params as $key => $value) {
            $entityParams[] = htmlspecialchars($key) . '="' .
                htmlspecialchars($value) . '"';
        }
        return implode(' ', $entityParams);
    }

    /**
     * replaces template placeholders with given data
     *
     * @param string $template
     * @param array $data
     * @throws wrxNext_Slot_Render_Exception
     * @return string
     */
    protected function _buildTemplate($template, $data)
    {
        preg_match_all('@%(.*?)%@miu', $template, $vars);
        if (!is_array($vars[1]) || !in_array('content', $vars[1])) {
            throw new wrxNext_Slot_Render_Exception(
                'render template must contain at least "%content%" key placeholder',
                2);
        }

        // preparing placeholders data
        foreach ($vars[1] as $placeholder) {
            $replace['%' . $placeholder . '%'] = (isset($data[$placeholder])) ? $data[$placeholder] : '';
        }

        // replacing placeholders and returning content
        return strtr($template, $replace);
    }

    /**
     * gets slot template
     *
     * @return string
     */
    public function getTemplateSlot()
    {
        return $this->_templateSlot;
    }

    /**
     * sets slot template
     *
     * @param string $template
     * @return wrxNext_Slot_Render_Grid
     */
    public function setTemplateSlot($template)
    {
        $this->_templateSlot = $template;
        return $this;
    }

    /**
     * builds grid html
     *
     * @param string $content
     * @return string
     */
    protected function _buildGrid($content)
    {
        $data['class_raw'] = $this->getGridClass();
        $data['content'] = $content;
        return $this->_buildTemplate($this->getTemplateGrid(), $data);
    }

    /**
     * gets grid template
     *
     * @return string
     */
    public function getTemplateGrid()
    {
        return $this->_templateGrid;
    }

    /**
     * sets grid template
     *
     * @param string $template
     * @return wrxNext_Slot_Render_Grid
     */
    public function setTemplateGrid($template)
    {
        $this->_templateGrid = $template;
        return $this;
    }

    /**
     * sets flag for rendering additional content, true by default
     *
     * @param boolean $render
     * @return wrxNext_Slot_Render_Grid
     */
    public function setAdditionalParams($render)
    {
        $this->_renderAdditional = $render;
        return $this;
    }
}
