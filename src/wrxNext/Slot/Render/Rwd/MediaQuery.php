<?php

class wrxNext_Slot_Render_Rwd_MediaQuery extends wrxNext_Slot_Render_Css
{
    use wrxNext_Trait_Template;
    const CSS_TEMPLATE = "@media %queries% \n{\n %css% \n}\n";

    /**
     * holds template for generating css media query code
     *
     * @var string
     */
    protected $_queryTemplate = self::CSS_TEMPLATE;

    /**
     * queries to add to media query definitions
     *
     * @var string
     */
    protected $_queries = array();

    /**
     * @inheritdoc
     */
    public function render()
    {
        $data['css'] = trim(parent::render());
        $queriesParsed = array();
        foreach ($this->_queries as $key => $value) {
            $queriesParsed[] = '(' . $key . ':' .
                (is_int($value) ? $value . 'px' : $value) . ')';
        }
        $data['queries'] = implode(' and ', $queriesParsed);
        $cssCode = $this->_buildTemplate($this->getMediaQueryTemplate(), $data);
        return $cssCode;
    }

    /**
     * gets media query template
     *
     * @return string
     */
    public function getMediaQueryTemplate()
    {
        return $this->_queryTemplate;
    }

    /**
     * gets media queries
     *
     * @return array
     */
    public function getQueries()
    {
        return $this->_queries;
    }

    /**
     * sets media queries
     *
     * @param array $queries
     * @return wrxNext_Slot_Render_Rwd_MediaQuery
     */
    public function setQueries($queries)
    {
        $this->_queries = $queries;
        return $this;
    }

    /**
     * sets media query template
     *
     * @param string $template
     * @return wrxNext_Slot_Render_Rwd_MediaQuery
     */
    public function setMediaQueryTemplate($template)
    {
        $this->_queryTemplate = $template;
        return $this;
    }
}
