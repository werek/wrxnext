<?php

abstract class wrxNext_Slot_Render_Abstract
{

    const BACKEND = 'backend';

    const FRONTEND = 'frontend';

    const CLASS_GRID = 'grid';

    const CLASS_SLOT = 'slot';

    /**
     * holds view instance
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;
    /**
     * flag which slot content to output
     *
     * @var string
     */
    protected $_render = self::FRONTEND;

    /**
     * single slot wrapper class
     *
     * @var string
     */
    protected $_slotClass = self::CLASS_SLOT;

    /**
     * grid wrapper class
     *
     * @var string
     */
    protected $_gridClass = self::CLASS_GRID;

    /**
     * position class prefix
     *
     * @var string
     */
    protected $_positionClass = 'position';

    /**
     * size class prefix
     *
     * @var string
     */
    protected $_sizeClass = 'size';

    /**
     * hold grid collection instance
     *
     * @var wrxNext_Slot_Grid_Collection
     */
    protected $_gridCollection = null;

    /**
     * constructor, optionally accepts array of params
     *
     * @param wrxNext_Slot_Grid_Collection $gridCollection
     * @param array $options
     */
    public function __construct(wrxNext_Slot_Grid_Collection $gridCollection,
                                $options = array())
    {
        $this->_gridCollection = $gridCollection;
        $this->setOptions($options);
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * gets grid class
     *
     * @return string
     */
    public function getGridClass()
    {
        return $this->_gridClass;
    }

    /**
     * sets grid class
     *
     * @param string $class
     * @return wrxNext_Slot_Render_Grid
     */
    public function setGridClass($class)
    {
        $this->_gridClass = $class;
        return $this;
    }

    /**
     * sets render context
     *
     * @param string $context
     * @return wrxNext_Slot_Render_Grid
     */
    public function setContext($context)
    {
        $this->_render = $context;
        return $this;
    }

    /**
     * gets render context
     *
     * @return string
     */
    public function getContext()
    {
        return $this->_render;
    }

    /**
     * builds slot template data
     *
     * @param wrxNext_Slot_Collection_Item $slotItem
     * @return array
     */
    protected function _buildSlotData(wrxNext_Slot_Collection_Item $slotItem)
    {
        $data = array();
        $data['size'] = $slotItem->getSize();
        if (($position = wrxNext_Slot_Grid::splitCoordinates($slotItem->getPosition())) !== false) {
            $data['row'] = $position[0];
            $data['column'] = $position[1];
        }
        $data['position'] = $slotItem->getPosition();
        if (($size = wrxNext_Slot_Grid::splitCoordinates($slotItem->getSize())) !== false) {
            $data['width'] = $size[0];
            $data['height'] = $size[1];
        }
        $data['class_raw'] = $this->_buildSlotClassRaw($slotItem);
        $data['params'] = (array)$slotItem->getAllParams();
        return $data;
    }

    /**
     * builds slot css class definition
     *
     * @param wrxNext_Slot_Collection_Item $slotItem
     * @return string
     */
    protected function _buildSlotClassRaw(wrxNext_Slot_Collection_Item $slotItem)
    {
        $slotClassRaw[] = $this->getSlotClass();
        $slotClassRaw[] = $this->getPositionClass() . $slotItem->getPosition();
        $slotClassRaw[] = $this->getSizeClass() . $slotItem->getSize();
        return implode(' ', $slotClassRaw);
    }

    /**
     * gets slot class
     *
     * @return string
     */
    public function getSlotClass()
    {
        return $this->_slotClass;
    }

    /**
     * sets slot class
     *
     * @param string $class
     * @return wrxNext_Slot_Render_Grid
     */
    public function setSlotClass($class)
    {
        $this->_slotClass = $class;
        return $this;
    }

    /**
     * gets position class prefix
     *
     * @return string
     */
    public function getPositionClass()
    {
        return $this->_positionClass;
    }

    /**
     * sets position class prefix
     *
     * @param string $prefix
     * @return wrxNext_Slot_Render_Grid
     */
    public function setPositionClass($prefix)
    {
        $this->_positionClass = $prefix;
        return $this;
    }

    /**
     * gets size class prefix
     *
     * @return string
     */
    public function getSizeClass()
    {
        return $this->_sizeClass;
    }

    /**
     * sets size class prefix
     *
     * @param string $prefix
     * @return wrxNext_Slot_Render_Grid
     */
    public function setSizeClass($prefix)
    {
        $this->_sizeClass = $prefix;
        return $this;
    }

    /**
     * returns slot context object
     *
     * @param wrxNext_Slot_Collection_Item $slotItem
     * @return wrxNext_Slot_Frontend_Interface|wrxNext_Slot_Backend_Interface
     */
    protected function _getSlotContextObject(wrxNext_Slot_Collection_Item $slotItem)
    {
        switch ($this->_render) {
            case self::BACKEND:
                $slot = $slotItem->getBackend();
                break;
            case self::FRONTEND:
            default:
                $slot = $slotItem->getFrontend();
                break;
        }
        $slot->setView($this->getView());
        return $slot;
    }

    /**
     * gets view object
     *
     * @return Zend_View_Interface
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * sets view object
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Grid_Render_Abstract
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }
}
