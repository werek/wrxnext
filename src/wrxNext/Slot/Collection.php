<?php

class wrxNext_Slot_Collection
{

    /**
     * Iterator interface
     *
     * @var int
     */
    protected $_position = 0;

    /**
     * holds slots rows
     *
     * @var wrxNext_Slot_Collection_Item[]
     */
    protected $_slots = array();

    /**
     * taken array of slot rows
     *
     * @param array $slotCollection
     */
    public function __construct($slotCollection = null)
    {
        if (is_array($slotCollection)) {
            $this->setCollection($slotCollection);
        }
    }

    /**
     * sets slots collection
     *
     * @param array $slotCollection
     * @return wrxNext_Slot_Collection
     */
    public function setCollection($slotCollection)
    {
        $this->_slots = array();
        foreach ($slotCollection as $data) {
            if ($data instanceof wrxNext_Slot_Collection_Item) {
                $this->_slots[] = $data;
            } elseif (is_array($data) && !empty($data['slot_class'])) {
                $this->_slots[] = new wrxNext_Slot_Collection_Item($data);
            }
        }
        return $this;
    }

    /**
     * adds slot to collection
     *
     * @param string $slotSize
     * @param string $slotPosition
     * @param string $slotClass
     * @param mixed $slotData
     * @return wrxNext_Slot_Collection_Item
     */
    public function addSlot($slotSize, $slotPosition, $slotClass,
                            $slotData = null)
    {
        $data['slot_size'] = $slotSize;
        $data['slot_position'] = $slotPosition;
        $data['slot_class'] = $slotClass;
        $data['slot_content'] = $slotData;
        $slot = new wrxNext_Slot_Collection_Item($data);
        $this->_slots[] = $slot;
        return $slot;
    }

    /**
     * gets slots collection
     *
     * @return array
     */
    public function getCollection()
    {
        return $this->toArray();
    }

    /**
     * returns array of slot item objects
     *
     * @return wrxNext_Slot_Collection_Item[]
     */
    public function toArray($itemsAsArray = false)
    {
        if ($itemsAsArray) {
            $tmp = array();
            foreach ($this->_slots as $slot) {
                $tmp[] = $slot->toArray();
            }
            return $tmp;
        }
        return $this->_slots;
    }

    /**
     * returns iterator
     *
     * @return wrxNext_Slot_Controller_Item[]
     */
    public function getIterator()
    {
        return new ArrayIterator($this->_slots);
    }

    /**
     * returns slot number
     *
     * @return number
     */
    public function count()
    {
        return count($this->_slots);
    }

    /**
     * override shallow copy clone every slot object
     */
    public function __clone()
    {
        $newSlots = array();
        foreach ($this->_slots as $slot) {
            $newSlots[] = clone $slot;
        }
        $this->_slots = $newSlots;
    }
}
