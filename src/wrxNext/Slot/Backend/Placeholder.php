<?php

class wrxNext_Slot_Backend_Placeholder extends wrxNext_Slot_Backend_Abstract
{
    /**
     * (non-PHPdoc)
     * @see wrxNext_Slot_Backend_Abstract::init()
     */
    public function init()
    {
        if (!is_array($this->_data) || !isset ($this->_data ['placeholder'])) {
            $this->_data = array(
                'placeholder' => null
            );
        }
    }

    /**
     * (non-PHPdoc)
     * @see wrxNext_Slot_Backend_Abstract::edit()
     */
    public function edit()
    {
        $form = new Zend_Form ();
        $form->setMethod('POST');
        $form->addElement('text', 'placeholder', array(
            'label' => 'placeholder_name',
            'required' => true,
            'filters' => array(
                'StringTrim',
                'StripTags'
            )
        ));
        $form->addElement('submit', 'submit', array(
            'label' => 'change',
            'ignore' => true
        ));
        $form->populate($this->getData());
        if (count($_POST) && $form->isValid($_POST)) {
            $this->setData($form->getValues());
        }
        return $form;
    }

    /**
     * (non-PHPdoc)
     * @see wrxNext_Slot_Backend_Abstract::__toString()
     */
    public function __toString()
    {
        return 'placeholder: ' . (empty ($this->_data ['placeholder']) ? $this->_view->translate('empty') : $this->_data ['placeholder']);
    }
}
