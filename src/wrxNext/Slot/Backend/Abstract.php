<?php

abstract class wrxNext_Slot_Backend_Abstract implements wrxNext_Slot_Backend_Interface
{

    /**
     * holds size information
     *
     * @var string
     */
    protected $_size = null;

    /**
     * holds instance data
     *
     * @var mixed
     */
    protected $_data = null;

    /**
     * holds view object instance
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * constructs slot frontend object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null)
    {
        $this->setData($data);
        $this->setSize($size);
        $this->init();
    }

    /**
     * called as last upon cunstruction of object
     */
    public function init()
    {
    }

    /**
     * returns size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * sets size
     *
     * @param string $size
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * method for passing view object, it will be called, right after
     * instance of object is created, be cautious, that this object is
     * passed from within the view sytem for given zend module.
     * ie. if you plan to use
     * file based template, be sure to save it in correct directory
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->_view = $view;
        return $this;
    }

    /**
     * method called when slot is edited/created, returned value will be echoed
     * in view on place for configuration of slot.
     * typical use will be to return html
     * with form with configuration data, with action set to same link, the when
     * foe example
     * POST request will be detected process data.
     *
     * method will be called in controller not view, controller however will not
     * handle internal flow.
     * internal flow need to ba handled inside edit method. so if you would like
     * that presentation be build
     * in view, simply pass object that will execute html build upon echo (ieg.
     * through __toString() method),
     * you will have reference to that view
     *
     * @return mixed
     */
    public function edit()
    {
        $form = new Zend_Form();
        $form->setMethod('POST');
        $form->setAction($this->_view->url());
        $form->addElement('textarea', 'data',
            array(
                'label' => 'data',
                'value' => $this->getData()
            ));
        $form->addElement('submit', 'submit',
            array(
                'label' => 'save',
                'ignore' => true
            ));
        if (count($_POST) >= 2) {
            if ($form->isValid($_POST)) {
                $postData = $form->getValues();
                $this->setData($postData['data']);
            }
        }
        return $form->setView($this->_view);
    }

    /**
     * method returns instance data that is needed to be saved.
     * is called after edit method,
     * this data will be used in cunstructor upon later edit/display of slot.
     * since not every storage supports
     * storing of objects it is best to use data type that is easily represented
     * in common
     * types, the best type would be string.
     *
     * @return string mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data
     *
     * @param mixed $data
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * method used to display simple representation of slot content ieg.
     * object is echoed
     * in admin template representing simplistic view of slot, prefered short
     * text description
     * of configuration. for example: slot diplaying info about product XYZA
     * would be like
     * "product: XYZA"
     *
     * @return string
     */
    public function __toString()
    {
        return 'Plain text slot, size: ' . strlen($this->_data) . ' characters';
    }

    /**
     * returns horizontal
     *
     * @return Ambigous <number, NULL>
     */
    protected function _getWidth()
    {
        return wrxNext_Slot_Grid::getSlotWidth($this->_size);
    }

    /**
     * returns vertical
     *
     * @return Ambigous <number, NULL>
     */
    protected function _getHeight()
    {
        return wrxNext_Slot_Grid::getSlotHeight($this->_size);
    }

    /**
     * checks internal data variable if it's array, if not it resets it to empty
     * array
     * and also checks if there is any key from given defaults array missing, if
     * so
     * then adds missing key
     *
     * @param array $defaults
     * @return wrxNext_Slot_Backend_Abstract
     */
    protected function _checkArrayData($defaults = array())
    {
        if (!is_array($this->_data)) {
            $this->_data = array();
        }
        if (count($defaults)) {
            foreach ($defaults as $key => $value) {
                if (!array_key_exists($key, $this->_data)) {
                    $this->_data[$key] = $value;
                }
            }
        }
        return $this;
    }
}
