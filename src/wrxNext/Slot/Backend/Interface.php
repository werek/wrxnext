<?php

interface wrxNext_Slot_Backend_Interface
{
    /**
     * constructs slot frontend object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null);

    /**
     * method for passing view object, it will be called, right after
     * instance of object is created, be cautious, that this object is
     * passed from within the view sytem for given zend module. ie. if you plan to use
     * file based template, be sure to save it in correct directory or use
     * partial helper with strict module name in which the template resides
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Frontend_Interface
     */
    public function setView(Zend_View_Interface $view);

    /**
     * method returns instance data that is needed to be saved. is called after edit method,
     * this data will be used in cunstructor upon later edit/display of slot. since not every storage supports
     * storing of objects it is best to use data type that is easily represented in common
     * types, the best type would be string.
     *
     * @return string|mixed
     */
    public function getData();

    /**
     * method called when slot is edited/created, returned value will be echoed
     * in view on place for configuration of slot. typical use will be to return html
     * with form with configuration data, with action set to same link, the when for example
     * POST request will be detected process data.
     *
     * method will be called in controller not view, controller however will not handle internal flow.
     * internal flow need to ba handled inside edit method. so if you would like that presentation be build
     * in view, simply pass object that will execute html build upon echo (ieg. through __toString() method),
     * you will have reference to that view
     *
     * @return mixed
     */
    public function edit();

    /**
     * method used to display simple representation of slot content ieg. object is echoed
     * in admin template representing simplistic view of slot type/configuration,
     * prefered short text description of configuration. for example:
     * slot diplaying info about product XYZA would be like
     * "product: XYZA"
     *
     * @return string
     */
    public function __toString();
}
