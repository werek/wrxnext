<?php

class wrxNext_Slot_View_Helper_Grid extends Zend_View_Helper_Abstract
{

    /**
     * holds render entrypoint
     *
     * @var wrxNext_Slot_Render
     */
    protected $_render = null;

    /**
     * holds grid slot collection
     *
     * @var wrxNext_Slot_Grid_Collection
     */
    protected $_collection = null;

    /**
     * helper entry point, optionally gets current collection on which to work
     *
     * @param array|wrxNext_Slot_Collection|wrxNext_Slot_Grid_Collection $collection
     */
    public function grid($collection = null)
    {
        if (!is_null($collection)) {
            $this->setCollection($collection);
        }
        return $this;
    }

    /**
     * sets collection
     *
     * @param array|wrxNext_Slot_Collection|wrxNext_Slot_Grid_Collection $collection
     * @throws wrxNext_Slot_Exception
     * @return wrxNext_Slot_View_Helper_Grid
     */
    public function setCollection($collection)
    {
        if (is_array($collection)) {
            $collection = new wrxNext_Slot_Collection($collection);
        }

        if ($collection instanceof wrxNext_Slot_Collection) {
            $collection = new wrxNext_Slot_Grid_Collection($collection);
        }

        if (!$collection instanceof wrxNext_Slot_Grid_Collection) {
            throw new wrxNext_Slot_Exception(
                'view helper grid, requires either array,wrxNext_Slot_Collection or wrxNext_Slot_Grid_Collection as parameter to set slot items',
                5);
        }
        $this->_collection = $collection;
        $this->_render = null;
        return $this;
    }

    /**
     * returns entrypoint renderer
     *
     * @return wrxNext_Slot_Render
     */
    public function render()
    {
        if (!$this->_render instanceof wrxNext_Slot_Render) {
            $this->_render = new wrxNext_Slot_Render();
            $this->_render->setCollection($this->_collection);
            $this->_render->setView($this->view);
        }
        return $this->_render;
    }
}
