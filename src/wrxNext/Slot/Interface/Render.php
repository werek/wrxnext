<?php

interface wrxNext_Slot_Interface_Render
{
    /**
     * renders whole html for given slot type
     *
     * @param string $size
     * @param string $position
     */
    public function render($size, $position);
}
