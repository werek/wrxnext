<?php

/**
 * interface designed for slot backend objects, intended for refreshing/reloading
 * instance data to its latest version. upon invoking of refresh method object should
 * reload any data from external sources based on instance data.
 *
 * for example if object holds select data from product then upon refreshing it should
 * fetch same set of data currently set on that product
 *
 * @author bweres01
 *
 */
interface wrxNext_Slot_Interface_Refresh
{

    /**
     * refreshes slot data and return status of operation
     *
     * if data was successfully refreshed then true, otherwise false
     *
     * @return boolean
     */
    public function refresh();
}
