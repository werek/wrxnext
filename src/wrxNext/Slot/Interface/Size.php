<?php

interface wrxNext_Slot_Interface_Size
{
    /**
     * returns slot width in pixels
     *
     * @return int
     */
    public function getWidth();

    /**
     * returns slot height in pixels
     *
     * @return int
     */
    public function getHeight();
}
