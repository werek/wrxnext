<?php

/**
 * @method int getMaxRows()
 * @method int getMaxColumns()
 * @method array getMapArray()
 * @method wrxNext_Slot_Grid_Map setColumns(int $columns)
 * @method wrxNext_Slot_Grid_Map recalculate()
 * @author Bartlomiej Wereszczynski <werexx@gmail.com
 *
 */
class wrxNext_Slot_Grid_Collection
{

    /**
     * holds slot map
     *
     * @var wrxNext_Slot_Grid_Map
     */
    protected $_map = null;

    /**
     * constructor, , optionally accepts array of params
     *
     * @param wrxNext_Slot_Collection $collection
     * @param array $options
     */
    public function __construct(wrxNext_Slot_Collection $collection,
                                $options = array())
    {
        $this->_map = new wrxNext_Slot_Grid_Map($collection, $options);
        $this->setOptions($options);
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } elseif (method_exists($this->_map, $method)) {
                $this->_map->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * builds slot grid collection from given data set
     *
     * Created grid will have maximum columns specified by $columns variable.
     * Each $dataSet entry will create slot placed on grid. created slot will
     * have
     * adapter class set to $class, and $sizes can be a string or array of
     * sizes.
     * in case $sizes recievies an array the it will cyckle through each entry
     * and give
     * that size to coresponding slot
     *
     * @param array $dataSet
     * @param string|array $sizes
     *            holds array of slot sizes
     * @param string $class
     *            slot adapter class
     * @param int $columns
     *            number of restraining columns
     * @return wrxNext_Slot_Grid_Collection
     */
    public static function buildFromDataSet(array $dataSet, $sizes,
                                            $class, $columns = 6)
    {
        $gridCollection = new self(new wrxNext_Slot_Collection(array()),
            array(
                'columns' => $columns
            ));
        $sizes = (array)$sizes;
        $sequence = 0;
        foreach ($dataSet as $data) {
            $gridCollection->addSlot($sizes[$sequence], $class,
                serialize($data));
            $sequence++;
            if (!isset($sizes[$sequence])) {
                $sequence = 0;
            }
        }
        return $gridCollection;
    }

    /**
     * adds slot to grid at first available position from left top corner
     *
     * @param string $slotSize
     * @param string $slotClass
     * @param mixed $slotData
     * @return wrxNext_Slot_Collection_Item
     */
    public function addSlot($slotSize, $slotClass, $slotData = null)
    {
        return $this->_map->addSlot($slotSize, $slotClass, $slotData);
    }

    /**
     * returns slots iterator
     *
     * @return wrxNext_Slot_Collection_Item[]
     */
    public function getIterator()
    {
        return $this->_map->getIterator();
    }

    /**
     * returns grid collection map
     *
     * @return wrxNext_Slot_Grid_Map
     */
    public function getMap()
    {
        return $this->_map;
    }

    public function __clone()
    {
        $this->_map = clone $this->_map;
    }

    /**
     * proxy for map object
     *
     * @param string $name
     * @param mixed $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        if (method_exists($this->_map, $name)) {
            return call_user_func_array(
                array(
                    $this->_map,
                    $name
                ), $args);
        } else {
            throw wrxNext_Slot_Grid_Exception(
                'method "' . $name . '" does not exists in class ' .
                get_class($this));
        }
    }
}
