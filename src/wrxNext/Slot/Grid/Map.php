<?php

class wrxNext_Slot_Grid_Map
{
    use wrxNext_Trait_RegistryLog;
    /**
     * holds map for specific slot resize at certain columns width
     *
     * @var array
     */
    protected $_predefinedResizeMap = array(
        '4' => array(
            '6x2' => '4x2'
        ),
        '2' => array(
            '6x2' => '2x1',
            '4x4' => '2x2'
        )
    );
    /**
     * holds status wheter to allow resize
     *
     * @var boolean
     */
    protected $_allowResize = true;

    /**
     * holds status wheter to resize with aspect ratio intact
     *
     * @var boolean
     */
    protected $_keepAspectRatio = true;

    /**
     * holds status wheter to reposition slots for
     * best fit in grid
     *
     * @var boolean
     */
    protected $_reposition = true;

    /**
     * holds max num of columns
     *
     * @var int
     */
    protected $_columns = null;

    /**
     * holds max num of rows
     *
     * @var int
     */
    protected $_rows = null;

    /**
     * holds max recognised columns
     *
     * @var int
     */
    protected $_maxColumns = 0;

    /**
     * holds max recognised rows
     *
     * @var int
     */
    protected $_maxRows = 0;

    /**
     * holds map of slots in grid, used for recalculations
     *
     * @var array
     */
    protected $_slotMap = array();

    /**
     * holds status if map was already calculated
     *
     * @var boolean
     */
    protected $_mapCalculated = false;

    /**
     * holds slots collection
     *
     * @var wrxNext_Slot_Collection
     */
    protected $_collection = null;

    /**
     * constructor, , optionally accepts array of params
     *
     * @param wrxNext_Slot_Collection $collection
     * @param array $options
     */
    public function __construct(wrxNext_Slot_Collection $collection,
                                $options = array())
    {
        $this->_collection = $collection;
        $this->setOptions($options);
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * overrides shallow copy
     *
     * @return wrxNext_Slot_Grid_Map
     */
    public function __clone()
    {
        $this->_collection = clone $this->_collection;
    }

    /**
     * returns added slot item
     *
     * @param string $slotSize
     * @param string $slotClass
     * @param mixed $slotData
     * @return wrxNext_Slot_Collection_Item
     */
    public function addSlot($slotSize, $slotClass, $slotData = null)
    {
        $slotPosition = $this->getPositionForSlotSize($slotSize);
        if ($slotPosition !== false) {
            $slot = $this->_collection->addSlot($slotSize, $slotPosition, $slotClass, $slotData);
            $this->_placeSlot($slot);
        }
        return ($slotPosition !== false) ? $slot : $slotPosition;
    }

    /**
     * returns next available position for slot of given size in current map
     * returns false if place is not available
     *
     * @param string $size
     * @param bool $disableMapCheck
     * @return string false if size cannot be found
     */
    public function getPositionForSlotSize($size, $disableMapCheck = false)
    {
        // making sure that map is all setup
        if (!$disableMapCheck) {
            $this->_fillMap();
        }
        // spliting coordinates
        $sizeCoordinates = wrxNext_Slot_Grid::splitCoordinates($size);
        // determining max grid allowed size, if no restrictions are set then
        // assuming maximum to fill given size
        $gridMaxWidth = is_null($this->_columns) ? $this->_maxColumns +
            $sizeCoordinates[0] : $this->_columns;
        $gridMaxHeight = is_null($this->_rows) ? $this->_maxRows +
            $sizeCoordinates[1] : $this->_rows;
        // base start position if no rows are in grid
        $position = false;
        for ($row = 1; $row <= $gridMaxHeight; $row++) {
            // quick check if row have enough free space
            if (count(@$this->_slotMap[$row]) - 1 + $sizeCoordinates[0] <=
                $gridMaxWidth
            ) {
                // proper search of free space in row
                for ($column = 1; $column <= $gridMaxWidth; $column++) {
                    // check if current space is occupied
                    if (!isset($this->_slotMap[$row][$column]) || empty($this->_slotMap[$row][$column])) {
                        // found free spot, check if it will fit given size,
                        // first check if
                        // it's larger that maks columns
                        if (($column + $sizeCoordinates[0] - 1) <= $gridMaxWidth) {
                            // it's posibble that it will fit, going with exact
                            // check
                            for ($jj = $column; $jj <
                            ($column + $sizeCoordinates[0]); $jj++) {
                                for ($ii = $row; $ii <
                                ($row + $sizeCoordinates[1]); $ii++) {
                                    if (@$this->_slotMap[$ii][$jj] instanceof wrxNext_Slot_Collection_Item)
                                        break 3;
                                }
                            }
                            // got perfect spot, saving position and exiting
                            // main loop
                            $position = $row . 'x' . $column;
                            break 2;
                        }
                    }
                }
            }
        }
        return $position;
    }

    /**
     * checks if slot map needs to be filled
     */
    protected function _fillMap()
    {
        if (!$this->_mapCalculated) {
            $start = microtime(true);
            $this->_slotMap = array();
            foreach ($this->_collection->getIterator() as $slot) {
                $this->_placeSlot($slot);
            }
            if ($this->_columns < $this->_maxColumns && !is_null(
                    $this->_columns)
            ) {
                $this->_reflowColumns();
            }
            if ($this->_rows < $this->_maxRows && !is_null($this->_rows)) {
                $this->_reflowRows();
            }
            // filling out the blanks
            $this->_reflowForEmptySpots();
            $this->_mapCalculated = true;
        }
    }

    /**
     * places slot on map
     *
     * @param wrxNext_Slot_Collection_Item $slot
     */
    protected function _placeSlot(wrxNext_Slot_Collection_Item $slot)
    {
        for ($row = $slot->getGridRow(); $row <
        ($slot->getGridHeight() + $slot->getGridRow()); $row++) {
            for ($column = $slot->getGridColumn(); $column <
            ($slot->getGridWidth() + $slot->getGridColumn()); $column++) {
                $this->_slotMap[$row][$column] = $slot;
                if ($row > $this->_maxRows) {
                    $this->_maxRows = $row;
                }
                if ($column > $this->_maxColumns) {
                    $this->_maxColumns = $column;
                }
            }
        }
    }

    /**
     * goes through slot fields which expands over the column limit and
     * resizing/repositioning/deleting slot according to settings
     *
     * @return wrxNext_Slot_Grid_Map
     */
    protected function _reflowColumns()
    {
        for ($row = 1; $row <= $this->_maxRows; $row++) {
            for ($column = ($this->_columns + 1); $column <= $this->_maxColumns; $column++) {
                if (@$this->_slotMap[$row][$column] instanceof wrxNext_Slot_Collection_Item) {
                    $this->_processOverflownSlot($this->_slotMap[$row][$column]);
                }
            }
        }
        $this->_maxColumns = $this->_columns;
        return $this;
    }

    /**
     * processes overflown slot according to map settings
     *
     * @param wrxNext_Slot_Collection_Item $slot
     */
    protected function _processOverflownSlot(wrxNext_Slot_Collection_Item $slot)
    {
        $this->removeSlotFromMap($slot);
        $slotResized = false;
        if ($this->getAllowResize()) {
            //checking for mapped size for current slot
            if (!$this->_resizedToPreDefined($slot)) {
                // finding base and resizing also unseting all rows
                // mapping overflow
                $overColumns = $this->_slotOverColumn($slot);
                $overRows = $this->_slotOverRow($slot);
                // checking if slot can resized at least to 1x1, if so then resizing
                if (($slot->getGridWidth() - $overColumns) >= 1 &&
                    ($slot->getGridHeight() - $overRows) >= 1
                ) {
                    // overflow is possible to resize
                    if ($this->_keepAspectRatio) {
                        $this->_resizeWithAspectIntact($slot, $overColumns, $overRows);
                    } else {
                        // changing size
                        $this->_resizeForBestFit($slot, $overColumns, $overRows);
                    }
                    // placing slot again on map
                    $this->_placeSlot($slot);
                    $slotResized = true;
                }
            } else {
                // placing slot again on map
                $this->_placeSlot($slot);
                $slotResized = true;
            }
        }
        if ($this->getReposition() && $slotResized == false) {
            // finding new position for slot
            $newPosition = $this->getPositionForSlotSize($slot->getSize(), true);
            if ($newPosition !== false) {
                // new position for slot is available, setting new position
                // coordinates
                $slot->setPosition($newPosition);
                // filling map with news slot position
                $this->_placeSlot($slot);
            }
        }
    }

    /**
     * unsets map space for given slot, according to slots position/size
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @return wrxNext_Slot_Grid_Map
     */
    public function removeSlotFromMap(wrxNext_Slot_Collection_Item $slot)
    {
        for ($row = $slot->getGridRow(); $row <
        ($slot->getGridHeight() + $slot->getGridRow()); $row++) {
            for ($column = $slot->getGridColumn(); $column <
            ($slot->getGridWidth() + $slot->getGridColumn()); $column++) {
                unset($this->_slotMap[$row][$column]);
            }
        }
        return $this;
    }

    /**
     * gets slot resize flag, wheter to allow resize of slot dimensions to allow
     * fit in smaller spaces in case of relocation due to smaller grid
     * dimensions that current slot position/size allows
     *
     * @return boolean
     */
    public function getAllowResize()
    {
        return $this->_allowResize;
    }

    /**
     * sets slot resize flag, wheter to allow resize of slot dimensions to allow
     * fit in smaller spaces in case of relocation due to smaller grid
     * dimensions that current slot position/size allows
     *
     * @param boolean $allowResize
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setAllowResize($allowResize)
    {
        $this->_allowResize = $allowResize;
        return $this;
    }

    /**
     * resizes slot acording to map, returns true if new size was found and false if not
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @return boolean
     */
    protected function _resizedToPreDefined(wrxNext_Slot_Collection_Item $slot)
    {
        if (array_key_exists($this->_columns, $this->_predefinedResizeMap) &&
            array_key_exists($slot->getSize(), $this->_predefinedResizeMap[$this->_columns])
        ) {
            $slot->setSize($this->_predefinedResizeMap[$this->_columns][$slot->getSize()]);
            return true;
        }
        return false;
    }

    /**
     * calculates how many slot columns overflow map limit
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @return number
     */
    protected function _slotOverColumn(wrxNext_Slot_Collection_Item $slot)
    {
        if (is_null($this->_columns))
            return 0;
        return ($slot->getGridWidth() + $slot->getGridColumn() - 1) -
        $this->_columns;
    }

    /**
     * calculates how many slot rows overflow map limit
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @return number
     */
    protected function _slotOverRow(wrxNext_Slot_Collection_Item $slot)
    {
        if (is_null($this->_rows))
            return 0;
        return ($slot->getGridWidth() + $slot->getGridColumn() - 1) -
        $this->_rows;
    }

    /**
     * tries to lower slot size while keeping aspect ratio
     * if that is not possible falls back to "best fit" resize
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @param int $overflownColumns
     * @param int $overflownRows
     */
    protected function _resizeWithAspectIntact(wrxNext_Slot_Collection_Item $slot, $overflownColumns, $overflownRows)
    {
        $aspectRatio = $slot->getGridWidth() / $slot->getGridHeight();
        if ($overflownColumns > $overflownRows) {
            //slot extends more in horizontal direction, using width as base
            //for slot main resize
            $destinationSlotWidth = $slot->getGridWidth() - $overflownColumns;
            while ($destinationSlotWidth >= 1 &&
                (($destinationSlotWidth / $aspectRatio) % 1) != 0) {
                //change does not conform with grid sizes, downsizing for another check
                $destinationSlotWidth--;
            }
            $height = intval($destinationSlotWidth / $aspectRatio);
            if ($height < 1) {
                //lowest aspect ratio could not be found, fallback to non aspect ratio resize
                $this->_resizeForBestFit($slot, $overflownColumns, $overflownRows);
            } else {
                $slot->setSize($destinationSlotWidth . 'x' . $height);
            }
        } else {
            //slot extends more in vertical direction, using height as base
            //for slot main resize
            $destinationSlotHeight = $slot->getGridHeight() - $overflownRows;
            while ($destinationSlotHeight >= 1 &&
                (($destinationSlotHeight * $aspectRatio) % 1) != 0) {
                //change does not conform with grid sizes, downsizing for another check
                $destinationSlotHeight--;
            }
            $width = intval($destinationSlotHeight * $aspectRatio);
            if ($width < 1) {
                //lowest aspect ratio could not be found, fallback to non aspect ratio resize
                $this->_resizeForBestFit($slot, $overflownColumns, $overflownRows);
            } else {
                $slot->setSize($width . 'x' . $destinationSlotHeight);
            }
        }
    }

    /**
     * resizes slot without keeping aspect ratio
     *
     * @param wrxNext_Slot_Collection_Item $slot
     * @param int $overflownColumns
     * @param int $overflownRows
     * @return wrxNext_Slot_Grid_Map
     */
    protected function _resizeForBestFit(wrxNext_Slot_Collection_Item $slot, $overflownColumns, $overflownRows)
    {
        $slot->setSize(
            ($slot->getGridWidth() - $overflownColumns) . 'x' .
            ($slot->getGridHeight() - $overflownRows));
        return $this;
    }

    /**
     * gets reposition status, wheter to repositon slots in grid of currently
     * specified dimensions.
     * default false
     *
     * @return boolean
     */
    public function getReposition()
    {
        return $this->_reposition;
    }

    /**
     * sets reposition status, wheter to repositon slots in grid of currently
     * specified dimensions.
     * default false
     *
     * @param boolean $reposition
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setReposition($reposition = false)
    {
        $this->_reposition = $reposition;
        return $this;
    }

    /**
     * goes through slot fields which expands over the row limit and
     * resizing/repositioning/deleting slot according to settings
     *
     * @return wrxNext_Slot_Grid_Map
     */
    protected function _reflowRows()
    {
        for ($row = ($this->_rows + 1); $row <= $this->_maxRows; $row++) {
            for ($column = 1; $column <= $this->_maxColumns; $column++) {
                if (@$this->_slotMap[$row][$column] instanceof wrxNext_Slot_Collection_Item) {
                    $this->_processOverflownSlot($this->_slotMap[$row][$column]);
                }
            }
        }
        $this->_maxRows = $this->_rows;
        return $this;
    }

    /**
     * reflows whole grid for situations where changing slots size
     * leaves empty space, literally filling out the blanks
     *
     * @return wrxNext_Slot_Grid_Map
     */
    protected function _reflowForEmptySpots()
    {
        // temporarily reseting internal restrictions
        $currentSlotMap = $this->_slotMap;
        $currentColumns = $this->_columns;
        $currentRows = $this->_rows;
        $this->_columns = $this->_maxColumns;
        $this->_rows = $this->_maxRows;
        $this->_slotMap = array();
        // list of processed slots
        $processedSlots = array();
        for ($row = 1; $row <= $this->_maxRows; $row++) {
            for ($column = 1; $column <= $this->_maxColumns; $column++) {
                if (@$currentSlotMap[$row][$column] instanceof wrxNext_Slot_Collection_Item) {
                    $slot = $currentSlotMap[$row][$column];
                    if (!in_array($slot->slotIdentifier(), $processedSlots)) {
                        $position = $this->getPositionForSlotSize($slot->getSize(), true);
                        $slot->setPosition($position);
                        $this->_placeSlot($slot);
                        $processedSlots[] = $slot->slotIdentifier();
                    }
                }
            }
        }
        // returning previous restrictions
        $this->_columns = $currentColumns;
        $this->_rows = $currentRows;
        // updating max rows
        $this->_maxRows = count($this->_slotMap);
        return $this;
    }

    /**
     * gets collection
     *
     * @return wrxNext_Slot_Collection
     */
    public function getCollection()
    {
        $this->_fillMap();
        return $this->_collection;
    }

    /**
     * sets collection
     *
     * @param wrxNext_Slot_Collection $collection
     * @return wrxNext_Slot_Grid_Map
     */
    public function setCollection(wrxNext_Slot_Collection $collection)
    {
        $this->_collection = $collection;
        $this->_mapCalculated = false;
        return $this;
    }

    /**
     * sets flag for resize with keeping aspect ratio
     *
     * @param boolean $keepAspect
     * @return wrxNext_Slot_Grid_Map
     */
    public function setAspectRatio($keepAspect)
    {
        $this->_keepAspectRatio = $keepAspect;
        return $this;
    }

    /**
     * gets flag for resize with keeping aspect ratio
     *
     * @return boolean
     */
    public function getAspectRatio()
    {
        return $this->_keepAspectRatio;
    }

    /**
     * returns slots iterator
     *
     * @return wrxNext_Slot_Collection_Item[]
     */
    public function getIterator()
    {
        $this->_fillMap();
        return $this->_collection->getIterator();
    }

    /**
     * gets max num of columns
     *
     * @return int
     */
    public function getColumns()
    {
        return $this->_columns;
    }

    /**
     * sets max num of columns
     *
     * @param int $columns
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setColumns($columns)
    {
        if ($columns != $this->_columns) {
            $this->_mapCalculated = false;
        }
        $this->_columns = $columns;
        return $this;
    }

    /**
     * gets max num of rows
     *
     * @return int
     */
    public function getRows()
    {
        return $this->_rows;
    }

    /**
     * sets max num of rows
     *
     * @param int $rows
     * @return wrxNext_Slot_Grid_Collection
     */
    public function setRows($rows)
    {
        if ($rows != $this->_rows) {
            $this->_mapCalculated = false;
        }
        $this->_rows = $rows;
        return $this;
    }

    /**
     * forces map recalculation
     *
     * @return wrxNext_Slot_Grid_Map
     */
    public function recalculate()
    {
        $this->_mapCalculated = false;
        $this->_fillMap();
        return $this;
    }

    /**
     * returns array filled slot columns/rows map
     *
     * @return array
     */
    public function getMapArray()
    {
        $this->_fillMap();
        return $this->_slotMap;
    }

    /**
     * returns number of rows recognised in current grid slots configuration
     *
     * @return int
     */
    public function getMaxRows()
    {
        $this->_fillMap();
        return $this->_maxRows;
    }

    /**
     * returns number of columns recognised in current grid slots configuration
     *
     * @return int
     */
    public function getMaxColumns()
    {
        $this->_fillMap();
        return $this->_maxColumns;
    }
}
