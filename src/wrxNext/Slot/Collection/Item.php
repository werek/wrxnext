<?php

class wrxNext_Slot_Collection_Item
{

    /**
     * holds identifier for this object
     *
     * @var string
     */
    protected $_identifier = null;

    /**
     * holds param for given slot
     *
     * @var array
     */
    protected $_params = array();

    /**
     * holds size configuration
     *
     * @var string
     */
    protected $_size = '';

    /**
     * holds position configuration
     *
     * @var string
     */
    protected $_position = '';

    /**
     * holds adapter class name
     *
     * @var string
     */
    protected $_class = '';

    /**
     * holds data
     *
     * @var mixed
     */
    protected $_data = '';

    /**
     * takes slot full config data
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->_parseData($data);
        $this->_buildIdentifier();
    }

    /**
     * parses array data into properties
     *
     * @param array $data
     */
    protected function _parseData($data)
    {
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'slot_size':
                    $this->_size = $value;
                    break;
                case 'slot_position':
                    $this->_position = $value;
                    break;
                case 'slot_content':
                    $this->_data = $value;
                    break;
                case 'slot_class':
                    $this->_class = $value;
                    break;
                default:
                    $this->setParam($key, $value);
                    break;
            }
        }
    }

    /**
     * sets slot param
     *
     * @param string $name
     * @param mixed $value
     * @return wrxNext_Slot_Collection_Item
     */
    public function setParam($name, $value)
    {
        $this->_params[$name] = $value;
        return $this;
    }

    /**
     * builds identifier
     */
    public function _buildIdentifier()
    {
        $this->_identifier = preg_replace('@[^a-zA-Z0-9_]@', '_',
            $this->_class . '_' . $this->_position . '_' . $this->_size);
    }

    /**
     * gets slot param
     *
     * @param string $name
     * @return mixed
     */
    public function getParam($name)
    {
        return $this->_params[$name];
    }

    /**
     * gets all params
     *
     * @return array
     */
    public function getAllParams()
    {
        return $this->_params;
    }

    /**
     * returns slot backend
     *
     * @return wrxNext_Slot_Backend_Interface
     */
    public function getBackend()
    {
        return $this->getAdapter()->getBackend();
    }

    /**
     * returns slot adapter instance
     *
     * @return wrxNext_Slot_Adapter_Interface
     */
    public function getAdapter()
    {
        $data = (@unserialize($this->getData()) === false) ? $this->getData() : unserialize(
            $this->getData());
        return wrxNext_Slot::factory($this->getClass(), $data, $this->_size);
    }

    /**
     * gets data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data
     *
     * @param mixed $data
     * @return wrxNext_Slot_Collection_Item
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * gets class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * sets class
     *
     * @param string $class
     * @return wrxNext_Slot_Collection_Item
     */
    public function setClass($class)
    {
        $this->_class = $class;
        return $this;
    }

    /**
     * returns slot frontend
     *
     * @return wrxNext_Slot_Frontend_Interface
     */
    public function getFrontend()
    {
        return $this->getAdapter()->getFrontend();
    }

    /**
     * gets grid row
     *
     * @return int
     */
    public function getGridRow()
    {
        $position = wrxNext_Slot_Grid::splitCoordinates($this->getPosition());
        return $position[0];
    }

    /**
     * gets position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->_position;
    }

    /**
     * sets position
     *
     * @param string $position
     * @return wrxNext_Slot_Collection_Item
     */
    public function setPosition($position)
    {
        $this->_position = $position;
        return $this;
    }

    /**
     * gets grid column
     *
     * @return int
     */
    public function getGridColumn()
    {
        $position = wrxNext_Slot_Grid::splitCoordinates($this->getPosition());
        return $position[1];
    }

    /**
     * gets grid slot width (how many columns)
     *
     * @return int
     */
    public function getGridWidth()
    {
        $position = wrxNext_Slot_Grid::splitCoordinates($this->getSize());
        return $position[0];
    }

    /**
     * gets size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * sets size
     *
     * @param string $size
     * @return wrxNext_Slot_Collection_Item
     */
    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * gets grid slot width (how many rows)
     *
     * @return int
     */
    public function getGridHeight()
    {
        $position = wrxNext_Slot_Grid::splitCoordinates($this->getSize());
        return $position[1];
    }

    /**
     * retruns hashed identifier based on current slot size/position/class
     *
     * @return string
     */
    public function slotIdentifier()
    {
        return $this->_identifier;
    }

    /**
     * returns item size,position,class,data as array
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            'slot_size' => $this->_size,
            'slot_position' => $this->_position,
            'slot_class' => $this->_class,
            'slot_data' => $this->_data
        );
    }
}
