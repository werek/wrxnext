<?php

class wrxNext_Slot_Frontend_Placeholder extends wrxNext_Slot_Frontend_Abstract
{
    /**
     * (non-PHPdoc)
     * @see wrxNext_Slot_Frontend_Abstract::__toString()
     */
    public function __toString()
    {
        ob_start();
        echo $this->_view->placeholder($this->_data['placeholder']);
        return ob_get_clean();
    }
}
