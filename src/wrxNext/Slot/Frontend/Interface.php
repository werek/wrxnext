<?php

interface wrxNext_Slot_Frontend_Interface
{
    /**
     * constructs slot frontend object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null);

    /**
     * method for passing view object, it will be called, right after
     * instance of object is created, be cautious, that this object is
     * passed from within the view sytem for given zend module. ie. if you plan to use
     * file based template, be sure to save it in correct directory or use
     * partial helper with strict module name in which the template resides
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Frontend_Interface
     */
    public function setView(Zend_View_Interface $view);

    /**
     * magic method used for echoing object data, should return html content
     * that is supposed to be inserted into slots place, based on data/size
     *
     * @return string
     */
    public function __toString();
}
