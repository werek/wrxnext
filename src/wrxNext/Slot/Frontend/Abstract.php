<?php

abstract class wrxNext_Slot_Frontend_Abstract implements wrxNext_Slot_Frontend_Interface,
    wrxNext_Slot_Interface_Size
{

    /**
     * holds size information
     *
     * @var string
     */
    protected $_size = null;

    /**
     * holds instance data
     *
     * @var mixed
     */
    protected $_data = null;

    /**
     * holds view object instance
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * constructs slot frontend object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null)
    {
        $this->setData($data);
        $this->setSize($size);
        $this->init();
    }

    /**
     * called as last upon cunstruction of object
     */
    public function init()
    {
    }

    /**
     * returns data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data
     *
     * @param mixed $data
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * returns size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * sets size
     *
     * @param string $size
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * returns width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_getWidth();
    }

    /**
     * returns horizontal
     *
     * @return int
     */
    protected function _getWidth()
    {
        return wrxNext_Slot_Grid::getSlotWidth($this->_size);
    }

    /**
     * returns height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_getHeight();
    }

    /**
     * returns vertical
     *
     * @return int
     */
    protected function _getHeight()
    {
        return wrxNext_Slot_Grid::getSlotHeight($this->_size);
    }

    /**
     * removes all values in array which key starts with an underscore
     *
     * @param array $array
     * @return array
     */
    public function cleanSemiPrivateKeys($array)
    {
        $tmp = array();
        foreach ($array as $key => $value) {
            if (strpos($key, '_') !== 0) {
                $tmp[$key] = $value;
            }
        }
        return $tmp;
    }

    /**
     * method for passing view object, it will be called, right after
     * instance of object is created, be cautious, that this object is
     * passed from within the view sytem for given zend module.
     * ie. if you plan to use
     * file based template, be sure to save it in correct directory
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setView(Zend_View_Interface $view)
    {
        $this->_view = $view;
        return $this;
    }

    /**
     * magic method used for echoing object data, should return html content
     * that is supposed to be inserted into slots place, based on data/size
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->_data;
    }
}
