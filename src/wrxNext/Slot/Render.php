<?php

class wrxNext_Slot_Render
{

    /**
     * holds collection object
     *
     * @var wrxNext_Slot_Grid_Collection
     */
    protected $_gridCollection = null;

    /**
     * holds css renderer object
     *
     * @var wrxNext_Slot_Render_Css
     */
    protected $_cssObject = null;

    /**
     * holds grid renderer object
     *
     * @var wrxNext_Slot_Render_Grid
     */
    protected $_gridObject = null;

    /**
     * holds rwd renderer object
     *
     * @var wrxNext_Slot_Render_Rwd
     */
    protected $_rwdObject = null;

    /**
     * view object
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * sets grid collection
     *
     * @param wrxNext_Slot_Grid_Collection $collection
     * @return wrxNext_Slot_Render
     */
    public function setCollection(wrxNext_Slot_Grid_Collection $collection)
    {
        $this->_gridCollection = $collection;
        $this->_cssObject = null;
        $this->_gridObject = null;
        $this->_rwdObject = null;
        return $this;
    }

    /**
     * returns rwd renderer object
     *
     * @return wrxNext_Slot_Render_Rwd
     */
    public function rwd()
    {
        if (!$this->_rwdObject instanceof wrxNext_Slot_Render_Rwd) {
            $this->_rwdObject = new wrxNext_Slot_Render_Rwd($this->getCollection());
            $this->_rwdObject->setView($this->_view);
        }
        return $this->_rwdObject;
    }

    /**
     * gets grid collection
     *
     * @return wrxNext_Slot_Grid_Collection
     */
    public function getCollection()
    {
        return $this->_gridCollection;
    }

    /**
     * gets view object
     *
     * @return Zend_View_Interface
     */
    public function getView()
    {
        return $this->_view;
    }

    /**
     * sets view object
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_Slot_Render
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }

    /**
     * echoing full content (grid + css)
     *
     * @return string
     */
    public function __toString()
    {
        try {
            ob_start();
            echo $this->grid();
            echo '<style>';
            echo $this->css();
            echo '</style>';
            return ob_get_clean();
        } catch (Exception $e) {
            return get_class($e) . ' error code: ' . $e->getCode() .
            ', message: ' . $e->getMessage() . ' ' .
            $e->getTraceAsString();
        }
    }

    /**
     * returns grid renderer object
     *
     * @return wrxNext_Slot_Render_Grid
     */
    public function grid()
    {
        if (!$this->_gridObject instanceof wrxNext_Slot_Render_Grid) {
            $this->_gridObject = new wrxNext_Slot_Render_Grid($this->getCollection());
            $this->_gridObject->setView($this->_view);
        }
        return $this->_gridObject;
    }

    /**
     * returns css renderer object
     *
     * @return wrxNext_Slot_Render_Css
     */
    public function css()
    {
        if (!$this->_cssObject instanceof wrxNext_Slot_Render_Css) {
            $this->_cssObject = new wrxNext_Slot_Render_Css($this->getCollection());
            $this->_cssObject->setView($this->_view);
        }
        return $this->_cssObject;
    }
}
