<?php

interface wrxNext_Slot_Adapter_Interface
{
    /**
     * constructs slot object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null);

    /**
     * returns object for frontend, this object handles presentation
     * for website visitor
     *
     * @return wrxNext_Slot_Frontend_Interface
     */
    public function getFrontend();

    /**
     * returns object for backend, this object handles administration actions
     * such as display configuration of a slot, processing of new configuration,
     * returning instance data to save
     *
     * @return wrxNext_Slot_Backend_Interface
     */
    public function getBackend();
}
