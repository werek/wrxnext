<?php

abstract class wrxNext_Slot_Adapter_Abstract implements wrxNext_Slot_Adapter_Interface
{
    /**
     * holds size information
     *
     * @var string
     */
    protected $_size = null;
    /**
     * holds instance data
     *
     * @var mixed
     */
    protected $_data = null;
    /**
     * holds frontend class name
     *
     * @var string
     */
    protected $_frontendClass = '';
    /**
     * holds instance of frontend class
     *
     * @var wrxNext_Slot_Frontend_Interface
     */
    protected $_frontend = null;
    /**
     * holds backend class name
     *
     * @var string
     */
    protected $_backendClass = '';
    /**
     * holds instance of backend class
     *
     * @var wrxNext_Slot_Backend_Interface
     */
    protected $_backend = null;

    /**
     * constructs slot object, used by factory.
     * data will be null if it's a creation of new content slot,
     * size will be null if slot is resizeable in layout
     *
     * @param string $data
     * @param string $size
     */
    public function __construct($data = null, $size = null)
    {
        $this->setData($data);
        $this->setSize($size);
        $this->init();
    }

    /**
     * called as last upon cunstruction of object
     */
    public function init()
    {

    }

    /**
     * returns object for frontend, this object handles presentation
     * for website visitor
     *
     * @return wrxNext_Slot_Frontend_Interface
     */
    public function getFrontend()
    {
        if (!$this->_frontend instanceof wrxNext_Slot_Frontend_Interface) {
            $this->_frontend = $this->getClassObject($this->_frontendClass);
        }
        return $this->_frontend;
    }

    /**
     * returns instance of given class
     *
     * @param string $className
     * @throws wrxNext_Slot_Adapter_Exception
     * @return object
     */
    protected function getClassObject($className)
    {
        if (class_exists($className)) {
            $reflection = new ReflectionClass($className);
            $object = $reflection->newInstance($this->getData(), $this->getSize());
            return $object;
        } else {
            throw new wrxNext_Slot_Adapter_Exception('given class does not exists: "' . $className . '"');
        }
    }

    /**
     * returns data
     *
     * @return mixed
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data
     *
     * @param mixed $data
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setData($data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * returns size
     *
     * @return string
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * sets size
     *
     * @param string $size
     * @return wrxNext_Slot_Frontend_Abstract
     */
    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * returns object for backend, this object handles administration actions
     * such as display configuration of a slot, processing of new configuration,
     * returning instance data to save
     *
     * @return wrxNext_Slot_Backend_Interface
     */
    public function getBackend()
    {
        if (!$this->_backend instanceof wrxNext_Slot_Backend_Interface) {
            $this->_backend = $this->getClassObject($this->_backendClass);
        }
        return $this->_backend;
    }
}
