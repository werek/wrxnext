<?php

class wrxNext_Slot_Adapter_Plain extends wrxNext_Slot_Adapter_Abstract
{
    /**
     * holds backend class name
     *
     * @var string
     */
    protected $_backendClass = 'wrxNext_Slot_Backend_Plain';
    /**
     * holds frontend class name
     *
     * @var string
     */
    protected $_frontendClass = 'wrxNext_Slot_Frontend_Plain';
}
