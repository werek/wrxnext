<?php

class wrxNext_Slot_Adapter_Placeholder extends wrxNext_Slot_Adapter_Abstract
{
    protected $_frontendClass = 'wrxNext_Slot_Frontend_Placeholder';
    protected $_backendClass = 'wrxNext_Slot_Backend_Placeholder';
}
