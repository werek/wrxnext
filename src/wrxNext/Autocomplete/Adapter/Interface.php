<?php

interface wrxNext_Autocomplete_Adapter_Interface
{

    /**
     * searches term and returns resultset
     *
     * @param string $term compared value
     * @param int $limit null means the is no limit on returned set
     * @return wrxNext_Autocomplete_Result
     */
    public function search($term, $limit = null);
}
