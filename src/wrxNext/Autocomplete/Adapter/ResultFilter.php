<?php

/**
 * adapter alloving filtration of results
 *
 * @author bweres01
 *
 */
class wrxNext_Autocomplete_Adapter_ResultFilter implements
    wrxNext_Autocomplete_Adapter_Interface,
    wrxNext_Autocomplete_Adapter_OptionsInterface
{

    /**
     * holds adapter instance
     *
     * @var wrxNext_Autocomplete_Adapter_Interface
     */
    protected $_adapter = null;

    /**
     * holds filter instance
     *
     * @var Zend_Filter_Interface
     */
    protected $_filter = null;

    /**
     * constructor, takes adapter and filter to use against it
     *
     * @param wrxNext_Autocomplete_Adapter_Interface $adapter
     * @param Zend_Filter_Interface $filter
     */
    public function __construct(
        wrxNext_Autocomplete_Adapter_Interface $adapter = null,
        Zend_Filter_Interface $filter = null)
    {
        if (!is_null($adapter)) {
            $this->setAdapter($adapter);
        }
        if (!is_null($filter)) {
            $this->setFilter($filter);
        }
    }

    /**
     * gets adapter
     *
     * @return wrxNext_Autocomplete_Adapter_Interface
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    /**
     * sets adapter
     *
     * @param wrxNext_Autocomplete_Adapter_Interface $adapter
     * @return wrxNext_Autocomplete_Adapter_ResultFilter
     */
    public function setAdapter(wrxNext_Autocomplete_Adapter_Interface $adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * gets filter
     *
     * @return Zend_Filter_Interface
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * sets filter
     *
     * @param Zend_Filter_Interface $filter
     * @return wrxNext_Autocomplete_Adapter_ResultFilter
     */
    public function setFilter(Zend_Filter_Interface $filter)
    {
        $this->_filter = $filter;
        return $this;
    }

    /**
     * returns adapter cache identifier
     *
     * @return string
     */
    public function getCacheIdentifier()
    {
        return 'cache_identifier_' . md5(serialize($this->_filter));
    }

    /**
     * @inheritdoc
     */
    public function searchOptions($options)
    {
        if (isset($options['adapter'])) {
            $this->setAdapter($options['adapter']);
        }
        if (isset($options['filter'])) {
            $this->setFilter($options['filter']);
        }
    }

    /**
     * @inheritdoc
     */
    public function search($term, $limit = null)
    {
        $result = $this->_adapter->search($term, $limit);
        if ($this->_filter instanceof Zend_Filter_Interface) {
            foreach ($result as &$value) {
                $value = $this->_filter->filter($value);
            }
        }
        return $result;
    }
}
