<?php

interface wrxNext_Autocomplete_Adapter_OptionsInterface
{

    /**
     * provides single method to set adapter options
     *
     * @param mixed $options
     */
    public function searchOptions($options);
}
