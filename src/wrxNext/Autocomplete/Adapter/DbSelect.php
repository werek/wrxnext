<?php

class wrxNext_Autocomplete_Adapter_DbSelect implements
    wrxNext_Autocomplete_Adapter_Interface,
    wrxNext_Autocomplete_Adapter_OptionsInterface
{

    /**
     * holds select
     *
     * @var Zend_Db_Select
     */
    protected $_select = null;

    /**
     * holds instance of db adapter
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_adapter = null;

    /**
     * holds field names to search
     *
     * @var array
     */
    protected $_searchFields = array();

    /**
     * selects fetch mode
     *
     * @var int
     */
    protected $_fetchMode = Zend_Db::FETCH_ASSOC;

    /**
     *
     * @param Zend_Db_Select|array $select
     * @param Zend_Db_Adapter_Abstract $adapter
     * @param string $searchFields
     */
    public function __construct($select,
                                Zend_Db_Adapter_Abstract $adapter = null, $searchFields = null)
    {
        $options = array(
            'select' => $select,
            'adapter' => $adapter,
            'searchFields' => $searchFields
        );
        $this->searchOptions($options);
    }

    /**
     * @inheritdoc
     */
    public function searchOptions($options)
    {
        if (isset($options['select'])) {
            $this->setSelect($options['select']);
        }
        if (isset($options['adapter'])) {
            $this->setAdapter($options['adapter']);
        }
        if (isset($options['searchFields'])) {
            $this->setSearchFields($options['searchFields']);
        }
        if (isset($options['fetchMode'])) {
            $this->setFetchMode($options['fetchMode']);
        }
    }

    /**
     * gets select instance
     *
     * @return Zend_Db_Select
     */
    public function getSelect()
    {
        return $this->_select;
    }

    /**
     * sets select instance
     *
     * @param Zend_Db_Select $select
     * @return wrxNext_Autocomplete_Adapter_DbSelect
     */
    public function setSelect(Zend_Db_Select $select)
    {
        $this->_select = $select;
        return $this;
    }

    /**
     * gets db adapter
     *
     * @return Zend_Db_Adapter_Abstract
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    /**
     * sets db adapter
     *
     * @param Zend_Db_Adapter_Abstract $adapter
     * @return wrxNext_Autocomplete_Adapter_DbSelect
     */
    public function setAdapter($adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * gets searched fields
     *
     * @return string|array|Zend_Db_Expr
     */
    public function getSearchFields()
    {
        return $this->_searchFields;
    }

    /**
     * sets searched fields
     *
     * @param string|array|Zend_Db_Expr $fields
     * @return wrxNext_Autocomplete_Adapter_DbSelect
     */
    public function setSearchFields($fields)
    {
        $this->_searchFields = (array)$fields;
        return $this;
    }

    /**
     * gets fetch mode
     *
     * @return int
     */
    public function getFetchMode()
    {
        return $this->_fetchMode;
    }

    /**
     * sets fetch mode
     *
     * @param int $mode
     * @return wrxNext_Autocomplete_Adapter_DbSelect
     */
    public function setFetchMode($mode)
    {
        $this->_fetchMode = $mode;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function search($term, $limit = null)
    {
        // TODO Auto-generated method stub
        $adapter = ($this->_adapter instanceof Zend_Db_Adapter_Abstract) ? $this->_adapter : Zend_Db_Table_Abstract::getDefaultAdapter();
        $select = clone $this->_select;

        // adding sql like search for all search fields and adding where
        // condition to cloned select statement
        $value = '%' . $term . '%';
        $cond = array();
        foreach ($this->_searchFields as $field) {
            $cond[] = '(' . $adapter->quoteInto($field . ' like ?', $value) . ')';
        }
        $select->where(implode(' OR ', $cond));

        if (!is_null($limit)) {
            $select->limit($limit);
        }

        $stmt = $adapter->query($select);
        $stmt->setFetchMode($this->_fetchMode);

        $result = new wrxNext_Autocomplete_Result(array());
        // populating resultset
        while ($row = $stmt->fetch()) {
            $result->append($row);
        }

        return $result;
    }
}
