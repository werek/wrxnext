<?php

class wrxNext_Autocomplete_Adapter_DbTable implements
    wrxNext_Autocomplete_Adapter_Interface,
    wrxNext_Autocomplete_Adapter_OptionsInterface
{

    /**
     * holds table instance
     *
     * @var Zend_Db_Table_Abstract
     */
    protected $_table = null;

    /**
     * holds field names to search
     *
     * @var array
     */
    protected $_searchFields = array();

    /**
     * selects fetch mode
     *
     * @var int
     */
    protected $_fetchMode = Zend_Db::FETCH_ASSOC;

    /**
     * additionall sorting
     *
     * @var array
     */
    protected $_order = array();

    /**
     * constructor
     *
     * @param Zend_Db_Table_Abstract $table
     * @param string|array $searchFields
     * @param string|array $order
     */
    public function __construct($table, $searchFields = null, $order = null)
    {
        $options = array(
            'table' => $table,
            'searchFields' => $searchFields,
            'order' => $order
        );
        $this->searchOptions($options);
    }

    /**
     * @inheritdoc
     */
    public function searchOptions($options)
    {
        if (isset($options['table'])) {
            $this->setTable($options['table']);
        }
        if (isset($options['searchFields'])) {
            $this->setSearchFields($options['searchFields']);
        }
        if (isset($options['fetchMode'])) {
            $this->setFetchMode($options['fetchMode']);
        }
        if (isset($options['order'])) {
            $this->setOrder($options['order']);
        }
    }

    /**
     * gets table instance
     *
     * @return Zend_Db_Table_Abstract
     */
    public function getTable()
    {
        return $this->_table;
    }

    /**
     * sets table instance
     *
     * @param Zend_Db_Table_Abstract $table
     * @return wrxNext_Autocomplete_Adapter_DbTable
     */
    public function setTable($table)
    {
        $this->_table = $table;
        return $this;
    }

    /**
     * gets searched fields
     *
     * @return string|array
     */
    public function getSearchFields()
    {
        return $this->_searchFields;
    }

    /**
     * sets searched fields
     *
     * @param string|array $fields
     * @return wrxNext_Autocomplete_Adapter_DbTable
     */
    public function setSearchFields($fields)
    {
        $this->_searchFields = (array)$fields;
        return $this;
    }

    /**
     * gets fetch mode
     *
     * @return int
     */
    public function getFetchMode()
    {
        return $this->_fetchMode;
    }

    /**
     * sets fetch mode
     *
     * @param int $mode
     * @return wrxNext_Autocomplete_Adapter_DbTable
     */
    public function setFetchMode($mode)
    {
        $this->_fetchMode = $mode;
        return $this;
    }

    /**
     * gets order of entries
     *
     * @return array
     */
    public function getOrder()
    {
        return $this->_order;
    }

    /**
     * sets order of entries
     *
     * @param string|array $order
     * @return wrxNext_Autocomplete_Adapter_DbTable
     */
    public function setOrder($order)
    {
        $this->_order = (array)$order;
        return $this;
    }

    /**
     * returns adapter cache identifier
     *
     * @return string
     */
    public function getCacheIdentifier()
    {
        return 'cache_autocomplete_dbtable_' . md5(
            serialize(
                array(
                    $this->_table->info(Zend_Db_Table::NAME),
                    $this->_fetchMode,
                    $this->_order,
                    $this->_searchFields
                )
            ));
    }

    /**
     * @inheritdoc
     */
    public function search($term, $limit = null)
    {
        $select = $this->_table->select();

        foreach ($this->_order as $order) {
            $select->order($order);
        }
        $value = '%' . $term . '%';
        foreach ($this->_searchFields as $field) {
            $select->orWhere($field . ' like ?', $value);
        }
        $stmt = $select->query($this->_fetchMode);
        $result = new wrxNext_Autocomplete_Result(array());

        while ($row = $stmt->fetch()) {
            $result->append($row);
        }

        return $result;
    }
}
