<?php

class wrxNext_Autocomplete_Adapter_Null implements
    wrxNext_Autocomplete_Adapter_Interface,
    wrxNext_Autocomplete_Adapter_OptionsInterface
{

    /**
     * @inheritdoc
     */
    public function searchOptions($options)
    {
        // dummy method, does nothing
    }

    /**
     * @inheritdoc
     */
    public function search($term, $limit = null)
    {
        return new wrxNext_Autocomplete_Result(array());
    }
}
