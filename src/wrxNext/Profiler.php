<?php

/**
 * klasa profiler'a czasowego, zbiera czasy wykonywania poszczegolnych etapow
 *
 */
class wrxNext_Profiler
{

    /**
     * przechowuje tablice z czasami startu
     *
     * @var array
     */
    private static $_timeTable;

    /**
     * przechowuje czasy zakonczonych sesji
     *
     * @var array
     */
    private static $_timeTableEnded;

    /**
     * przechowuje miedzyczasy
     *
     * @var array
     */
    private static $_markers;

    /**
     * startuje pomiar czasu
     *
     * @param string $sessionName
     */
    public static function start($sessionName = 'default')
    {
        self::$_timeTable[$sessionName] = microtime();
    }

    /**
     * ustawia marker i zlicza dla niego czas z konkretnej sesji
     * zwraca czas markera
     *
     * @param string $markerName
     * @param string $sessionName
     * @return mixed
     */
    public static function mark($markerName, $sessionName = 'default')
    {
        $tmp = self::end($sessionName, true);
        self::$_markers[$markerName] = $tmp;
        return $tmp;
    }

    /**
     * konczy/zlicza pomiar czasu, opcjonalnie zostawiajac wynik dla
     * pozniejszego sprawdzenia
     *
     * @param string $sessionName
     * @param bool $keepStart
     * @return mixed
     */
    public static function end($sessionName = 'default', $keepStart = false)
    {
        $tmp = microtime();
        $result = $tmp - self::$_timeTable[$sessionName];
        if (!$keepStart) {
            self::$_timeTableEnded[$sessionName] = $result;
            unset(self::$_timeTable[$sessionName]);
        }
        return $result;
    }

    /**
     * zwraca wszystkie markery
     *
     * @return array
     */
    public static function getMarkers()
    {
        return self::$_markers;
    }

    /**
     * zwraca czasy zakonczonych sesji
     *
     * @return array
     */
    public static function getEndedSessions()
    {
        return self::$_timeTableEnded;
    }
}
