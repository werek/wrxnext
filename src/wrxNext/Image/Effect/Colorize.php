<?php

class wrxNext_Image_Effect_Colorize implements wrxNext_Image_Effect_Interface
{
    private $_params = array('red' => null, 'green' => null, 'blue' => null);

    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
    }

    public function setParam($paramName, $paramValue)
    {
        if (array_key_exists($paramName, $this->_params)) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    public function applyToImage(wrxNext_Image $image)
    {
        $missing = array();
        foreach ($this->_params as $k => $v) {
            if (in_array($k, array('red', 'green', 'blue')) && is_null($v)) {
                $missing[] = $k;
            }
        }
        if (count($missing) > 0) {
            throw new wrxNext_Image_Effect_Exception('missing value for ' . implode('&', $missing) . ' param of colorize effect');
        }
        if (!imagefilter($image->_imageGD, IMG_FILTER_COLORIZE, $this->_params['red'], $this->_params['green'], $this->_params['blue'])) {
            throw new wrxNext_Image_Effect_Exception('Could not apply colorize effect');
        }
    }

    public function effectName()
    {
        return "colorize";
    }
}
