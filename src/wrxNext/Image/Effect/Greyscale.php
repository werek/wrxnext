<?php

class wrxNext_Image_Effect_Greyscale implements wrxNext_Image_Effect_Interface
{
    public function __construct($params = null)
    {
        //not used
    }

    public function setParam($paramName, $paramValue)
    {
        //not used
    }

    public function applyToImage(wrxNext_Image $image)
    {
        $x = $image->getWidth();
        $y = $image->getHeight();
        for ($y = 0; $y < $image->getHeight(); $y++) {
            for ($x = 0; $x < $image->getWidth(); $x++) {
                $rgb = imagecolorat($image->_imageGD, $x, $y);
                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;
                //sephia
                $red2 = min($red * .393 + $green * .769 + $blue * .189, 255);
                $green2 = min($red * .349 + $green * .686 + $blue * .168, 255);
                $blue2 = min($red * .272 + $green * .534 + $blue * .131, 255);
                // shift gray level to the left

                $grayR = $red2 << 16;   // R: red
                $grayG = $green2 << 8;    // G: green
                $grayB = $blue2;         // B: blue

                // OR operation to compute gray value
                $grayColor = $grayR | $grayG | $grayB;

                // set the pixel color
                imagesetpixel($image->_imageGD, $x, $y, $grayColor);
                imagecolorallocate($image->_imageGD, $grayColor, $grayColor, $grayColor);
            }
        }
    }

    public function effectName()
    {
        return "greyscale";
    }
}
