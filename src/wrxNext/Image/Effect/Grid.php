<?php

/**
 * klasa rysuje siatke o zadanej rozdzielczosci niskiej(dokładniejszej) i wysokiej (zgrubnej) na całym obrazie
 *
 */
class wrxNext_Image_Effect_Grid implements wrxNext_Image_Effect_Interface
{
    protected $_params = array(
        'low_resolution' => 2,
        'high_resolution' => 10,
        'low_color' => '#999',
        'high_color' => '#fff',
        'draw_numbers' => false,
        'numbers_multiplier' => 1,
        'numbers_multiplier_round' => 2
    );

    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        } elseif (!is_null($params)) {
            throw new wrxNext_Image_Effect_Exception("Expected type of params is 'array'");
        }
    }

    public function setParam($paramName, $paramValue)
    {
        if (isset($this->_params[$paramName])) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    public function applyToImage(wrxNext_Image $image)
    {
        //generowanie siatki szczegolowej

        //alokacja koloru
        $color_tmp = wrxNext_Color::hexToRgb($this->_params['low_color']);
        $color = imagecolorallocate($image->_imageGD, $color_tmp[wrxNext_color::RED], $color_tmp[wrxNext_color::GREEN], $color_tmp[wrxNext_color::BLUE]);

        //generowanie pionowych linii
        for ($x = $this->_params['low_resolution']; $x <= $image->getWidth(); $x += $this->_params['low_resolution']) {
            imageline($image->_imageGD, $x, 0, $x, $image->getHeight(), $color);
        }
        unset($x);
        //generowanie poziomych lini
        for ($y = $this->_params['low_resolution']; $y <= $image->getHeight(); $y += $this->_params['low_resolution']) {
            imageline($image->_imageGD, 0, $y, $image->getWidth(), $y, $color);
        }
        unset($y);

        //generowanie siatki zgrubnej

        //alokacja koloru
        $color_tmp = wrxNext_Color::hexToRgb($this->_params['high_color']);
        $color = imagecolorallocate($image->_imageGD, $color_tmp[wrxNext_color::RED], $color_tmp[wrxNext_color::GREEN], $color_tmp[wrxNext_color::BLUE]);

        //przygotowywanie ew. rysowania numerów skali
        if ($this->_params['draw_numbers']) {
            $n = new wrxNext_Image_Effect_String(array('color' => $this->_params['high_color'], 'font' => 1));
        }

        //generowanie pionowych linii
        for ($x = $this->_params['high_resolution']; $x <= $image->getWidth(); $x += $this->_params['high_resolution']) {
            imageline($image->_imageGD, $x, 0, $x, $image->getHeight(), $color);
            if ($this->_params['draw_numbers']) {
                $displayX = round($x * $this->_params['numbers_multiplier'], $this->_params['numbers_multiplier_round']);
                $n->setParam('text', $displayX);
                $n->setParam('position', $x . 'x0');
                $n->applyToImage($image);
            }
        }
        unset($x);
        //generowanie poziomych lini
        for ($y = $this->_params['high_resolution']; $y <= $image->getHeight(); $y += $this->_params['high_resolution']) {
            imageline($image->_imageGD, 0, $y, $image->getWidth(), $y, $color);
            if ($this->_params['draw_numbers']) {
                $displayY = round($y * $this->_params['numbers_multiplier'], $this->_params['numbers_multiplier_round']);
                $n->setParam('text', $displayY);
                $n->setParam('position', '0x' . $y);
                $n->applyToImage($image);
            }
        }
        unset($y);
    }

    public function effectName()
    {
        return "Grid";
    }
}
