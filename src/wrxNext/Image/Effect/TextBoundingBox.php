<?php

class wrxNext_Image_Effect_TextBoundingBox implements wrxNext_Image_Effect_Interface
{
    use wrxNext_Trait_RegistryLog;

    /**
     * internall params array
     *
     * @var array
     */
    protected $_params = array(
        'position' => '0x0',
        'width' => null,
        'height' => null,
        'color' => '#000',
        'font' => null,
        'size' => 8,
        'spacing' => 0,
        'margin-left' => 5,
        'margin-right' => 5,
        'margin-top' => 5,
        'margin-bottom' => 5,
        'line-height' => null,
        'ignoreNewLine' => true
    );

    /**
     * holds text stack
     *
     * @var wrxNext_Image_Effect_TextBoundingBox_Stack
     */
    protected $_text = null;

    /**
     * constructor
     *
     * @param string $params
     */
    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->setParams($params);
        }
    }

    /**
     * sets multiple params in one pass
     *
     * @param array $params
     * @return wrxNext_Image_Effect_TextBoundingBox
     */
    public function setParams($params = array())
    {
        foreach ($params as $key => $value) {
            $this->setParam($key, $value);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setParam($paramName, $paramValue)
    {
        $method = 'set' . ucfirst($paramName);
        if (method_exists($this, $method)) {
            $this->{$method}($paramValue);
        } else {
            $this->_params[$paramName] = $paramValue;
        }
        return $this;
    }

    /**
     * returns param value
     *
     * @param string $paramName
     * @return mixed
     */
    public function getParam($paramName)
    {
        $method = 'get' . ucfirst($paramName);
        if (method_exists($this, $method)) {
            return $this->{$method}();
        } else {
            return $this->_params[$paramName];
        }
    }

    /**
     * gets text
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * sets text
     *
     * @param string $text
     * @return wrxNext_Image_Effect_TextBoundingBox
     */
    public function setText($text)
    {
        $this->_text = $text;
        return $this;
    }

    /**
     * sets font
     *
     * @param string $path
     * @return wrxNext_Image_Effect_TextBoundingBox
     */
    public function setFont($path)
    {
        $this->_params['font'] = $path;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function applyToImage(wrxNext_Image $image)
    {
        // sprawdzenie czy bedziemy mogli wykorzystac font
        if (!file_exists($this->getFont()) || !is_readable($this->getFont())) {
            throw new wrxNext_Image_Effect_Exception(
                'given path to ttf font: ' . $this->getFont() .
                ' is not accesible');
        }

        // coordinates
        $reflow = new wrxNext_Image_Effect_TextBoundingBox_Reflow($this);
        $width = (!empty($this->_params['width'])) ? $this->_params['width'] : $image->getWidth();
        $height = (!empty($this->_params['height'])) ? $this->_params['height'] : $image->getHeight();
        $width = $width - $this->_params['margin-left'] -
            $this->_params['margin-right'];
        $height = $height - $this->_params['margin-top'] -
            $this->_params['margin-bottom'];
        $reflow->reflow($width, $height);

        // position
        preg_match('@(\d*)x(\d*)@', $this->_params['position'], $xy);
        $x = (empty($xy[1])) ? 0 : $xy[1];
        $y = (empty($xy[2])) ? 0 : $xy[2];
        $offsetX = $x + $this->_params['margin-left'];
        $offsetY = $y + $this->_params['margin-top'];

        $this->_log('rendering using font: ' . $this->getFont());
        // pushing render
        foreach ($reflow as $line) {
            $this->_renderLine($image, $line, $offsetX, $offsetY);
        }
    }

    /**
     * gets font
     *
     * @return string
     */
    public function getFont()
    {
        if (is_null($this->_params['font'])) {
            $this->_params['font'] = realpath(
                dirname(__FILE__) . '/Text/default.ttf');
        }
        return $this->_params['font'];
    }

    /**
     * renders given letter to image
     *
     * @param wrxNext_Image $image
     * @param wrxNext_Image_Effect_TextBoundingBox_Reflow_Line $line
     * @param int $offsetX
     * @param int $offsetY
     */
    public function _renderLine(wrxNext_Image $image,
                                wrxNext_Image_Effect_TextBoundingBox_Reflow_Line $line, $offsetX,
                                $offsetY)
    {
        $x = $offsetX + $line->getX();
        $y = $offsetY + $line->getY();
        $text = $line->getText();
        $font = $this->getFont();
        $size = $this->_params['size'];
        $colorBase = $this->_params['color'];
        $this->_log('rendering line: ' . $text);
        $this->_log('coordinates: ' . $x . '/' . $y);
        // allocationg color
        if (preg_match('@#\d*@', $colorBase)) {
            $data = wrxNext_Color::hexToRgb($colorBase);
            $color = imagecolorallocate($image->_imageGD, $data[wrxNext_Color::RED],
                $data[wrxNext_Color::GREEN], $data[wrxNext_Color::BLUE]);
        } else {
            $color = imagecolorallocate($image->_imageGD, 0, 0, 0);
        }
        imagettftext($image->_imageGD, $size, 0, $x, $y, $color, $font, $text);
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Image_Effect_Text::effectName()
     */
    public function effectName()
    {
        return 'text_boundingbox';
    }
}
