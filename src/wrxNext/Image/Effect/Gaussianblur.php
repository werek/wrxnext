<?php

class wrxNext_Image_Effect_Gaussianblur implements wrxNext_Image_Effect_Interface
{
    public function __construct($params = null)
    {
        //not used
    }

    public function setParam($paramName, $paramValue)
    {
        //not used
    }

    public function applyToImage(wrxNext_Image $image)
    {
        if (!imagefilter($image->_imageGD, IMG_FILTER_GAUSSIAN_BLUR)) {
            throw new wrxNext_Image_Effect_Exception('Could not apply gaussian blur effect');
        }
    }

    public function effectName()
    {
        return "gaussian_blur";
    }
}
