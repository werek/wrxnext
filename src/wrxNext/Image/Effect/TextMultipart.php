<?php

class wrxNext_Image_Effect_TextMultipart implements wrxNext_Image_Effect_Interface
{
    use wrxNext_Trait_RegistryLog;

    /**
     * internall params array
     *
     * @var array
     */
    protected $_params = array(
        'position' => '0x0',
        'color' => '#000',
        'font' => null,
        'size' => 8,
        'spacing' => 0,
        'margin-left' => 5,
        'margin-right' => 5,
        'margin-top' => 5,
        'margin-bottom' => 5,
        'line-height' => null
    );

    /**
     * holds text stack
     *
     * @var wrxNext_Image_Effect_TextMultipart_Stack
     */
    protected $_text = null;

    /**
     * constructor
     *
     * @param string $params
     */
    public function __construct($params = null)
    {
        $this->_text = new wrxNext_Image_Effect_TextMultipart_Stack(array());
        if (is_array($params)) {
            $this->setParams($params);
        }
    }

    /**
     * sets multiple params in one pass
     *
     * @param array $params
     * @return wrxNext_Image_Effect_TextMultipart
     */
    public function setParams($params = array())
    {
        foreach ($params as $key => $value) {
            $this->setParam($key, $value);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setParam($paramName, $paramValue)
    {
        $method = 'set' . ucfirst($paramName);
        if (method_exists($this, $method)) {
            $this->{$method}($paramValue);
        } else {
            $this->_params[$paramName] = $paramValue;
        }
        return $this;
    }

    /**
     * gets text
     *
     * @return wrxNext_Image_Effect_TextMultipart_Stack
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * sets text
     *
     * @param wrxNext_Image_Effect_TextMultipart_Stack $stack
     * @return wrxNext_Image_Effect_TextMultipart
     */
    public function setText($stack)
    {
        if (!$stack instanceof wrxNext_Image_Effect_TextMultipart_Stack) {
            $stack = (array)$stack;
            $newStack = new wrxNext_Image_Effect_TextMultipart_Stack(array());
            $newStack->exchangeArray($stack);
            $stack = $newStack;
        }
        $this->_text = $stack;
        return $this;
    }

    /**
     * sets font
     *
     * @param string $path
     * @return wrxNext_Image_Effect_TextMultipart
     */
    public function setFont($path)
    {
        $this->_params['font'] = $path;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function applyToImage(wrxNext_Image $image)
    {
        // sprawdzenie czy bedziemy mogli wykorzystac font
        if (!file_exists($this->getFont()) || !is_readable($this->getFont())) {
            throw new wrxNext_Image_Effect_Exception(
                'given path to ttf font: ' . $this->getFont() .
                ' is not accesible');
        }

        // coordinates
        $coordinates = new wrxNext_Image_Effect_TextMultipart_Coordinates(
            $this->_text);
        // calculating dimansions and location of letters
        $coordinates->calculateCoordinates($this->_params['spacing'],
            $this->getFont(), $this->_params['size']);
        // bounding box dimensions
        $textWidth = $coordinates->getWidth();
        $textHeight = $coordinates->getHeight();

        // przygotowanie parametrow do nalozenia na obraz
        preg_match('@(\d*)x(\d*)@', $this->_params['position'], $xy);
        $x = (empty($xy[1])) ? 0 : $xy[1];
        $y = (empty($xy[2])) ? 0 : $xy[2];

        // sprawdzenie czy tekst zmiesci sie na obrazie
        if (($x + $textWidth + $this->_params['margin-left'] +
                $this->_params['margin-right']) < $image->getWidth() && ($y +
                $textHeight + $this->_params['margin-top'] +
                $this->_params['margin-bottom']) < $image->getHeight()
        ) {
            // caly tekst zmiesci sie na obrazie
            $x += $this->_params['margin-left'];
            $y += $this->_params['margin-top'];
        } elseif (($x + $textWidth) < $image->getWidth() &&
            ($y + $textHeight) < $image->getHeight()
        ) {
            // tekst zmiesci sie ale bedzie trzeba usunac marginesy
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image with current margin setting');
        } elseif (($textWidth) < $image->getWidth() &&
            ($textHeight) < $image->getHeight()
        ) {
            // tekst zmiesci sie ale bez marginesow i nie na ustalonej pozycji
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image with current margin setting and position');
        } else {
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image');
        }

        // pushing render
        foreach ($coordinates as $letter) {
            $this->_renderLetter($image, $letter, $x, $y);
        }
    }

    /**
     * gets font
     *
     * @return string
     */
    public function getFont()
    {
        if (is_null($this->_params['font'])) {
            $this->_params['font'] = realpath(
                dirname(__FILE__) . '/Text/default.ttf');
        }
        return $this->_params['font'];
    }

    /**
     * renders given letter to image
     *
     * @param wrxNext_Image $image
     * @param wrxNext_Image_Effect_TextMultipart_Coordinates_Letter $letter
     * @param int $offsetX
     * @param int $offsetY
     */
    public function _renderLetter(wrxNext_Image $image,
                                  wrxNext_Image_Effect_TextMultipart_Coordinates_Letter $letter, $offsetX,
                                  $offsetY)
    {
        $x = $offsetX + $letter->getX();
        $y = $offsetY + $letter->getY();
        if (!is_null($this->_params['line-height'])) {
            $y = $offsetY + intval($this->_params['line-height']);
        }
        $text = $letter->getLetter();
        $font = is_null($letter->getPart()->getFont()) ? $this->getFont() : $letter->getPart()->getFont();
        $size = is_null($letter->getPart()->getSize()) ? $this->_params['size'] : $letter->getPart()->getSize();
        $colorBase = is_null($letter->getPart()->getColor()) ? $this->_params['color'] : $letter->getPart()->getColor();
        // allocationg color
        if (preg_match('@#\d*@', $colorBase)) {
            $data = wrxNext_Color::hexToRgb($colorBase);
            $color = imagecolorallocate($image->_imageGD, $data[wrxNext_Color::RED],
                $data[wrxNext_Color::GREEN], $data[wrxNext_Color::BLUE]);
        } else {
            $color = imagecolorallocate($image->_imageGD, 0, 0, 0);
        }
        imagettftext($image->_imageGD, $size, 0, $x, $y, $color, $font, $text);
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Image_Effect_Text::effectName()
     */
    public function effectName()
    {
        return 'text_multipart';
    }
}
