<?php

interface wrxNext_Image_Effect_Interface
{

    /**
     * constructor, takes configuration params
     *
     * @param mixed $params
     */
    public function __construct($params = null);

    /**
     * sets single param
     *
     * @param string $paramName
     * @param mixed $paramValue
     */
    public function setParam($paramName, $paramValue);

    /**
     * main effect logic, applies to given image
     *
     * @param wrxNext_Image $image
     */
    public function applyToImage(wrxNext_Image $image);

    /**
     * default effect name
     *
     * @return string
     */
    public function effectName();
}
