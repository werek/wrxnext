<?php

/**
 * adjusts brightness
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Image_Effect_Brightness implements wrxNext_Image_Effect_Interface
{

    /**
     * effect params
     *
     * @var array
     */
    private $_params = array(
        'brightness' => null
    );

    /**
     * constructor, accepts configuration params
     *
     * @param null|array $params
     */
    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
    }

    /**
     * @inheritdoc
     */
    public function setParam($paramName, $paramValue)
    {
        if (array_key_exists($paramName, $this->_params)) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    /**
     * @inheritdoc
     */
    public function applyToImage(wrxNext_Image $image)
    {
        if (is_null($this->_params['brightness'])) {
            throw new wrxNext_Image_Effect_Exception(
                'there is no value for applying brightness effect');
        }
        if (!imagefilter($image->_imageGD, IMG_FILTER_BRIGHTNESS,
            $this->_params['brightness'])
        ) {
            throw new wrxNext_Image_Effect_Exception(
                'Could not apply brightness effect');
        }
    }

    public function effectName()
    {
        return "brightness";
    }
}
