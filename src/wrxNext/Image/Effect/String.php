<?php

class wrxNext_Image_Effect_String implements wrxNext_Image_Effect_Interface
{
    protected $_params = array(
        'font' => 1,
        'position' => '0x0',
        'text' => '',
        'color' => '#000'
    );

    public function __construct($params = null)
    {
        $this->_params = array_merge($this->_params, $params);
    }

    public function setParam($paramName, $paramValue)
    {
        if (isset($this->_params[$paramName])) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    public function applyToImage(wrxNext_Image $image)
    {
        //przygotowanie parametrow do nalozenia na obraz
        preg_match('@(\d*)x(\d*)@', $this->_params['position'], $xy);
        $x = (empty($xy[1])) ? 0 : $xy[1];
        $y = (empty($xy[2])) ? 0 : $xy[2];

        //alokacja koloru
        $data = wrxNext_Color::hexToRgb($this->_params['color']);
        $color = imagecolorallocate($image->_imageGD, $data[wrxNext_Color::RED], $data[wrxNext_Color::GREEN], $data[wrxNext_Color::BLUE]);

        //wstawianie tekstu
        imagestring($image->_imageGD, $this->_params['font'], $x, $y, $this->_params['text'], $color);
    }

    public function effectName()
    {
        return "String";
    }
}
