<?php

class wrxNext_Image_Effect_TextBoundingBox_Reflow extends \ArrayObject
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds effect reference
     *
     * @var wrxNext_Image_Effect_TextBoundingBox
     */
    protected $_boundingBox = null;

    /**
     * constructor
     *
     * @param wrxNext_Image_Effect_TextBoundingBox $boundingBoxEffect
     */
    public function __construct(
        wrxNext_Image_Effect_TextBoundingBox $boundingBoxEffect)
    {
        parent::__construct(array());
        $this->_boundingBox = $boundingBoxEffect;
    }

    /**
     * reflows text for given width/height
     *
     * @param int $width
     * @param int $height
     * @return wrxNext_Image_Effect_TextBoundingBox_Reflow
     */
    public function reflow($width, $height)
    {
        $text = $this->_boundingBox->getText();
        // resetting current line stack
        $this->exchangeArray(array());
        $ignoreNewLine = $this->_boundingBox->getParam('ignoreNewLine');
        if ($ignoreNewLine) {
            $text = preg_replace("@(\n|\r\n)@u", ' ', $text);
            $inputLines = (array)$text;
        } else {
            $inputLines = explode("\n", $text);
        }
        $top = 0;
        $font = $this->_boundingBox->getFont();
        $size = $this->_boundingBox->getParam('size');
        $lineHeight = $this->_boundingBox->getParam('line-height');
        $this->_log('line-height');
        $this->_log($lineHeight);
        foreach ($inputLines as $line) {
            $words = preg_split('@ @', $line);
            while (count($words) && $top < $height) {
                $rawLine = array_shift($words);
                while (count($words) && $this->_lineWidth(
                        $rawLine . ' ' . $words[0], $font, $size) < $width) {
                    $rawLine .= ' ' . array_shift($words);
                }
                if (empty($lineHeight)) {
                    $lineHeight = $this->_lineHeight($text, $font, $size);
                }
                if (($top + $lineHeight) <= $height) {
                    $top += $lineHeight;
                    $splitLine = new wrxNext_Image_Effect_TextBoundingBox_Reflow_Line(
                        $rawLine, 0, $top);
                    $this->append($splitLine);
                }
            }
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function exchangeArray($input)
    {
        foreach ($input as $key => $value) {
            if (!$value instanceof wrxNext_Image_Effect_TextBoundingBox_Reflow_Line) {
                throw new wrxNext_Image_Effect_Exception(
                    'Reflow expects value to be instance of wrxNext_Image_Effect_TextBoundingBox_Reflow_Line class');
            }
        }
        return parent::exchangeArray($input);
    }

    /**
     * returns text width
     *
     * @param string $text
     * @param string $font
     * @param float $size
     * @throws wrxNext_Image_Effect_Exception
     * @return int
     */
    protected function _lineWidth($text, $font, $size)
    {
        $dimensions = imagettfbbox($size, 0, $font, $text);
        if ($dimensions === false) {
            throw new wrxNext_Image_Effect_Exception(
                'error getting bounding box dimensions for size: ' . $size .
                ', font: ' . $font . ', text: ' . $text);
        }
        return round($dimensions[2] - $dimensions[0]);
    }

    /**
     * returns text height
     *
     * @param string $text
     * @param string $font
     * @param float $size
     * @throws wrxNext_Image_Effect_Exception
     * @return int
     */
    protected function _lineHeight($text, $font, $size)
    {
        $dimensions = imagettfbbox($size, 0, $font, $text);
        if ($dimensions === false) {
            throw new wrxNext_Image_Effect_Exception(
                'error getting bounding box dimensions for size: ' . $size .
                ', font: ' . $font . ', text: ' . $text);
        }
        return round($dimensions[1] - $dimensions[7]);
    }

    /**
     * @inheritdoc
     */
    public function append($value)
    {
        if (!$value instanceof wrxNext_Image_Effect_TextBoundingBox_Reflow_Line) {
            throw new wrxNext_Image_Effect_Exception(
                'Reflow expects value to be instance of wrxNext_Image_Effect_TextBoundingBox_Reflow_Line class');
        }
        parent::append($value);
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        if (!$value instanceof wrxNext_Image_Effect_TextBoundingBox_Reflow_Line) {
            throw new wrxNext_Image_Effect_Exception(
                'Reflow expects value to be instance of wrxNext_Image_Effect_TextBoundingBox_Reflow_Line class');
        }
        parent::offsetSet($offset, $value);
    }
}
