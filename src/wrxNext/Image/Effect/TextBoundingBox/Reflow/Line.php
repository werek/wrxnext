<?php

class wrxNext_Image_Effect_TextBoundingBox_Reflow_Line
{

    /**
     * holds text to render
     *
     * @var string
     */
    protected $_text = null;

    /**
     * holds rendering position x coordinate
     *
     * @var int
     */
    protected $_x = null;

    /**
     * holds rendering position y coordinate
     *
     * @var int
     */
    protected $_y = null;

    /**
     * constructor
     *
     * @param string $text
     * @param int $x
     * @param int $y
     */
    public function __construct($text, $x, $y)
    {
        $this->_text = $text;
        $this->_x = $x;
        $this->_y = $y;
    }

    /**
     * returns text to render
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * returns x coordinate
     *
     * @return int
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * returns y coordinate
     *
     * @return int
     */
    public function getY()
    {
        return $this->_y;
    }
}
