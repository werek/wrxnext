<?php

class wrxNext_Image_Effect_Text implements wrxNext_Image_Effect_Interface
{

    protected $_params = array(
        'text' => null,
        'position' => '0x0',
        'color' => '#000',
        'font' => null,
        'size' => 8,
        'angle' => 0,
        'margin-left' => 5,
        'margin-right' => 5,
        'margin-top' => 5,
        'margin-bottom' => 5
    );

    /**
     * constructor
     *
     * @param array $params
     */
    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
    }

    /**
     * @inheritdoc
     */
    public function setParam($paramName, $paramValue)
    {
        if (isset($this->_params[$paramName])) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    /**
     * @inheritdoc
     */
    public function applyToImage(wrxNext_Image $image)
    {
        if (is_null($this->_params['font'])) {
            $this->_params['font'] = realpath(
                dirname(__FILE__) . '/Text/default.ttf');
        }
        $this->generateTtf($image);
    }

    /**
     * applies text to image
     *
     * @param wrxNext_Image $image
     * @throws wrxNext_Image_Effect_Exception
     */
    protected function generateTtf(wrxNext_Image $image)
    {
        // sprawdzenie czy bedziemy mogli wykorzystac font
        if (!file_exists($this->_params['font']) ||
            !is_readable($this->_params['font'])
        ) {
            throw new wrxNext_Image_Effect_Exception(
                'given path to ttf font: ' . $this->_params['font'] .
                ' is not accesible');
        }

        // check box dimensions
        $boxDimensions = imagettfbbox($this->_params['size'],
            $this->_params['angle'], $this->_params['font'],
            $this->_params['text']);
        $textWidth = (($boxDimensions[2] > $boxDimensions[4]) ? $boxDimensions[2] : $boxDimensions[4]) -
            (($boxDimensions[0] < $boxDimensions[6]) ? $boxDimensions[0] : $boxDimensions[6]);
        $textHeight = (($boxDimensions[3] > $boxDimensions[1]) ? $boxDimensions[3] : $boxDimensions[1]) -
            (($boxDimensions[5] < $boxDimensions[7]) ? $boxDimensions[5] : $boxDimensions[7]);

        // przygotowanie parametrow do nalozenia na obraz
        preg_match('@(\d*)x(\d*)@', $this->_params['position'], $xy);
        $x = (empty($xy[1])) ? 0 : $xy[1];
        $y = (empty($xy[2])) ? 0 : $xy[2];
        // $y+=$textHeight;

        // sprawdzenie czy tekst zmiesci sie na obrazie
        if (($x + $textWidth + $this->_params['margin-left'] +
                $this->_params['margin-right']) < $image->getWidth() &&
            ($y + $textHeight + $this->_params['margin-top'] +
                $this->_params['margin-bottom']) < $image->getHeight()
        ) {
            // caly tekst zmiesci sie na obrazie
            $x += $this->_params['margin-left'];
            $y += $this->_params['margin-top'];
        } elseif (($x + $textWidth) < $image->getWidth() &&
            ($y + $textHeight) < $image->getHeight()
        ) {
            // tekst zmiesci sie ale bedzie trzeba usunac marginesy
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image with current margin setting');
        } elseif (($textWidth) < $image->getWidth() &&
            ($textHeight) < $image->getHeight()
        ) {
            // tekst zmiesci sie ale bez marginesow i nie na ustalonej pozycji
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image with current margin setting and position');
        } else {
            throw new wrxNext_Image_Effect_Exception(
                'given text at current display settings will not fit image');
        }

        // okreslenie koloru
        if (preg_match('@#\d*@', $this->_params['color'])) {
            $data = wrxNext_Color::hexToRgb($this->_params['color']);
            $color = imagecolorallocate($image->_imageGD, $data[wrxNext_Color::RED],
                $data[wrxNext_Color::GREEN], $data[wrxNext_Color::BLUE]);
        } else {
            $color = imagecolorallocate($image->_imageGD, 0, 0, 0);
        }

        // dodanie tekstu do obrazu
        imagettftext($image->_imageGD, $this->_params['size'],
            $this->_params['angle'], $x, $y + $textHeight, $color,
            $this->_params['font'], $this->_params['text']);
    }

    /**
     * (non-PHPdoc)
     * @see wrxNext_Image_Effect_Interface::effectName()
     */
    public function effectName()
    {
        return "text";
    }
}
