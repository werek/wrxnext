<?php

class wrxNext_Image_Effect_TextMultipart_Coordinates extends \ArrayObject
{

    /**
     * holds bounding box width
     *
     * @var int
     */
    protected $_width = 0;

    /**
     * hold bounding box height
     *
     * @var int
     */
    protected $_height = 0;

    /**
     * holds stack
     *
     * @var wrxNext_Image_Effect_TextMultipart_Stack
     */
    protected $_stack = null;

    /**
     * constructor
     *
     * @param wrxNext_Image_Effect_TextMultipart_Stack $stack
     */
    public function __construct(wrxNext_Image_Effect_TextMultipart_Stack $stack)
    {
        $this->setStack($stack);
        parent::__construct(array());
    }

    /**
     * gets text stack
     *
     * @return wrxNext_Image_Effect_TextMultipart_Stack
     */
    public function getStack()
    {
        return $this->_stack;
    }

    /**
     * sets text stack
     *
     * @param wrxNext_Image_Effect_TextMultipart_Stack $stack
     * @return wrxNext_Image_Effect_TextMultipart_Coordinates
     */
    public function setStack(wrxNext_Image_Effect_TextMultipart_Stack $stack)
    {
        $this->_width = null;
        $this->_height = null;
        $this->exchangeArray(array());
        $this->_stack = $stack;
        return $this;
    }

    /**
     * calculates coordinates for each letter
     *
     * @param int $spacing
     * @param string $defaultFont
     *            path to default font
     * @param int $defaultSize
     *            default font size
     * @return wrxNext_Image_Effect_TextMultipart_Coordinates
     */
    public function calculateCoordinates($spacing, $defaultFont, $defaultSize)
    {
        $this->_width = 0;
        $this->_height = 0;
        $t = 0;
        foreach ($this->_stack as $part) {
            $text = $part->getText();
            $length = mb_strlen($text, 'UTF-8');
            if ($length) {

                $size = is_null($part->getSize()) ? $defaultSize : $part->getSize();
                $font = is_null($part->getFont()) ? $defaultFont : $part->getFont();
                $partBoundingBox = imagettfbbox($size, 0, $font, $text);
                $partHeight = -($partBoundingBox[5] + $partBoundingBox[3]);
                for ($i = 0; $i < $length; $i++) {
                    $t++;
                    $letter = mb_substr($text, $i, 1, 'UTF-8');
                    $boundingBox = imagettfbbox($size, 0, $font, $letter);
                    $charWidth = $boundingBox[2] - $boundingBox[0];
                    $charHeight = -($boundingBox[5] + $boundingBox[3]);
                    if ($charHeight > $this->_height) {
                        $this->_height = $charHeight;
                    }
                    $this->_width += $charWidth;
                    if ($t) {
                        $this->_width += $spacing;
                    }
                    $this->append(
                        new wrxNext_Image_Effect_TextMultipart_Coordinates_Letter(
                            $letter, $part, ($this->_width - $charWidth),
                            $partHeight));
                }
            }
        }
        return $this;
    }

    /**
     * returns box width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * returns box height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }
}
