<?php

class wrxNext_Image_Effect_TextMultipart_Stack extends \ArrayObject
{

    /**
     * exchanges stack
     *
     * @param mixed $input
     * @return array
     */
    public function exchangeArray($input)
    {
        $tmp = array();
        foreach ($input as $key => $value) {
            if (!$value instanceof wrxNext_Image_Effect_TextMultipart_Part) {
                $value = new wrxNext_Image_Effect_TextMultipart_Part($value);
            }
            $tmp[$key] = $value;
        }
        return parent::exchangeArray($tmp);
    }

    /**
     * set multipart text part
     *
     * @param mixed $offset
     * @param wrxNext_Image_Effect_TextMultipart_Part $value
     */
    public function offsetSet($offset, $value)
    {
        if (!$value instanceof wrxNext_Image_Effect_TextMultipart_Part) {
            $value = new wrxNext_Image_Effect_TextMultipart_Part($value);
        }
        parent::offsetSet($offset, $value);
    }

    /**
     * add part to stack
     *
     * @param wrxNext_Image_Effect_TextMultipart_Part $value
     */
    public function append($value)
    {
        if (!$value instanceof wrxNext_Image_Effect_TextMultipart_Part) {
            $value = new wrxNext_Image_Effect_TextMultipart_Part($value);
        }
        parent::append($value);
    }

    /**
     * magic method to string
     *
     * @return string
     */
    public function __toString()
    {
        $text = '';
        foreach ($this as $part) {
            $text .= $part;
        }
        return $text;
    }
}
