<?php

class wrxNext_Image_Effect_TextMultipart_Part
{

    /**
     * holds text to render
     *
     * @var string
     */
    protected $_text = '';

    /**
     * holds font path
     *
     * @var string
     */
    protected $_font = null;

    /**
     * holds font size in points
     *
     * @var int
     */
    protected $_size = null;

    /**
     * holds font color in hex (ie.
     * #000000)
     *
     * @var string
     */
    protected $_color = null;

    /**
     * constructor
     *
     * accepts either params as single array or extracted
     *
     * @param string|array $text
     * @param string $font
     * @param string $size
     * @param string $color
     */
    public function __construct($text, $font = null, $size = null, $color = null)
    {
        if (!is_array($text)) {
            $text = array(
                'text' => $text,
                'font' => $font,
                'size' => $size,
                'color' => $color
            );
        }
        $this->setOptions($text);
    }

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * gets text
     *
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * sets text
     *
     * @param string $text
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function setText($text)
    {
        $this->_text = $text;
        return $this;
    }

    /**
     * gets font path to use
     *
     * @return string
     */
    public function getFont()
    {
        return $this->_font;
    }

    /**
     * sets font path to use
     *
     * @param string $path
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function setFont($path)
    {
        $this->_font = $path;
        return $this;
    }

    /**
     * gets font size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->_size;
    }

    /**
     * sets font size
     *
     * @param int $size
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    /**
     * gets color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->_color;
    }

    /**
     * sets color
     *
     * @param string $color
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function setColor($color)
    {
        $this->_color = $color;
        return $this;
    }

    /**
     * magic method to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->_text;
    }
}
