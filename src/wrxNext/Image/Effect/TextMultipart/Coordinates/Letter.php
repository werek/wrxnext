<?php

class wrxNext_Image_Effect_TextMultipart_Coordinates_Letter
{

    /**
     * holds text leter
     *
     * @var string
     */
    protected $_letter = null;

    /**
     * holds originall part object
     *
     * @var wrxNext_Image_Effect_TextMultipart_Part
     */
    protected $_part = null;

    /**
     * holds x coordinate
     *
     * @var int
     */
    protected $_x = null;

    /**
     * holds y coordinate
     *
     * @var int
     */
    protected $_y = null;

    /**
     * constructor of value object
     *
     * @param char $letter
     * @param wrxNext_Image_Effect_TextMultipart_Part $part
     * @param int $x
     * @param int $y
     */
    public function __construct($letter, $part, $x, $y)
    {
        $this->_letter = $letter;
        $this->_part = $part;
        $this->_x = $x;
        $this->_y = $y;
    }

    /**
     * returns letter
     *
     * @return string
     */
    public function getLetter()
    {
        return $this->_letter;
    }

    /**
     * returns associated part
     *
     * @return wrxNext_Image_Effect_TextMultipart_Part
     */
    public function getPart()
    {
        return $this->_part;
    }

    /**
     * returns x coordinate
     *
     * @return int
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * returns y coordinate
     *
     * @return int
     */
    public function getY()
    {
        return $this->_y;
    }

    /**
     * magic method to string
     *
     * @return string
     */
    public function __toString()
    {
        return (string)$this->_letter;
    }
}
