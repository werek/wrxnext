<?php

class wrxNext_Image_Effect_Edgedetect implements wrxNext_Image_Effect_Interface
{
    public function __construct($params = null)
    {
        //not used
    }

    public function setParam($paramName, $paramValue)
    {
        //not used
    }

    public function applyToImage(wrxNext_Image $image)
    {
        if (!imagefilter($image->_imageGD, IMG_FILTER_EDGEDETECT)) {
            throw new wrxNext_Image_Effect_Exception('Could not apply edge detect effect');
        }
    }

    public function effectName()
    {
        return "edgedetect";
    }
}
