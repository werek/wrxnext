<?php

class wrxNext_Image_Effect_Watermark implements wrxNext_Image_Effect_Interface
{
    const POS_TOP_LEFT = 0;
    const POS_TOP_RIGHT = 1;
    const POS_BOTTOM_LEFT = 2;
    const POS_BOTTOM_RIGHT = 3;
    /**
     * zawiera parametry efektu
     *
     * @var Array
     */
    private $_params = array(
        'image' => '',
        'margin-left' => 10,
        'margin-right' => 10,
        'margin-top' => 10,
        'margin-bottom' => 10,
        'location' => self::POS_BOTTOM_RIGHT,
        'useAlpha' => true
    );

    /**
     * konstruktor, parametrem jest tablica asocjacyjna zawierajaca parametry dla efektu
     *
     * @param Array $params
     */
    function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
    }

    /**
     * ustawia wartosc parametru
     *
     * @param string $paramName
     * @param mixed $paramValue
     */
    public function setParam($paramName, $paramValue)
    {
        if (array_key_exists($paramName, $this->_params)) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    /**
     * aplikuje efekt na obrazie
     *
     * @param wrxNext_Image $image
     * @return bool
     * @throws wrxNext_Image_Effect_Exception
     */
    public function applyToImage(wrxNext_Image $image)
    {
        if (!empty($this->_params['image'])) {
            $x = 0;
            $y = 0;
            $waterMark = new wrxNext_Image($this->_params['image'], $this->_params['useAlpha']);
            $sumWatermarkHeight = $waterMark->getHeight() + $this->_params['margin-top'] + $this->_params['margin-bottom'];
            $sumWatermarkWidth = $waterMark->getWidth() + $this->_params['margin-left'] + $this->_params['margin-right'];
            if (!($image->getWidth() < $sumWatermarkWidth or $image->getHeight() < $sumWatermarkHeight)) {
                switch ($this->_params['location']) {
                    case self::POS_TOP_LEFT :
                        $x = $this->_params['margin-left'];
                        $y = $this->_params['margin-top'];
                        break;
                    case self::POS_TOP_RIGHT :
                        $x = $image->getWidth() - $this->_params['margin-right'] - $waterMark->getWidth();
                        $y = $this->_params['margin-top'];
                        break;
                    case self::POS_BOTTOM_LEFT :
                        $x = $this->_params['margin-left'];
                        $y = $image->getHeight() - $this->_params['margin-bottom'] - $waterMark->getHeight();
                        break;
                    case self::POS_BOTTOM_RIGHT :
                        $x = $image->getWidth() - $this->_params['margin-right'] - $waterMark->getWidth();
                        $y = $image->getHeight() - $this->_params['margin-bottom'] - $waterMark->getHeight();
                        break;
                }
                imagecopyresampled($image->_imageGD, $waterMark->_imageGD, $x, $y, 0, 0, $waterMark->getWidth(), $waterMark->getHeight(), $waterMark->getWidth(), $waterMark->getHeight());
                unset($waterMark);
                return true;
            } else {
                throw new wrxNext_Image_Effect_Exception('dimensions of watermark (' .
                    $waterMark->getWidth() . 'x' . $waterMark->getHeight() .
                    ') extends dimensions of image');
            }
        } else {
            throw new wrxNext_Image_Effect_Exception('Missing watermark image path');
        }
        return false;
    }

    /**
     * zwraca nazwe efektu
     *
     * @return string
     */
    public function effectName()
    {
        return "watermark";
    }
}
