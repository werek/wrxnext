<?php

class wrxNext_Image_Effect_Sketchy implements wrxNext_Image_Effect_Interface
{
    public function __construct($params = null)
    {
        //not used
    }

    public function setParam($paramName, $paramValue)
    {
        //not used
    }

    public function applyToImage(wrxNext_Image $image)
    {
        if (!imagefilter($image->_imageGD, IMG_FILTER_MEAN_REMOVAL)) {
            throw new wrxNext_Image_Effect_Exception('Could not apply sketchy effect');
        }
    }

    public function effectName()
    {
        return "sketchy";
    }
}
