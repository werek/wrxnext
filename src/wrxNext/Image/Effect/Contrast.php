<?php

class wrxNext_Image_Effect_Contrast implements wrxNext_Image_Effect_Interface
{
    private $_params = array('contrast' => null);

    public function __construct($params = null)
    {
        if (is_array($params)) {
            $this->_params = array_merge($this->_params, $params);
        }
    }

    public function setParam($paramName, $paramValue)
    {
        if (array_key_exists($paramName, $this->_params)) {
            $this->_params[$paramName] = $paramValue;
        }
    }

    public function applyToImage(wrxNext_Image $image)
    {
        if (is_null($this->_params['contrast'])) {
            throw new wrxNext_Image_Effect_Exception('there is no value for applying contrast effect');
        }
        if (!imagefilter($image->_imageGD, IMG_FILTER_CONTRAST, $this->_params['contrast'])) {
            throw new wrxNext_Image_Effect_Exception('Could not apply contrast effect');
        }
    }

    public function effectName()
    {
        return "contrast";
    }
}
