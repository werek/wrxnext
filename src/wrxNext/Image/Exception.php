<?php

/**
 * exception for Image related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Image_Exception extends wrxNext_Exception
{
}
