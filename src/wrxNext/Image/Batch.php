<?php

class wrxNext_Image_Batch
{

    /**
     * holds image paths array
     *
     * @var array
     */
    protected $_images = array();

    /**
     * holds requested sizes output
     *
     * @var array
     */
    protected $_sizes = array();

    /**
     * holds effect stack
     *
     * @var wrxNext_Image_Effect
     */
    protected $_effect = null;

    /**
     * holds temporary directory
     *
     * @var string
     */
    protected $_tmpDirectory = null;

    /**
     * holds batch export directory
     *
     * @var string
     */
    protected $_directory = '';

    /**
     * holds log instance
     *
     * @var Zend_Log
     */
    protected $_log = null;

    /**
     * holds wheter to throw exceptions
     *
     * @var boolean
     */
    protected $_throwExceptions = true;

    /**
     * constructor, optionally can recieve options
     *
     * @param array|Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @return wrxNext_Image_Batch
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }

        $options = (array)$options;

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * runs batch
     *
     * @return wrxNext_Image_Batch_Result
     */
    public function runBatch()
    {
        $effects = $this->getEffect();
        $result = new wrxNext_Image_Batch_Result();
        $this->_log(
            '--------------------------------------------------------------------------------');
        $this->_log(
            '----------------------starting batch image processing---------------------------');
        foreach ($this->getImages() as $imagePath) {
            try {
                $this->_log('creating image object from path: ' . $imagePath);
                // initiating image object
                $image = new wrxNext_Image($imagePath, true);

                // setting effect stack and applying effects
                if ($effects instanceof wrxNext_Image_Effect) {
                    $this->_log('applying effects');
                    $image->setEffectHandler($effects);
                    $image->applyEffects();
                }

                $originalType = $image->getType();
                $originalFilename = $image->getFileName();

                $tmpPath = tempnam($this->getTmpDirectory(), 'batch_image_');

                $this->_log('saving temporary image to path: ' . $tmpPath,
                    Zend_Log::DEBUG);
                $image->save(wrxNext_Image::PNG, $tmpPath);
                unset($image);

                // exporting sizes
                $this->_log('processing export sizes');
                foreach ($this->getSizes() as $size) {
                    $this->_log('opening temporary file');
                    $image = new wrxNext_Image($tmpPath);
                    $this->_log('resizing file');
                    $this->_log('width: ' . $size->getWidth(), Zend_Log::DEBUG);
                    $this->_log('height: ' . $size->getHeight(),
                        Zend_Log::DEBUG);
                    $this->_log(
                        'keep aspect ratio: ' .
                        (($size->getKeepAspectRatio()) ? 'true' : 'false'),
                        Zend_Log::DEBUG);
                    $this->_log(
                        'crop: ' . (($size->getCrop()) ? 'true' : 'false'),
                        Zend_Log::DEBUG);
                    $image->resizeBatch($size);
                    if ($size->getDirectoryFullPath()) {
                        $saveDir = $size->getDirectory();
                    } else {
                        $saveDir = $this->getDirectory() . DIRECTORY_SEPARATOR .
                            $size->getDirectory();
                    }
                    $saveDir .= DIRECTORY_SEPARATOR . $originalFilename;
                    $this->_log('saving resized file to: ' . $saveDir);
                    $image->save($originalType, $saveDir);
                    unset($image);
                }
                $this->_log('deleting temporary file');
                unlink($tmpPath);
            } catch (Exception $e) {
                if ($this->getThrowExceptions()) {
                    throw $e;
                } else {
                    $result->addError($e);
                }
            }
        }
        $this->_log(
            '----------------------batch image processing ended------------------------------');
        return $result;
    }

    /**
     * gets effects stack
     *
     * @return wrxNext_Image_Effect
     */
    public function getEffect()
    {
        return $this->_effect;
    }

    /**
     * sets effects stack
     *
     * @param wrxNext_Image_Effect $effect
     * @return wrxNext_Image_Batch
     */
    public function setEffect(wrxNext_Image_Effect $effect)
    {
        $this->_effect = $effect;
        return $this;
    }

    /**
     * logs message
     *
     * @param string $message
     * @param int $level
     * @return wrxNext_Image_Batch
     */
    protected function _log($message, $level = null)
    {
        $log = $this->getLog();
        if ($log instanceof Zend_Log) {
            $level = is_null($level) ? Zend_Log::INFO : $level;
            $log->log($message, $level);
        }
        return $this;
    }

    /**
     * gets log
     *
     * @return Zend_Log
     */
    public function getLog()
    {
        return $this->_log;
    }

    /**
     * sets log
     *
     * @param Zend_Log $log
     * @return wrxNext_Image_Batch
     */
    public function setLog(Zend_Log $log)
    {
        $this->_log = $log;
        return $this;
    }

    /**
     * gets images list
     *
     * @return array
     */
    public function getImages()
    {
        return $this->_images;
    }

    /**
     * sets images list
     *
     * @param array $images
     * @return wrxNext_Image_Batch
     */
    public function setImages($images)
    {
        $this->_images = array();
        foreach ($images as $image) {
            $this->addImage($image);
        }
        return $this;
    }

    /**
     * adds image path to stack
     *
     * @param string $path
     * @return wrxNext_Image_Batch
     */
    public function addImage($path)
    {
        $this->_images[] = $path;
        return $this;
    }

    /**
     * gets temporary directory, used for image resize
     *
     * @return string
     */
    public function getTmpDirectory()
    {
        return is_null($this->_tmpDirectory) ? sys_get_temp_dir() : $this->_tmpDirectory;
    }

    /**
     * sets temporary directory, used for image resize
     *
     * @param string $directory
     * @return wrxNext_Image_Batch
     */
    public function setTmpDirectory($directory)
    {
        $this->_tmpDirectory = $directory;
        return $this;
    }

    /**
     * gets output sizes
     *
     * @return array
     */
    public function getSizes()
    {
        return $this->_sizes;
    }

    /**
     * sets output sizes
     *
     * @param array $sizes
     * @return wrxNext_Image_Batch
     */
    public function setSizes($sizes)
    {
        $this->_sizes = array();
        foreach ($sizes as $size) {
            $this->addSize($size);
        }
        return $this;
    }

    /**
     * adds new export size
     *
     * @param array $sizeOptions
     * @return wrxNext_Image_Batch
     */
    public function addSize($sizeOptions)
    {
        $this->_sizes[] = new wrxNext_Image_Batch_Size($sizeOptions);
        return $this;
    }

    /**
     * gets batch export directory
     *
     * @return string
     */
    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * sets batch export directory
     *
     * @param string $directory
     * @return wrxNext_Image_Batch
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;
        return $this;
    }

    /**
     * gets wheter to throw exceptions
     *
     * @return boolean
     */
    public function getThrowExceptions()
    {
        return $this->_throwExceptions;
    }

    /**
     * sets wheter to throw exceptions
     *
     * @param boolean $throw
     * @return wrxNext_Image_Batch
     */
    public function setThrowExceptions($throw)
    {
        $this->_throwExceptions = $throw;
        return $this;
    }
}
