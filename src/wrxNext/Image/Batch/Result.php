<?php

class wrxNext_Image_Batch_Result
{

    /**
     * holds flag if result of processing is valid
     *
     * @var boolean
     */
    protected $_valid = true;

    /**
     * holds error stack
     *
     * @var array
     */
    protected $_errors = array();

    /**
     * adds error to stack
     *
     * @param mixed $error
     * @return wrxNext_Image_Batch_Result
     */
    public function addError($error)
    {
        $this->_errors[] = $error;
        $this->_valid = false;
        return $this;
    }

    /**
     * returns result success
     *
     * @return boolean
     */
    public function isValid()
    {
        return $this->_valid;
    }
}
