<?php

class wrxNext_Image_Batch_Size
{

    /**
     * holds destination width
     *
     * @var int
     */
    protected $_width = null;

    /**
     * holds destination height
     *
     * @var int
     */
    protected $_height = null;

    /**
     * holds wheter to keep aspect ratio
     *
     * @var boolean
     */
    protected $_aspectRatio = true;

    /**
     * holds wheter to crop image
     *
     * @var boolean
     */
    protected $_crop = true;

    /**
     * holds directory for given size
     *
     * @var string
     */
    protected $_directory = '';

    /**
     * holds flag if currently set directory is fullpath or subdirectory
     *
     * @var boolean
     */
    protected $_directoryFull = false;

    /**
     * constructor, optionally can recieve options
     *
     * @param array|Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @return wrxNext_Image_Batch_Size
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }

        $options = (array)$options;

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * gets destination width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * sets destination width
     *
     * @param int $width
     * @return wrxNext_Image_Batch_Size
     */
    public function setWidth($width)
    {
        $this->_width = $width;
        return $this;
    }

    /**
     * gets destination height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * sets destination height
     *
     * @param int $height
     * @return wrxNext_Image_Batch_Size
     */
    public function setHeight($height)
    {
        $this->_height = $height;
        return $this;
    }

    /**
     * sets wheter to keep aspekt ration
     *
     * @param boolean $keepAspectRatio
     * @return wrxNext_Image_Batch_Size
     */
    public function setKeepAspectRatio($keepAspectRatio)
    {
        $this->_aspectRatio = $keepAspectRatio;
        return $this;
    }

    /**
     * gets wheter to keep aspekt ration
     *
     * @return boolean
     */
    public function getKeepAspectRatio()
    {
        return $this->_aspectRatio;
    }

    /**
     * gets wheter to crop image
     *
     * @return boolean
     */
    public function getCrop()
    {
        return $this->_crop;
    }

    /**
     * sets wheter to crop image
     *
     * @param boolean $crop
     * @return wrxNext_Image_Batch_Size
     */
    public function setCrop($crop)
    {
        $this->_crop = $crop;
        return $this;
    }

    /**
     * gets directory for processed images
     *
     * @return string
     */
    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * sets directory for processed images
     *
     * @param string $directory
     * @return wrxNext_Image_Batch_Size
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;
        return $this;
    }

    /**
     * sets if directory is full path
     *
     * @param boolean $fullPath
     * @return wrxNext_Image_Batch_Size
     */
    public function setDirectoryFullPath($fullPath)
    {
        $this->_directoryFull = $fullPath;
        return $this;
    }

    /**
     * gets if directory is full path
     *
     * @return boolean
     */
    public function getDirectoryFullPath()
    {
        return $this->_directoryFull;
    }
}
