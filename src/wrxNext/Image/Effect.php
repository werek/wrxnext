<?php

/**
 * stack object for Image Effects
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Image_Effect
{
    /**
     * przechowuje liste Efektow
     *
     * @var Array
     */
    private $_effects = array();

    /**
     * dodaje efekt do stosu
     *
     * @param wrxNext_Image_Effect_Interface $effect
     * @param null|string $effectName optional effect name
     * @return wrxNext_Image_Effect
     */
    public function addEffect(wrxNext_Image_Effect_Interface $effect, $effectName = null)
    {
        if (is_null($effectName)) {
            $effectName = $effect->effectName();
        }
        $this->_effects[$effectName] = $effect;
        return $this;
    }

    /**
     * zwraca efekt ze stosu, nie podanie nazwy zwraca liste efektow w postaci tablicy
     *
     * @param string $effectName
     * @return wrxNext_Image_Effect_Interface
     */
    public function getEffect($effectName = null)
    {
        if (is_null($effectName)) {
            return array_keys($this->_effects);
        }
        return $this->_effects[$effectName];
    }

    /**
     * zastosowuje efekty do obrazu i zwraca go
     *
     * @param wrxNext_Image $image
     * @return wrxNext_Image
     * @throws wrxNext_Image_Exception
     */
    public function applyToImage(wrxNext_Image $image)
    {
        $errors = array();
        foreach ($this->_effects as $k => $effect) {
            try {
                $effect->applyToImage($image);
            } catch (wrxNext_Image_Effect_Exception $e) {
                $errors[] = $k . '(type: ' . $effect->effectName() . '):' . $e->getMessage();
            }
        }

        if (count($errors) > 0) {
            throw new wrxNext_Image_Exception('During application of effects an error occured:' . "\n\n" . implode("\n", $errors));
        }

        return $image;
    }

    /**
     * usuwa efekt ze stosu
     *
     * @param string $effectName
     */
    public function removeEffect($effectName)
    {
        if (array_key_exists($effectName)) {
            unset($this->_effects[$effectName]);
        }
    }
}
