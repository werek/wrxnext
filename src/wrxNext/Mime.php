<?php

/**
 * klasa przechwouje typy MIME plikow
 *
 */
class wrxNext_Mime extends Zend_Mime
{

    const JPEG2000 = 'image/jpeg200';

    const GIF = 'image/gif';

    const PNG = 'image/png';

    const JPEG = 'image/jpg';

    const BMP = 'image/x-windows-bmp';

    const PDF = 'application/pdf';

    const XLS = 'application/vnd.ms-excel';

    const DOC = 'application/vnd.ms-word';

    const TYPE_XML = 'application/xml';

    const TYPE_JSON = 'application/x-json';
}

?>
