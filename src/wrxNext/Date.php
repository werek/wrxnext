<?php

class wrxNext_Date
{

    const PHP_DATEFUNC_FORMAT_DATETIME = 'Y-m-d H:i:s';

    const PHP_DATEFUNC_FORMAT_DATE = 'Y-m-d';

    const PHP_DATEFUNC_FORMAT_TIME = 'H:i:s';

    const ISO_FORMAT_DATE = 'yyyy-MM-dd';

    const ISO_FORMAT_TIME = 'HH:mm:ss';

    const ISO_FORMAT_DATETIME = 'yyyy-MM-dd HH:mm:ss';

    const ISO_FORMAT_DATETIME_HUMANREADABLE = 'HH:mm, dd MMMM yyyy';

    /**
     * przeparsowuje date (YYYY-MM-DD lub YYYY-MM-DD hh:mm:ss) na nowy format (w
     * formie php date),
     * opcjonalnie moze przyjac tablice podstawien wartosci dla konkretnego
     * elementu
     *
     * @param string $isoDate
     * @param string $newDateFormat
     * @param array $substituteValues
     * @return string
     */
    public static function reparse($isoDate, $newDateFormat,
                                   $substituteValues = array())
    {
        $string = '';
        if (is_array($substituteValues) && count($substituteValues) > 0) {
            $kStart = 0;
            $klucze = array();
            foreach ($substituteValues as $k => $v) {
                $klucze[$k] = $kStart;
                $newDateFormat = str_replace($k, '%&' . $klucze[$k] . '&%',
                    $newDateFormat);
                $kStart++;
            }
        }
        $isoDate = trim($isoDate);
        $timestampDate = strtotime($isoDate);
        $string = date($newDateFormat, $timestampDate);

        if (is_array($substituteValues) && count($substituteValues) > 0) {
            foreach ($klucze as $k => $v) {
                $string = str_replace('%&' . $v . '&%',
                    $substituteValues[$k][date($k, $timestampDate)], $string);
            }
        }

        return $string;
    }
}
