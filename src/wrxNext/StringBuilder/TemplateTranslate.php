<?php

class wrxNext_StringBuilder_TemplateTranslate extends wrxNext_StringBuilder_Template
{
    use wrxNext_Trait_RegistryTranslate;

    /**
     * builds string from template using data
     *
     * @param array $data
     * @return string
     */
    public function render(array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getData();
        }
        return $this->_buildTemplate($this->_translate($this->getTemplate()),
            $data);
    }
}
