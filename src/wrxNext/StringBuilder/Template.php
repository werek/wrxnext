<?php

class wrxNext_StringBuilder_Template implements wrxNext_StringBuilder_Interface
{
    use wrxNext_Trait_Template;

    /**
     * holds template
     *
     * @var string
     */
    protected $_template = '';

    /**
     * holds data
     *
     * @var array
     */
    protected $_data = array();

    /**
     * takes template string and array data
     *
     * @param string $template
     * @param array $data
     */
    public function __construct($template, array $data = array())
    {
        $this->setTemplate($template);
        $this->setData($data);
    }

    /**
     * returns rendered template
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * builds string from template using data
     *
     * @param array $data
     * @return string
     */
    public function render(array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getData();
        }
        return $this->_buildTemplate($this->getTemplate(), $data);
    }

    /**
     * gets data
     *
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data
     *
     * @param array $data
     * @return wrxNext_StringBuilder_Template
     */
    public function setData(array $data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * gets template
     *
     * @return string
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     * sets template
     *
     * @param string $template
     * @return wrxNext_StringBuilder_Template
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
        return $this;
    }
}
