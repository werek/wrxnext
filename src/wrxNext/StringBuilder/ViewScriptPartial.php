<?php

class wrxNext_StringBuilder_ViewScriptPartial implements wrxNext_StringBuilder_Interface
{

    /**
     * holds view object
     *
     * @var Zend_View_Interface
     */
    protected $_view = null;

    /**
     * holds ZF1 module name
     *
     * @var string
     */
    protected $_module = 'default';

    /**
     * holds script path
     *
     * @var string
     */
    protected $_script = '';

    /**
     * holds data to use in script rendering
     *
     * @var array
     */
    protected $_data = array();

    /**
     * takes script path and data used in rendering, optionally module in which
     * script is located
     *
     * @param string $scriptPath
     * @param array $data
     * @param string $module
     */
    public function __construct($scriptPath, array $data = array(),
                                $module = 'default')
    {
        $this->setScript($scriptPath);
        $this->setData($data);
        $this->setModule($module);
    }

    /**
     * magic method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * @inheritdoc
     */
    public function render(array $data = null)
    {
        if (is_null($data)) {
            $data = $this->getData();
        }
        return $this->getView()->partial($this->getScript(), $this->getModule(),
            $data);
    }

    /**
     * gets data used in rendering
     *
     * @return array
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * sets data used in rendering
     *
     * @param array $data
     * @return wrxNext_StringBuilder_ViewScriptPartial
     */
    public function setData(array $data)
    {
        $this->_data = $data;
        return $this;
    }

    /**
     * gets View object
     *
     * @return Zend_View_Interface
     */
    public function getView()
    {
        if (!$this->_view instanceof Zend_View_Interface) {
            $this->_view = Zend_Controller_Front::getInstance()->getParam(
                'bootstrap')->getResource('view');
        }
        return $this->_view;
    }

    /**
     * sets View object
     *
     * @param Zend_View_Interface $view
     * @return wrxNext_StringBuilder_ViewScriptPartial
     */
    public function setView($view)
    {
        $this->_view = $view;
        return $this;
    }

    /**
     * gets template script path
     *
     * @return string
     */
    public function getScript()
    {
        return $this->_script;
    }

    /**
     * sets template script path
     *
     * @param string $scriptPath
     * @return wrxNext_StringBuilder_ViewScriptPartial
     */
    public function setScript($scriptPath)
    {
        $this->_script = $scriptPath;
        return $this;
    }

    /**
     * gets ZF1 module name
     *
     * @return string
     */
    public function getModule()
    {
        return $this->_module;
    }

    /**
     * sets ZF1 module name
     *
     * @param string $name
     * @return wrxNext_StringBuilder_ViewScriptPartial
     */
    public function setModule($name)
    {
        $this->_module = $name;
        return $this;
    }
}
