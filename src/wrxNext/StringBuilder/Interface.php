<?php

interface wrxNext_StringBuilder_Interface
{

    /**
     * builds string from given data
     *
     * @param array $data
     */
    public function render(array $data = null);
}
