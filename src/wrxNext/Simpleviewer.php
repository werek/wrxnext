<?php

/**
 * klasa umozliwiajaca tworzenie konfiguracji dla przegladarki obrazow simpleviewer
 *
 * @todo implementacja mozliwosci generowania kodu do obslugi wyswietlania przegladarki
 */
class wrxNext_Simpleviewer
{

    /**
     * przechowje opcje do galerii
     *
     * @var array
     */
    protected $_options = array(
        "maxImageWidth" => "690",
        "maxImageHeight" => "400",
        "textColor" => "FFFFFF",
        "frameColor" => "FFFFFF",
        "thumbColumns" => "8",
        "thumbRows" => "1",
        "thumbPosition" => "BOTTOM",
        "title" => "SimpleViewer Gallery",
        "enableRightClickOpen" => "true",
        "backgroundImagePath" => "",
        "imagePath" => "",
        "thumbPath" => "",
        "galleryStyle" => "COMPACT",
        "showOpenButton" => "TRUE",
        "showFullscreenButton" => "TRUE"
    );

    /**
     * przechowje xml'a startowego dla viewer'a
     *
     * @var string
     */
    private $_simpleviewerXmlNodeSource = '<?xml version="1.0" encoding="UTF-8"?>
<simpleviewergallery>

</simpleviewergallery>';

    /**
     * przechwuje liste obrazow
     *
     * @var array
     */
    private $_images = array();

    /**
     * przyjmje jako parametr opcje dla simpleviewer'a
     *
     * @param array|Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    public function __get($option)
    {
        $this->getOption($option);
    }

    public function __set($option, $value)
    {
        $this->setOption($option, $value);
    }

    /**
     * ustawia opcje dla simpleviewer'a
     *
     * @param string $option
     * @param string $value
     */
    public function setOption($option, $value)
    {
        if (array_key_exists($option, $this->_options)) {
            $this->_options[$option] = $value;
        }
    }

    /**
     * zwraca opcje dla simpleviewer'a
     *
     * @param string $option
     * @return string
     */
    public function getOption($option)
    {
        return $this->_options[$option];
    }

    /**
     * zwraca opcje simpleviewer
     *
     * @return array
     */
    public function getOptions()
    {
        return $this->_options;
    }

    /**
     * ustawia zbiorczo opcje dla simpleviewer
     *
     * @param array|Zend_Config $options
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            foreach ($options->toArray() as $k => $v) {
                $this->setOption($k, $v);
            }
        } elseif (is_array($options)) {
            foreach ($options as $k => $v) {
                $this->setOption($k, $v);
            }
        }
    }

    /**
     * dodaje zdjecie
     *
     * @param string $name
     * @param string $caption
     */
    public function addImage($name, $caption = "")
    {
        $this->_images[$name] = $caption;
    }

    /**
     * usuwa zdjecie po nazwie pliku
     *
     * @param string $name
     */
    public function removeImage($name)
    {
        unset($this->_images[$name]);
    }

    /**
     * zwraca zawartosc klasy do tablicy
     *
     * @return array
     */
    public function toArray()
    {
        $t = $this->_options;
        $t['images'] = $this->_images;
        return $t;
    }

    /**
     * magiczna funkcja wywolywana po echo
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toXml();
    }

    /**
     * zwraca zawartosc jako XML
     *
     * @return string
     */
    public function toXml()
    {
        return $this->toSimpleXml()->asXML();
    }

    /**
     * zwraca do postaci SimpleXMLElement
     *
     * @return SimpleXMLElement
     */
    public function toSimpleXml()
    {
        $simplexml = simplexml_load_string($this->_simpleviewerXmlNodeSource);
        foreach ($this->_options as $k => $v) {
            $simplexml[$k] = $v;
        }
        foreach ($this->_images as $name => $caption) {
            $node = $simplexml->addChild('image');
            $node['imageURL'] = $this->_options['imagePath'] . $name;
            $node['linkURL'] = $this->_options['imagePath'] . $name;
            $node['thumbURL'] = $this->_options['thumbPath'] . $name;
            // $node->addChild('filename');
            $capt = $node->addChild('caption');
            $capt = dom_import_simplexml($capt);
            $no = $capt->ownerDocument;
            $capt->appendChild($no->createCDATASection($caption));
        }
        return $simplexml;
    }
}
