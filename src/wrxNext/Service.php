<?php
use Pimple\Container;

/**
 * based on PimpleContainer 3.0
 *
 * @author bweres01
 *
 */
class wrxNext_Service extends Container
{

    /**
     * holds callmethod service key mapping
     *
     * @var array
     */
    protected $_serviceKeyMapping = array();

    /**
     * holds filter instance
     *
     * @var Zend_Filter_Word_CamelCaseToUnderscore
     */
    protected $_filter = NULL;

    /**
     * construct
     *
     * @param array $values
     */
    public function __construct(array $values = array())
    {
        parent::__construct($values);
        $this->init();
    }

    /**
     * post construct method
     */
    public function init()
    {
    }

    /**
     * handles all get{name} invocations
     *
     * @param string $name
     * @param array $arguments
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        if (!isset($this->_serviceKeyMapping[$name])) {
            $method = preg_replace('#^get#', '', $name);
            $service = strtolower($this->_filter()->filter($method));
            $this->_serviceKeyMapping[$name] = $service;
        } else {
            $service = $this->_serviceKeyMapping[$name];
        }
        return $this->offsetGet($service);
    }

    /**
     * returns filter
     *
     * @return Zend_Filter_Word_CamelCaseToUnderscore
     */
    protected function _filter()
    {
        if (!$this->_filter instanceof Zend_Filter_Word_CamelCaseToUnderscore) {
            $this->_filter = new Zend_Filter_Word_CamelCaseToUnderscore();
        }
        return $this->_filter;
    }
}
