<?php

class wrxNext_Mail_Transport_ConditionallLog extends Zend_Mail_Transport_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * flag to allow sending email
     *
     * @var boolean
     */
    public static $allowSend = true;
    /**
     * flag to log message
     *
     * @var boolean
     */
    public static $logMessage = true;
    /**
     * previous transport
     *
     * @var Zend_Mail_Transport_Abstract
     */
    protected $_mailTransport = null;

    /**
     * decorator for another mail transport agent
     *
     * if none provided will use sendmail
     *
     * @param Zend_Mail_Transport_Abstract $transport
     */
    public function __construct(Zend_Mail_Transport_Abstract $transport = null)
    {
        if (is_null($transport)) {
            $transport = new Zend_Mail_Transport_Sendmail();
        }
        $this->_mailTransport = $transport;
    }

    /**
     * handles any method calls to original transport agent
     *
     * @param string $name
     * @param array $args
     * @return mixed
     */
    public function __call($name, $args)
    {
        return call_user_func_array(
            array(
                $this->_mailTransport,
                $name
            ), $args);
    }

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Mail_Transport_Abstract::_sendMail()
     */
    protected function _sendMail()
    {
        if (self::$logMessage) {
            $this->_log('headers:');
            $this->_log($this->header);
            $this->_log('body:');
            $this->_log($this->body);
        }
        if (self::$allowSend) {
            $this->_mailTransport->send($this->_mail);
        }
    }
}
