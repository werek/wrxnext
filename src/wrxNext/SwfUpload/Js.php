<?php

/**
 * wraper dla kodu javascript
 */
class wrxNext_SwfUpload_Js implements wrxNext_SwfUpload_Js_Interface
{

    /**
     * przechwouje kod Javascript
     *
     * @var string
     */
    private $_code;

    /**
     * konstruktor
     *
     * @param string $code
     */
    public function __construct($code)
    {
        $this->setCode($code);
    }

    /**
     * automatycznie rozpoznaje typ zmiennej i wybiera nabardziej odpowiedni
     * filtr
     * po czym zwraca kod
     *
     * @param mixed $val
     * @return string
     */
    public static function parse($val)
    {
        if ($val instanceof Zend_Config) {
            $obj = new wrxNext_SwfUpload_Js_Array($val->toArray());
            return $obj->getCode();
        } elseif ($val instanceof wrxNext_SwfUpload_Js_Interface) {
            return $val->getCode();
        } elseif (is_int($val)) {
            $obj = new self($val);
            return $obj->getCode();
        } elseif (is_bool($val)) {
            $obj = new self($val ? "true" : "false");
            return $obj->getCode();
        } elseif (is_array($val)) {
            $obj = new wrxNext_SwfUpload_Js_Array($val);
            return $obj->getCode();
        } else {
            $obj = new wrxNext_SwfUpload_Js_String($val);
            return $obj->getCode();
        }
    }

    /**
     * funkcja tworząindentacje dla juz gotowego koduy
     *
     * @param string $code
     * @param string $indent
     * @return string
     */
    public static function indentCode($code, $indent = "    ")
    {
        return str_replace("\n", "\n" . $indent, $code);
    }

    /**
     * magiczna metoda
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * zwraca kod Javascript Obiektu
     *
     * @return string
     */
    public function getCode()
    {
        return $this->_code;
    }

    /**
     * ustala kod Javascript obiektu
     *
     * @param string $code
     */
    public function setCode($code)
    {
        if (!is_object($code)) {
            $this->_code = $code;
        } else {
            throw new Exception("Given Parameter is object");
        }
    }
}
