<?php

/**
 * klasa umozliwiajaca zamiane tablicy php na odpowiednik w Js
 */
class wrxNext_SwfUpload_Js_Array implements Iterator, Countable, ArrayAccess,
    wrxNext_SwfUpload_Js_Interface
{

    protected $_index = 0;

    /**
     * status zamiany na stringi kluczy
     *
     * @var bool
     */
    private $_encodeKeys = true;

    /**
     * przechwouje wartosc tablicy
     *
     * @var array
     */
    private $_value;

    /**
     * klasa obudowujaca do wyswietlania tablic
     *
     * @param array $value
     */
    public function __construct($value = array())
    {
        $this->setValue($value);
    }

    public function getEncodeKeys()
    {
        return $this->_encodeKeys;
    }

    public function setEncodeKeys($value = true)
    {
        $this->_encodeKeys = $value;
    }

    /**
     * zwraca wartosc tablicy
     *
     * @return array
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * ustawianie wartosci tablicy
     *
     * @param array $value
     */
    public function setValue($value)
    {
        if (is_array($value)) {
            $this->_value = $value;
        } else {
            throw new wrxNext_SwfUpload_Exception("value should be an Array()");
        }
    }

    /**
     * magiczna metoda mapujaca wlasnosci na wewnetrzne wartosci
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->_value[$name];
    }

    /**
     * magiczna metoda mapujaca wlasnosci na wewnetrzne wartosci
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->_value[$name] = $value;
    }

    /**
     * magiczna metoda do echowania
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * zwraca przeparsowany kod dla zawartosci tablicy
     *
     * @return string
     */
    public function getCode()
    {
        $code = "{ \n";
        $indentSpaces = "    ";
        foreach ($this->_value as $k => $v) {

            $_k = $k;

            if ($this->_encodeKeys) {
                $key = new wrxNext_SwfUpload_Js_String($k);
                $_k = $key->getCode();
            }

            $code .= $indentSpaces . $_k . " : ";

            if (is_array($v)) {
                $val = new wrxNext_SwfUpload_Js_Array($v);
                $code .= "\n" . $indentSpaces .
                    wrxNext_SwfUpload_Js::indentCode($val->getCode(),
                        $indentSpaces . $indentSpaces);
            } else {
                $code .= wrxNext_SwfUpload_Js::parse($v);
            }

            $code .= ",\n";
        }
        $code .= "}";
        return $code;
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return mixed
     */
    public function offsetGet($name)
    {
        return $this->_value[$name];
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @param mixed $value
     */
    public function offsetSet($name, $value)
    {
        $this->_value[$name] = $value;
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return bool
     */
    public function offsetExists($name)
    {
        return isset($this->_value[$name]);
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     */
    public function offsetUnset($name)
    {
        unset($this->_value[$name]);
    }

    public function __isset($name)
    {
        return isset($this->_value[$name]);
    }

    public function __unset($name)
    {
        unset($this->_value[$name]);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function current()
    {
        return current($this->_value);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function key()
    {
        return key($this->_value);
    }

    /**
     * Defined by Iterator interface
     */
    public function next()
    {
        next($this->_value);
        $this->_index++;
    }

    /**
     * Defined by Iterator interface
     */
    public function rewind()
    {
        reset($this->_value);
        $this->_index = 0;
    }

    /**
     * Defined by Iterator interface
     *
     * @return boolean
     */
    public function valid()
    {
        return $this->_index < $this->count();
    }

    /**
     * Defined by Countable interface
     *
     * @return int
     */
    public function count()
    {
        return count($this->_value);
    }

    /**
     * funkcja tworząindentacje dla juz gotowego koduy
     *
     * @param string $code
     * @param string $indent
     * @return string
     */
    private function indentCode($code, $indent = "    ")
    {
        return str_replace("\n", "\n" . $indent, $code);
    }
}
