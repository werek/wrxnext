<?php

/**
 * klasa zapeniajaca obsluge wartosci jako string dla Javascript
 */
class wrxNext_SwfUpload_Js_String implements wrxNext_SwfUpload_Js_Interface
{

    /**
     * definiuje czy zamieniac nowe linie na kod deklaratywny
     *
     * @var bool
     */
    public static $escapeNewLines = true;

    /**
     * przechwuje wartosc
     *
     * @var mixed
     */
    private $_value;

    /**
     * konstruktor przyjmuje za parametr string badz obiekt ktory mozna
     * wyechowac
     *
     * @param string|object $value
     */
    public function __construct($value)
    {
        $this->setValue($value);
    }

    /**
     * zwraca aktualna wartosc obiektu
     *
     * @return string object
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * ustala wartosc dla obiektu
     *
     * @param string|object $value
     */
    public function setValue($value)
    {
        if (is_object($value)) {
            if (in_array("__toString", get_class_methods($value))) {
                $this->_value = $value;
            } else {
                throw new wrxNext_SwfUpload_Exception(
                    "given object cannot be echo'ed to string, missing __toString() method");
            }
        } else {
            $this->_value = $value;
        }
    }

    /**
     * magiczna funkcja do echowania
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getCode();
    }

    /**
     * zwraca kod w typie stringa gotowy do wstawienia w blok Javascript
     *
     * @return string
     */
    public function getCode()
    {
        $str = $this->_value;
        if (is_object($str)) {
            ob_start();
            echo $str;
            $str = ob_get_clean();
        }
        $str = str_replace('"', '\"', $str);
        if (self::$escapeNewLines) {
            str_replace("\n", '\n', $str);
        }
        return '"' . $str . '"';
    }
}
