<?php

/**
 * interfejs dla elementow ktore maja byc zamieniane na Javascript
 *
 */
interface wrxNext_SwfUpload_Js_Interface
{

    /**
     * zwraca kod Javascript
     *
     * @return string
     */
    public function getCode();
}
