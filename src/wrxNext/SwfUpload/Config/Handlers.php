<?php

class wrxNext_SwfUpload_Config_Handlers extends wrxNext_SwfUpload_Config_Abstract
{

    /**
     * przechwuje flage decydujaca o uzyciu domyslnych handlerow
     *
     * @var unknown_type
     */
    protected $_useDefaultHandlers = true;

    protected $_defaultHandlers = array(
        'file_dialog_complete_handler' => 'fileDialogComplete',
        'upload_complete_handler' => 'uploadComplete',
        'upload_error_handler' => 'uploadError',
        'upload_progress_handler' => 'uploadProgress'
    );

    /**
     * zwraca config
     *
     * @return Zend_Config
     */
    public function getConfig()
    {
        if ($this->_useDefaultHandlers) {
            $prepend = array();
            foreach ($this->_defaultHandlers as $handler => $function) {
                if (!isset($this->_config->{$handler})) {
                    $prepend[] = file_get_contents(
                        dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Handlers' .
                        DIRECTORY_SEPARATOR . $handler . '.js');
                    $this->_config->{$handler} = new wrxNext_SwfUpload_Js(
                        $function);
                }
            }
            if (count($prepend) > 0) {
                $this->addPrependCode(implode("\n\n\n", $prepend));
            }
        }
        return $this->_config;
    }

    /**
     * zwraca status domyslnych handlerow
     *
     * @return mixed
     */
    public function getUseDefaultHandlers()
    {
        return $this->_useDefaultHandlers;
    }

    /**
     * ustala status uzycia domyslnych handlerow
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUseDefaultHandlers($value)
    {
        $this->_useDefaultHandlers = $value;
        return $this;
    }

    /**
     * zwraca funkcje dla uchwytu swfupload_loaded_handler
     *
     * @return mixed
     */
    public function getSwfuploadLoadedHandler()
    {
        if (isset($this->_config->swfupload_loaded_handler)) {
            return $this->_config->swfupload_loaded_handler;
        }
    }

    /**
     * ustala funkcje przypisana dla uchwytu swfupload_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setSwfuploadLoadedHandler($value)
    {
        $this->_config->swfupload_loaded_handler = $value;
        return $this;
    }

    /**
     * zwraca ustalona funkcje dla uchwytu file_dialog_start_handler
     *
     * @return mixed
     */
    public function getFileDialogStartHandler()
    {
        if (isset($this->_config->file_dialog_start_handler)) {
            return $this->_config->file_dialog_start_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu file_dialog_start_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setFileDialogStartHandler($value)
    {
        $this->_config->file_dialog_start_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana do uchwytu file_queued_handler
     *
     * @return mixed
     */
    public function getFileQueuedHandler()
    {
        if (isset($this->_config->file_queued_handler)) {
            return $this->_config->file_queued_handler;
        }
    }

    /**
     * ustala funkcje przypisana do uchwytu file_queued_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setFileQueuedHandler($value)
    {
        $this->_config->file_queued_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana do uchwytu file_queue_error_handler
     *
     * @return mixed
     */
    public function getFileQueueErrorHandler()
    {
        if (isset($this->_config->file_queue_error_handler)) {
            return $this->_config->file_queue_error_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu file_queue_error_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setFileQueueErrorHandler($value)
    {
        $this->_config->file_queue_error_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana do uchwytu file_dialog_complete_handler
     *
     * @return mixed
     */
    public function getFileDialogCompleteHandler()
    {
        if (isset($this->_config->file_dialog_complete_handler)) {
            return $this->_config->file_dialog_complete_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu file_dialog_complete_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setFileDialogCompleteHandler($value)
    {
        $this->_config->file_dialog_complete_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi upload_start_handler
     *
     * @return mixed
     */
    public function getUploadStartHandler()
    {
        if (isset($this->_config->upload_start_handler)) {
            return $this->_config->upload_start_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu upload_start_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUploadStartHandler($value)
    {
        $this->_config->upload_start_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi upload_progress_handler
     *
     * @return mixed
     */
    public function getUploadProgressHandler()
    {
        if (isset($this->_config->upload_progress_handler)) {
            return $this->_config->upload_progress_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu upload_progress_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUploadProgressHandler($value)
    {
        $this->_config->upload_progress_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi upload_error_handler
     *
     * @return mixed
     */
    public function getUploadErrorHandler()
    {
        if (isset($this->_config->upload_error_handler)) {
            return $this->_config->upload_error_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu upload_progress_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUploadErrorHandler($value)
    {
        $this->_config->upload_error_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi upload_success_handler
     *
     * @return mixed
     */
    public function getUploadSuccessHandler()
    {
        if (isset($this->_config->upload_success_handler)) {
            return $this->_config->upload_success_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu upload_success_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUploadSuccessHandler($value)
    {
        $this->_config->upload_success_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi upload_complete_handler
     *
     * @return mixed
     */
    public function getUploadCompleteHandler()
    {
        if (isset($this->_config->upload_complete_handler)) {
            return $this->_config->upload_complete_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu upload_complete_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setUploadCompleteHandler($value)
    {
        $this->_config->upload_complete_handler = $value;
        return $this;
    }

    /**
     * zwraca funkcje przypisana uchwytowi debug_handler
     *
     * @return mixed
     */
    public function getDebugHandler()
    {
        if (isset($this->_config->debug_handler)) {
            return $this->_config->debug_handler;
        }
    }

    /**
     * ustala funkcje dla uchwytu debug_handler
     *
     * @param mixed $value
     * @return wrxNext_SwfUpload_Config_Handlers
     */
    public function setDebugHandler($value)
    {
        $this->_config->debug_handler = $value;
        return $this;
    }
}
