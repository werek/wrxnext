<?php

class wrxNext_SwfUpload_Config_FileTypes extends wrxNext_SwfUpload_Config_Abstract
{

    private $_extensions = array();

    private $_description = "";

    private $_uploadLimit = 0;

    private $_queueLimit = 0;

    /**
     * dodaje rozszezenie ktore bedzie wyswietlane
     *
     * @param string $extension
     */
    public function addExtension($extension)
    {
        if (!in_array($extension, $this->_extensions)) {
            $this->_extensions[] = trim($extension, ".");
        }
    }

    /**
     * usuwa rozszerzenie z listy
     *
     * @param string $extension
     */
    public function removeExtension($extension)
    {
        if (in_array($extension, $this->_extensions)) {
            unset($this->_extensions[array_search($extension)]);
        }
    }

    /**
     * zwraca liste rozszerzen zarejestrowanych
     *
     * @return array
     */
    public function getExtensions()
    {
        return $this->_extensions;
    }

    /**
     * czysci liste rozszerzen
     */
    public function clearExtensions()
    {
        $this->_extensions = array();
    }

    /**
     * zwraca opis dla listy rozszezen
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * ustala opis dla listy rozszerzen
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * zwraca limit wgrywanych plikow, '0' oznacza brak limtu wgrywania plikow
     *
     * @return int
     */
    public function getUploadLimit()
    {
        return $this->_uploadLimit;
    }

    /**
     * ustala limit dla liczby wgrywanych plikow, '0' oznacza brak limitu
     *
     * @param int $count
     */
    public function setUploadLimit($count = 0)
    {
        $this->_uploadLimit = (int)$count;
    }

    public function getQueueLimit()
    {
        return $this->_queueLimit;
    }

    public function setQueueLimit($count = 0)
    {
        $this->_queueLimit = (int)$count;
    }

    /**
     * zwraca konfiguracje SWFUpload
     *
     * @return Zend_Config
     */
    public function getConfig()
    {
        $ft = array();
        foreach ($this->_extensions as $ext) {
            $ft[] = "*." . $ext;
        }
        if (count($this->_extensions) > 0) {
            $this->_config->file_types = new wrxNext_SwfUpload_Js_String(
                implode(";", $ft));
        }
        if (!empty($this->_description)) {
            $this->_config->file_types_description = new wrxNext_SwfUpload_Js_String(
                $this->_description);
        }
        $this->_config->file_queue_limit = new wrxNext_SwfUpload_Js(
            $this->_queueLimit);
        return $this->_config;
    }

    protected function init()
    {
        $maxSize = ini_get('post_max_size');
        $unit = strtoupper(substr($maxSize, -1)) . "B";
        $size = Zend_Filter::get($maxSize, 'int');
        $this->_config->file_size_limit = new wrxNext_SwfUpload_Js_String(
            $size . " " . $unit);
    }
}
