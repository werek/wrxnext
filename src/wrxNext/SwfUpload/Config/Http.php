<?php

class wrxNext_SwfUpload_Config_Http extends wrxNext_SwfUpload_Config_Abstract
{

    public function setFlashUrl($url)
    {
        $this->_config->flash_url = $url;
    }

    public function getFlashUrl()
    {
        if (isset($this->_config->flash_url)) {
            return $this->_config->flash_url;
        }
    }

    public function setUploadUrl($url)
    {
        $this->_config->upload_url = $url;
    }

    public function getUploadUrl()
    {
        if (isset($this->_config->upload_url)) {
            return $this->_config->upload_url;
        }
    }

    public function setPostName($name)
    {
        $this->_config->file_post_name = $name;
    }

    public function getPostName()
    {
        if (isset($this->_config->file_post_name)) {
            return $this->_config->file_post_name;
        }
    }

    public function setFileUploadName($name)
    {
        $this->_config->file_upload_name = $name;
    }

    public function getFileUploadName()
    {
        if (isset($this->_config->file_upload_name)) {
            return $this->_config->file_upload_name;
        }
    }

    public function setHttpSuccessCode($code)
    {
        $a = (array)$code;
        $this->_config->http_success = "[" . implode(", ", $a) . "]";
    }

    public function getHttpSuccessCode()
    {
        if (isset($this->_config->http_success)) {
            $a = explode(", ", trim($this->_config->http_success));
            return $a;
        }
    }
}
