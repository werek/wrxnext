<?php

interface wrxNext_SwfUpload_Config_Interface
{

    /**
     * zwraca instancje @link Zend_Config z parametrami instancji
     *
     * @return Zend_Config
     */
    public function getConfig();
}
