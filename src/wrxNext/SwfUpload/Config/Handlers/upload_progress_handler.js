function uploadProgress(file, bytesLoaded) {

    try {
        var percent = Math.ceil((bytesLoaded / file.size) * 100);
        var filesToGo = this.getStats().files_queued - 1
        if (percent === 100) {
            window.defaultStatus = file.name + ": upload complete!! " + filesToGo + " files to go";
        } else {
            window.defaultStatus = file.name + ": " + percent + "% complete. " + filesToGo + " files to go";
        }
    } catch (ex) {
        this.debug(ex);
    }
}
