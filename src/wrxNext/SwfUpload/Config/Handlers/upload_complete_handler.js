function uploadComplete(file) {
    try {
        /*  I want the next upload to continue automatically so I'll call startUpload here */
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        } else {
            window.defaultStatus = "";
        }
    } catch (ex) {
        this.debug(ex);
    }
}
