<?php

class wrxNext_SwfUpload_Config_HandlersQueue extends wrxNext_SwfUpload_Config_Handlers
{

    public function init()
    {
        $this->addPrependCode(
            file_get_contents(
                dirname(__FILE__) . DIRECTORY_SEPARATOR .
                'HandlersQueue.js'));
        $this->_config->file_queued_handler = new wrxNext_SwfUpload_Js(
            "fileQueued");
        $this->_config->upload_complete_handler = new wrxNext_SwfUpload_Js(
            "uploadComplete");
        $this->_config->upload_progress_handler = new wrxNext_SwfUpload_Js(
            "uploadProgress");
        $this->_config->swfupload_loaded_handler = new wrxNext_SwfUpload_Js(
            "swfUploadLoaded");
    }
}
