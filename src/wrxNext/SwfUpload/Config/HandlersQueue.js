document.writeln("<div id=\"swfUploadContainer\"></div>");

function progressBar(percentage) {
    var progressBarContainer = document.getElementById("progressBarContainer");
    if (progressBarContainer == null) {
        var container = document.getElementById("swfUploadContainer");
        progressBarContainer = document.createElement("div");
        progressBarContainer.id = "progressBarContainer";
        container.appendChild(progressBarContainer);
    }
    var pbcStatus = document.getElementById("pbcStatus");
    if (pbcStatus == null) {
        pbcStatus = document.createElement("div");
        pbcStatus.id = "pbcStatus";
        progressBarContainer.appendChild(pbcStatus);
    }
    pbcStatus.style.width = percentage + "%";
    pbcStatus.innerHTML = percentage + "%";
}

function swfUploadLoaded() {
    var container = document.getElementById("swfUploadContainer");
    var selectFiles = document.createElement("select");
    selectFiles.id = "selectFileList";
    selectFiles.size = 1;
    selectFiles.multiple = "multiple";
    container.appendChild(selectFiles);
    /*if (container != null) {
     var buttonStartUpload=document.createElement("input");
     buttonStartUpload.type="button";
     //buttonStartUpload.onclick=startUpload;
     buttonStartUpload.value="Start Upload";
     var buttonStopUpload=document.createElement("input");
     buttonStopUpload.type="button";
     //buttonStopUpload.onclick=stopUpload;
     buttonStopUpload.value="Stop Upload";
     var buttonCancelUpload=document.createElement("input");
     buttonCancelUpload.type="button";
     //buttonCancelUpload.onclick=cancelUpload;
     buttonCancelUpload.value="Cancel Upload";
     container.appendChild(buttonStartUpload);
     container.appendChild(buttonStopUpload);
     container.appendChild(buttonCancelUpload);
     }*/
}

function fileQueued(file) {
    var selectFiles = document.getElementById("selectFileList");
    var opt = document.createElement("option");
    opt.innerHTML = file.name;
    opt.id = "file_" + file.id;
    selectFiles.appendChild(opt);
    if (selectFiles.length > 1) {
        selectFiles.size++;
    }
}

function uploadComplete(file) {
    try {
        var opt = document.getElementById("file_" + file.id);
        opt.innerHTML = file.name + " completed";
        if (this.getStats().files_queued > 0) {
            this.startUpload();
        } else {
            window.defaultStatus = "";
        }
    } catch (ex) {
        this.debug(ex);
    }
}

function uploadProgress(file, bytesLoaded) {

    try {
        var percent = Math.ceil((bytesLoaded / file.size) * 100);
        var opt = document.getElementById("file_" + file.id);
        if (percent === 100) {

            opt.innerHTML = file.name + ": upload complete. ";
        } else {
            opt.innerHTML = file.name + ": " + percent + "% complete. ";
        }
        progressBar(percent);
    } catch (ex) {
        this.debug(ex);
    }
}
