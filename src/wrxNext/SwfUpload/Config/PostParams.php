<?php

class wrxNext_SwfUpload_Config_PostParams extends wrxNext_SwfUpload_Config_Abstract
{

    private $_params = array();

    public function addPostParam($name, $value)
    {
        $this->_params[$name] = $value;
    }

    public function addPostParams($values)
    {
        $this->_params = array_merge($this->_params, $values);
    }

    public function getPostParam($name)
    {
        if (isset($this->_params[$name])) {
            return $this->_params[$name];
        }
    }

    public function getPostParams()
    {
        return $this->_params;
    }

    public function clearPostParams()
    {
        $this->_params = array();
    }

    public function getConfig()
    {
        $this->_config->post_params = new wrxNext_SwfUpload_Js_Array(
            $this->_params);
        return $this->_config;
    }
}
