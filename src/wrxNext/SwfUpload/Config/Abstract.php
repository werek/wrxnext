<?php

abstract class wrxNext_SwfUpload_Config_Abstract implements ArrayAccess,
    Countable, Iterator, wrxNext_SwfUpload_Config_Interface
{

    /**
     * przechowuje config
     *
     * @var Zend_Config
     */
    protected $_config;

    /**
     * konstruktor konfiguracji, moze przyjmowac dodatkowe parametry kaskadowe
     *
     * @param array|Zend_Confg|wrxNext_SwfUpload_Config_Interface $config
     */
    public function __construct($config = null)
    {
        $this->_config = new Zend_Config(array(), true);

        if ($config instanceof wrxNext_SwfUpload_Config_Interface) {
            $this->_config->merge($config->getConfig());
        } elseif ($config instanceof Zend_Config) {
            $this->_config->merge($config);
        } elseif (is_array($config)) {
            $this->_config->merge(new Zend_Config($config));
        }

        $this->init();
    }

    protected function init()
    {
        // dodanie wlasnych elementow
    }

    /**
     * zwraca config
     *
     * @return Zend_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * zwraca tablice
     *
     * @return array
     */
    public function toArray()
    {
        return $this->_config->toArray();
    }

    /**
     * magiczna metoda mapujaca wlasnosci na wewnetrzne wartosci
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->_config->{$name};
    }

    /**
     * magiczna metoda mapujaca wlasnosci na wewnetrzne wartosci
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->_config->{$name} = $value;
    }

    /**
     * Support isset() overloading on PHP 5.1
     *
     * @param string $name
     * @return boolean
     */
    public function __isset($name)
    {
        return isset($this->_config->{$name});
    }

    /**
     * Support unset() overloading on PHP 5.1
     *
     * @param string $name
     * @throws Zend_Config_Exception
     * @return void
     */
    public function __unset($name)
    {
        unset($this->_config->{$name});
    }

    /**
     * Defined by Countable interface
     *
     * @return int
     */
    public function count()
    {
        return count($this->_config);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function current()
    {
        return current($this->_config);
    }

    /**
     * Defined by Iterator interface
     *
     * @return mixed
     */
    public function key()
    {
        return key($this->_config);
    }

    /**
     * Defined by Iterator interface
     */
    public function next()
    {
        next($this->_config);
    }

    /**
     * Defined by Iterator interface
     */
    public function rewind()
    {
        reset($this->_config);
    }

    /**
     * Defined by Iterator interface
     *
     * @return boolean
     */
    public function valid()
    {
        return current($this->_config) < count($this->_config);
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return mixed
     */
    public function offsetGet($name)
    {
        return $this->_config->{$name};
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @param mixed $value
     */
    public function offsetSet($name, $value)
    {
        $this->_config->{$name} = $value;
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     * @return bool
     */
    public function offsetExists($name)
    {
        return isset($this->_config->{$name});
    }

    /**
     * implementacja ArrayAccess
     *
     * @param string $name
     */
    public function offsetUnset($name)
    {
        unset($this->_config->{$name});
    }

    /**
     * dodaje kod do dolaczenia przed inicjalizacja instancji
     *
     * @param string $v
     */
    protected function addPrependCode($v)
    {
        if (!isset($this->_config->prependCode)) {
            $this->_config->prependCode = "\n\n\n" . $v . "\n\n\n";
        } else {
            if (is_array($this->_config->prependCode)) {
                $this->_config->prependCode[] = $v;
            } else {
                $this->_config->prependCode = array(
                    $this->_config->prependCode,
                    $v
                );
            }
        }
    }
}
