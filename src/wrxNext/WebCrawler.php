<?php

/**
 * class for configuration/running webcrawler
 *
 * @author bweres01
 *
 */
class wrxNext_WebCrawler extends wrxNext_WebCrawler_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds page instance
     *
     * @var wrxNext_WebCrawler_Page
     */
    protected $_page = null;

    /**
     * sets page object
     *
     * @param wrxNext_WebCrawler_Page $page
     * @return wrxNext_WebCrawler
     */
    public function setPageObject($page)
    {
        $this->_page = $page;
        return $this;
    }

    /**
     * main action starting page crawler
     *
     * @param string $url
     * @param string $savePath
     */
    public function crawlPage($url, $savePath)
    {
        $url = new wrxNext_Url($url);
        $page = $this->getPageObject();
        $page->setFilter($this->getFilter());
        $page->setSavePath($savePath);
        $page->setHttpClient($this->getHttpClient());
        $page->setUrl($url);
        $page->setUrlCollection($this->getUrlCollection());
        $page->setDiscardExternalDomains($this->getDiscardExternalDomains());
        $page->setMaxDepth($this->getMaxDepth());
        $page->setAllowedSchemes($this->getAllowedSchemes());
        $this->_log('starting crawling process');
        try {
            $return = $page->processPage();
        } catch (Exception $e) {
            $this->_log($e);
            $this->_log($this->getUrlCollection());
        }
    }

    /**
     * gets page object
     *
     * @return wrxNext_WebCrawler_Page
     */
    public function getPageObject()
    {
        if (!$this->_page instanceof wrxNext_WebCrawler_Page) {
            $this->_page = new wrxNext_WebCrawler_Page();
        }
        return $this->_page;
    }

    /**
     * gets filter object
     *
     * @return wrxNext_WebCrawler_Filter_Interface
     */
    public function getFilter()
    {
        if (!$this->_filter instanceof wrxNext_WebCrawler_Filter_Interface) {
            $this->_filter = new wrxNext_WebCrawler_Filter_Stack();
        }
        return $this->_filter;
    }

    /**
     * gets http client
     *
     * @return Zend_Http_Client
     */
    public function getHttpClient()
    {
        if (!$this->_httpClient instanceof Zend_Http_Client) {
            $this->_httpClient = new Zend_Http_Client();
        }
        return $this->_httpClient;
    }

    /**
     * gets url collection
     *
     * @return wrxNext_WebCrawler_UrlCollection
     */
    public function getUrlCollection()
    {
        if (!$this->_urlCollection instanceof wrxNext_WebCrawler_UrlCollection) {
            $this->_urlCollection = new wrxNext_WebCrawler_UrlCollection(array());
        }
        return $this->_urlCollection;
    }
}
