<?php

/**
 * failover class for GeoIP functionality support
 *
 * install php5-geoip
 * put GeoIP/GeoLiteCity.dat in /usr/share/GeoIP/GeoIPCity.dat
 *
 */
class wrxNext_GeoIp
{
    protected $_ip = null;
    protected $_info = array(
        'city' => null,
        'latitude' => null,
        'longitude' => null,
        'country_name' => null
    );

    /**
     *
     * @param string $ip
     */
    public function __construct($ip)
    {
        $this->_ip = $ip;
        if (extension_loaded('geoip')) {
            $this->_info = @geoip_record_by_name($ip);
        } else { // fallback to pear
            $geoip = Net_GeoIP::getInstance(APPLICATION_PATH . "/../library/Hpm/GeoIp/GeoLiteCity.dat");

            $location = $location = $geoip->lookupLocation($ip);

            if (!is_null($location)) {
                $this->_info = array(
                    'city' => $location->city,
                    'latitude' => $location->latitude,
                    'longitude' => $location->longitude,
                    'country_name' => $location->countryName
                );


            }
        }

        if (empty($this->_info)) {
            $this->_info = array(
                'city' => 'Warsaw',
                'latitude' => 52.25,
                'longitude' => 21,
                'country_name' => 'Poland'
            );
        }
    }

    /**
     * return string
     */
    public function getCityName()
    {
        return $this->_info ['city'];
    }

    /**
     * return float
     */
    public function getLat()
    {
        return $this->_info ['latitude'];
    }

    /**
     * return float
     */
    public function getLng()
    {
        return $this->_info ['longitude'];
    }

    /**
     * return string
     */
    public function getCountryName()
    {
        return $this->_info ['country_name'];
    }
}
