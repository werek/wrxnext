<?php

/**
 * abstract implementation of {@link wrxNext_Auth_ResourceContext_Interface} for determining
 * authorisation resource context.
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
abstract class wrxNext_Auth_ResourceContext_Abstract implements
    wrxNext_Auth_ResourceContext_Interface
{
    use wrxNext_Trait_RegistryLog;

    /**
     * default reroute params in case of authorisation failure
     *
     * @var array
     */
    protected $_rerouteParams = array(
        'module'     => 'default',
        'controller' => 'index',
        'action'     => 'index'
    );

    /**
     * holds context name
     *
     * @var string
     */
    protected $_context = null;

    /**
     * holds resource name
     *
     * @var string
     */
    protected $_resource = null;

    /**
     * zend_acl object
     *
     * @var Zend_Acl
     */
    protected $_acl = null;

    /**
     * constructor, accepts Zend_Acl object or options in form of
     * array/Zend_Config
     *
     * @param string $options
     */
    public function __construct($options = null)
    {
        if ($options instanceof Zend_Acl) {
            $this->setAcl($options);
        } elseif ( !is_null($options)) {
            $this->setOptions($options);
        }
        $this->init();
    }

    /**
     * sets object options
     *
     * @param mixed $options
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    /**
     * user method for access construct stage from extending class
     */
    public function init()
    {
    }

    /**
     * returns resource name
     *
     * @return string
     */
    public function getResourceName()
    {
        return $this->_resource;
    }

    /**
     * returns context name
     *
     * @return string
     */
    public function getContextName()
    {
        return $this->_context;
    }

    /**
     * @inheritdoc
     */
    public function authorise($resource, $context)
    {
        // seting up needed variables
        $this->setResourceName($resource);
        $this->setContextName($context);

        // fetching needed objects
        $acl  = $this->getAcl();
        $role = $this->getContextRole($context);
        //$this->_log($role);
        if ( !is_null($role)) {
            return $acl->isAllowed($role, $resource);
        }
        return false;
    }

    /**
     * sets resource name
     *
     * @param string $resourceName
     * @return wrxNext_Auth_ResourceContext_Abstract
     */
    public function setResourceName($resourceName)
    {
        $this->_resource = $resourceName;
        return $this;
    }

    /**
     * sets context name
     *
     * @param string $contextName
     * @return wrxNext_Auth_ResourceContext_Abstract
     */
    public function setContextName($contextName)
    {
        $this->_context = $contextName;
        return $this;
    }

    /**
     * returns acl object
     *
     * @return Zend_Acl
     */
    public function getAcl()
    {
        return $this->_acl;
    }

    /**
     * sets acl object
     *
     * @param Zend_Acl $acl
     * @return wrxNext_Auth_ResourceContext_Abstract
     */
    public function setAcl(Zend_Acl $acl)
    {
        $this->_acl = $acl;
        return $this;
    }

    /**
     * returns role based on context
     *
     * @param string $context
     */
    abstract public function getContextRole($context);

    /**
     * triggered when authorisation fails, this method will set request object to action that should handle
     * logon/privileges elevation
     *
     * @param \Zend_Controller_Request_Abstract $request
     */
    public function reroute(Zend_Controller_Request_Abstract $request)
    {
        $reroute = $this->_rerouteParams;
        $request->setModuleName($reroute['module']);
        unset($reroute['module']);
        $request->setControllerName($reroute['controller']);
        unset($reroute['controller']);
        $request->setActionName($reroute['action']);
        unset($reroute['action']);
        $request->setParams($reroute);
    }
}
