<?php

interface wrxNext_Auth_ResourceContext_OverrideInterface
{

    /**
     * overrides authorisation ACL rules, if override should not be enacted then
     * it should return null
     *
     * @param Zend_Controller_Request_Abstract $request
     * @param string $context
     * @return boolean null if override should not be enacted
     */
    public function override(Zend_Controller_Request_Abstract $request,
                             $context);
}
