<?php

/**
 * resource authorisation interface, for authorising resources in given context
 *
 * the idea behind it is tu use single ACL with many authorisation methods.
 * Object implementing this interface should check access to given resource
 * according to given context. ie. resource with name 'view' could be accessible
 * in context 'backend', but not accessible in context 'frontend'. Authorise method
 * should determine if given resource is accessible in authorisation mechanism coresponding to
 * given context name.
 *
 * If authorisation for given resource/context will fail, object should remember last used
 * context/resource because after fail, its reroute method will be invoked with
 * request object from current request, purpose of reroute method is to reroute/redirect
 * user to authorisation page where user can logi in/confirm his credentials
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Auth_ResourceContext_Interface
{

    /**
     * authorises given resource in given context, returns true on success
     * false on failure
     *
     * @param string $resource
     * @param string $context
     * @return boolean
     */
    public function authorise($resource, $context);

    /**
     * reroutes/redirects current request to authorisation location
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    public function reroute(Zend_Controller_Request_Abstract $request);
}
