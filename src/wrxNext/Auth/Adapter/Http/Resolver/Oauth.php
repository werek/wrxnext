<?php

/**
 * Http Auth adapter handling authorising api request with oauth access keys
 *
 * it assummes that username is oauth access key. While password is derived from
 * concatenation of oauth access secret, passed nonce and passed timestamp.
 * separated by underscore and at the end hashed with sha1().
 *
 * also this adapter registers each authorisation request in nonce table to prevent
 * request hijacking
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Auth_Adapter_Http_Resolver_Oauth implements
    Zend_Auth_Adapter_Http_Resolver_Interface
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds http request instance
     *
     * @var Zend_Controller_Request_Http
     */
    protected $_requestHttp = null;

    /**
     * holds recognised user object
     *
     * @var bd_Model_DbTableRow_Users
     */
    protected $_recognisedUser = null;

    /**
     * constructor accepting configuration options
     *
     * @param array $options
     */
    public function __construct($options)
    {
        $this->setOptions($options);
    }

    /**
     * handles setting options
     *
     * @param array $options
     */
    public function setOptions($options)
    {
        $options = (array)$options;
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    /*
     * (non-PHPdoc) @see Zend_Auth_Adapter_Http_Resolver_Interface::resolve()
     */
    public function resolve($username, $realm)
    {
        $oauthNonceTable = new bd_Model_DbTable_OauthNonce();
        $oauthAuthorizedTable = new bd_Model_DbTable_OauthAuthorizedConsumer();

        // fetching user by token
        $authorized = $oauthAuthorizedTable->getByToken($username);
        if ($authorized instanceof bd_Model_DbTableRow_OauthAuthorizedConsumer) {
            $nonceGet = $this->getRequest()->getParam('nonce');
            $timestampGet = $this->getRequest()->getParam('timestamp');
            if (!empty($nonceGet) && !empty($timestampGet)) {
                // saving request for future check
                $oauthNonceTable->insert(
                    array(
                        'key' => $username,
                        'nonce' => $nonceGet,
                        'timestamp' => $timestampGet
                    ));

                // generating proper password
                $password = $nonceGet . '_' . $timestampGet . '_' .
                    $authorized->oauth_token_secret;
                $hashedPassword = sha1($password);

                // saving time of access
                $authorized->last_access = new Zend_Db_Expr('NOW()');
                $authorized->save();

                // saving user row for further use
                $this->_recognisedUser = $authorized->getUserRow();

                // returning password
                return $hashedPassword;
            }
        }
        return false;
    }

    /**
     * gets http request object
     *
     * @return Zend_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->_requestHttp;
    }

    /**
     * returns recognised user row
     *
     * @return bd_Model_DbTableRow_Users
     */
    public function getRecognisedUser()
    {
        return $this->_recognisedUser;
    }

    /**
     * sets http request object
     *
     * @param Zend_Controller_Request_Http $request
     * @return wrxNext_Auth_Adapter_Http_Resolver_Oauth
     */
    public function setRequest($request)
    {
        $this->_requestHttp = $request;
        return $this;
    }
}
