<?php

class wrxNext_Shortcodes
{

    /**
     * hold parsers instances
     *
     * @var wrxNext_Shortcodes_Parser_Interface[]
     */
    protected $_parsers = array();

    /**
     * constructor, accepts array of options
     *
     * @param string $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Shortcodes
     */
    public function setOptions($options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * gets parsers
     *
     * @return wrxNext_Shortcodes_Parser_Interface[]
     */
    public function getParsers()
    {
        return $this->_parsers;
    }

    /**
     * sets parsers array, whith check for parser compatibility
     *
     * @param array $parsers
     * @return wrxNext_Shortcodes
     */
    public function setParsers($parsers)
    {
        foreach ($parsers as $parser) {
            $this->addParser($parser);
        }
        return $this;
    }

    /**
     * adds parser
     *
     * @param string|wrxNext_Shortcodes_Parser_Interface $parser
     * @throws wrxNext_Shortcodes_Exception
     * @return wrxNext_Shortcodes
     */
    public function addParser($parser)
    {
        if ($parser instanceof wrxNext_Shortcodes_Parser_Interface) {
            $this->_parsers[$parser->name()] = $parser;
        } elseif (is_string($parser)) {
            if (class_exists($parser)) {
                $reflection = new ReflectionClass($parser);
                if ($reflection->implementsInterface(
                    'wrxNext_Shortcodes_Parser_Interface')
                ) {
                    $this->addParser($reflection->newInstance());
                } else {
                    throw new wrxNext_Shortcodes_Exception(
                        'Given class name does not implements interface wrxNext_Shortcodes_Parser_Interface',
                        10002);
                }
            }
        } else {
            throw new wrxNext_Shortcodes_Exception(
                'parser should be either a string class name or parser object implementing wrxNext_Shortcodes_Parser_Interface',
                10001);
        }
        return $this;
    }
}
