<?php

/**
 * klasa zajmujaca sie obsluga pol formularza typu FILE
 *
 */
class wrxNext_Upload
{

    private $_errors = array(
        'upload ok',
        'The uploaded file exceeds the upload_max_filesize directive in php.ini.',
        'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.',
        'The uploaded file was only partially uploaded.',
        'No file was uploaded.',
        'Missing a temporary folder.',
        'Failed to write file to disk.',
        'A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop; examining the list of loaded extensions with phpinfo() may help.'
    );

    /**
     * przechowuje flage czy dane pole jest wielokrotne
     *
     * @var null boolean
     */
    private $multiple = null;

    /**
     * dla pola wielokrotnego przechowuje index aktualnego pola
     *
     * @var int
     */
    private $counter = 0;

    /**
     * przechowuje naze pola w tablicy file
     *
     * @var string
     */
    private $fieldName;

    /**
     * konstruktor, przyjmuje jako parametr nazwe pola file
     *
     * @param string $fieldName
     */
    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;
        $this->isMultiple();
    }

    /**
     * zwraca status czy pole jest wielokrotne
     *
     * @return boolean
     */
    public function isMultiple()
    {
        if (is_null($this->multiple)) {
            $this->multiple = is_array($_FILES[$this->fieldName]['name']) ? true : false;
        }
        return $this->multiple;
    }

    /**
     * zapisuje plik w podanej sciezce, zwraca false gdy zapis sie nie powiedzie
     *
     * @param string|wrxNext_Cdn $savePath
     * @return boolean
     */
    public function saveFile2($savePath)
    {
        if ($savePath instanceof wrxNext_Cdn) {
            $savePath = $savePath->getFullPath();
        }
        if (move_uploaded_file($this->getFileTmpPath(), $savePath)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * zwraca sciezke pod ktora tymczasowo znajduje sie plik
     *
     * @return string
     */
    public function getFileTmpPath()
    {
        return $this->isMultiple() ? $_FILES[$this->fieldName]['tmp_name'][$this->counter] : $_FILES[$this->fieldName]['tmp_name'];
    }

    /**
     * zwraca nazwe pliku
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->isMultiple() ? $_FILES[$this->fieldName]['name'][$this->counter] : $_FILES[$this->fieldName]['name'];
    }

    /**
     * zwraca rozmiar pliku w bajtach
     *
     * @return int
     */
    public function getFileSize()
    {
        return (int)$this->isMultiple() ? $_FILES[$this->fieldName]['size'][$this->counter] : $_FILES[$this->fieldName]['size'];
    }

    /**
     * zwraca typ pliku jezeli jest to udostepniane przez system
     *
     * @return string
     */
    public function getFileType()
    {
        return $this->isMultiple() ? $_FILES[$this->fieldName]['type'][$this->counter] : $_FILES[$this->fieldName]['type'];
    }

    /**
     * zwraca zawartosc pliku
     *
     * @return mixed
     */
    public function getFileContents()
    {
        return file_get_contents($this->getFileTmpPath());
    }

    /**
     * zwraca czyste nieobrobione dane
     *
     * @return array
     */
    public function getRaw()
    {
        if ($this->isMultiple()) {
            $data = array();
            foreach ($_FILES as $key => $value) {
                $data[$key] = $value[$this->counter];
            }
            return $data;
        }
        return $_FILES[$this->fieldName];
    }

    /**
     * zwraca tekst bledu jezeli taki wystapi
     *
     * @return string
     */
    public function error()
    {
        if ($this->isMultiple()) {
            return (array_key_exists(
                $_FILES[$this->fieldName]['error'][$this->counter],
                $this->_errors)) ? $this->_errors[$_FILES[$this->fieldName]['error'][$this->counter]] : $_FILES[$this->fieldName]['error'][$this->counter];
        } else {
            return (array_key_exists($_FILES[$this->fieldName]['error'],
                $this->_errors)) ? $this->_errors[$_FILES[$this->fieldName]['error']] : $_FILES[$this->fieldName]['error'];
        }
    }

    /**
     * zwraca liczbe pól
     *
     * @return int
     */
    public function count()
    {
        if ($this->isMultiple()) {
            return count($_FILES[$this->fieldName]['name']);
        }
        return 1;
    }

    /**
     * sets current file index in multiple condition, starting from 0
     *
     * @param int $index
     * @return wrxNext_Upload
     */
    public function setFieldIndex($index)
    {
        $this->counter = $index;
        return $this;
    }

    /**
     * gets current file index in multiple condition, starting from 0
     *
     * @return int
     */
    public function getFieldIndex()
    {
        return $this->counter;
    }

    /**
     * okrtesla czy wystapil blad podczas uploadu
     *
     * @return bool
     */
    public function isError()
    {
        return ($_FILES[$this->fieldName]['error'] > 0) ? true : false;
    }
}

?>
