<?php

class wrxNext_Commerce_Payu
{

    const TRANSACTION_NEW = 1;

    const TRANSACTION_CANCELED = 2;

    const TRANSACTION_REJECTED = 3;

    const TRANSACTION_STARTED = 4;

    const TRANSACTION_AWAITING_RECEPTION = 5;

    const TRANSACTION_REJECTED_CLIENT_BILLED = 7;

    const TRANSACTION_ENDED = 99;

    const TRANSACTION_WRONG_STATUS = 888;

    private $_errorCodes = array(
        100 => 'brak lub błędna wartość parametru pos_id',
        101 => 'brak parametru session_id',
        102 => 'brak parametru ts',
        103 => 'brak lub błędna wartość parametru sig',
        104 => 'brak parametru desc',
        105 => 'brak parametru client_ip',
        106 => 'brak parametru first_name',
        107 => 'brak parametru last_name',
        108 => 'brak parametru street',
        109 => 'brak parametru city',
        110 => 'brak parametru post_code',
        111 => 'brak parametru amount (lub/oraz amount_netto dla usługi SMS)',
        112 => 'błędny numer konta bankowego',
        113 => 'brak parametru email',
        114 => 'brak numeru telefonu',
        200 => 'inny chwilowy błąd',
        201 => 'inny chwilowy błąd bazy danych',
        202 => 'POS o podanym identyfikatorze jest zablokowany',
        203 => 'niedozwolona wartość pay_type dla danego pos_id',
        204 => 'podana metoda płatności (wartość pay_type) jest chwilowo zablokowana dla danego pos_id, np. przerwa konserwacyjna bramki płatniczej',
        205 => 'kwota transakcji mniejsza od wartości minimalnej',
        206 => 'kwota transakcji większa od wartości maksymalnej',
        207 => 'przekroczona wartość wszystkich transakcji dla jednego klienta w ostatnim przedziale czasowym',
        208 => 'POS działa w wariancie ExpressPayment lecz nie nastąpiła aktywacja tego wariantu współpracy (czekamy na zgodę działu obsługi klienta)',
        209 => 'błędny numer pos_id lub pos_auth_key',
        500 => 'transakcja nie istnieje',
        501 => 'brak autoryzacji dla danej transakcji',
        502 => 'transakcja rozpoczęta wcześniej',
        503 => 'autoryzacja do transakcji była już przeprowadzana',
        504 => 'transakcja anulowana wcześniej',
        505 => 'transakcja przekazana do odbioru wcześniej',
        506 => 'transakcja już odebrana',
        507 => 'błąd podczas zwrotu środków do klienta',
        599 => 'błędny stan transakcji, np. nie można uznać transakcji kilka razy lub inny, prosimy o kontakt',
        999 => 'inny błąd krytyczny - prosimy o kontakt'
    );

    private $_path = '';

    function __construct($configFile = null)
    {
    }

    protected function loadConfig($config)
    {
    }
}
