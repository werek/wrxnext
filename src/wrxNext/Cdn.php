<?php

/**
 * handles common actions on CDN entries
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Cdn
{

    /**
     * holds root cdn directory
     *
     * @var string
     */
    protected static $cdnDirectory = null;

    /**
     * holds current directory
     *
     * @var string
     */
    protected $_currentLocation = '/';

    /**
     * instantiates cdn object, passing options if string is given then it is
     * recognised as location
     *
     * @param string|array $options
     */
    public function __construct($options = null)
    {
        if (is_string($options)) {
            $options = array(
                'location' => $options
            );
        }
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Cdn
     */
    public function setOptions(array $options)
    {
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * sets root cdn directory
     *
     * @param string $path
     */
    public static function setRootDirectory($path)
    {
        self::$cdnDirectory = $path;
    }

    /**
     * returns true if location is r3eadable
     *
     * @return boolean
     */
    public function readable()
    {
        return is_readable($this->_location());
    }

    /**
     * returns full cdn path of location
     *
     * @return string
     */
    protected function _location()
    {
        return self::getRootDirectory() . DIRECTORY_SEPARATOR .
        ltrim($this->_currentLocation, '\\/');
    }

    /**
     * gets root cdn directory
     *
     * @return string
     */
    public static function getRootDirectory()
    {
        if (is_null(self::$cdnDirectory)) {
            self::$cdnDirectory = APPLICATION_CDN_DIRECTORY;
        }
        return self::$cdnDirectory;
    }

    /**
     * returns true if location is writable
     *
     * @return boolean
     */
    public function writable()
    {
        return is_writable($this->_location());
    }

    /**
     * returns true if location is file
     *
     * @return boolean
     */
    public function isFile()
    {
        return is_file($this->_location());
    }

    /**
     * returns true if location is "." or ".."
     *
     * @return boolean
     */
    public function isDot()
    {
        return in_array($this->basename(), array(
            '.',
            '..'
        ));
    }

    /**
     * returns basename of current location
     *
     * @return string
     */
    public function basename()
    {
        return basename($this->_currentLocation);
    }

    /**
     * returns file size of current location
     *
     * @return number
     */
    public function size()
    {
        return filesize($this->_location());
    }

    /**
     * deletes current location from cdn
     *
     * @param bool $recursively
     * @return bool
     */
    public function delete($recursively = false)
    {
        $dir = $this->_location();
        if ($recursively && is_dir($dir)) {
            return $this->_deleteRecursively($dir);
        } else {
            return unlink($dir);
        }
    }

    /**
     * recursive delete function
     *
     * @param string $dir
     * @return boolean
     */
    protected function _deleteRecursively($dir)
    {
        $entries = array_diff(scandir($dir),
            array(
                '.',
                '..'
            ));
        foreach ($entries as $entry) {
            $dirEntry = $dir . DIRECTORY_SEPARATOR . $entry;
            if (is_dir($dirEntry) && !is_link($dirEntry)) {
                $this->_deleteRecursively($dirEntry);
            } else {
                unlink($dirEntry);
            }
        }
        return unlink($dir);
    }

    /**
     * returns handle for current cdn location, $mode is compatible with @see
     * fopen
     *
     * @param string $mode
     * @return resource
     */
    public function fopen($mode)
    {
        return fopen($this->_location(), $mode);
    }

    /**
     * moves cdn file/directory onto another cdn location
     *
     * @param string|wrxNext_Cdn $location
     * @throws wrxNext_Cdn_Exception
     * @return wrxNext_Cdn
     */
    public function moveToCdnLocation($location)
    {
        if (!$location instanceof wrxNext_Cdn) {
            $location = new wrxNext_Cdn($location);
        } else {
            $location = clone $location;
        }
        if (!rename($this->getFullPath(), $location->getFullPath())) {
            throw new wrxNext_Cdn_Exception(
                'could not move cdn location "' . $this->getLocation() .
                '" to cdn location "' . $location->getLocation() .
                '"');
        }
        $this->_currentLocation = $location->getLocation();
        return $this;
    }

    /**
     * returns full path to cdn location
     *
     * @return string
     */
    public function getFullPath()
    {
        return $this->_location();
    }

    /**
     * gets location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->_currentLocation;
    }

    /**
     * checks for existence of given file/directory under current location
     *
     * @param string $name
     */
    public function hasEntry($name)
    {
        return file_exists($this->_location() . DIRECTORY_SEPARATOR . $name);
    }

    /**
     * returns cdn object with location set for non existent entry under current
     * location, but do not creates it.
     *
     * @param unknown $name
     * @param string $prefix
     * @throws wrxNext_Cdn_Exception
     * @return wrxNext_Cdn
     */
    public function getFreeEntry($name, $prefix = '_')
    {
        if (!$this->exists()) {
            throw new wrxNext_Cdn_Exception(
                'cdn location "' . $this->_currentLocation .
                '" does not exists, it\'s impossible to check for free entry');
        }
        if (!$this->isDirectory()) {
            throw new wrxNext_Cdn_Exception(
                'cdn location "' . $this->_currentLocation .
                '" is not directory');
        }
        if (empty($prefix) || in_array($prefix,
                array(
                    '\\',
                    '/'
                ))
        ) {
            throw new wrxNext_Cdn_Exception(
                'prefix for free entry search cannot be empty or match directory separator');
        }
        $rightTrim = new wrxNext_Filter_StringTrimRight('/\\');
        $leftTrim = new wrxNext_Filter_StringTrimLeft('/\\');
        // filtering check location from unnecesary directory slash
        $location = $rightTrim->filter($this->_location());
        // filtering name from unnecesary directory slash at begining
        $name = $leftTrim->filter($name);
        while (file_exists($location . '/' . $name)) {
            $name = $prefix . $name;
        }
        return new wrxNext_Cdn(
            $rightTrim->filter($this->_currentLocation) . '/' . $name);
    }

    /**
     * returns true if location exists
     *
     * @return boolean
     */
    public function exists()
    {
        return file_exists($this->_location());
    }

    /**
     * returns true if location is directory
     *
     * @return boolean
     */
    public function isDirectory()
    {
        return is_dir($this->_location());
    }

    /**
     * creates directory with given accessRights recursivelly
     *
     * @param int $accessRights
     * @param boolean $recursive
     * @return boolean
     */
    public function createDirectory($accessRights = 0777, $recursive = true)
    {
        return mkdir($this->_location(), $accessRights, $recursive);
    }

    /**
     * sets location
     *
     * @param string $path
     * @return wrxNext_Cdn
     */
    public function setLocation($path)
    {
        $this->_currentLocation = $path;
        return $this;
    }

    /**
     * returns parent location, but not higher that cdn base dir
     *
     * @return wrxNext_Cdn
     */
    public function parentLocation()
    {
        $location = dirname($this->_currentLocation);
        return new wrxNext_Cdn($location == '.' ? '/' : $location);
    }

    /**
     * returns new instance of cdn location of given child
     *
     * @param string $name
     * @return wrxNext_Cdn
     */
    public function child($name)
    {
        $child = new wrxNext_Cdn($this->getLocation() . '/' . $name);
        return $child;
    }
}
