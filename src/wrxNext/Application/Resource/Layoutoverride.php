<?php

class wrxNext_Application_Resource_Layoutoverride extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        wrxNext_Controller_Plugin_LayoutOverride::setMapTree($this->getOptions());
    }
}
