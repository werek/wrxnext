<?php

class wrxNext_Application_Resource_Sessioncookie extends Zend_Application_Resource_ResourceAbstract
{

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Application_Resource_Resource::init()
     */
    public function init()
    {
        // check for not started session
        if (session_id() === '' && (!array_key_exists('disableOverride',
                    $this->_options) ||
                intval($this->_options['disableOverride']) !== 1)
        ) {
            $params = session_get_cookie_params();
            if (array_key_exists('lifetime', $this->_options) &&
                !empty($this->_options['lifetime'])
            ) {
                $params['lifetime'] = intval($this->_options['lifetime']);
            }
            if (array_key_exists('domain', $this->_options) && !empty(
                $this->_options['domain'])
            ) {
                $params['domain'] = $this->_options['domain'];
            }
            if (array_key_exists('path', $this->_options) &&
                !empty($this->_options['path'])
            ) {
                $params['path'] = $this->_options['path'];
            }
            if (array_key_exists('secure', $this->_options) && !empty(
                $this->_options['secure'])
            ) {
                $params['secure'] = (boolean)$this->_options['secure'];
            }
            if (array_key_exists('name', $this->_options) &&
                !empty($this->_options['name'])
            ) {
                session_name($this->_options['name']);
            }
            session_set_cookie_params($params['lifetime'], $params['path'],
                $params['domain'], $params['secure']);
        }
    }
}
