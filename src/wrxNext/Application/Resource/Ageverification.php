<?php

/**
 * resource plugin for age verification action helper to populate adapters
 *
 * @author bweres01
 *
 */
class wrxNext_Application_Resource_Ageverification extends Zend_Application_Resource_ResourceAbstract
{

    /**
     * (non-PHPdoc)
     * @see Zend_Application_Resource_Resource::init()
     */
    public function init()
    {
        $ageVerification = new wrxNext_Controller_Action_Helper_AgeVerification();
        foreach ($this->_options as $name => $data) {
            if (isset($data['adapter'])) {
                $adapter = new $data['adapter']();
                if (isset($data['options']) &&
                    method_exists($adapter, 'setOptions')
                ) {
                    $adapter->setOptions($data['options']);
                }
                $ageVerification->addAdapter($adapter, $name);
            }
        }
    }
}
