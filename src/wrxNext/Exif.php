<?php

/**
 * class for handling exif data in image
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 */
class wrxNext_Exif implements ArrayAccess
{

    /**
     * zawiera opisy poszczegolnych wartosci opcji
     *
     * @var array
     */
    private $_description = array(
        'PhotometricInterpretation' => array(
            0,
            'Monochrome',
            'RGB',
            0,
            0,
            0,
            'YCbCr'
        ),
        'Orientation' => array(
            0,
            'Landscape',
            'Landscape',
            'Landscape',
            'Landscape',
            'Portrait',
            'Portrait',
            'Portrait',
            'Portrait'
        ),
        'ResolutionUnit' => array(
            0,
            'No unit',
            'Inch',
            'Centimeter'
        ),
        'ExposureProgram' => array(
            0,
            'Manual',
            'Program/normal',
            'Aperture Priority',
            'Shutter Priority',
            'Program/creative(slow)',
            'Program/action(hi-speed)',
            'Portrait',
            'Landscape'
        ),
        'Flash' => array(
            'Not fired',
            'Auto',
            'on',
            'Red-Eye Reduction',
            'slow synchro',
            'Auto/Red-Eye Reduction',
            'On/Red-Eye Reduction',
            'External flash/None'
        ),
        'ColorSpace' => array(
            0,
            'sRGB',
            65535 => 'Uncalibrated'
        ),
        'FocalPlaneResolutionUnit' => array(
            0,
            'No unit',
            'Inch',
            'Centimeter'
        ),
        'ExposureMode' => array(
            'Easy shooting',
            'Program',
            'Tv-priority',
            'Av-priority',
            'Manual',
            'A-DEP'
        ),
        'WhiteBalance' => array(
            'Auto',
            'Daylight',
            'Cloudy',
            'Tungsten',
            'Fluorescent',
            'Flash',
            'Custom'
        )
    );

    /**
     * zmienna przechowujaca sciezke do pliku
     *
     * @var string
     */
    private $_filePath;

    /**
     * zmienna przechowujaca dane exif obrazu
     *
     * @var array
     */
    private $_imageData = array();

    /**
     * konstruktor, przyjmujacy za parametr sciezke do pliku graficznego
     *
     * @param string $filename
     */
    function __construct($filename = null)
    {
        if (!is_null($filename)) {
            $this->parseImageData($filename);
        }
    }

    /**
     * ustawia kontekst klasy na dany obraz i odczytuje
     * dane exif zdjecia
     *
     * @param string $filename
     */
    public function parseImageData($filename)
    {
        if (function_exists('exif_read_data')) {
            $this->_imageData = @exif_read_data($filename);
        }
        $this->_filePath = $filename;
    }

    /**
     * zwraca wartosci poszczegolnych wlasciwosci zdjecia
     * na ktore jest ustawiony kontekst
     *
     * @param string $propertyName
     * @return mixed
     */
    public function __get($propertyName)
    {
        if (preg_match('/^__.*/', trim($propertyName))) {
            $propertyName = substr($propertyName, 2);
            if (array_key_exists($propertyName, $this->_imageData)) {
                return $this->_imageData[$propertyName];
            }
        } else {
            $methods = get_class_methods($this);
            $propertyName = 'property_' . $propertyName;
            if (in_array($propertyName, $methods)) {
                return $this->$propertyName();
            }
        }
    }

    /**
     * zwraca wszystkie dane o zdjeciu w formie tablicy, bez mapowania wartosci
     *
     * @param mixed $name
     * @return mixed
     */
    public function getRawData($name = null)
    {
        if (!is_null($name)) {
            // supressing notice about not existing key
            return @$this->_imageData[$name];
        }
        return $this->_imageData;
    }

    /**
     * zwraca dane jako tablica
     *
     * @return array
     */
    public function toArray()
    {
        return $this->getData();
    }

    /**
     * zwraca wszystkie dane o zdjeciu w formie tablicy
     *
     * @return array
     */
    public function getData()
    {
        $tmp = $this->_imageData;
        $methods = get_class_methods($this);
        foreach ($methods as $met) {
            if (preg_match('/^property_.*/', $met)) {
                $strip = str_replace('property_', '', $met);
                $tmp[$strip] = $this->$met();
            }
        }
        return $tmp;
    }

    /**
     * zwraca wszystkie dane o zdjeciu w formie tablicy
     * razem z rozwiazanymi Trybami numerycznymi
     *
     * @return array
     */
    public function getDescribedData()
    {
        $tmp = $this->getData();
        foreach ($tmp as $k => $v) {
            if (array_key_exists($k, $this->_description)) {
                $tmp[$k] = $this->_description[$k][$v];
            }
        }
        return $tmp;
    }

    /**
     * zwraca zasob GD
     *
     * @return GDresource
     */
    public function getGdImage()
    {
        switch ($this->_imageData['FileType']) {
            case wrxNext_Image::GIF:
                return imagecreatefromgif($this->_filePath);
                break;
            case IMAGETYPE_JPEG:
                return imagecreatefromjpeg($this->_filePath);
                break;
            case IMAGETYPE_PNG:
                return imagecreatefrompng($this->_filePath);
                break;
        }
    }

    /**
     * tworzy obiekt wrxNext_Image z powiazanego obrazu
     *
     * @return wrxNext_Image
     */
    public function getHpmImage()
    {
        return new wrxNext_Image($this->_filePath);
    }

    /**
     * sprawdza czy istnieje naglowek EXIF
     *
     * @return bool
     */
    public function hasExifHeader()
    {
        if (is_array($this->_imageData)) {
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->_imageData);
    }

    /**
     * @inheritdoc
     */
    public function offsetGet($offset)
    {
        return $this->_imageData[$offset];
    }

    /**
     * @inheritdoc
     */
    public function offsetSet($offset, $value)
    {
        $this->_imageData[$offset] = $value;
    }

    /**
     * @inheritdoc
     */
    public function offsetUnset($offset)
    {
        unset($this->_imageData[$offset]);
    }

    /**
     * zwraca wartosc przyslony
     *
     * @return double
     */
    private function property_apperture()
    {
        $ap = explode('/', $this->_imageData['FNumber']);
        return (double)round($ap[0] / $ap[1], 1);
    }

    /**
     * zwraca wartosc daty kiedy zrobiono zdjecie
     *
     * @return string
     */
    private function property_dateshot()
    {
        $t = explode(' ', $this->_imageData['DateTimeOriginal']);
        return str_replace(':', '-', $t[0]) . ' ' . $t[1];
    }

    /**
     * zwraca ogniskowa
     *
     * @return double
     */
    private function property_focallength()
    {
        $ap = explode('/', $this->_imageData['FocalLength']);
        return (double)round($ap[0] / $ap[1], 1);
    }
}
