<?php

class wrxNext_Form_Element_Csrf extends Zend_Form_Element_Hidden
{

    /**
     * use formCsrf helper
     *
     * @var string
     */
    public $helper = 'formCsrf';

    /**
     * constructor, requires that options have key 'csrfName'
     *
     * @param string $spec
     * @param array|Zend_Config $options
     */
    public function __construct($spec, $options)
    {
        if (!isset($options['csrfName'])) {
            throw new Zend_Form_Element_Exception("no 'csrfName' key given");
        }
        $this->addValidator(new wrxNext_Validate_CsrfValue($options['csrfName']));
        parent::__construct($spec, $options);
    }
}
