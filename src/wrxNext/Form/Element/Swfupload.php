<?php

class wrxNext_Form_Element_Swfupload extends Zend_Form_Element_Button
{

    /**
     * Use formButton view helper by default
     *
     * @var string
     */
    public $helper = 'formSwfupload';

    /**
     * ustala status czy domyslne repozytorium ma byc uzyte w przypadku braku
     * zdefiniowania sciezek do bibliotek
     *
     * @var bool
     */
    protected $useDefaultRepository = false;

    /**
     * przechowuje obiekt wrxNext_Swfupload
     *
     * @var wrxNext_SwfUpload
     */
    protected $swfupload = null;

    public function init()
    {
        if ($this->swfupload instanceof wrxNext_SwfUpload) {
            // jezeli mamy instancje przekazana to nic nie robimy
        } else {
            $this->swfupload = new wrxNext_SwfUpload();
        }
        $this->setRequired(false);
        $this->setIgnore(true);
    }

    /**
     * definiuje czy w przypadku braku wpisow ma korzystac z domyslnych
     * bibliotek
     *
     * @param bool $useCdn
     */
    public function setUseDefaultRepository($useCdn = true)
    {
        if ($useCdn) {
            $this->useDefaultRepository = true;
        } else {
            $this->useDefaultRepository = false;
        }
    }
}
