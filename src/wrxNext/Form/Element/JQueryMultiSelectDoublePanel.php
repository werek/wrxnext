<?php

class wrxNext_Form_Element_JQueryMultiSelectDoublePanel extends Zend_Form_Element_Multiselect
{
    use wrxNext_Trait_RegistryLog;

    public $helper = 'formJqueryMultiSelectDoublePanel';
    protected $_idSet = false;

    /**
     * Get element id
     *
     * @return string
     */
    public function getId()
    {
        if (!$this->_idSet) {
            $this->id = uniqid('JqueryMultiSelectDoublePanel');
            $this->_idSet = true;
        }

        return $this->id;
    }
}
