<?php

/**
 *
 * @author krzysiek
 *
 */
class wrxNext_Form_Element_CKEditor extends Zend_Form_Element_Textarea
{
    const TOOLBAR_DEFAULT = "[
				{ name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source' ] },
	{ name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
	{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Scayt' ] },
	'/',
	{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Strike', '-', 'RemoveFormat' ] },
	{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote' ] },
	{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
	{ name: 'insert', items: [ 'Image', 'Table', 'HorizontalRule', 'SpecialChar', 'filemanager' ] },
	'/',
	{ name: 'styles', items: [ 'Styles', 'Format' ] },
	{ name: 'tools', items: [ 'Maximize' ] },
	{ name: 'others', items: [ '-' ] },
	{ name: 'about', items: [ 'About' ] }]";
    const TOOLBAR_BASIC = "[{ name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] }]";
    const SKIN_DEFAULT = 'moono';
    public $helper = 'formCKEditor';
    public $options = array();

    /**
     * Constructor
     *
     * $spec may be:
     * - string: name of element
     * - array: options with which to configure element
     * - Zend_Config: Zend_Config with options for configuring element
     *
     * @param  string|array|Zend_Config $spec
     * @param  array|Zend_Config $options
     * @throws Zend_Form_Exception if no element name after initialization
     */
    public function __construct($spec, $options = null)
    {
        // defaults when needed
        $this->setCkeToolbar(self::TOOLBAR_DEFAULT);
        $this->setCkeSkin(self::SKIN_DEFAULT);

        parent::__construct($spec, $options);
    }

    /**
     * Sets CKEditor toolbar features
     *
     * @param string $set string with json. For examples check http://ckeditor.com/ckeditor_4.1rc/samples/plugins/toolbar/toolbar.html
     */
    public function setCkeToolbar($set)
    {
        $this->options['ckeToolbar'] = (string)$set;
        return $this;
    }

    /**
     * Sets CKEditor skin
     *
     * @param string $set skin name
     */
    public function setCkeSkin($set)
    {
        $this->options['ckeSkin'] = (string)$set;
        return $this;
    }

    /**
     * Sets Cke plugins
     *
     * @param string $set plugins comma delimited
     */
    public function setCkePlugins($set)
    {
        // when using filemanager plugin we need some serious ..
        if (in_array('filemanager', explode(',', $set))) {
            $view = $this->getView();
            $view->headScript()->appendFile($view->staticHost('js/vendor/jquery.ui.widget.js'));
            $view->headScript()->appendFile($view->staticHost('js/jquery.iframe-transport.js'));
            $view->headScript()->appendFile($view->staticHost('js/jquery.fileupload.js'));
            $view->headScript()->appendFile($view->staticHost('js/fileManager.js'));
            $view->headLink()->appendStylesheet($view->staticHost('css/fileManager.css'));

            $script = "
                    var fileManagerCkeSettings = {
                        ajaxJsonFileListUrl: " . Zend_Json::encode($view->url(array('module' => 'api', 'controller' => 'filemanager', 'action' => 'getlist'), 'default', true)) . ",
                        ajaxJsonUploadUrl:  " . Zend_Json::encode($view->url(array('module' => 'api', 'controller' => 'filemanager', 'action' => 'upload'), 'default', true)) . ",

                        thumbURL:  " . Zend_Json::encode($view->cdn('')) . ",
                        cdnURL:  " . Zend_Json::encode($view->cdn('')) . "
                    }

                ";

            $view->headScript()->appendScript($script);
        }

        $this->options['ckePlugins'] = (string)$set;
        return $this;
    }

    /**
     * Retrieve plugins
     *
     * @return string
     */
    public function getCkePlugins()
    {
        return $this->options['ckePlugins'];
    }

    /**
     * Retrieve CKEditor skin
     *
     * @return string
     */
    public function getCkeSkin()
    {
        return $this->options['ckeSkin'];
    }

    /**
     * Retrieve CKEditor toolbar
     *
     * @return string
     */
    public function getCkeToolbar()
    {
        return $this->options['ckeToolbar'];
    }

}
