<?php

class wrxNext_Form_Element_DatePick extends Zend_Form_Element_Xhtml
{
    /**
     * helper used to initialise given element
     *
     * @var string
     */
    public $helper = 'formDatePickJQueryUI';
}
