<?php

class wrxNext_Form_Element_Tinymce extends Zend_Form_Element_Textarea
{

    /**
     * Use formButton view helper by default
     *
     * @var string
     */
    public $helper = 'formTinymce';
}
