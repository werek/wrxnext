<?php

class wrxNext_Form_Element_JQueryUIAutocomplete extends Zend_Form_Element_Hidden
{

    /**
     * Use formHidden view helper by default
     *
     * @var string
     */
    public $helper = 'formJqueryUIAutocomplete';
    protected $routeName = 'default';
    protected $routeParams = array();
    protected $url = null;
    protected $routeAutocompleteField = 'term';
    protected $responseKeyField = 'id';
    protected $responseNameField = 'name';
    protected $displayCallback = null;

    /**
     * gets route name
     *
     * @return string
     */
    public function getRouteName()
    {
        return $this->routeName;
    }

    /**
     * sets route name
     *
     * @param string $name
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setRouteName($name)
    {
        $this->routeName = $name;
        return $this;
    }

    /**
     * gets route params
     *
     * @return array
     */
    public function getRouteParams()
    {
        return $this->routeParams;
    }

    /**
     * sets route params
     *
     * @param array $params
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setRouteParams($params)
    {
        $this->routeParams = $params;
        return $this;
    }

    /**
     * gets url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * sets url
     *
     * @param string $url
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * gets route autocomplete field used in query requests
     *
     * @return string
     */
    public function getRouteAutocompleteField()
    {
        return $this->routeAutocompleteField;
    }

    /**
     * sets route autocomplete field used in query requests
     *
     * @param string $name
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setRouteAutocompleteField($name)
    {
        $this->routeAutocompleteField = $name;
        return $this;
    }

    /**
     * gets response set key field used to set value
     *
     * @return string
     */
    public function getResponseKeyField()
    {
        return $this->responseKeyField;
    }

    /**
     * sets response set key field used to set value
     *
     * @param string $name
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setResponseKeyField($name)
    {
        $this->responseKeyField = $name;
        return $this;
    }

    /**
     * gets response name field displayed in prompt
     *
     * @return string
     */
    public function getResponseNameField()
    {
        return $this->responseNameField;
    }

    /**
     * sets response name field displayed in prompt
     *
     * @param string $name
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setResponseNameField($name)
    {
        $this->responseNameField = $name;
        return $this;
    }

    /**
     * gets callback to initially populate display value for autocomplete input
     *
     * @return Callable
     */
    public function getDisplayCallback()
    {
        return $this->displayCallback;
    }

    /**
     * sets callback to initially populate display value for autocomplete input
     *
     * @param Callable $callback
     * @return wrxNext_Form_Element_JqueryUIAutocomplete
     */
    public function setDisplayCallback(callable $callback)
    {
        $this->displayCallback = $callback;
        return $this;
    }
}
