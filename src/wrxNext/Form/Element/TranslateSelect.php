<?php

class Admin_Form_Element_TranslateSelect extends Zend_Form_Element_Select
{

    public function init()
    {
        if (Zend_Registry::isRegistered('Zend_Translate')) {
            $translate = Zend_Registry::get('Zend_Translate');
            if ($translate instanceof Zend_Translate) {
                $this->addMultiOptions($translate->getList());
            }
        }
    }
}
