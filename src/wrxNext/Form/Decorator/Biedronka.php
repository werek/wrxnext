<?php

/**
 * decorator for rendering form elements on biedronka.pl frontend
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Form_Decorator_Biedronka extends Zend_Form_Decorator_Abstract
{
    use wrxNext_Trait_Template;

    /**
     * holds "required" text
     *
     * @var string
     */
    protected $_requiredText = '*';

    /**
     * holds required text template
     *
     * @var unknown
     */
    protected $_requiredTag = '<p class="required">%text%</p>';

    /**
     * holds error tag template
     *
     * @var string
     */
    protected $_errorsTag = '<p class="required error"><span class="error-indicator">(*)</span><span class="error">%messages%</span></p>';

    /**
     * holds element template tag
     *
     * @var string
     */
    protected $_elementTag = '%required%%errors% <label for="%id%">%label%</label>%content%';

    /**
     * @inheritdoc
     */
    public function render($content)
    {
        /**
         *
         * @var Zend_Form_Element $element
         */
        $element = $this->getElement();
        if (!$element instanceof Zend_Form_Element) {
            return $content;
        }
        if (null === $element->getView()) {
            return $content;
        }

        $data['content'] = $content;
        if ($element->isRequired()) {
            $requiredText = $element->getView()->translate($this->_requiredText);
            if (!$element->hasErrors()) {
                $data['required'] = $this->_buildTemplate($this->_requiredTag,
                    array(
                        'text' => $requiredText
                    ));
            }
        }
        $data['errors'] = $this->_buildErrors();
        $data['label'] = $element->getLabel();
        $data['id'] = $element->getId();
        $separator = $this->getSeparator();
        $template = $this->getElementTemplate();

        if ($element instanceof Zend_Form_Element_Button ||
            $element instanceof Zend_Form_Element_Submit
        ) {
            $template = '%errors%<button id="%id%" class="btn-more">%label%</button>';
        } elseif ($element instanceof Zend_Form_Element_Hidden) {
            $template = '%content%';
        } elseif ($element instanceof Zend_Form_Element_Checkbox) {
            $template = '%content% ' . str_replace('%content%', '', $template);
        }
        $html = $this->_buildTemplate($template, $data);

        switch ($this->getPlacement()) {
            case self::PREPEND:
                return $html . $separator;
            case self::APPEND:
            default:
                return $separator . $html;
        }
    }

    /**
     * builds errors
     *
     * @return string
     */
    protected function _buildErrors()
    {
        $element = $this->getElement();
        $messages = $element->getMessages();
        if (empty($messages)) {
            return '';
        }

        $data['messages'] = $element->getView()->escape(
            implode(' / ', $element->getMessages()));
        return $this->_buildTemplate($this->_errorsTag, $data);
    }

    /**
     * gets element template
     *
     * @return string
     */
    public function getElementTemplate()
    {
        return $this->_elementTag;
    }

    /**
     * sets error tag template, key for concatenated messages is %messages%
     *
     * @param string $template
     * @return wrxNext_Form_Decorator_Biedronka
     */
    public function setErrorTemplate($template)
    {
        $this->_errorsTag = $template;
        return $this;
    }

    /**
     * gets error tag template, key for concatenated messages is %messages%
     *
     * @return string
     */
    public function getErrorTemplate()
    {
        return $this->_errorsTag;
    }

    /**
     * sets element template keys are:
     *
     * %label% - element label
     * %id% - element id
     * %errors% - element errors
     * %content% - previous decorator content
     *
     * @param string $template
     * @return wrxNext_Form_Decorator_Biedronka
     */
    public function setElementTemplate($template)
    {
        $this->_elementTag = $template;
        return $this;
    }

    /**
     * gets "required" text
     *
     * @return string
     */
    public function getRequiredText()
    {
        return $this->_requiredText;
    }

    /**
     * sets "required" text
     *
     * @param string $content
     * @return wrxNext_Form_Decorator_Biedronka
     */
    public function setRequiredText($content)
    {
        $this->_requiredText = $content;
        return $this;
    }

    /**
     * sets "required" text template
     *
     * @param string $template
     * @return wrxNext_Form_Decorator_Biedronka
     */
    public function setRequiredTextTemplate($template)
    {
        $this->_requiredTag = $template;
        return $this;
    }

    /**
     * gets "required" text template
     *
     * @return string
     */
    public function getRequiredTextTemplate()
    {
        return $this->_requiredTag;
    }
}
