<?php

/**
 * main class for processing page, can instantiate clones of herself
 *
 * @author bweres01
 *
 */
class wrxNext_WebCrawler_Page extends wrxNext_WebCrawler_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds repository of disabled url's
     *
     * @var wrxNext_WebCrawler_UrlCollection
     */
    protected $_disabledUrlCollection = null;

    /**
     * stores link attributes of given tags to process
     *
     * @var array
     */
    protected $_linkTagsAttributes = array(
        'a' => 'href',
        'area' => 'href',
        'audio' => 'src',
        'blockquote' => 'cite',
        'button' => 'formaction',
        'command' => 'icon',
        'del' => 'cite',
        'embed' => 'src',
        'form' => 'action',
        'iframe' => 'src',
        'img' => 'src',
        'input' => 'formaction',
        'ins' => 'cite',
        'link' => 'href',
        'object' => 'data',
        'q' => 'cite',
        'script' => 'src',
        'source' => 'src',
        'track' => 'src',
        'video' => array(
            'src',
            'poster'
        )
    );

    /**
     * holds level number in page fetch
     *
     * @var int
     */
    protected $_level = 0;

    /**
     * holds page baseUrl if one was provided in html
     *
     * @var wrxNext_Url
     */
    protected $_baseUrl = null;

    /**
     * accepted content types, and flag to process html
     *
     * @var array
     */
    protected $_acceptedContentType = array(
        'text/html' => true,
        'text/xml' => false,
        'text/css' => false,
        'application/json' => false,
        'application/xhtml+xml' => true,
        'application/xml' => false,
        'application/javascript' => false
    );

    /**
     * holds content type default extensions
     *
     * @var array
     */
    protected $_contentTypeExtensions = array(
        'text/html' => 'html',
        'text/xml' => 'xml',
        'text/css' => 'css',
        'application/json' => 'json',
        'application/xhtml+xml' => 'html',
        'application/xml' => 'xml',
        'application/javascript' => 'js'
    );

    /**
     * holds current processed url
     *
     * @var wrxNext_Url
     */
    protected $_currentUrl = null;

    /**
     * holds fetched http response object
     *
     * @var Zend_Http_Response
     */
    protected $_httpResponse = null;

    /**
     * holds page content
     *
     * @var string
     */
    protected $_content = null;

    /**
     * handles cloning operations
     */
    public function __clone()
    {
        $this->_currentUrl = clone $this->_currentUrl;
        $this->_baseUrl = null;
        $this->_httpResponse = null;
        $this->_content = null;
    }

    /**
     * sets url to check
     *
     * @param wrxNext_Url $url
     * @return wrxNext_WebCrawler_Page
     */
    public function setUrl($url)
    {
        if (!$url instanceof wrxNext_Url) {
            $url = new wrxNext_Url((string)$url);
        }
        $this->_currentUrl = $url;
        return $this;
    }

    /**
     * launches clone of current page configuration and starts processing given
     * url,
     * returns new url to substitute for given one, and false if url should not
     * change
     *
     * @param string $url
     * @return string
     */
    protected function _processPageForUrl($url)
    {
        $page = clone $this;
        if (!is_null($this->_maxDepth)) {
            if ($this->_level < $this->_maxDepth) {
                $page->_level++;
            } else {
                return false;
            }
        }
        $url = $this->_normaliseUrl($url);
        $url = $this->_filterUrl($url);
        if (is_null($url)) {
            return false;
        }
        // check if given url is already registered, if so no processing is
        // needed
        if ($this->getUrlCollection()->hasUrl($url)) {
            return $this->getUrlCollection()->get($url);
        }
        // check if given url is disabled from processing
        if ($this->getDisabledUrlCollection()->hasUrl($url)) {
            return false;
        }
        $page->getUrl()->parseUrl($url);
        $return = $page->processPage();
        if ($return !== false) {
            $return = $this->getUrlCollection()->get($url);
        }
        return $return;
    }

    /**
     * normalises url, from relative form to domain based.
     * according to current base url. base url could be different after parsing
     * page
     *
     * @param string $url
     * @return string
     */
    protected function _normaliseUrl($url)
    {
        if ($url === "") {
            $url = $this->_currentUrl->buildUrl();
        } elseif (!is_null($url)) {
            $urlObject = new wrxNext_Url($url);
            $baseUrl = $this->_getBaseUrl();
            if (strpos($urlObject->getPath(), '/') !== 0) {
                // given url is relative, normalising to domain base
                $basePath = preg_replace('@/[^/]*?$@', '/', $baseUrl->getPath());
                $urlObject->setPath($basePath . $urlObject->getPath());
                $urlObject->setHost($baseUrl->getHost());
                $urlObject->setScheme($baseUrl->getScheme());
                $url = $urlObject->buildUrl();
            }
            if ($urlObject->getHost() === null) {
                $urlObject->setHost($baseUrl->getHost());
                $urlObject->setScheme($baseUrl->getScheme());
                $url = $urlObject->buildUrl();
            }
        }
        return $url;
    }

    /**
     * returns url object which should be used as baseUrl for normalising other
     * url's
     *
     * @return wrxNext_Url
     */
    protected function _getBaseUrl()
    {
        return is_null($this->_baseUrl) ? $this->_currentUrl : $this->_baseUrl;
    }

    /**
     * filters url
     *
     * @param string $url
     * @return string null if url shold not be processed
     */
    protected function _filterUrl($url)
    {
        $url = $this->getFilter()->filterUrl($url);
        return $this->_allowedToProcess($url);
    }

    /**
     * checks if url is allowed to be processed
     *
     * @param unknown $url
     * @return string null if url should not be processed
     */
    protected function _allowedToProcess($url)
    {
        if (!is_null($url) && $this->_discardExternalDomains) {
            $host = (new wrxNext_Url($url))->getHost();
            if (!is_null($host) && $host != $this->_currentUrl->getHost()) {
                $url = null;
            }
        }
        return $url;
    }

    /**
     * gets disabled url collection
     *
     * @return wrxNext_WebCrawler_UrlCollection
     */
    public function getDisabledUrlCollection()
    {
        if (!$this->_disabledUrlCollection instanceof wrxNext_WebCrawler_UrlCollection) {
            $this->_disabledUrlCollection = new wrxNext_WebCrawler_UrlCollection(
                array());
        }
        return $this->_disabledUrlCollection;
    }

    /**
     * sets disabled url collection
     *
     * @param wrxNext_WebCrawler_UrlCollection $collection
     * @return wrxNext_WebCrawler_Page
     */
    public function setDisabledUrlCollection($collection)
    {
        $this->_disabledUrlCollection = $collection;
        return $this;
    }

    /**
     * gets url to check
     *
     * @return wrxNext_Url
     */
    public function getUrl()
    {
        return $this->_currentUrl;
    }

    /**
     * starts processing of current page
     *
     * @return boolean
     */
    public function processPage()
    {
        $this->_log('starting to process page');
        $this->_log('url: ' . $this->_currentUrl->buildUrl());
        if (!in_array($this->_currentUrl->getScheme(), $this->_allowedSchemes)) {
            $this->_log(
                'given address scheme "' . $this->_currentUrl->getScheme() .
                '" is not supported');
            return false;
        }
        try {
            $response = $this->_fetchPage();
        } catch (Zend_Uri_Exception $e) {
            $this->_log(
                'error in url address while trying to fetch url: ' .
                $this->_currentUrl->buildUrl());
            $this->_log($e);
            return false;
        } catch (Exception $e) {
            $this->_log('error while fetching page, disabling processing');
            $this->_log($e);
            return false;
        }
        $this->_registerUrlAndCreateLocalName();
        if ($response->isRedirect()) {
            // generating javascript redirect
            $this->_log('detected redirect');
            $success = $this->_generateJavascriptRedirect();
            if (!$success) {
                $this->_log('further redirect processing disabled');
                return false;
            }
        } elseif ($this->_isHtmlType()) {
            // here will be html processing
            $this->_log('found html content-type for processing');
            $this->_content = $this->_parseHtml($response->getBody());
            if ($this->_content === false) {
                return false;
            }
        } elseif (!$this->_isAcceptedContentType()) {
            $this->_log(
                'content-type unsupported: ' .
                $this->_getResponseContentType());
            return false;
        } else {
            $this->_content = $response->getBody();
        }
        // saving fetched content
        $this->_savePage();
        return true;
    }

    /**
     * fetches page from current url, also keeping response
     *
     * @return Zend_Http_Response
     */
    protected function _fetchPage()
    {
        $httpClient = $this->getHttpClient();
        // reseting any previous parameters, keeping non esential headers
        $httpClient->resetParameters();
        $httpClient->setUri($this->_currentUrl->buildUrl());
        $this->_log('fetching page: ' . $httpClient->getUri(true));
        $this->_httpResponse = $httpClient->request('GET');
        return $this->_httpResponse;
    }

    /**
     * registers url and its local mapping, returns local mapping
     *
     * @return string
     */
    protected function _registerUrlAndCreateLocalName()
    {
        $localPath = $this->_convertUrlToLocal($this->_currentUrl);
        $this->_log('local representation of current url: ' . $localPath);
        // workaround for making html work without '.html' extension in server
        // enviroment
        if ($this->_isHtmlType() && !preg_match('@/$@', $localPath)) {
            $localPath .= '/';
        }
        $filename = preg_match('@/$@', $localPath) ? 'index' : basename(
            $localPath);
        $dirname = preg_match('@/$@', $localPath) ? $localPath : dirname(
                $localPath) . '/';

        // adding missing extension
        $fileExtension = $this->_getResponseContentTypeExtension(true);
        if (!preg_match('@' . preg_quote($fileExtension) . '$@', $filename)) {
            $filename .= $fileExtension;
        }

        // adding processed url to collection
        $newUrl = $dirname . $filename;
        $this->_log(
            'saving url mapping in collection, source "' .
            $this->_currentUrl->buildUrl() . '", replace "' .
            $newUrl . '"');
        $this->getUrlCollection()->set($this->_currentUrl, $newUrl);
        return $newUrl;
    }

    /**
     * converts given url to local (relative, domain based) also escaping
     * special characters not allowable on filesystem
     *
     * @param unknown $url
     */
    protected function _convertUrlToLocal($url)
    {
        $url = new wrxNext_Url($url);
        $url->setHost(null);
        $urlString = $url->buildUrl();
        return $urlString;
    }

    /**
     * returns status of html content type
     *
     * @return boolean
     */
    protected function _isHtmlType()
    {
        $contentType = $this->_getResponseContentType();
        if (array_key_exists($contentType, $this->_acceptedContentType)) {
            return $this->_acceptedContentType[$contentType];
        }
        return false;
    }

    /**
     * returns response content type
     *
     * @return string
     */
    protected function _getResponseContentType()
    {
        $contentType = $this->_httpResponse->getHeader('content-type');
        $contentType = trim(explode(';', $contentType)[0]);
        return $contentType;
    }

    /**
     * return default extension associated with response content type
     *
     * @param bool $addDot
     * @return string NULL
     */
    protected function _getResponseContentTypeExtension($addDot = false)
    {
        $contentType = $this->_getResponseContentType();
        if (array_key_exists($contentType, $this->_contentTypeExtensions)) {
            $extension = $this->_contentTypeExtensions[$contentType];
            if ($addDot) {
                $extension = '.' . $extension;
            }
            return $extension;
        }
        return null;
    }

    /**
     * generates javascript redirect body
     *
     * @throws wrxNext_WebCrawler_Page_Exception
     * @return boolean
     */
    protected function _generateJavascriptRedirect()
    {
        $location = $this->_httpResponse->getHeader('location');
        $this->_log('redirect location: ' . $location);
        // normalising url
        $url = $this->_normaliseUrl($location);
        if (is_null($url)) {
            throw new wrxNext_WebCrawler_Page_Exception(
                'location header is not set on response from fetched page');
        }
        // filtering given url should be processed
        $url = $this->_filterUrl($url);
        if (is_null($url)) {
            // url should be disabled
            $this->_log('processing of url is disabled');
            return false;
        }

        // if url exists then using its saved data
        if ($this->getUrlCollection()->hasUrl($url)) {
            $this->_log('url exists in collection: ' . $url);
            $url = $this->getUrlCollection()->get($url);
            $this->_log('redirect now maps to collection derived url: ' . $url);
        } else {
            // processing redirect
            $this->_log('sending redirect url for further processing: ' . $url);
            $this->_processPageForUrl($url);
        }

        // clearing domain to generate relative url
        $urlObject = new wrxNext_Url($url);
        $urlObject->setHost(null);
        $redirectUrl = $urlObject->buildUrl();

        // checking if it is not self redirect if so then redirecting to main
        // domain
        $locationObject = new wrxNext_Url();
        $locationObject->setHost(null);
        $compareUrl = $locationObject->buildUrl();
        if ($redirectUrl == $compareUrl) {
            $redirectUrl = '/';
        }

        $scriptRedirect = '<!DOCTYPE html><html><head><script type="text/javascript">window.location.replace(%s)</script></head><body></body></html>';
        $this->_content = sprintf($scriptRedirect,
            Zend_Json::encode($redirectUrl));

        return true;
    }

    /**
     * parses html
     *
     * @param unknown $html
     * @return unknown
     */
    protected function _parseHtml($html)
    {
        // prefiltering html
        $html = $this->getFilter()->preFilterHtml($html);

        // link processing
        $domHtml = $this->_htmlToDOMDocument($html);
        $this->_setupBaseUrl($domHtml);
        $this->_procesDOMHtmlLinks($domHtml);
        $html = $domHtml->saveHTML();
        if ($html === false) {
            throw new wrxNext_WebCrawler_Page_Exception(
                'there was an error during exporting domdocument to html');
        }

        // post filtering html
        $html = $this->getFilter()->postFilterHtml($html);
        return $html;
    }

    /**
     * loads html string and converts it to UTF-8 DOMDocument
     *
     * @param unknown $html
     * @return DOMDocument
     */
    protected function _htmlToDOMDocument($html)
    {
        $dom = new DOMDocument();
        @$dom->loadHTML('<?xml encoding="UTF-8">' . $html);

        foreach ($dom->childNodes as $item)
            if ($item->nodeType == XML_PI_NODE)
                $dom->removeChild($item);
        $dom->encoding = 'UTF-8'; // reset original encoding
        return $dom;
    }

    /**
     * setups base url from html DOM document
     *
     * @param DOMDocument $dom
     */
    protected function _setupBaseUrl(DOMDocument $dom)
    {
        $baseNodeList = $dom->getElementsByTagName('base');
        if ($baseNodeList->length > 0) {
            $baseNode = $baseNodeList->item(0);
            if ($baseNode->hasAttribute('href')) {
                $baseUrl = $baseNode->getAttribute('href');
                if (!empty($baseUrl)) {
                    $this->_baseUrl = new wrxNext_Url($baseUrl);
                    $this->_log('found base url: ' . $baseUrl);
                    $this->_log('removing base tag');
                    $dom->removeChild($baseNode);
                }
            }
        }
    }

    /**
     * processes all links in DOMDocument
     *
     * @param DOMDocument $dom
     */
    protected function _procesDOMHtmlLinks(DOMDocument $dom)
    {
        $this->_log('starting to process html links');
        foreach ($this->_linkTagsAttributes as $tagName => $attributes) {
            $attributes = (array)$attributes;
            $this->_log(
                'searching html for tag "' . $tagName .
                '", then checking following attributes for url: ' .
                implode(', ', $attributes));
            foreach ($dom->getElementsByTagName($tagName) as $domElement) {
                foreach ($attributes as $attributeName) {
                    if ($domElement instanceof DOMElement) {
                        // check for existence of dom attribute
                        if ($domElement->hasAttribute($attributeName)) {
                            $this->_log(
                                'found "' . $attributeName . '" on tag "' .
                                $tagName . '"');
                            $url = $domElement->getAttribute($attributeName);
                            $this->_log(
                                'running page processing for url: ' . $url);
                            $urlCheck = $this->_normaliseUrl($url);
                            if (preg_match('@^#@i', 'url')) {
                                $this->_log(
                                    'detected local anchor form, disabling processing and change');
                            } elseif ($this->getDisabledUrlCollection()->hasUrl(
                                $urlCheck)
                            ) {
                                $this->_log('url processing is disabled');
                            } elseif ($this->getUrlCollection()->hasUrl(
                                $urlCheck)
                            ) {
                                $replace = $this->getUrlCollection()->get(
                                    $urlCheck);
                                $domElement->setAttribute($attributeName,
                                    $replace);
                                $this->_log(
                                    'page already registered in collection, using local name url: ' .
                                    $replace);
                            } else {
                                $replace = $this->_processPageForUrl($url);
                                if ($replace === false) {
                                    $this->_log(
                                        'page processing disabled change, leaving url as is. adding to disabled list');
                                    $this->getDisabledUrlCollection()->set(
                                        $urlCheck, true);
                                } else {
                                    $this->_log(
                                        'changing url from "' . $url .
                                        '" to "' . $replace . '"');
                                    $domElement->setAttribute($attributeName,
                                        $replace);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * returns status if given content type is accepted
     *
     * @return boolean
     */
    protected function _isAcceptedContentType()
    {
        $contentType = $this->_getResponseContentType();
        return array_key_exists($contentType, $this->_acceptedContentType);
    }

    /**
     * save page to disk and updates urlcollection
     *
     * @throws wrxNext_WebCrawler_Page_Exception
     */
    protected function _savePage()
    {
        $savePath = $this->getSavePath();
        if (!is_dir($savePath)) {
            throw new wrxNext_WebCrawler_Page_Exception(
                "'" . $savePath . "' is not a directory");
        }
        if (!is_writable($savePath)) {
            throw new wrxNext_WebCrawler_Page_Exception(
                "directory '" . $savePath . "' is not writable");
        }

        $localPath = $this->getUrlCollection()->get($this->_currentUrl);
        $this->_log(
            'local representation of url: "' . $this->_currentUrl->buildUrl() .
            '" is "' . $localPath . '"');

        $filename = basename($localPath);
        $dirname = dirname($localPath) . '/';

        // checking if given directory exists
        $destinationDirectory = $savePath . $dirname;
        if (!file_exists($destinationDirectory)) {
            if (!mkdir($destinationDirectory, 0777, true)) {
                throw new wrxNext_WebCrawler_Page_Exception(
                    'could not create directory: ' . $destinationDirectory);
            }
        } else {
            if (!is_writable($destinationDirectory)) {
                throw new wrxNext_WebCrawler_Page_Exception(
                    'directory is not writable: ' . $destinationDirectory);
            }
        }

        // writing page content to file
        $filePath = $savePath . $localPath;
        if (file_put_contents($filePath, $this->_content) === false) {
            throw new wrxNext_WebCrawler_Page_Exception(
                'there was a problem during writing content to file: ' .
                $filePath);
        }
    }
}
