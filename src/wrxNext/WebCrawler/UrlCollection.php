<?php

/**
 * holds collection of url's mapped to new ones
 *
 * @author bweres01
 *
 */
class wrxNext_WebCrawler_UrlCollection extends wrxNext_Collection_Abstract
{

    use wrxNext_Trait_RegistryLog;

    /**
     * @inheritdoc
     */
    public function get($url)
    {
        return parent::get($this->_filterUrl($url));
    }

    /**
     * handles key filtering
     *
     * @param string $url
     * @return string
     */
    protected function _filterUrl($url)
    {
        if ($url instanceof wrxNext_Url) {
            $url = $url->buildUrl();
        } else {
            $url = (string)$url;
        }
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function set($url, $value)
    {
        return parent::set($this->_filterUrl($url), $value);
    }

    /**
     * checks if url is present in collection
     *
     * @param mixed $url
     * @return boolean
     */
    public function hasUrl($url)
    {
        return $this->hasKey($this->_filterUrl($url));
    }
}
