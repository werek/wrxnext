<?php

/**
 * abstract class containing common methods for WebCrawler functionalities
 *
 * @author bweres01
 *
 */
abstract class wrxNext_WebCrawler_Abstract
{

    /**
     * url collection
     *
     * @var wrxNext_WebCrawler_UrlCollection
     */
    protected $_urlCollection = null;

    /**
     * holds list of allowed schemes
     *
     * @var array
     */
    protected $_allowedSchemes = array(
        'http'
    );

    /**
     * flag for allowing to process url from different domain tha starting one
     *
     * @var boolean
     */
    protected $_discardExternalDomains = true;

    /**
     * holds filter instance
     *
     * @var wrxNext_WebCrawler_Filter_Interface
     */
    protected $_filter = null;

    /**
     * holds http client instance
     *
     * @var Zend_Http_Client
     */
    protected $_httpClient = null;

    /**
     * holds max depth of webcrawler links, null states as inifinte
     *
     * @var int
     */
    protected $_maxDepth = null;

    /**
     * holds directory to save pages
     *
     * @var string
     */
    protected $_savePath = null;

    /**
     * gets filter object
     *
     * @return wrxNext_WebCrawler_Filter_Interface
     */
    public function getFilter()
    {
        return $this->_filter;
    }

    /**
     * sets filter object
     *
     * @param wrxNext_WebCrawler_Filter_Interface $filter
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setFilter($filter)
    {
        $this->_filter = $filter;
        return $this;
    }

    /**
     * gets url collection
     *
     * @return wrxNext_WebCrawler_UrlCollection
     */
    public function getUrlCollection()
    {
        return $this->_urlCollection;
    }

    /**
     * sets url collection
     *
     * @param wrxNext_WebCrawler_UrlCollection $collection
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setUrlCollection($collection)
    {
        $this->_urlCollection = $collection;
        return $this;
    }

    /**
     * gets http client
     *
     * @return Zend_Http_Client
     */
    public function getHttpClient()
    {
        return $this->_httpClient;
    }

    /**
     * sets http client
     *
     * @param Zend_Http_Client $client
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setHttpClient($client)
    {
        $this->_httpClient = $client;
        return $this;
    }

    /**
     * gets flag to discard external domains
     *
     * @return boolean
     */
    public function getDiscardExternalDomains()
    {
        return $this->_discardExternalDomains;
    }

    /**
     * sets flag to discard external domains
     *
     * @param boolean $discard
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setDiscardExternalDomains($discard)
    {
        $this->_discardExternalDomains = $discard;
        return $this;
    }

    /**
     * gets allowed schemes
     *
     * @return array
     */
    public function getAllowedSchemes()
    {
        return $this->_allowedSchemes;
    }

    /**
     * sets allowed schemes
     *
     * @param array $schemes
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setAllowedSchemes($schemes)
    {
        $this->_allowedSchemes = $schemes;
        return $this;
    }

    /**
     * gets max depth
     *
     * @return int
     */
    public function getMaxDepth()
    {
        return $this->_maxDepth;
    }

    /**
     * sets max depth
     *
     * @param int $depth
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setMaxDepth($depth)
    {
        $this->_maxDepth = $depth;
        return $this;
    }

    /**
     * gets save path
     *
     * @return string
     */
    public function getSavePath()
    {
        return $this->_savePath;
    }

    /**
     * sets save path
     *
     * @param string $path
     * @return wrxNext_WebCrawler_Abstract
     */
    public function setSavePath($path)
    {
        $this->_savePath = $path;
        return $this;
    }
}
