<?php

/**
 * specific filter allowing using several filters in place of one, used as default if no other filter is defined
 *
 * @author bweres01
 *
 */
class wrxNext_WebCrawler_Filter_Stack extends wrxNext_WebCrawler_Filter_Abstract
{

    /**
     * holds filter stack
     *
     * @var array
     */
    protected $_filtersStack = array();

    /**
     * adds filter to stack
     *
     * @param wrxNext_WebCrawler_Filter_Interface $filter
     * @param string $name
     *            override default name - filter class name
     * @return wrxNext_WebCrawler_Filter_Stack
     */
    public function addFilter(wrxNext_WebCrawler_Filter_Interface $filter,
                              $name = null)
    {
        $name = is_null($name) ? get_class($filter) : $name;
        $this->_filtersStack[$name] = $filter;
        return $this;
    }

    /**
     * removes filter from stack
     *
     * @param string $name
     * @throws wrxNext_WebCrawler_Filter_Exception
     * @return wrxNext_WebCrawler_Filter_Stack
     */
    public function removeFilter($name)
    {
        if (!$this->hasFilter($name)) {
            throw new wrxNext_WebCrawler_Filter_Exception(
                "filter with name '" . $name . "' is not registered in stack");
        }
        unset($this->_filtersStack[$name]);
        return $this;
    }

    /**
     * check if filter is registered
     *
     * @param string $name
     * @return boolean
     */
    public function hasFilter($name)
    {
        return isset($this->_filtersStack[$name]);
    }

    /**
     * @inheritdoc
     */
    public function filterUrl($url)
    {
        foreach ($this->getFiltersStack() as $filter) {
            $url = $filter->filterUrl($url);
            if (is_null($url)) {
                break;
            }
        }
        return $url;
    }

    /**
     * gets filter stack
     *
     * @return wrxNext_WebCrawler_Filter_Interface[]
     */
    public function getFiltersStack()
    {
        return $this->_filtersStack;
    }

    /**
     * sets filter stack
     *
     * @param wrxNext_WebCrawler_Filter_Interface[] $stack
     * @return wrxNext_WebCrawler_Filter_Stack
     */
    public function setFiltersStack($stack)
    {
        $this->_filtersStack = (array)$stack;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function preFilterHtml($html)
    {
        foreach ($this->getFiltersStack() as $filter) {
            $html = $filter->preFilterHtml($html);
        }
        return $html;
    }

    /**
     * @inheritdoc
     */
    public function postFilterHtml($html)
    {
        foreach ($this->getFiltersStack() as $filter) {
            $html = $filter->postFilterHtml($html);
        }
        return $html;
    }
}
