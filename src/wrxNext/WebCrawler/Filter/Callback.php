<?php

/**
 * webcrawler callback filter gives the ability to define callback for each processing stage of webcrawler page
 *
 * @author bweres01
 *
 */
class wrxNext_WebCrawler_Filter_Callback extends wrxNext_WebCrawler_Filter_Abstract
{

    /**
     * holds url filter callback
     *
     * @var callable
     */
    protected $_urlCallback = null;

    /**
     * holds pre processing html filter callback
     *
     * @var callable
     */
    protected $_preHtmlCallback = null;

    /**
     * holds post processing html filter callback
     *
     * @var callable
     */
    protected $_postHtmlCallback = null;

    /**
     * sets url filter callback
     *
     * @param callable $callback
     * @return wrxNext_WebCrawler_Filter_Callback
     */
    public function setCallbackFilterUrl($callback)
    {
        $this->_urlCallback = $callback;
        return $this;
    }

    /**
     * gets url filter callback
     *
     * @return callable
     */
    public function getCallbackFilterUrl()
    {
        return $this->_urlCallback;
    }

    /**
     * sets pre html callback
     *
     * @param callable $callback
     * @return wrxNext_WebCrawler_Filter_Callback
     */
    public function setCallbackPreFilterHtml($callback)
    {
        $this->_preHtmlCallback = $callback;
        return $this;
    }

    /**
     * gets pre html callback
     *
     * @return callable
     */
    public function getCallbackPreFilterHtml()
    {
        return $this->_preHtmlCallback;
    }

    /**
     * sets post html callback
     *
     * @param callable $callback
     * @return wrxNext_WebCrawler_Filter_Callback
     */
    public function setCallbackPostFilterHtml($callback)
    {
        $this->_postHtmlCallback = $callback;
        return $this;
    }

    /**
     * gets post html callback
     *
     * @return callable
     */
    public function getCallbackPostFilterHtml()
    {
        return $this->_postHtmlCallback;
    }

    /**
     * @inheritdoc
     */
    public function filterUrl($url)
    {
        if (is_callable($this->_urlCallback)) {
            $url = call_user_func_array($this->_urlCallback,
                array(
                    $url
                ));
        }
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function preFilterHtml($html)
    {
        if (is_callable($this->_preHtmlCallback)) {
            $html = call_user_func_array($this->_preHtmlCallback,
                array(
                    $html
                ));
        }
        return $html;
    }

    /**
     * @inheritdoc
     */
    public function postFilterHtml($html)
    {
        if (is_callable($this->_postHtmlCallback)) {
            $html = call_user_func_array($this->_postHtmlCallback,
                array(
                    $html
                ));
        }
        return $html;
    }
}
