<?php

interface wrxNext_WebCrawler_Filter_Interface
{

    /**
     * filters url before crawler starts processing it, returns null if url
     * should not be processed
     *
     * @param string|wrxNext_Url $url
     * @return string null if given url should not be processed
     */
    public function filterUrl($url);

    /**
     * filters html before crawler start processing its content
     *
     * @param string $html
     * @return string filtered html
     */
    public function preFilterHtml($html);

    /**
     * filters html after crawler has processed its content
     *
     * @param string $html
     * @return string filtered html
     */
    public function postFilterHtml($html);
}
