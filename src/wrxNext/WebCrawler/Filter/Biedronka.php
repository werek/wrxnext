<?php

class wrxNext_WebCrawler_Filter_Biedronka extends wrxNext_WebCrawler_Filter_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * @inheritdoc
     */
    public function filterUrl($url)
    {
        // check for shops url with params
        $urlObject = new wrxNext_Url($url);
        if (preg_match('@^/pl/sklepy@i', $urlObject->getPath())) {
            $urlObject->setPath('/pl/sklepy');
            return $urlObject->buildUrl();
        }
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function preFilterHtml($html)
    {
        // parsing document
        $domDocument = $this->_htmlToDOMDocument($html);
        // xpath
        $xpath = new DOMXPath($domDocument);

        // finding all form element and removing them
        foreach ($xpath->query('//form') as $formNode) {
            $formNode->parentNode->removeChild($formNode);
        }

        // saving processed html
        $html = $domDocument->saveHTML();
        return $html;
    }

    /**
     * loads html string and converts it to UTF-8 DOMDocument
     *
     * @param string $html
     * @return DOMDocument
     */
    protected function _htmlToDOMDocument($html)
    {
        $dom = new DOMDocument();
        @$dom->loadHTML('<?xml encoding="UTF-8">' . $html);

        foreach ($dom->childNodes as $item)
            if ($item->nodeType == XML_PI_NODE)
                $dom->removeChild($item);
        $dom->encoding = 'UTF-8'; // reset original encoding
        return $dom;
    }
}
