<?php

/**
 * abstract class allowing to extend only part of filter functionality
 *
 * @author bweres01
 *
 */
abstract class wrxNext_WebCrawler_Filter_Abstract implements
    wrxNext_WebCrawler_Filter_Interface
{

    /**
     * @inheritdoc
     */
    public function filterUrl($url)
    {
        return $url;
    }

    /**
     * @inheritdoc
     */
    public function preFilterHtml($html)
    {
        return $html;
    }

    /**
     * @inheritdoc
     */
    public function postFilterHtml($html)
    {
        return $html;
    }
}
