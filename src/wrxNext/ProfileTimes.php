<?php

class wrxNext_ProfileTimes
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds instance of profiler
     *
     * @var wrxNext_ProfileTimes
     */
    protected static $_instance = NULL;
    /**
     * holds clas initialisation time
     *
     * @var float
     */
    protected $_startTime = 0;
    /**
     * how many round to use in display
     *
     * @var int
     */
    protected $_round = 5;
    /**
     * holds all marks
     *
     * @var array
     */
    protected $_marks = array();

    /**
     * constructor
     */
    public function __construct()
    {
        $this->_startTime = microtime(true);
    }

    /**
     * returns instance of profiler
     *
     * @return wrxNext_ProfileTimes
     */
    public static function instance()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * returns mark with time from initialisation
     */
    public function toArray()
    {
        return $this->_marks;
    }

    /**
     * returns string representation of given marks, separated by $br
     *
     * @param string $br
     * @return string
     */
    public function toString($br = "\n")
    {
        $rendered = array();
        foreach ($this->_marks as $mark => $time) {
            $rendered[] = $mark . ': ' . round($time, $this->_round) . 's';
        }
        return implode($br, $rendered);
    }

    /**
     * registers shutdown function
     *
     * @return wrxNext_ProfileTimes
     */
    public function registerShutdownFunction()
    {
        $pt = $this;
        register_shutdown_function(
            function () use ($pt) {
                $pt->shutdown();
            });
        return $this;
    }

    /**
     * shutdown function
     *
     * @return wrxNext_ProfileTimes
     */
    public function shutdown()
    {
        $this->mark('FINAL_SHUTDOWN');
        $this->_log("total included files:\n" . implode("\n", get_included_files()));
        return $this;
    }

    /**
     * marks time, throws exception if given mark is registered
     *
     * @param string $name
     * @throws Exception
     * @return wrxNext_ProfileTimes
     */
    public function mark($name)
    {
        if ($this->hasMark($name)) {
            throw new Exception(
                'time mark "' . $name . '" is already registered');
        }
        $current = microtime(true);
        $this->_marks[$name] = $current - $this->_startTime;
        $this->_log(
            $name . ': ' . round($this->_marks[$name], $this->_round) . 's');
        return $this;
    }

    /**
     * checks if given mark name is registered
     *
     * @param string $name
     * @return boolean
     */
    public function hasMark($name)
    {
        return array_key_exists($name, $this->_marks);
    }
}
