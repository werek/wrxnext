<?php

/**
 * klasa automatycznie wyświetlajaca array jako tablice html
 *
 */
class wrxNext_TableArray
{

    private $_array = array();

    private $_tableClass = null;

    private $_rowClass = null;

    private $_rowClassCount = 0;

    private $_columnClass = null;

    private $_columnClassCount = 0;

    /**
     * konstruktor opcjonalnie przyjmujacy array na wejsciu
     *
     * @param array $array
     */
    public function __construct($array = null)
    {
        if (!is_null($array)) {
            $this->setArray($array);
        }
    }

    /**
     * ustawia zrodlowa tablice
     *
     * @param array $array
     * @return wrxNext_TableArray
     */
    public function setArray($array)
    {
        $this->_array = $array;
        return $this;
    }

    /**
     * ustala klase css wiersza, opcjonalnie tablice klas css ktore
     * beda sie rotowaly w trakcie wyswietlania
     *
     * @param string|array $rowClass
     * @return wrxNext_TableArray
     */
    public function setRowClass($rowClass = null)
    {
        if (is_string($rowClass) || is_array($rowClass)) {
            $this->_rowClass = $rowClass;
        } elseif (is_null($rowClass)) {
            $this->_rowClass = null;
        }
        return $this;
    }

    /**
     * ustala klase css kolumny, opcjonalnie tablice klas css ktore
     * bada sie rotowaly w trakcie wyswietlania
     *
     * @param string|array $columnClass
     * @return wrxNext_TableArray
     */
    public function setColumnClass($columnClass)
    {
        if (is_string($columnClass) || is_array($columnClass)) {
            $this->_columnClass = $columnClass;
        } elseif (is_null($columnClass)) {
            $this->_columnClass = null;
        }
        return $this;
    }

    /**
     * ustala klase css dla tablicy
     *
     * @param string $tableClass
     * @return wrxNext_TableArray
     */
    public function setTableClass($tableClass = null)
    {
        $this->_tableClass = $tableClass;
        return $this;
    }

    /**
     * zwraca kod html tablicy
     *
     * @return string
     */
    public function getCode()
    {
        $str = "";
        for ($i = 0, $i_ = count($this->_array); $i < $i_; $i++) {
            $currentRow = $this->_array[$i];
        }
        return $str;
    }

    /**
     * metoda zwracajaca aktualna klase dla wiersza
     *
     * @param bool $iterate
     * @return string
     */
    private function currentRowClass($iterate = false)
    {
        $c = null;
        if (is_array($this->_rowClass)) {
            if (count($this->_rowClass) > 1) {
                $c = $this->_rowClass[$this->_rowClassCount];
                if ($iterate) {
                    if (($this->_rowClassCount + 1) >= count($this->_rowClass)) {
                        $this->_rowClassCount = 0;
                    } else {
                        $this->_rowClassCount++;
                    }
                }
            }
        } else {
            $c = $this->_rowClass;
        }
        return $c;
    }
}
