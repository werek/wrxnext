<?php

/**
 * class for action helper related problems
 *
 * @author bweres01
 *
 */
class wrxNext_Controller_Action_Helper_Exception extends wrxNext_Controller_Action_Exception
{
}
