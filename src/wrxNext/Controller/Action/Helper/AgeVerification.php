<?php

/**
 * action helper for age verification
 *
 * usage of this helper in action is fairly simple, just invoke verifyAge method to
 * check if current user is old enough according to setup'ed adapters.
 *
 * (inside controller)
 * <code>
 * if($this->_helper
 *          ->ageVerification()
 *          ->verifyAge($this->getRequest())
 *          ->getResult() > 0){
 *      // age verification successfull, user is an adult
 * } else {
 *      // age verification failed, this is good time to show user age verification form
 * }
 * </code>
 *
 * To add adapters use addAdapter method
 *
 * @author bweres01
 *
 */
class wrxNext_Controller_Action_Helper_AgeVerification extends Zend_Controller_Action_Helper_Abstract
{

    /**
     * stores adapter objects
     *
     * @var ArrayObject
     */
    protected $_adapters = null;

    /**
     * constructor, setups adapters storage
     */
    public function __construct()
    {
        if (Zend_Registry::isRegistered(__CLASS__)) {
            $this->_adapters = Zend_Registry::get(__CLASS__);
        } else {
            $this->_adapters = new ArrayObject(array());
            Zend_Registry::set(__CLASS__, $this->_adapters);
        }
    }

    /**
     * returns ageverification plugin instance
     *
     * @return wrxNext_Controller_Action_Helper_AgeVerification
     */
    function direct()
    {
        return $this;
    }

    /**
     * adds adapter to stack
     *
     * @param wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface $adapter
     * @param string $name
     * @return wrxNext_Controller_Action_Helper_AgeVerification
     */
    public function addAdapter(
        wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface $adapter,
        $name = null)
    {
        if (is_null($name)) {
            $name = get_class($adapter);
        }
        $this->_adapters[$name] = $adapter;
        return $this;
    }

    /**
     * gets adapters
     *
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface[]
     */
    public function getAdapters()
    {
        return $this->_adapters;
    }

    /**
     * sets adapters
     *
     * @param wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface[] $adapters
     * @return wrxNext_Controller_Action_Helper_AgeVerification
     */
    public function setAdapters($adapters)
    {
        $this->_adapters = $adapters;
        return $this;
    }

    /**
     * verifies given request against adapter to accertain user age
     *
     * @param Zend_Controller_Request_Http $request
     * @param string $adapter
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Result
     */
    public function verifyAge(Zend_Controller_Request_Http $request, $adapter)
    {
        return $this->getAdapter($adapter)->verify($request);
    }

    /**
     * retrieves adapter with given name
     *
     * @param string $name
     * @throws wrxNext_Controller_Action_Helper_AgeVerification_Exception
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface
     */
    public function getAdapter($name)
    {
        if (!$this->_adapters->offsetExists($name)) {
            throw new wrxNext_Controller_Action_Helper_AgeVerification_Exception(
                "no adapter with name '" . $name . '" was found');
        }
        return $this->_adapters->offsetGet($name);
    }
}
