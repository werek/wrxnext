<?php

interface wrxNext_Controller_Action_Helper_DisplayType_EntityInterface
{

    /**
     * returns display type codename
     *
     * @see bd_Model_ProductType
     * @return string null if display type is not set
     */
    public function displayType();
}
