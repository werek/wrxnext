<?php

interface wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface
{

    /**
     * returns display type based on provided entity.
     *
     * @param mixed $entity
     * @return string|null returns null if not set, empty string if default, otherwise
     *         displaytype codename
     */
    public function fromEntity($entity);

    /**
     * returns display type based on provided request object
     *
     * @param Zend_Controller_Request_Abstract $request
     * @return string|null returns null if not set, empty string if default, otherwise
     *         displaytype codename
     */
    public function fromRequest(Zend_Controller_Request_Abstract $request);
}
