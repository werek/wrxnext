<?php

class wrxNext_Controller_Action_Helper_DisplayType_Adapter_Callback implements
    wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface
{

    /**
     * holds entity callback
     *
     * @var Callable
     */
    protected $_entityCallback = null;

    /**
     * holds request Callback
     *
     * @var Callable
     */
    protected $_requestCallback = null;

    /**
     * constructor, takes callback for entity/request checks
     *
     * @param callable $entityCallback
     * @param callable $requestCallback
     */
    public function __construct(callable $entityCallback,
                                callable $requestCallback)
    {
        $this->_entityCallback = $entityCallback;
        $this->_requestCallback = $requestCallback;
    }

    /**
     * gets entity callback
     *
     * @return Callable
     */
    public function getEntityCallback()
    {
        return $this->_entityCallback;
    }

    /**
     * sets entity callback
     *
     * @param Callable $callback
     * @return wrxNext_Controller_Action_Helper_DisplayType_Adapter_Callback
     */
    public function setEntityCallback(Callable $callback)
    {
        $this->_entityCallback = $callback;
        return $this;
    }

    /**
     * gets request callback
     *
     * @return Callable
     */
    public function getRequestCallback()
    {
        return $this->_requestCallback;
    }

    /**
     * sets request callback
     *
     * @param Callable $callback
     * @return wrxNext_Controller_Action_Helper_DisplayType_Adapter_Callback
     */
    public function setRequestCallback(Callable $callback)
    {
        $this->_requestCallback = $callback;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function fromEntity($entity)
    {
        return call_user_func_array($this->_entityCallback,
            array(
                $entity
            ));
    }

    /**
     * @inheritdoc
     */
    public function fromRequest(Zend_Controller_Request_Abstract $request)
    {
        return call_user_func_array($this->_requestCallback,
            array(
                $request
            ));
    }
}
