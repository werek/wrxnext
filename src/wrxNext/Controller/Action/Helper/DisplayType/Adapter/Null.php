<?php

class wrxNext_Controller_Action_Helper_DisplayType_Adapter_Null implements
    wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface
{

    /**
     * @inheritdoc
     */
    public function fromEntity($entity)
    {
        return null;
    }

    /**
     * @inheritdoc
     */
    public function fromRequest(Zend_Controller_Request_Abstract $request)
    {
        return null;
    }
}
