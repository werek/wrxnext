<?php

interface wrxNext_Controller_Action_Helper_DisplayType_EntityTypeInterface extends
    wrxNext_Controller_Action_Helper_DisplayType_EntityInterface
{

    /**
     * returns named entity type
     *
     * @return string
     */
    public function displayEntityType();
}
