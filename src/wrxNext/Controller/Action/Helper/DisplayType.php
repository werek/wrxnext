<?php

class wrxNext_Controller_Action_Helper_DisplayType extends Zend_Controller_Action_Helper_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * holds displaytype check adapter
     *
     * @var wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface
     */
    protected $_adapter = null;

    /**
     * holds display type codename to use
     *
     * @var string
     */
    protected $_displayType = null;

    /**
     * holds startup suffix
     *
     * @var string
     */
    protected $_startupSuffix = null;

    /**
     * constructor, accepts adapter
     *
     * @param wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface $adapter
     */
    public function __construct(
        wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface $adapter = null)
    {
        $this->_adapter = $adapter;
    }

    /**
     * gets display type codename
     *
     * @return string
     */
    public function getDisplayType()
    {
        return $this->_displayType;
    }

    /**
     * sets display type codename
     *
     * @param string $codename
     * @return wrxNext_Controller_Action_Helper_DisplayType
     */
    public function setDisplayType($codename)
    {
        $this->_displayType = (string)$codename;
        return $this;
    }

    /**
     * direct callback on action stack helper
     *
     * @param mixed $entity
     * @param boolean $use
     *            whether to save it internally to future use
     * @return string returns null if not set, empty string if default,
     *         otherwise displaytype codename
     */
    public function direct($entity, $use = TRUE)
    {
        $displayType = $this->_displayType;
        $this->_log(
            'checking entity: ' . (is_object($entity) ? get_class($entity) : gettype(
                $entity)));
        if (is_string($entity)) {
            $displayType = $entity;
        } else {
            $displayType = $this->getAdapter()->fromEntity($entity);
        }
        if ($use) {
            $this->_log(
                'using display type:' .
                (is_null($displayType) ? 'NULL' : $displayType));
            $this->_displayType = $displayType;
        }
        return $displayType;
    }

    /**
     * gets displaytype adapter
     *
     * @return wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface
     */
    public function getAdapter()
    {
        if (is_null($this->_adapter)) {
            $this->_adapter = new wrxNext_Controller_Action_Helper_DisplayType_Adapter_Null();
        }
        return $this->_adapter;
    }

    /**
     * sets displaytype adapter
     *
     * @param wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface $adapter
     * @return wrxNext_Controller_Action_Helper_DisplayType
     */
    public function setAdapter(
        wrxNext_Controller_Action_Helper_DisplayType_AdapterInterface $adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * clears currently set display type
     *
     * @return wrxNext_Controller_Action_Helper_DisplayType
     */
    public function clear()
    {
        $this->_log('clearing set display type to default');
        $this->_displayType = null;
        if (Zend_Controller_Action_HelperBroker::hasHelper('viewRenderer')) {
            Zend_Controller_Action_HelperBroker::getExistingHelper(
                'viewRenderer')->setViewSuffix(
                empty($this->_startupSuffix) ? 'phtml' : $this->_startupSuffix);
        }
        return $this;
    }

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Controller_Action_Helper_Abstract::preDispatch()
     */
    public function preDispatch()
    {
        if (Zend_Controller_Action_HelperBroker::hasHelper('viewRenderer') &&
            is_null($this->_startupSuffix)
        ) {
            $this->_startupSuffix = Zend_Controller_Action_HelperBroker::getExistingHelper(
                'viewRenderer')->getViewSuffix();
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see Zend_Controller_Action_Helper_Abstract::postDispatch()
     */
    public function postDispatch()
    {
        if (!Zend_Controller_Action_HelperBroker::hasHelper('viewRenderer') &&
            !$this->_shouldRender()
        ) {
            $this->_log('skipping check&change');
            return;
        }
        // check if display type was already set
        if (is_null($this->_displayType)) {
            $this->_log(
                'display type was not set, checking for any mvc overrides');
            $this->_displayType = $this->getAdapter()->fromRequest(
                $this->getRequest());
            $this->_log('result:');
            $this->_log($this->_displayType);
        }
        // check if display type is set
        if (!empty($this->_displayType)) {
            $viewRenderer = Zend_Controller_Action_HelperBroker::getExistingHelper(
                'viewRenderer');
            $this->_log('Setting display type to: ' . $this->_displayType);
            $viewRenderer->setViewSuffix(
                implode('.',
                    array(
                        $this->_displayType,
                        $viewRenderer->getViewSuffix()
                    )));
        }
    }

    /**
     * Should the ViewRenderer render a view script?
     *
     * @return boolean
     */
    protected function _shouldRender()
    {
        return (!$this->getFrontController()->getParam('noViewRenderer') &&
            $this->getRequest()->isDispatched() &&
            !$this->getResponse()->isRedirect());
    }
}
