<?php

class wrxNext_Controller_Action_Helper_Authorisation extends Zend_Controller_Action_Helper_Abstract
{
    /**
     * holds session namespace
     *
     * @var Zend_Session_Namespace
     */
    protected $_session = null;

    /**
     * holds session name to use
     *
     * @var string
     */
    static protected $_sessionName = __CLASS__;

    /**
     * holds static instance
     *
     * @var wrxNext_Controller_Action_Helper_Authorisation
     */
    static protected $_instance = null;

    /**
     * @return \wrxNext_Controller_Action_Helper_Authorisation
     */
    static public function getInstance()
    {
        if ( !self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * returns session instance
     *
     * @return \Zend_Session_Namespace
     */
    public function getSession()
    {
        if ( !$this->_session instanceof Zend_Session_Namespace) {
            $this->_session = new Zend_Session_Namespace(self::$_sessionName);
        }
        return $this->_session;
    }

    /**
     * sets name of session
     *
     * @param string $name
     */
    static public function setSessionName($name)
    {
        self::$_sessionName = $name;
    }

    /**
     * returns name of session
     *
     * @return string
     */
    static public function getSessionName()
    {
        return self::$_sessionName;
    }

    /**
     * redirects to stored request or if not then to passed params
     *
     * @param string $defaultAction
     * @param string $defaultController
     * @param string $defaultModule
     * @param array  $defaultParams
     */
    public function direct($defaultAction = 'index', $defaultController = 'index', $defaultModule = 'default', array $defaultParams = array())
    {
        $this->redirectToStart($defaultAction, $defaultController, $defaultModule, $defaultParams);
    }

    /**
     * stores failed request
     *
     * @param \Zend_Controller_Request_Abstract $request
     * @param bool                              $force
     * @return \wrxNext_Controller_Plugin_Authorisation
     */
    public function storeFailedRequest(Zend_Controller_Request_Abstract $request, $force = false)
    {
        if ( !Zend_Session::namespaceIsset(self::$_sessionName) || $force) {
            $session             = $this->getSession();
            $session->module     = $request->getModuleName();
            $session->controller = $request->getControllerName();
            $session->action     = $request->getActionName();
            $session->params     = $request->getUserParams();
        }
        return $this;
    }

    /**
     * redirects to stored request or if it's not stored then to given params
     *
     * @param string $defaultAction
     * @param string $defaultController
     * @param string $defaultModule
     * @param array  $defaultParams
     */
    public function redirectToStart($defaultAction = 'index', $defaultController = 'index', $defaultModule = 'default', array $defaultParams = array())
    {
        $session    = $this->getSession();
        $action     = isset($session->action) ? $session->action : $defaultAction;
        $controller = isset($session->controller) ? $session->controller : $defaultController;
        $module     = isset($session->module) ? $session->module : $defaultModule;
        $params     = isset($session->params) ? $session->params : $defaultParams;
        $this->resetStoredRequest();
        $this->_actionController->getHelper('redirector')->direct($action, $controller, $module, $params);
    }

    /**
     * resets stored request
     *
     * @return wrxNext_Controller_Plugin_Authorisation
     */
    public function resetStoredRequest()
    {
        Zend_Session::namespaceUnset(self::$_sessionName);
        return $this;
    }

    /**
     * checks if there is stored request
     *
     * @return bool
     */
    public function hasStoredRequest()
    {
        return Zend_Session::namespaceIsset(self::$_sessionName);
    }
}
