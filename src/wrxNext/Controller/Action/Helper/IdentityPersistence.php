<?php

/**
 * action plugin proxy for Identity Persistence plugin for Front Controller
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Action_Helper_IdentityPersistence extends Zend_Controller_Action_Helper_Abstract
{

    /**
     * Strategy pattern; call object as method
     *
     * Returns layout object
     *
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function direct()
    {
        return $this->getIdentityPersistenceInstance();
    }

    /**
     * returns identity persistence plugin instance
     *
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function getIdentityPersistenceInstance()
    {
        $plugin = $this->getFrontController()->getPlugin(
            'wrxNext_Controller_Plugin_IdentityPersistence');
        if (is_array($plugin)) {
            return $plugin[0];
        }
        if (is_null($plugin)) {
            $plugin = new wrxNext_Controller_Plugin_IdentityPersistence();
            $this->getFrontController()->registerPlugin($plugin);
        }
        return $plugin;
    }

    /**
     * Proxy method calls to Identity Persistence object
     *
     * @param string $method
     * @param array $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        $identityPersistence = $this->getIdentityPersistenceInstance();
        if (method_exists($identityPersistence, $method)) {
            return call_user_func_array(
                array(
                    $identityPersistence,
                    $method
                ), $args);
        }

        throw new wrxNext_Controller_Plugin_IdentityPersistence_Exception(
            sprintf(
                "Invalid method '%s' called on Identity Persitence action helper",
                $method));
    }
}
