<?php

class wrxNext_Controller_Action_Helper_AgeVerification_Result
{

    const POSITIVE = 1;

    const UNKNOWN = 0;

    const NEGATIVE = -1;

    /**
     * holds result
     *
     * @var int
     */
    protected $_result = self::UNKNOWN;

    /**
     * takes verification result
     *
     * @param int $result
     */
    public function __construct($result)
    {
        $this->_result = $result;
    }

    /**
     * returns result
     *
     * @return number
     */
    public function getResult()
    {
        return $this->_result;
    }
}
