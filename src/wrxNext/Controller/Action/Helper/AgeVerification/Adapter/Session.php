<?php

class wrxNext_Controller_Action_Helper_AgeVerification_Adapter_Session implements
    wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface
{
    use wrxNext_Trait_Object_Options, wrxNext_Trait_RegistryLog;

    /**
     * holds session object, lazy loaded
     *
     * @var Zend_Session_Namespace
     */
    protected $_session = null;

    /**
     * holds session key
     *
     * @var string
     */
    protected $_sessionKey = __CLASS__;

    /**
     * holds route name for redirect
     *
     * @var string
     */
    protected $_route = 'default';

    /**
     * holds route params
     *
     * @var unknown
     */
    protected $_routeParams = array();

    /*
     * (non-PHPdoc) @see
     * wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface::verify()
     */
    public function verify(Zend_Controller_Request_Http $request)
    {
        $this->_log('session state before verification');
        $this->_log($this->_getSession());
        if ($this->_getSession()->adult === true) {
            return new wrxNext_Controller_Action_Helper_AgeVerification_Result(
                wrxNext_Controller_Action_Helper_AgeVerification_Result::POSITIVE);
        } elseif ($this->_getSession()->adult === false) {
            return new wrxNext_Controller_Action_Helper_AgeVerification_Result(
                wrxNext_Controller_Action_Helper_AgeVerification_Result::NEGATIVE);;
        } else {
            return new wrxNext_Controller_Action_Helper_AgeVerification_Result(
                wrxNext_Controller_Action_Helper_AgeVerification_Result::UNKNOWN);
        }
    }

    /*
     * (non-PHPdoc) @see
     * wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface::redirectToVerification()
     */

    /**
     * returns session object
     *
     * @return Zend_Session_Namespace
     */
    protected function _getSession()
    {
        if (!$this->_session instanceof Zend_Session_Namespace) {
            $this->_session = new Zend_Session_Namespace($this->_sessionKey);
        }
        return $this->_session;
    }

    public function redirectToVerification()
    {
        $this->_storeCurrentUrl();
        $this->_redirectToVerificationPage();
    }

    /**
     * stores current url in session
     */
    public function _storeCurrentUrl()
    {
        $this->_getSession()->verificationStart = new wrxNext_Url();

    }

    /**
     * redirects to age verification pafined by stored route/params
     */
    public function _redirectToVerificationPage()
    {
        $this->_log('session state before redirect to verification page');
        $this->_log($this->_getSession());
        $url = Zend_Controller_Front::getInstance()->getRouter()->assemble(
            $this->_routeParams, $this->_route, true, true);
        header('Location: ' . $url, 302);
        exit();
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface::redirectToVerificationStart()
     */
    public function redirectToVerificationStart()
    {
        $this->_redirectToStoredUrl();
    }

    /**
     * redirects to url stored in session
     */
    public function _redirectToStoredUrl()
    {
        $this->_log('session state before redirect to page requesting verification');
        $this->_log($this->_getSession());
        if ($this->_getSession()->verificationStart instanceof wrxNext_Url) {
            $this->_getSession()->verificationStart->redirect();
        } else {
            header('Location: /', 302);
            exit();
        }
    }

    /**
     * (non-PHPdoc)
     *
     * @see wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface::userVerified()
     */
    public function userVerified()
    {
        $this->_getSession()->adult = true;
    }

    /**
     * gets route name to use when redirecting to verification
     *
     * @return string
     */
    public function getRoute()
    {
        return $this->_route;
    }

    /**
     * sets route name to use when redirecting to verification
     *
     * @param string $name
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Adapter_Session
     */
    public function setRoute($name)
    {
        $this->_route = $name;
        return $this;
    }

    /**
     * gets verification route params
     *
     * @return array
     */
    public function getRouteParams()
    {
        return $this->_routeParams;
    }

    /**
     * sets verification route params
     *
     * @param array $params
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Adapter_Session
     */
    public function setRouteParams($params)
    {
        $this->_routeParams = $params;
        return $this;
    }

    /**
     * sets session key
     *
     * @param string $name
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Adapter_Session
     */
    public function setKey($name)
    {
        $this->_sessionKey = $name;
        return $this;
    }

    /**
     * gets session key
     *
     * @return string
     */
    public function getKey()
    {
        return $this->_sessionKey;
    }
}
