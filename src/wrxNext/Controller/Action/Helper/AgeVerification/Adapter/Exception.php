<?php

/**
 * class for ageverification adapter related problems
 *
 * @author bweres01
 *
 */
class wrxNext_Controller_Action_Helper_AgeVerification_Adapter_Exception extends wrxNext_Controller_Action_Helper_AgeVerification_Exception
{
}
