<?php

/**
 * interface for ageverification adapter class which allows age verification
 *
 * idea is that class implementing this interface should return information about
 * fact that current user is old enough to view adult content. upon verification resulting object will
 * have information about reason and upon that information user can be identified
 *
 * @author bweres01
 *
 */
interface wrxNext_Controller_Action_Helper_AgeVerification_Adapter_VerifyInterface
{

    /**
     * checks if current user is valid for adult content
     *
     * returning true will mean is old enough, false will mean
     * that verification was unsuccessfull
     *
     * @param Zend_Controller_Request_Http $request
     * @return wrxNext_Controller_Action_Helper_AgeVerification_Result
     */
    public function verify(Zend_Controller_Request_Http $request);

    /**
     * redirects user to age verification page
     *
     * this action should store page address which requested verification so
     * user could return to it upon successfull age verification.
     */
    public function redirectToVerification();

    /**
     * redirects user to page which requested age verification
     */
    public function redirectToVerificationStart();

    /**
     * informs adapter that user have been successfully verified, adapter
     * should store this information so upon verification it can be retrieved
     */
    public function userVerified();
}
