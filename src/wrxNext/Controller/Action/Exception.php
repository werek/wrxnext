<?php

/**
 * exception for controller action related problems
 *
 * @author bweres01
 *
 */
class wrxNext_Controller_Action_Exception extends wrxNext_Controller_Exception
{
}
