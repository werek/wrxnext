<?php

/**
 * exception for Controller related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Exception extends wrxNext_Exception
{
}
