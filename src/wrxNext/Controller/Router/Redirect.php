<?php

/**
 * router for handling redirects
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Router_Redirect implements
    Zend_Controller_Router_Route_Interface
{

    use wrxNext_Trait_RegistryLog;

    /**
     * holds callback for matching redirects
     *
     * @var callable
     */
    protected $_matchCallback = null;

    /**
     * holds redirects matches
     *
     * redirects are stored as key=>value pairs in manner of
     * <matchpath>=><redirect>
     *
     * @var array
     */
    protected $_redirects = array();
    /**
     * holds redirects http code
     *
     * @var int
     */
    protected $_redirectHttpCode = 301;

    /**
     * constructor
     *
     * @param null|array|Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array|Zend_Config $options
     * @throws wrxNext_Controller_Router_Exception
     * @return wrxNext_Controller_Router_Redirect
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        if (!is_array($options)) {
            throw new wrxNext_Controller_Router_Exception(
                'options should be passed as key=>value array or Zend_Config object');
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /*
     * (non-PHPdoc) @see Zend_Controller_Router_Route_Interface::match()
     */

    public static function getInstance(Zend_Config $config)
    {
        return new self($config);
    }

    /*
     * (non-PHPdoc) @see Zend_Controller_Router_Route_Interface::assemble()
     */

    public function match($path)
    {
        $this->_log('searching for path:');
        $this->_log($path);
        $urlString = '';
        if (array_key_exists($path, $this->_redirects)) {
            $urlString = $this->_redirects[$path];
        } elseif (is_callable($this->_matchCallback)) {
            $urlString = call_user_func_array($this->_matchCallback,
                array(
                    $path
                ));
        }
        if (!empty($urlString)) {
            $this->_log('found match for: ' . $path);
            $this->_log('redirecting to: ' . $urlString);
            $url = new wrxNext_Url($urlString);
            $url->redirect($this->_redirectHttpCode);
        }
        return false;
    }

    public function assemble($data = array(), $reset = false, $encode = false)
    {
        // this router does not support creating urls, returning empty
        return '';
    }

    /**
     * gets redirects
     *
     * @return array
     */
    public function getRedirects()
    {
        return $this->_redirects;
    }

    /**
     * sets redirects
     *
     * @param array $redirects
     * @return wrxNext_Controller_Router_Redirect
     */
    public function setRedirects($redirects)
    {
        $this->_redirects = $redirects;
        return $this;
    }

    /**
     * adds redirect to stack
     *
     * @param string $matchPath
     * @param string $redirectUrl
     * @return wrxNext_Controller_Router_Redirect
     */
    public function addRedirect($matchPath, $redirectUrl)
    {
        $this->_redirects[$matchPath] = (string)$redirectUrl;
        return $this;
    }

    /**
     * gets match callback for redirects
     *
     * @return callable
     */
    public function getMatchCallBack()
    {
        return $this->_matchCallback;
    }

    /**
     * sets match callback for redirects
     *
     * as first param callback will have path to match against
     *
     * @param callable $callBack
     * @return wrxNext_Controller_Router_Redirect
     */
    public function setMatchCallBack($callBack)
    {
        $this->_matchCallback = $callBack;
        return $this;
    }

    /**
     * gets redirect http code
     *
     * @return int
     */
    public function getRedirectHttpCode()
    {
        return $this->_redirectHttpCode;
    }

    /*
     * (non-PHPdoc) @see Zend_Controller_Router_Route_Interface::getInstance()
     */

    /**
     * sets redirect http code
     *
     * @param int $code
     * @return wrxNext_Controller_Router_Redirect
     */
    public function setRedirectHttpCode($code)
    {
        $this->_redirectHttpCode = $code;
        return $this;
    }
}
