<?php

/**
 * exception for Router related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Router_Exception extends wrxNext_Controller_Exception
{
}
