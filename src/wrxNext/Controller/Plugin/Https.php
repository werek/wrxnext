<?php

/**
 * plugin for redirects to https/http version of url
 *
 * it works on base of reading enforceHttps/enforceHttp key values in
 * map.
 *
 * map is simple three level array (with base level it gives four), at
 * each level params are checked and if deeper level has them, then it
 * will override previous values
 *
 * array[<module>][<controller>][<action>]
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_Https extends Zend_Controller_Plugin_Abstract
{
    use wrxNext_Trait_RegistryLog, wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap;

    /**
     * holds recognised url object
     *
     * @var wrxNext_Url
     */
    protected $_url = null;

    /**
     * @inheritdoc
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($this->_redirectNeeded($request)) {
            $url = $this->_url();
            $url->setScheme(wrxNext_SecureCheck::isSecure() ? 'http' : 'https');
            $url->redirect();
        }
    }

    /**
     * checks if redirect is needed
     *
     * @param Zend_Controller_Request_Abstract $request
     * @return boolean
     */
    protected function _redirectNeeded(
        Zend_Controller_Request_Abstract $request)
    {
        $param = $this->_extractParamsFromMapByRequest($request,
            array(
                'enforceHttps' => 0,
                'enforceHttp' => 0
            ));
        if (array_sum($param) == 2) {
            $this->_log('Enforce clash, no redirect needed');
            // contradictory enforce values, no redirect is needed
            return false;
        }
        $this->_log($this->_url()
            ->buildUrl());
        $this->_log(
            'https enforce is "' .
            ($param['enforceHttps'] == 1 ? 'true' : 'false') . '"');
        $this->_log(
            'http enforce is "' .
            ($param['enforceHttp'] == 1 ? 'true' : 'false') . '"');
        $redirect = false;
        if (intval($param['enforceHttps']) == 1 && !wrxNext_SecureCheck::isSecure()) {
            $redirect = true;
        } elseif (intval($param['enforceHttp']) == 1 &&
            wrxNext_SecureCheck::isSecure()
        ) {
            $redirect = true;
        } else {
            $this->_log('http(s) redirect is not needed');
        }
        return $redirect;
    }

    /**
     * returns current url object
     *
     * @return wrxNext_Url
     */
    protected function _url()
    {
        if (is_null($this->_url)) {
            $this->_url = new wrxNext_Url();
        }
        return $this->_url;
    }
}
