<?php

/**
 * Identity Persistence plugin, giving the ability to persist Identity beetween php sessions
 *
 * idea is that after calling persist() method with identity value, plugin setups Identity
 * Persitence cookie with key coresponding to current request browser. If current
 * {@link Zend_Auth} have no identity assigned, and plugin will recognise presence of
 * Identity Persistence cookie, then it will read persisted Identity from storage and
 * write it to Zend_Auth Identity Storage
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence_Storage
 * @see Zend_Auth
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence extends Zend_Controller_Plugin_Abstract
{
    use wrxNext_Trait_RegistryLog;

    /**
     * sets lifetime of identity persistence
     *
     * default is 1 month = 31 days
     *
     * @var int
     */
    protected $_lifetime = 2678400;

    /**
     * name of the cookie for setting public key
     *
     * @var string
     */
    protected $_cookieName = 'ip';

    /**
     * holds fetching callback
     *
     * @var callable
     */
    protected $_callbackIdentityFetch = null;

    /**
     * holds identity set callback
     *
     * @var callable
     */
    protected $_callbackIdentitySet = null;

    /**
     * holds identity check callback
     *
     * @var callable
     */
    protected $_callbackIdentityCheck = null;

    /**
     * holds public key length
     *
     * @var int
     */
    protected $_publicKeyLength = 32;

    /**
     * holds storage object, should be accessed through @method getStorage()
     *
     * @var wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
     */
    protected $_storage = null;

    /**
     * @inheritdoc
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        // @todo add logic for checking,fetching and setting identity
        if (!$this->_checkIdentity() && $this->_getCookie()) {
            $this->_log('found public key: ' . $this->_getCookie());
            $token = $this->_generateHashToken($this->_getCookie());
            $identity = $this->_fetchIdentity($token);
            if (!is_null($identity)) {
                $this->_log('restoring identity for token: ' . $token,
                    Zend_Log::INFO);
                $this->_setIdentity($identity);
            } else {
                $this->_log(
                    'clearing cookie, no identity found for token: ' . $token);
                $this->_destroyCookie();
            }
        }
    }

    /**
     * returns status of current identity existence
     *
     * @return boolean
     */
    protected function _checkIdentity()
    {
        if (is_callable($this->_callbackIdentityCheck)) {
            return call_user_func_array($this->_callbackIdentityCheck, array());
        }
        return Zend_Auth::getInstance()->hasIdentity();
    }

    /**
     * gets cookie value, if not set then null is given
     *
     * @return string null
     */
    protected function _getCookie()
    {
        return (isset($_COOKIE[$this->_cookieName])) ? $_COOKIE[$this->_cookieName] : null;
    }

    /**
     * generates hash token according to current browser data and
     * public part
     *
     * method takes under consideration browsers user agent and remote address
     *
     * @param string $publicPart
     * @return string
     */
    protected function _generateHashToken($publicPart)
    {
        $hashData[] = $publicPart;
        $hashData[] = $_SERVER['HTTP_USER_AGENT'];
        $hashData[] = wrxNext_SecureCheck::remoteAddress();
        return hash('sha512', implode('|', $hashData));
    }

    /**
     * fetches identity from storage
     *
     * @param string $token
     * @return mixed
     */
    protected function _fetchIdentity($token)
    {
        $identity = $this->getStorage()->read($token);
        if (is_callable($this->_callbackIdentityFetch)) {
            $identity = call_user_func_array($this->_callbackIdentityFetch,
                array(
                    $identity
                ));
        }
        return $identity;
    }

    /**
     * gets storage
     *
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
     */
    public function getStorage()
    {
        if (!$this->_storage instanceof wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface) {
            if (is_array($this->_storage) || is_string($this->_storage)) {
                $this->_storage = wrxNext_Controller_Plugin_IdentityPersistence_Storage::factory(
                    $this->_storage);
            } else {
                $this->_storage = wrxNext_Controller_Plugin_IdentityPersistence_Storage::factory(
                    'wrxNext_Controller_Plugin_IdentityPersistence_Storage_File');
            }
        }
        return $this->_storage;
    }

    /**
     * sets storage
     *
     * @param array|wrxNext_Controller_Plugin_IdentityParsistence_Storage_Interface $storage
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setStorage($storage)
    {
        $this->_storage = $storage;
        return $this;
    }

    /**
     * sets identity to auth object
     *
     * @param mixed $identity
     */
    protected function _setIdentity($identity)
    {
        if (is_callable($this->_callbackIdentitySet)) {
            call_user_func_array($this->_callbackIdentitySet,
                array(
                    $identity
                ));
        } else {
            Zend_Auth::getInstance()->getStorage()->write($identity);
        }
    }

    /**
     * unsets key from $_COOKIE array and sends to browser cookie with expire
     * date set in past (browser will discard that cookie)
     *
     * @return boolean
     */
    protected function _destroyCookie()
    {
        unset($_COOKIE[$this->_cookieName]);
        return setcookie($this->_cookieName, 'expired', time() - 86400, '/');
    }

    /**
     * saves given identity in persistent storage
     *
     * @param mixed $identity
     * @return boolean
     */
    public function persist($identity)
    {
        $publicPart = $this->_generatePublicPart();
        $token = $this->_generateHashToken($publicPart);
        $this->_log('persisiting identity for token:' . $token);
        if ($this->getStorage()->write($token, $identity, $this->_lifetime)) {
            return $this->_setCookie($publicPart);
        } else {
            $this->_log(
                'writing to persistent storage failed for token: ' . $token);
        }
        return false;
    }

    /**
     * generates random public part of token
     *
     * @param int $length
     * @return string
     */
    protected function _generatePublicPart($length = null)
    {
        if (is_null($length)) {
            $length = $this->_publicKeyLength;
        }
        $hashTool = new wrxNext_Authentication();
        return $hashTool->randomAlphaNumeric($length);
    }

    /**
     * sets cookie with given public key
     *
     * @param string $publicKey
     * @return boolean
     */
    protected function _setCookie($publicKey)
    {
        return setcookie($this->_cookieName, $publicKey,
            time() + $this->_lifetime, '/');
    }

    /**
     * clears current identity persistence
     *
     * @return boolean
     */
    public function clear()
    {
        $token = $this->_generateHashToken($this->_getCookie());
        if ($this->getStorage()->clear($token)) {
            return $this->_destroyCookie();
        }
        return false;
    }

    /**
     * gets cookie name
     *
     * @return string
     */
    public function getCookieName()
    {
        return $this->_cookieName;
    }

    /**
     * sets cookie name
     *
     * @param string $name
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setCookieName($name)
    {
        $this->_cookieName = $name;
        return $this;
    }

    /**
     * gets callable identity fetch function
     *
     * @return callable
     */
    public function getCallbackIdentityFetch()
    {
        return $this->_callbackIdentityFetch;
    }

    /**
     * sets callable identity fetch function
     *
     * this callback will recieve as first parameter identity data from
     * persistence storage, after completion its return value will be used
     * instead of identity retrieved from persistent storage
     *
     * @param callable $callback
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setCallbackIdentityFetch($callback)
    {
        $this->_callbackIdentityFetch = $callback;
        return $this;
    }

    /**
     * gets callable identity set function
     *
     * @return callable
     */
    public function getCallbackIdentitySet()
    {
        return $this->_callbackIdentitySet;
    }

    /**
     * sets callable identity set function
     *
     * this callback will be recieve identity as first param, will be run
     * instead of standard function. this could be used to set
     * retrieved identity in other auth mechanism than Zend_Auth
     *
     * @param callable $callback
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setCallbackIdentitySet($callback)
    {
        $this->_callbackIdentitySet = $callback;
        return $this;
    }

    /**
     * gets identity check callback
     *
     * @return callable
     */
    public function getCallbackIdentityCheck()
    {
        return $this->_callbackIdentityCheck;
    }

    /**
     * sets identity check callback
     *
     * this callback will be run instead of standard one to check if user is
     * currently logged in returning true will stop further check for persistent
     * identity
     *
     * @param callable $callback
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setCallbackIdentityCheck($callback)
    {
        $this->_callbackIdentityCheck = $callback;
        return $this;
    }

    /**
     * gets public key length
     *
     * @return int
     */
    public function getPublicKeyLength()
    {
        return $this->_publicKeyLength;
    }

    /**
     * sets public key length
     *
     * @param int $length
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setPublicKeyLength($length)
    {
        $this->_publicKeyLength = $length;
        return $this;
    }

    /**
     * gets lifetime
     *
     * @return int
     */
    public function getLifetime()
    {
        return $this->_lifetime;
    }

    /**
     * sets lifetime
     *
     * @param int $time
     * @return wrxNext_Controller_Plugin_IdentityPersistence
     */
    public function setLifetime($time)
    {
        $this->_lifetime = $time;
        return $this;
    }
}
