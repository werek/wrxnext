<?php

/**
 * authorisation plugin with context aware check.
 *
 * plugin maps from configuration current module/controller/action to
 * passed map of coresponding resourceName/contextName, and the verifies
 * if request shoould pass or be rerouted to authorisation
 *
 * it works on base of reading resourceName/contextName keys values in
 * map.
 *
 * map is simple three level array (with base level it gives four), at
 * each level params are checked and if deeper level has them, then it
 * will override previous values
 *
 * array[<module>][<controller>][<action>]
 *
 * @see    wrxNext_Auth_ResourceContext_Interface
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_Authorisation extends Zend_Controller_Plugin_Abstract
{
    use wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap, wrxNext_Trait_RegistryLog;

    /**
     * holds authositation object
     *
     * @var wrxNext_Auth_ResourceContext_Interface
     */
    protected static $_authoriseObject = null;

    /**
     * holds resource name
     *
     * @var string
     */
    protected $_resourceName = null;

    /**
     * holds context name
     *
     * @var string
     */
    protected $_contextName = null;

    /**
     * sets authorisation object
     *
     * @param wrxNext_Auth_ResourceContext_Interface $authorisationObject
     */
    public static function setAuthorisationObject(
        wrxNext_Auth_ResourceContext_Interface $authorisationObject)
    {
        self::$_authoriseObject = $authorisationObject;
    }

    /**
     * gets authorisation object
     *
     * @return wrxNext_Auth_ResourceContext_Interface
     */
    public static function getAuthorisationObject()
    {
        return self::$_authoriseObject;
    }

    /*
     * (non-PHPdoc) @see Zend_Controller_Plugin_Abstract::preDispatch()
     */
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $this->_mapResourceContext($request);
        if ( !is_null($this->_resourceName)) {
            if ( !self::$_authoriseObject instanceof wrxNext_Auth_ResourceContext_Interface) {
                throw new wrxNext_Controller_Plugin_Authorisation_Exception(
                    'authorisation object invalid'
                );
            }
            $override = null;
            if (self::$_authoriseObject instanceof wrxNext_Auth_ResourceContext_OverrideInterface) {
                $override = self::$_authoriseObject->override(
                    $request,
                    $this->_contextName
                );
                if ( !is_null($override) && $override === false) {
                    $this->_saveAndReroute($request);
                }
            }

            if (is_null($override) && !self::$_authoriseObject->authorise(
                    $this->_resourceName, $this->_contextName
                )
            ) {
                $this->_saveAndReroute($request);
            }
        }
    }

    /**
     * maps resource name and context
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    protected function _mapResourceContext(
        Zend_Controller_Request_Abstract $request)
    {
        $params = $this->_extractParamsFromMapByRequest(
            $request,
            array(
                'resourceName' => null,
                'contextName'  => null,
                'disableCheck' => 0
            )
        );

        $this->_contextName = $params['contextName'];
        if (intval($params['disableCheck']) === 0) {
            $this->_resourceName = $params['resourceName'];
        }
        $this->_log(
            'mapped ResourceContext, resource: ' . $this->_resourceName .
            ', context: ' . $this->_contextName
        );
    }

    /**
     * saves current route and passes request to authorisation object in order
     * to reroute
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    protected function _saveAndReroute(
        Zend_Controller_Request_Abstract $request)
    {
        $session = new Zend_Session_Namespace('login');
        if ( !$request->isXmlHttpRequest()) {
            $authorisation = wrxNext_Controller_Action_Helper_Authorisation::getInstance();
            if ( !$authorisation->hasStoredRequest()) {
                $authorisation->storeFailedRequest($request);
            } else {
                $this->_log('previously saved origin action wasn\'t marked as rerouted, skipping saving of current one');
                $this->_log($request->getParams());
            }
        } else {
            $this->_log('Encountered XMLHttpRequest skipping saving requested action');
        }
        $this->_log('authorisation failed, rerouting');
        self::$_authoriseObject->reroute($request);
    }
}
