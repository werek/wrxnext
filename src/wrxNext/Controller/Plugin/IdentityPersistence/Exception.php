<?php

/**
 * exception for identity persistence related problems
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence_Exception extends wrxNext_Controller_Plugin_Exception
{
}
