<?php

/**
 * abstract base class for Identity Persistence storage objects
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 * @see wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
abstract class wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract implements
    wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
{

    /**
     * basic constructor, with ability to parse options
     *
     * @param null|array|Zend_Config $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * parses given options
     *
     * @param array|Zend_Config $options
     * @throws wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } elseif (!is_array($options)) {
            throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
                'options should be array or Zend_Config instance, ' .
                gettype($options) . ' given', 1);
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function read($token)
    {
        throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
            'read method is not implemented', 2);
    }

    /**
     * @inheritdoc
     */
    public function write($token, $data, $lifetime)
    {
        throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
            'write method is not implemented', 3);
    }

    /**
     * @inheritdoc
     */
    public function clear($token)
    {
        throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
            'clear method is not implemented', 4);
    }
}
