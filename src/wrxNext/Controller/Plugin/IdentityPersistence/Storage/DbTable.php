<?php

/**
 * database based Identity Persistence Storage object
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 * @see wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable extends wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract
{

    /**
     * adapter used for connection to database
     *
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_adapter = null;

    /**
     * table name
     *
     * @var string
     */
    protected $_tableName = 'identity_persistence';

    /**
     * identity column name
     *
     * @var string
     */
    protected $_identityColumn = 'identifier';

    /**
     * token column name
     *
     * @var string
     */
    protected $_tokenColumn = 'token';

    /**
     * lifetime column name
     *
     * should be int
     *
     * @var string
     */
    protected $_lifetimeColumn = 'lifetime';

    /**
     * sets table name
     *
     * @param string $table
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable
     */
    public function setTable($table)
    {
        $this->_tableName = $table;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function read($token)
    {
        $db = $this->getAdapter();
        $data = null;
        $record = $db->select()
            ->from($this->getTable())
            ->where($this->getTokenColumn() . ' = ?', $token)
            ->query(Zend_Db::FETCH_ASSOC)
            ->fetch();
        if ($record[$this->getLifetimeColumn()] > time()) {
            $data = unserialize($record[$this->getIdentityColumn()]);
        } else {
            $this->clear($token);
        }
        return $data;
    }

    /**
     * gets db adapter
     *
     * @return Zend_Db_Adapter_Abstract
     */
    public function getAdapter()
    {
        if (is_null($this->_adapter)) {
            $adapter = Zend_Db_Table_Abstract::getDefaultAdapter();
            if (!$adapter instanceof Zend_Db_Adapter_Abstract) {
                throw new Zend_Db_Exception('no default adapter present');
            }
            $this->_adapter = $adapter;
        }
        return $this->_adapter;
    }

    /**
     * sets db adapter
     *
     * @param Zend_Db_Adapter_Abstract $adapter
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable
     */
    public function setAdapter($adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }

    /**
     * gets table name
     *
     * @return string
     */
    public function getTable()
    {
        return $this->_tableName;
    }

    /**
     * gets token column name
     *
     * @return string
     */
    public function getTokenColumn()
    {
        return $this->_tokenColumn;
    }

    /**
     * sets token column name
     *
     * @param string $columnName
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable
     */
    public function setTokenColumn($columnName)
    {
        $this->_tokenColumn = $columnName;
        return $this;
    }

    /**
     * gets lifetime column name
     *
     * @return string
     */
    public function getLifetimeColumn()
    {
        return $this->_lifetimeColumn;
    }

    /**
     * sets lifetime column name
     *
     * @param string $columnName
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable
     */
    public function setLifetimeColumn($columnName)
    {
        $this->_lifetimeColumn = $columnName;
        return $this;
    }

    /**
     * gets identity identifier column name
     *
     * @return string
     */
    public function getIdentityColumn()
    {
        return $this->_identityColumn;
    }

    /**
     * sets identity identifier column name
     *
     * @param string $columnName
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_DbTable
     */
    public function setIdentityColumn($columnName)
    {
        $this->_identityColumn = $columnName;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function clear($token)
    {
        $db = $this->getAdapter();
        $where = array(
            $this->getTokenColumn() . ' = ?' => $token
        );
        $db->delete($this->getTable(), $where);
        return true;
    }

    /**
     * @inheritdoc
     */
    public function write($token, $data, $lifetime)
    {
        $db = $this->getAdapter();
        $data = array(
            $this->getIdentityColumn() => serialize($data),
            $this->getTokenColumn() => $token,
            $this->getLifetimeColumn() => time() + $lifetime
        );
        $result = $db->insert($this->getTable(), $data);
        return ($result > 0) ? true : false;
    }
}
