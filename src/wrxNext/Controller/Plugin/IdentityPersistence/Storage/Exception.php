<?php

/**
 * exception for Identity Persistence Storage related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception extends wrxNext_Controller_Plugin_IdentityPersistence_Exception
{
}
