<?php

/**
 * interface for storage object used in Identity Persistence
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
interface wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
{

    /**
     * handles reading identity identifier
     *
     * returns null if given token has no associated identifier
     *
     * @param string $token
     * @return mixed identity identifier
     */
    public function read($token);

    /**
     * handles writing identity identifier
     *
     * return true if write is successful, false on failure
     *
     * @param string $token
     * @param mixed $data
     *            identity identifier
     * @param int $lifetime
     *            in seconds
     * @return boolean
     */
    public function write($token, $data, $lifetime);

    /**
     * handles deleting stored identity identifier
     *
     * returns true on succes, false on failure
     *
     * @param string $token
     * @return bool
     */
    public function clear($token);
}
