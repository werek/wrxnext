<?php

/**
 * file storage class for Identity Persistence
 *
 * by default it will create Indenity Persistence files in current directory
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 * @see wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence_Storage_File extends wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract
{

    /**
     * holds save directory
     *
     * @var string
     */
    protected $_directory = null;

    /**
     * holds file prefix
     *
     * @var string
     */
    protected $_filePrefix = null;

    /**
     * holds file extension
     *
     * @var string
     */
    protected $_fileExtension = 'ip';

    /**
     * @inheritdoc
     */
    public function read($token)
    {
        $filePath = $this->_filePath($token);
        if (file_exists($filePath)) {
            $readData = @unserialize(file_get_contents($filePath));
            if ($readData === false) {
                return null;
            }
            if ($readData['time'] < time()) {
                return null;
            }
            return $readData['data'];
        } else {
            return null;
        }
    }

    /**
     * generates file path
     *
     * @param string $token
     * @return string
     */
    protected function _filePath($token)
    {
        $filePath = $this->_directory;
        $filePath .= DIRECTORY_SEPARATOR . $token . '.' . $this->_fileExtension;
        return $filePath;
    }

    /**
     * @inheritdoc
     */
    public function write($token, $data, $lifetime)
    {
        $filePath = $this->_filePath($token);
        $writeData = array(
            'time' => time() + $lifetime,
            'data' => $data
        );
        $result = file_put_contents($filePath, serialize($writeData));
        return ($result === false) ? false : true;
    }

    /**
     * @inheritdoc
     */
    public function clear($token)
    {
        $filePath = $this->_filePath($token);
        if (!file_exists($filePath)) {
            return true;
        }
        return unlink($filePath);
    }
}
