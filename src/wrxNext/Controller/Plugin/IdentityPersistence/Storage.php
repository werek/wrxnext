<?php

/**
 * Identity Persistence Storage factory class
 *
 * @see wrxNext_Controller_Plugin_IdentityPersistence
 * @see wrxNext_Controller_Plugin_IdentityPersistence_Storage_Abstract
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_IdentityPersistence_Storage
{

    /**
     * returns instance of identity persistence storage object
     *
     * @param string|array|zend_config $adapter
     * @param array $params
     * @throws wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception
     * @return wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface
     */
    public static function factory($adapter, $params = array())
    {
        if ($adapter instanceof Zend_Config) {
            $adapter = $adapter->toArray();
        }
        if (is_array($adapter)) {
            if (!array_key_exists('adapter', $adapter)) {
                throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
                    'no adapter name provided', 101);
            }
            if (array_key_exists('params', $adapter)) {
                $params = $adapter['params'];
            }
            $adapter = $adapter['adapter'];
        }

        // inspecting adapter
        $reflectionClass = new ReflectionClass($adapter);
        if (!$reflectionClass->implementsInterface(
            'wrxNext_Controller_Plugin_IdentityPersistence_Storage_Interface')
        ) {
            throw new wrxNext_Controller_Plugin_IdentityPersistence_Storage_Exception(
                'given adapter: ' . $adapter .
                ' does not implements storage interface', 102);
        }
        $instance = $reflectionClass->newInstance();
        if (method_exists($instance, 'setOptions')) {
            $instance->setOptions($params);
        }
        return $instance;
    }
}
