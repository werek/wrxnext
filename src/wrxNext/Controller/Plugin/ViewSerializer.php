<?php

class wrxNext_Controller_Plugin_ViewSerializer extends Zend_Controller_Plugin_Abstract
{
    use wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap, wrxNext_Trait_RegistryLog, wrxNext_Trait_FrontControllerView;

    /**
     * mime types with internal types
     *
     * @var array
     */
    protected $_supportedMimeTypes = array(
        'application/json' => 'json',
        'text/json' => 'json',
        '*/*' => 'json'
    );

    /**
     * default headers for different content types
     *
     * @var array
     */
    protected $_typeHeaders = array(
        'json' => 'application/json'
    );

    /**
     * serializers for given content types
     *
     * @var array
     */
    protected $_serializers = array(
        'json' => 'Zend_Serializer_Adapter_Json'
    );

    /**
     * @inheritdoc
     */
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        if ($request->isDispatched()) {
            $params = $this->_extractParamsFromMapByRequest($request,
                array(
                    'serialize' => false
                ));
            if ($params['serialize'] &&
                $request instanceof Zend_Controller_Request_Http
            ) {
                $this->getResponse()->setHeader('Vary', 'Accept');
                $this->_serializeView($request);
            }
        }
    }

    /**
     * serializes view
     *
     * @param Zend_Controller_Request_Http $request
     */
    protected function _serializeView(Zend_Controller_Request_Http $request)
    {
        $mimeType = $this->_getMimeType($request->getHeader('Accept'));
        $type = $this->_supportedMimeTypes[$mimeType];
        if ($mimeType == null) {
            $this->getResponse()->setBody('Unsuported content-type');
            $this->getResponse()->setHeader('Content-Type', 'text/plain');
        } else {
            $viewVars = $this->_view()->getVars();
            if (count($viewVars) != 0) {
                $serializer = $this->_serializerFor(
                    $this->_supportedMimeTypes[$mimeType]);
                $this->getResponse()->setBody($serializer->serialize($viewVars));
                $this->getResponse()->setHeader('Content-Type',
                    array_key_exists($type, $this->_typeHeaders) ? $this->_typeHeaders[$type] : $mimeType);
            }
        }
    }

    /**
     * extracts mime type from header
     *
     * @param string $mimeTypes
     * @return string null if not found
     */
    private function _getMimeType($mimeTypes = null)
    {
        // Values will be stored in this array
        $AcceptTypes = Array();

        // Accept header is case insensitive, and whitespace isn't important
        $accept = strtolower(str_replace(' ', '', $mimeTypes));

        // divide it into parts in the place of a ","
        $accept = explode(',', $accept);

        $this->_log($accept);

        foreach ($accept as $a) {
            // the default quality is 1.
            $q = 1;

            // check if there is a different quality
            if (strpos($a, ';q=')) {
                // divide "mime/type;q=X" into two parts: "mime/type" i "X"
                list ($a, $q) = explode(';q=', $a);
            }

            // mime-type $a is accepted with the quality $q
            // WARNING: $q == 0 means, that mime-type isn't supported!
            $AcceptTypes[$a] = $q;
        }

        arsort($AcceptTypes);

        // let's check our supported types:
        foreach ($AcceptTypes as $mime => $q) {
            if ($q && array_key_exists($mime, $this->_supportedMimeTypes)) {
                return $mime;
            }
        }
        // no mime-type found
        return null;
    }

    /**
     * returns serializer instance
     *
     * @param string $type
     * @return Zend_Serializer_Adapter_AdapterInterface
     */
    protected function _serializerFor($type)
    {
        if ($this->_serializers[$type] instanceof Zend_Serializer_Adapter_AdapterInterface) {
            return $this->_serializers[$type];
        } else {
            return $this->_serializers[$type] = new $this->_serializers[$type]();
        }
    }
}
