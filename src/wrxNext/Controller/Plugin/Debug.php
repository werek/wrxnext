<?php

class wrxNext_Controller_Plugin_Debug extends Zend_Controller_Plugin_Abstract
{

    /**
     * przechowuje informacje z debug'a
     *
     * @var array
     */
    private static $debug = array();

    public function routeStartup()
    {
        self::addMessage(Zend_Debug::dump($_COOKIE, '<h3>$_COOKIE</h3>', false));
        self::addMessage(Zend_Debug::dump($_POST, '<h3>$_POST</h3>', false));
        self::addMessage(Zend_Debug::dump($_FILES, '<h3>$_FILES</h3>', false));
        self::addMessage(Zend_Debug::dump($_GET, '<h3>$_GET</h3>', false));
        self::addMessage(Zend_Debug::dump($_SESSION, '<h3>$_SESSION</h3>',
            false));
    }

    /**
     * dodaje informacje do debug'a
     *
     * @param string $msg
     */
    private static function addMessage($msg)
    {
        self::$debug[] = $msg;
    }

    public function routeShutdown($action)
    {
        self::addMessage(
            Zend_Debug::dump($action, '<h3>routeShutdown</h3>', false));
        return $action;
    }

    public function dispatchLoopStartup($action)
    {
        self::addMessage(
            Zend_Debug::dump($action, '<h3>dispatchLoopStartup</h3>', false));
        return $action;
    }

    public function preDispatch($action)
    {
        self::addMessage(Zend_Debug::dump($action, '<h3>preDispatch</h3>',
            false));
        return $action;
    }

    public function postDispatch($action)
    {
        self::addMessage(
            Zend_Debug::dump($action, '<h3>postDispatch</h3>', false));
        return $action;
    }

    public function dispatchLoopShutdown()
    {
        self::addMessage(
            Zend_Debug::dump($action, '<h3>dispatchLoopShutdown</h3>',
                false));
    }

    public function __toString()
    {
        return self::getMessages();
    }

    /**
     * zwraca wszystkie informacje z debug'a
     *
     * @return string
     */
    public static function getMessages()
    {
        ob_start();
        foreach (self::$debug as $v) {
            echo $v;
        }
        return ob_get_clean();
    }
}

?>
