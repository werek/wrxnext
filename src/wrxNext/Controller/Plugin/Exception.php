<?php

/**
 * exception for Controller Plugin related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_Exception extends wrxNext_Exception
{
}
