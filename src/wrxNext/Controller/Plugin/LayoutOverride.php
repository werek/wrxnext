<?php

/**
 * custom version of layout plugin, extended with ability to read layout configuration from config map
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_LayoutOverride extends Zend_Layout_Controller_Plugin_Layout
{
    use wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap, wrxNext_Trait_RegistryLog;

    protected static $_overrideLayout = null;

    /**
     * gets override for current dispatch
     *
     * @return string
     */
    public static function getOverrideLayout()
    {
        return self::$_overrideLayout;
    }

    /**
     * sets override for current dispatch
     *
     * @param string $name
     * @return wrxNext_Controller_Plugin_LayoutOverride
     */
    public static function setOverrideLayout($name)
    {
        self::$_overrideLayout = $name;
    }

    /**
     * postDispatch() plugin hook -- render layout
     *
     * @param Zend_Controller_Request_Abstract $request
     * @return void
     */
    public function postDispatch(Zend_Controller_Request_Abstract $request)
    {
        $this->_checkOverrides($request);
        return parent::postDispatch($request);
    }

    /**
     * checks override array to change/disable layout
     *
     * @param Zend_Controller_Request_Abstract $request
     */
    protected function _checkOverrides(
        Zend_Controller_Request_Abstract $request)
    {
        if ($this->getLayout()->isEnabled()) {

            $params = $this->_extractParamsFromMapByRequest($request,
                array(
                    'layoutFile' => null,
                    'disableLayout' => false
                ));

            // populating variables
            $layout = $params['layoutFile'];
            $disable = $params['disableLayout'];
            $module = $request->getModuleName();
            $controller = $request->getControllerName();
            $action = $request->getActionName();

            if ($request->isDispatched() && !empty(self::$_overrideLayout)) {
                $layout = self::$_overrideLayout;
            } else {
                self::$_overrideLayout = null;
            }

            // setting options
            if (!is_null($layout)) {
                $this->getLayout()->setLayout($layout);
                $this->_log(
                    'overriden layout for: ' . $module . '/' . $controller .
                    '/' . $action . '. current layout file: ' .
                    $layout);
            }
            if ($disable) {
                $this->getLayout()->disableLayout();
                $this->_log(
                    'overriden layout for: ' . $module . '/' . $controller .
                    '/' . $action . '. disabling layout ');
            }
        } else {
            $this->_log(
                'apparently layout has been disabled from : ' .
                $request->getModuleName() . '/' .
                $request->getControllerName() . '/' .
                $request->getActionName() . '. disabling layout ');
        }
    }
}
