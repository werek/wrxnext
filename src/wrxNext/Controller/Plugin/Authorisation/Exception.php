<?php

/**
 * exception for authorisation related problems
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_Authorisation_Exception extends wrxNext_Exception
{
}
