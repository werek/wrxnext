<?php

/**
 * keeps locale unchanged on automatic ie. non cms pages
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_LocalePersistence extends Zend_Controller_Plugin_Abstract
{
    /**
     * @inheritdoc
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $localeNamespace = new Zend_Session_Namespace('session_locale');
        if (bd_Router_Page::isPageRoute() ||
            empty($localeNamespace->locale)
        ) {
            $localeNamespace->locale = Zend_Registry::get('Zend_Translate')->getLocale();
        } else {
            Zend_Registry::get('Zend_Translate')->setLocale(
                $localeNamespace->locale);
        }
    }
}
