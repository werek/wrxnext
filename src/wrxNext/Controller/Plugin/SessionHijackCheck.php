<?php

/**
 * session hijack plugin, it keeps track of user related variables. on any change
 * resets session to prevent unathorised access to personal data.
 *
 * tracks $_SERVER keys:
 * - REMOTE_ADDR
 * - HTTP_USER_AGENT
 * - APPLICATION_ENV
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Controller_Plugin_SessionHijackCheck extends Zend_Controller_Plugin_Abstract
{
    use wrxNext_Trait_RegistryLog, wrxNext_Trait_Controller_Plugin_ModuleControllerActionMap;

    /**
     * @inheritdoc
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        if ($this->_checkEnabled($request) && $this->_sessionHijacked()) {
            // generating new session
            $this->_regenerateSession();
            // redirect for new session reload
            $url = new wrxNext_Url();
            $url->redirect();
        }
    }

    /**
     * returns status if session hijack check is enabled
     *
     * @param Zend_Controller_Request_Abstract $request
     * @return boolean
     */
    protected function _checkEnabled(Zend_Controller_Request_Abstract $request)
    {
        $params = $this->_extractParamsFromMapByRequest($request,
            array(
                'disableCheck' => 0
            ));
        return (intval($params['disableCheck']) === 0) ? true : false;
    }

    /**
     * checks if session has been hijacked
     *
     * this method check if session data have changed
     *
     * @return boolean
     */
    protected function _sessionHijacked()
    {
        $serverKeysToCheck = array(
            'REMOTE_ADDR',
            'HTTP_USER_AGENT',
            'APPLICATION_ENV'
        );
        $serverVariables = $this->_getServerVariables();
        $sessionMap = new Zend_Session_Namespace('session_check');
        if (!is_array($sessionMap->mapKeys)) {
            $this->_log('first run, building check array');
            $map = array();
            foreach ($serverKeysToCheck as $key) {
                $map[$key] = @$serverVariables[$key];
            }
            $sessionMap->mapKeys = $map;
        } else {
            foreach ($serverKeysToCheck as $key) {
                if (!array_key_exists($key, $sessionMap->mapKeys)) {
                    $this->_log(
                        'Key "' . $key .
                        '" does not exists, possible change of configuration, reseting session',
                        Zend_Log::ALERT);
                    return true;
                } elseif ($sessionMap->mapKeys[$key] != @$serverVariables[$key]) {
                    $this->_log(
                        'Key "' . $key .
                        '" mismatch, high probability of session hijack, reseting session',
                        Zend_Log::ALERT);
                    $this->_log('REQUEST_URI: ' . @$_SERVER['REQUEST_URI'],
                        Zend_Log::ALERT);
                    $this->_log('was: ' . $sessionMap->mapKeys[$key],
                        Zend_Log::ALERT);
                    $this->_log('is: ' . @$serverVariables[$key],
                        Zend_Log::ALERT);
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * returns serwer variables
     *
     * @return array
     */
    protected function _getServerVariables()
    {
        $data = $_SERVER;
        // workaround for production enviroment where REMOTE_ADDR points
        // to proxy machine and not to client
        $data['REMOTE_ADDR'] = wrxNext_SecureCheck::remoteAddress();
        return $data;
    }

    /**
     * regenerates session
     */
    protected function _regenerateSession()
    {
        // generating new session id
        session_regenerate_id();
        // emptying session
        $_SESSION = array();
        session_write_close();
    }
}
