<?php

/**
 * basic exception for Hypermedia Framework errors distinction
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Exception extends Exception
{

}
