<?php

class wrxNext_Oauth
{
    protected static $_instance;
    protected $_initiateClass = 'wrxNext_Oauth_Provider_Initiate';
    protected $_initiateObject = null;
    protected $_authorizeClass = 'wrxNext_Oauth_Provider_Authorize';
    protected $_authorizeObject = null;
    protected $_tokenClass = 'wrxNext_Oauth_Provider_Token';
    protected $_tokenObject = null;
    protected $_options = array();

    protected function __construct()
    {
    }

    /**
     * returns oauth provider instance
     *
     * @return wrxNext_Oauth
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function getInitiateObject()
    {
        if (is_null($this->_initiateObject)) {
            $this->_initiateObject = new $this->_initiateClass((array)$this->_options['initiate']);
        }
        return $this->_initiateObject;
    }

    public function setInitiateObject($initiateObject)
    {
        $this->_initiateObject = $initiateObject;
        return $this;
    }

    public function getAuthorizeObject()
    {
        if (is_null($this->_authorizeObject)) {
            $this->_authorizeObject = new $this->_authorizeClass((array)$this->_options['authorize']);
        }
        return $this->_authorizeObject;
    }

    public function setAuthorizeObject($authorizeObject)
    {
        $this->_authorizeObject = $authorizeObject;
        return $this;
    }

    public function getTokenObject()
    {
        if (is_null($this->_tokenObject)) {
            $this->_tokenObject = new $this->_tokenClass((array)$this->_options['token']);
        }
        return $this->_tokenObject;
    }

    public function setTokenObject($tokenObject)
    {
        $this->_tokenObject = $tokenObject;
        return $this;
    }

    /**
     * handles setting up options
     *
     * @param array $options
     * @return wrxNext_Oauth
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        } else {
            $options = (array)$options;
        }

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            } else {
                $this->_options[$key] = $value;
            }
        }
        return $this;
    }

    protected function __clone()
    {
    }
}
