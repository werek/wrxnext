<?php

/**
 * class dedicated to CDN fileManager operations
 *
 */
class wrxNext_FileManager
{
    use wrxNext_Trait_RegistryLog;
    const THUMB_WIDTH = 100;
    const THUMB_HEIGHT = 47;
    protected $_cdnDirectory;
    protected $_mimeTypeDatabase;

    public function __construct($cdnDirectory, $mimeTypeDatabase = null)
    {
        $this->_cdnDirectory = $cdnDirectory;
        $this->_mimeTypeDatabase = $mimeTypeDatabase;
    }

    public function delete($path)
    {
        $securePath = $this->_cdnDirectory . $this->normalizePath($path);
        if (is_dir($securePath)) {
            return @rmdir($securePath);
        } else {
            return @unlink($securePath);
        }
    }

    /**
     * Secures path in cdn chroot
     *
     * @param string $path
     * @throws wrxNext_Exception
     */
    public function normalizePath($path)
    {
        $patterns = array('/^(\/)?|(\/)+/', '/(\.+\/)/');
        $replacements = array('/', '');
        $path = realpath($this->_cdnDirectory . preg_replace($patterns, $replacements, $path));
        $path = str_replace($this->_cdnDirectory, '', (string)$path, $count);

        if ($count === 0) // path exists outside cdn, reset
        {
            $path = '/';
        }

        $path = preg_replace('/\/+/', '/', $path);

        return $path;
    }

    public function move($pathFrom, $pathTo)
    {
        $securePathFrom = $this->_cdnDirectory . $this->normalizePath($pathFrom);
        $securePathTo = $this->_cdnDirectory . $this->normalizePath(dirname($pathTo)) . '/' . basename($pathTo);

        if (is_dir($securePathTo)) {
            $securePathTo .= '/' . basename($pathFrom);
        }

        return @rename($securePathFrom, $securePathTo);
    }

    public function copy($pathFrom, $pathTo)
    {
        $securePathFrom = $this->_cdnDirectory . $this->normalizePath($pathFrom);
        $securePathTo = $this->_cdnDirectory . $this->normalizePath(dirname($pathTo)) . '/' . basename($pathTo);

        if (is_dir($securePathTo)) {
            $securePathTo .= '/' . basename($pathFrom);
        }

        return @copy($securePathFrom, $securePathTo);
    }

    public function mkdir($path)
    {
        $securePath = $this->_cdnDirectory . $this->normalizePath(dirname($path)) . '/' . basename($path);
        return @mkdir($securePath);
    }

    public function getDirTree($parentDir, $filter = '*')
    {
        $parentDir = $this->normalizePath($parentDir);

        $this->_log('gettree starting dir: ' . $parentDir);

        $array = array(
            'parent' => $parentDir,
            'files' => array()
        );

        if ($handle = opendir($this->_cdnDirectory . $parentDir)) {
            //$finfo = new finfo(FILEINFO_MIME, $this->_mimeTypeDatabase);

            while (false !== ($entry = readdir($handle))) {
                $relativeFilePath = $parentDir . '/' . $entry;
                $absoluteFilePath = $this->_cdnDirectory . '/' . $relativeFilePath;

                $array['files'][] = $this->getFileInfo($relativeFilePath);
            }
        }

        closedir($handle);


        return $array;
    }

    public function getFileInfo($relativeFilePath)
    {
        $finfo = new finfo(FILEINFO_MIME, $this->_mimeTypeDatabase);

        //$relativeFilePath = $this->normalizePath($relativeFilePath);
        $absoluteFilePath = $this->_cdnDirectory . '/' . $relativeFilePath;

        $fileType = filetype($absoluteFilePath);
        if ($fileType === 'file') {
            $mime = $finfo->file($absoluteFilePath);
        } else {
            $mime = $fileType;
        }

        // thumbnail
        if ($fileType === 'file' && in_array(@exif_imagetype($absoluteFilePath), array(IMAGETYPE_JPEG, IMAGETYPE_PNG, IMAGETYPE_GIF))) {
            $thumb = $this->_getThumb($relativeFilePath);
        } else {
            $thumb = null;
        }

        return array(
            'id' => basename($absoluteFilePath),
            'basename' => basename($absoluteFilePath),
            'path' => preg_replace('/\/+/', '/', $relativeFilePath),
            'mime' => $mime,
            'perm' => substr(sprintf('%o', fileperms($absoluteFilePath)), -4),
            'size' => filesize($absoluteFilePath),
            'lastModified' => date('Y-m-d G:i:s', filemtime($absoluteFilePath)),
            'type' => $fileType,
            'thumb' => $thumb,
        );
    }

    /**
     * Makes thumb if necessary
     *
     * @param string $file
     * @return string thumb path
     */
    protected function _getThumb($file)
    {
        //var_dump($file); die();
        $photoMapper = new Admin_Model_Mapper_Photos();
        $refreshThumbnail = !$photoMapper->isThumbnailExists($file, self::THUMB_WIDTH, self::THUMB_HEIGHT);

        if ($refreshThumbnail) {
            $thumbnailSrc = $photoMapper->resizeCdnPhoto($file, self::THUMB_WIDTH, self::THUMB_HEIGHT);
        } else {
            $thumbnailSrc = $photoMapper->getThumbnailPath($file, self::THUMB_WIDTH, self::THUMB_HEIGHT);
        }
        return $thumbnailSrc;
    }


}
