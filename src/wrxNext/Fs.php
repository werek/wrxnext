<?php

class wrxNext_Fs
{

    /**
     * usuwa rekursywnie wszystkie pliki/katalogi pasujace do wzorca regexp
     * $patern
     *
     * @param string $pattern
     * @param bool $usePHP
     * @param string $type
     *            dir|file|all
     * @param string $baseDir
     * @return array
     * @throws stdException
     */
    public static function deleteAllMatching($pattern, $usePHP = true, $type = 'dir',
                                             $baseDir = '.')
    {
        $type = $type == 'all' ? 'list' : $type;
        $entities = self::find($pattern, $type, $baseDir);
        foreach ($entities as $names) {
            self::usun($names, $usePHP);
        }
        return $entities;
    }

    /**
     * funkcja znajdujaca pliki/katalogi pasujace do wzorca,
     * zwraca tablice ze sciezkami.
     *
     * @param string $pattern
     * @param string $type
     *            dir|file|list
     * @param string $baseDir
     * @return array
     */
    public static function find($pattern, $type = 'dir', $baseDir = '.')
    {
        $entities = self::getDirList($baseDir, $type);
        $match = array();
        foreach ($entities as $names) {
            $path = $baseDir . DIRECTORY_SEPARATOR . $names;
            if (preg_match($pattern, $names)) {
                $match[] = $path;
            } elseif (!is_file($path)) {
                $tmpMatch = array();
                $tmpMatch = self::find($pattern, $type, $path);
                $match = array_merge($match, $tmpMatch);
            }
        }
        return $match;
    }

    /**
     * pobiera liste elementow w katalogu $dir, opcjonalnie mozna podac typ
     * elementow
     * dostepne opcje to:
     * <ul>
     * <li>dir - tylko katalogi</li>
     * <li>file - tylko pliki</li>
     * <li>all - wszystkie elementy</li>
     * </ul>
     * w przypadku wylistowywania wszystkich elementow zwracana tabela bedzie
     * skladala sie z
     * dwoch pod tabel. tabela 'file' przechowuje pliki, a tabela 'dir'
     * katalogi.
     * np. $lista_katalogu['dir'][1]
     *
     * @param string $dir
     * @param string $type
     *            dir|file|list|all
     * @return array
     */
    public static function getDirList($dir = '.', $type = 'dir')
    {
        $lista = array();
        if (file_exists($dir)) {
            $zabronione = array(
                '.',
                '..'
            );
            $katalog = opendir($dir);
            while ($file = readdir($katalog)) {
                if (!in_array($file, $zabronione)) {
                    switch ($type) {
                        case 'dir':
                            if (is_dir($dir . DIRECTORY_SEPARATOR . $file)) {
                                $lista[] = $file;
                            }
                            break;
                        case 'file':
                            if (!is_dir($dir . DIRECTORY_SEPARATOR . $file)) {
                                $lista[] = $file;
                            }
                            break;
                        case 'list':
                            // if (!is_dir($dir.'/'.$file)) {
                            $lista[] = $file;
                            // }
                            break;
                        case 'all':
                            if (is_dir($dir . DIRECTORY_SEPARATOR . $file)) {
                                $lista['dir'][] = $file;
                            } else {
                                $lista['file'][] = $file;
                            }
                            break;
                    }
                }
            }
            return $lista;
        }
    }

    /**
     * funkcja usuwajaca plik/katalog rekursywnie ze sprawdzeniem czy dany plik
     * istnieje
     * zwraca false jezeli nie uda sie usunac pliku
     *
     * @param string $sciezka
     * @param bool $usePHP
     * @return bool
     * @throws stdException
     */
    public static function usun($sciezka, $usePHP = true)
    {
        if (file_exists($sciezka)) {
            if ($usePHP) {
                if (is_file($sciezka)) {
                    if (unlink($sciezka)) {
                        return true;
                    } else {
                        throw new stdException('cannot remove file ' . $sciezka);
                    }
                } else {
                    $lista = self::getDirList($sciezka, 'all');
                    if (is_array($lista['file'])) {
                        foreach ($lista['file'] as $plik) {
                            self::usun($sciezka . DIRECTORY_SEPARATOR . $plik,
                                $usePHP);
                        }
                    }
                    if (count($lista['dir']) > 0) {
                        foreach ($lista['dir'] as $dir) {
                            self::usun($sciezka . DIRECTORY_SEPARATOR . $dir);
                        }
                    } else {
                        if (is_writeable($sciezka . '/..')) {
                            rmdir($sciezka);
                        } else {
                            throw new stdException('cannot remove dir ' .
                                $sciezka);
                        }
                    }
                    if (file_exists($sciezka) && is_writeable($sciezka . '/..') &&
                        count(self::getDirList($sciezka, 'list')) == 0
                    ) {
                        rmdir($sciezka);
                        return true;
                    } else {
                        throw new stdException('cannot remove dir ' . $sciezka);
                    }
                }
            } else {
                exec('rm -fr ' . $sciezka);
            }
        }
    }
}

/**
 * klasa bledu dla std.
 */
class stdException extends Exception
{
}
