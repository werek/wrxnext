<?php

class wrxNext_Validate_PostCode extends Zend_Validate_Abstract
{

    const POSTCODE_PATTERN = '@^\d{2,2}\-\d{3,3}$@';

    const INVALID = 'postcodeInvalid';

    const INVALID_POLISH = 'postcodeInvalidPolish';

    const INVALID_POLISH_RANGE = 'postcodeInvalidPolishRange';

    /**
     * holds message templates
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID => "postcode '%value%' does not match pattern '%pattern%'",
        self::INVALID_POLISH => "'%value%' does not match polish pattern XX-XXX",
        self::INVALID_POLISH_RANGE => "'%value%' is not valid polish postcode"
    );

    /**
     * holds varible mappings
     *
     * @var array
     */
    protected $_messageVariables = array(
        'pattern' => '_postCodePattern'
    );

    /**
     * holds postcode pattern to check against
     *
     * @var string
     */
    protected $_postCodePattern = self::POSTCODE_PATTERN;

    /**
     * flag to check against polish range
     *
     * @var boolean
     */
    protected $_checkAgainstPolishRange = true;

    /**
     * constructor, accepts set of options
     *
     * @param array $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Validate_Class
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /*
     * (non-PHPdoc) @see Zend_Validate_Interface::isValid()
     */
    public function isValid($value)
    {
        // TODO Auto-generated method stub
        $this->_setValue($value);
        if ($this->getCheckPolishRange()) {
            return $this->_isValidPolishPostCode($value);
        } else {
            if (!preg_match($this->getPattern(), $value)) {
                $this->_error(self::INVALID);
                return false;
            }
        }
        return true;
    }

    /**
     * gets flag to check against polish range
     *
     * @return boolean
     */
    public function getCheckPolishRange()
    {
        return $this->_checkAgainstPolishRange;
    }

    /**
     * checks postocde against polish rules
     *
     * @param string $value
     * @return boolean
     */
    protected function _isValidPolishPostCode($value)
    {
        $pattern = '@^(?P<kod>\d{2,2})\-\d{3,3}$@i';
        if (!preg_match($pattern, $value, $match)) {
            $this->_error(self::INVALID_POLISH);
            return false;
        }
        if (intval($match['kod']) === 79) {
            $this->_error(self::INVALID_POLISH_RANGE);
            return false;
        }
        return true;
    }

    /**
     * gets postocde perl regexp pattern
     *
     * @return string
     */
    public function getPattern()
    {
        return $this->_postCodePattern;
    }

    /**
     * sets flag to check against polish range
     *
     * @param boolean $check
     * @return wrxNext_Validate_PostCode
     */
    public function setCheckPolishRange($check)
    {
        $this->_checkAgainstPolishRange = $check;
        return $this;
    }

    /**
     * sets postocde perl regexp pattern
     *
     * @param string $pattern
     * @return wrxNext_Validate_PostCode
     */
    public function setPattern($pattern)
    {
        $this->_postCodePattern = $pattern;
        return $this;
    }
}
