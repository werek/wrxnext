<?php

class wrxNext_Validate_Class extends Zend_Validate_Abstract
{

    const ERROR_EMPTY = "Value is required and can't be empty";

    const ERROR_CLASSNOTFOUND = "class '%value%' not found";

    const ERROR_NOTSUBCLASS = "class '%value%' is not subclass of '%class%'";

    const ERROR_INTERFACENOTIMPLEMENTED = "interface '%interface%' is not implemented in class '%value%'";

    const ERROR_TRAITNOTIMPLEMENTED = "trait '%trait%' is not implemented in class '%value%'";

    const IS_EMPTY = 'isEmpty';

    const CLASS_NOT_FOUND = 'classNotFound';

    const NOT_SUBCLASS = 'notSubclass';

    const NO_INTERFACE = 'noInterface';

    const NO_TRAIT = 'noTrait';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::IS_EMPTY => self::ERROR_EMPTY,
        self::CLASS_NOT_FOUND => self::ERROR_CLASSNOTFOUND,
        self::NOT_SUBCLASS => self::ERROR_NOTSUBCLASS,
        self::NO_INTERFACE => self::ERROR_INTERFACENOTIMPLEMENTED,
        self::NO_TRAIT => self::ERROR_TRAITNOTIMPLEMENTED
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'trait' => '_lastTrait',
        'interface' => '_lastInterface',
        'class' => '_class'
    );

    /**
     *
     * @var string
     */
    protected $_lastInterface = null;

    /**
     *
     * @var string
     */
    protected $_lastTrait = null;

    /**
     * holds interfaces names against which check given class
     *
     * @var array
     */
    protected $_interfaces = array();

    /**
     * holds traits names against which to check given class
     *
     * @var array
     */
    protected $_traits = array();

    /**
     * holds class for check
     *
     * @var string
     */
    protected $_class = null;

    /**
     * holds flag if to allow empty value
     *
     * @var boolean
     */
    protected $_allowEmpty = false;

    /**
     * constructor, accepts set of options
     *
     * @param array $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Validate_Class
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /*
     * (non-PHPdoc) @see Zend_Validate_Interface::isValid()
     */
    public function isValid($value)
    {
        $valid = true;

        if ($this->_allowEmpty && empty($value)) {
            return true;
        } elseif (empty($value)) {
            $this->_error(self::IS_EMPTY);
            return false;
        }

        $this->_setValue($value);

        if (!class_exists($value, true)) {
            $this->_error(self::CLASS_NOT_FOUND);
            return false;
        }

        $classReflection = new ReflectionClass($value);

        // checking if extends given class
        if (!is_null($this->_class)) {
            if ($this->_class != $value) {
                if (!$classReflection->isSubclassOf(
                    new ReflectionClass($this->_class))
                ) {
                    $this->_error(self::NOT_SUBCLASS);
                    $valid = false;
                }
            }
        }

        // checking if implements given interfaces
        if (count($this->_interfaces)) {
            $implementedInterfaces = $classReflection->getInterfaceNames();
            foreach ($this->_interfaces as $interface) {
                $this->_lastInterface = $interface;
                if (!in_array($interface, $implementedInterfaces)) {
                    $this->_error(self::NO_INTERFACE);
                    $valid = false;
                }
            }
        }

        // checks if using given trats
        if (count($this->_traits)) {
            $implementedTraits = (array)$classReflection->getTraitNames();
            foreach ($this->_traits as $trait) {
                $this->_lastTrait = $trait;
                if (!in_array($trait, $implementedTraits)) {
                    $this->_error(self::NO_TRAIT);
                    $valid = false;
                }
            }
        }
        return $valid;
    }

    /**
     * gets class name against which to perform check
     *
     * @return string
     */
    public function getClass()
    {
        return $this->_class;
    }

    /**
     * sets class name against which to perform check
     *
     * @param string $className
     * @return wrxNext_Validate_Class
     */
    public function setClass($className)
    {
        $this->_class = $className;
        return $this;
    }

    /**
     * gets list of interfaces against which to perform check
     *
     * @return array
     */
    public function getInterfaces()
    {
        return $this->_interfaces;
    }

    /**
     * sets list of interfaces against which to perform check
     *
     * @param string|array $interfaces
     * @return wrxNext_Validate_Class
     */
    public function setInterfaces($interfaces)
    {
        $interfaces = (array)$interfaces;
        $this->_interfaces = $interfaces;
        return $this;
    }

    /**
     * gets traits names against which to perform check
     *
     * @return array
     */
    public function getTraits()
    {
        return $this->_traits;
    }

    /**
     * sets traits names against which to perform check
     *
     * @param string|array $traits
     * @return wrxNext_Validate_Class
     */
    public function setTraits($traits)
    {
        $traits = (array)$traits;
        $this->_traits = $traits;
        return $this;
    }

    /**
     * gets flag to allow empty value
     *
     * @return boolean
     */
    public function getAllowEmpty()
    {
        return $this->_allowEmpty;
    }

    /**
     * sets flag to allow empty value
     *
     * @param boolean $allowEmpty
     * @return wrxNext_Validate_Class
     */
    public function setAllowEmpty($allowEmpty)
    {
        $this->_allowEmpty = $allowEmpty;
        return $this;
    }
}
