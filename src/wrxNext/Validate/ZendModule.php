<?php

/**
 * validates if given ZF1 module is registered
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com
 *
 */
class wrxNext_Validate_ZendModule extends Zend_Validate_Abstract
{

    const MODULE_DONT_EXITS = 'moduleDontExists';

    /**
     * holds message templates
     *
     * @var unknown
     */
    protected $_messageTemplates = array(
        self::MODULE_DONT_EXITS => 'module "%value%" dont\'t exists in current application'
    );

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        $value = (string)$value;
        $this->_setValue($value);
        $modules = Zend_Controller_Front::getInstance()->getControllerDirectory();
        if (!array_key_exists($value, $modules)) {
            $this->_error(self::MODULE_DONT_EXITS);
            return false;
        }
        return true;
    }
}
