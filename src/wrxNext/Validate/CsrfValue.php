<?php

class wrxNext_Validate_CsrfValue extends Zend_Validate_Abstract
{

    const MISMATCH = 'mismatch';

    /**
     * holds key to check
     *
     * @var string
     */
    protected $_csrfSubKey = null;

    /**
     * holds message templates
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::MISMATCH => "CSRF value mismatch"
    );

    /**
     * constructor
     *
     * @param string $csrfSubKey
     */
    public function __construct($csrfSubKey)
    {
        $this->setCsrfSubKey($csrfSubKey);
    }

    public function isValid($value)
    {
        // TODO Auto-generated method stub
        $storage = $this->_storage();
        if (isset($storage->{$this->_csrfSubKey})) {
            if ($value == $storage->{$this->_csrfSubKey}) {
                return true;
            }
        }
        $this->_error(self::MISMATCH);
        return false;
    }

    /*
     * (non-PHPdoc) @see Zend_Validate_Interface::isValid()
     */

    /**
     * returns session storage
     *
     * @return Zend_Session_Namespace
     */
    protected function _storage()
    {
        return new Zend_Session_Namespace(__CLASS__);
    }

    /**
     * registers random value in storage and returns it
     *
     * @return string
     */
    public function registerValue()
    {
        $random = (new wrxNext_Authentication())->random(32);
        $this->_storage()->{$this->_csrfSubKey} = $random;
        return $random;
    }

    /**
     * gets CSRF session sub Key
     *
     * @return string
     */
    public function getCsrfSubKey()
    {
        return $this->_csrfSubKey;
    }

    /**
     * sets CSRF session sub Key
     *
     * @param string $key
     * @return wrxNext_Validate_CsrfValue
     */
    public function setCsrfSubKey($key)
    {
        $this->_csrfSubKey = $key;
        return $this;
    }
}
