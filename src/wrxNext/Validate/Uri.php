<?php

class wrxNext_Validate_Uri extends Zend_Validate_Abstract
{

    const MISSING_SCHEMA = 'missingSchema';

    const MISSING_DOMAIN = 'missingDomain';

    protected $_withSchema = true;

    protected $_messageTemplates = array(
        self::MISSING_SCHEMA => 'missing schema in url',
        self::MISSING_DOMAIN => 'missing domain/hostname'
    );

    public function isValid($value)
    {
        $this->_setValue($value);
        $url = new wrxNext_Url($value);
        if ($this->_withSchema) {
            if ($url->getScheme() === null) {
                $this->_error(self::MISSING_SCHEMA);
                return false;
            }
            if ($url->getHost() === null) {
                $this->_error(self::MISSING_DOMAIN);
                return false;
            }
        }
        return true;
    }
}
