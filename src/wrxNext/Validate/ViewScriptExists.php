<?php

/**
 * checks if given script path file exists and is readable under standard directory structure
 * for ZF1 application, optionally it can connect with another field for changing module in which to
 * check for file
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com
 *
 */
class wrxNext_Validate_ViewScriptExists extends Zend_Validate_Abstract
{
    use wrxNext_Trait_RegistryLog;

    const NOT_EXIST = 'notExist';

    const MODULE_UNKNOWN = 'moduleUnknown';

    const NOT_READABLE = 'notReadable';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_EXIST => "script '%value%' does not exists in module '%module%' under '%views%' directory",
        self::MODULE_UNKNOWN => "module '%module%' is not registered",
        self::NOT_READABLE => "script '%value%' exists but is not readable in module '%module%' under '%views%' directory"
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'module' => '_module',
        'views' => '_viewScriptSubDirectory'
    );

    /**
     * holds module against which to check view script file
     *
     * @var string
     */
    protected $_module = 'default';

    /**
     * holds views scripts directory inside module
     *
     * @var string
     */
    protected $_viewScriptSubDirectory = 'views/scripts';

    /**
     * holds field name with module name in context
     *
     * @var string
     */
    protected $_moduleField = null;

    /**
     * constructor, accepts set of options
     *
     * @param array $options
     */
    public function __construct($options = null)
    {
        if (!is_null($options)) {
            $this->setOptions($options);
        }
    }

    /**
     * sets options
     *
     * @param array $options
     * @return wrxNext_Validate_Class
     */
    public function setOptions($options)
    {
        if ($options instanceof Zend_Config) {
            $options = $options->toArray();
        }
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context = NULL)
    {
        $value = (string)$value;
        $this->_setValue($value);
        $viewScriptDirectory = $this->_getViewScriptDirectory($context);

        if ($viewScriptDirectory === false) {
            $this->_error(self::MODULE_UNKNOWN);
            return false;
        }

        $filePath = $viewScriptDirectory . DIRECTORY_SEPARATOR . $value;
        $this->_log('script full path: ' . $filePath);
        if (!file_exists($filePath)) {
            $this->_error(self::NOT_EXIST);
            return false;
        }

        if (!is_readable($filePath)) {
            $this->_error(self::NOT_READABLE);
            return false;
        }
        return true;
    }

    /**
     * returns view script directory
     *
     * @param string $context
     * @return string
     */
    protected function _getViewScriptDirectory($context = null)
    {
        if (!is_null($this->_moduleField)) {
            if (isset($context[$this->_moduleField]) &&
                !empty($context[$this->_moduleField])
            ) {
                $this->_module = $context[$this->_moduleField];
            }
        }
        $this->_log('recognised module: ' . $this->_module);
        $directory = Zend_Controller_Front::getInstance()->getModuleDirectory(
            $this->_module);
        $this->_log('module directory:');
        $this->_log($directory);
        if (is_null($directory)) {
            return false;
        }

        $directory .= DIRECTORY_SEPARATOR .
            trim($this->_viewScriptSubDirectory, '\\/');
        $this->_log('view script directory for context:' . $directory);
        return $directory;
    }

    /**
     * gets module field name
     *
     * @return string
     */
    public function getModuleField()
    {
        return $this->_moduleField;
    }

    /**
     * sets module field name
     *
     * @param string $fieldName
     * @return wrxNext_Validate_ViewScriptExists
     */
    public function setModuleField($fieldName)
    {
        $this->_moduleField = $fieldName;
        return $this;
    }

    /**
     * gets module
     *
     * @return string
     */
    public function getModule()
    {
        return $this->_module;
    }

    /**
     * sets module
     *
     * @param string $moduleName
     * @return wrxNext_Validate_ViewScriptExists
     */
    public function setModule($moduleName)
    {
        $this->_module = $moduleName;
        return $this;
    }
}
