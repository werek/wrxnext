<?php

class wrxNext_Validate_BlacklistHash extends Zend_Validate_Db_NoRecordExists
{

    /**
     *
     * @var array Message templates
     */
    protected $_messageTemplates = array(
        self::ERROR_RECORD_FOUND => "phrase is blacklisted"
    );

    protected $_table = 'blacklist';

    protected $_field = 'group_phrase_hash';

    protected $_groupName = '';

    public function __construct($options = null)
    {
        if (!is_null($options)) {
            if (is_string($options)) {
                $this->setGroup($options);
                $options = array();
            } elseif (array_key_exists('group', $options)) {
                $this->setGroup($options['group']);
            }
        }
        if (!array_key_exists('table', $options)) {
            $options['table'] = $this->_table;
        }
        if (!array_key_exists('field', $options)) {
            $options['field'] = $this->_field;
        }
        parent::__construct($options);
    }

    /**
     * sets group name
     *
     * @param string $groupName
     * @return wrxNext_Validate_BlacklistHash
     */
    public function setGroup($groupName)
    {
        $this->_groupName = $groupName;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        $value = md5($this->getGroup() . $value);
        return parent::isValid($value);
    }

    /**
     * gets group name
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->_groupName;
    }
}
