<?php

class wrxNext_Validate_PhoneNumber extends Zend_Validate_Abstract
{

    const INVALID_INTERNATIONAL_PREFIX = 'invalidPrefix';

    const INVALID_INTERNATIONAL_NUMBER = 'invalidPrefixNumber';

    const INVALID_NUMBER = 'invalidNumber';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::INVALID_INTERNATIONAL_PREFIX => "number '%value%' does not contain valid internationall code prefix",
        self::INVALID_INTERNATIONAL_NUMBER => "country code '%code%' is not valid",
        self::INVALID_NUMBER => "number '%value%' is not valid"
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'code' => '_prefix'
    );

    /**
     * holds list of country codes
     *
     * @var array
     */
    protected $_internationalPrefixes = array(
        '1',
        '20',
        '212',
        '213',
        '216',
        '218',
        '220',
        '221',
        '222',
        '223',
        '224',
        '225',
        '226',
        '227',
        '228',
        '229',
        '230',
        '231',
        '232',
        '233',
        '234',
        '235',
        '236',
        '237',
        '238',
        '239',
        '240',
        '241',
        '242',
        '243',
        '244',
        '245',
        '246',
        '247',
        '248',
        '249',
        '250',
        '251',
        '252',
        '253',
        '254',
        '255',
        '256',
        '257',
        '258',
        '260',
        '261',
        '262',
        '263',
        '264',
        '265',
        '266',
        '267',
        '268',
        '269',
        '27',
        '290',
        '291',
        '297',
        '298',
        '299',
        '30',
        '31',
        '32',
        '33',
        '34',
        '350',
        '351',
        '352',
        '353',
        '354',
        '355',
        '356',
        '357',
        '358',
        '359',
        '36',
        '370',
        '371',
        '372',
        '373',
        '374',
        '375',
        '376',
        '377',
        '378',
        '379',
        '380',
        '381',
        '382',
        '385',
        '386',
        '387',
        '388',
        '389',
        '39',
        '40',
        '41',
        '42',
        '43',
        '44',
        '45',
        '46',
        '47',
        '48',
        '49',
        '500',
        '501',
        '502',
        '503',
        '504',
        '505',
        '506',
        '507',
        '508',
        '509',
        '51',
        '52',
        '53',
        '54',
        '55',
        '56',
        '57',
        '58',
        '590',
        '591',
        '592',
        '593',
        '594',
        '595',
        '596',
        '597',
        '598',
        '599',
        '60',
        '61',
        '62',
        '63',
        '64',
        '65',
        '66',
        '670',
        '672',
        '673',
        '674',
        '675',
        '676',
        '677',
        '678',
        '679',
        '680',
        '681',
        '682',
        '683',
        '684',
        '685',
        '686',
        '687',
        '688',
        '689',
        '690',
        '691',
        '692',
        '7',
        '800',
        '808',
        '81',
        '82',
        '84',
        '850',
        '852',
        '853',
        '855',
        '856',
        '86',
        '870',
        '871',
        '872',
        '873',
        '874',
        '878',
        '880',
        '881',
        '882',
        '886',
        '90',
        '91',
        '92',
        '93',
        '94',
        '95',
        '960',
        '961',
        '962',
        '963',
        '964',
        '965',
        '966',
        '967',
        '968',
        '98',
        '991',
        '992',
        '993',
        '994',
        '995',
        '996',
        '998'
    );

    /**
     * holds recognised prefix
     *
     * @var string
     */
    protected $_prefix = '';

    /**
     * holds international prefix start
     *
     * @var string
     */
    protected $_internationalPrefixStart = '+';

    /**
     * holds flag for checking internationl prefix
     *
     * @var boolean
     */
    protected $_checkInternationalPrefix = false;

    /**
     * international code separated from number by space
     *
     * @var boolean
     */
    protected $_internationalPrefixSeparatedBySpace = true;

    /**
     * stores how long number is
     *
     * @var unknown
     */
    protected $_digits = 9;

    /**
     * gets number of digits in number
     *
     * @return string
     */
    public function getDigits()
    {
        return $this->_digits;
    }

    /**
     * sets number of digits in number
     *
     * @param string $digits
     * @return wrxNext_Validate_PhoneNumber
     */
    public function setDigits($digits)
    {
        $this->_digits = $digits;
        return $this;
    }

    /**
     * sets international code prefix
     *
     * @param string $prefix
     * @return wrxNext_Validate_PhoneNumber
     */
    public function setCodePrefix($prefix)
    {
        $this->_internationalPrefixStart = $prefix;
        return $this;
    }

    /**
     * gets international code prefix
     *
     * @return string
     */
    public function getCodePrefix()
    {
        return $this->_internationalPrefixStart;
    }

    /**
     * sets if international prefix is separated by space
     *
     * @param boolean $separatedBySpace
     * @return wrxNext_Validate_PhoneNumber
     */
    public function setSpaceSeparated($separatedBySpace)
    {
        $this->_internationalPrefixSeparatedBySpace = $separatedBySpace;
        return $this;
    }

    /**
     * gets if international prefix is separated by space
     *
     * @return boolean
     */
    public function getSpaceSeparated()
    {
        return $this->_internationalPrefixSeparatedBySpace;
    }

    /**
     * gets if check international prefix
     *
     * @return boolean
     */
    public function getCheckInternationalPrefix()
    {
        return $this->_checkInternationalPrefix;
    }

    /**
     * sets if check international prefix
     *
     * @param boolean $check
     * @return wrxNext_Validate_PhoneNumber
     */
    public function setCheckInternationalPrefix($check)
    {
        $this->_checkInternationalPrefix = $check;
        return $this;
    }

    /*
     * (non-PHPdoc) @see Zend_Validate_Interface::isValid()
     */

    public function isValid($value)
    {
        // TODO Auto-generated method stub
        $this->_setValue($value);
        if ($this->_checkInternationalPrefix) {
            $regexpEscapeFilter = new wrxNext_Filter_EscapeRegexpCharacters();
            $regexpEscapeFilter->setEnclosingChar('@');
            $codePrefix = $regexpEscapeFilter->filter(
                $this->_internationalPrefixStart);
            $codePattern = $codePrefix . '(?P<code>\d{1,3})';
            if ($this->_internationalPrefixSeparatedBySpace) {
                $codePattern .= ' ';
            }
            $codePattern = '@^' . $codePattern . '@';
            if (!preg_match($codePattern, $value, $match)) {
                $this->_error(self::INVALID_INTERNATIONAL_PREFIX);
                return false;
            }
            $this->_prefix = $match['code'];
            if (!in_array($match['code'], $this->_internationalPrefixes)) {
                $this->_error(self::INVALID_INTERNATIONAL_NUMBER);
                return false;
            }

            // separating international prefix from number
            $value = preg_replace($codePattern, '', $value);
        }
        if (preg_replace('@[\d]*@', '', $value) != '') {
            $this->_error(self::INVALID_NUMBER);
            return false;
        }
        if (strlen($value) != $this->_digits) {
            $this->_error(self::INVALID_NUMBER);
            return false;
        }
        return true;
    }
}
