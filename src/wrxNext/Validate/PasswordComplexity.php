<?php

class wrxNext_Validate_PasswordComplexity extends Zend_Validate_Abstract
{

    const NOT_ENOUGH = 'notEnough';

    /**
     * holds minimum length
     *
     * @var int
     */
    protected $_length = 8;

    /**
     * holds uppercase letters count
     *
     * @var int
     */
    protected $_uppercaseCount = 1;

    /**
     * holds digits count
     *
     * @var int
     */
    protected $_digits = 1;

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_ENOUGH => "password must have %length% characters length, and contain at least %digit% digits and %uppercase% uppercase letters"
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'length' => '_length',
        'digit' => '_digits',
        'uppercase' => '_uppercaseCount'
    );

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        $this->_setValue($value);
        if (mb_strlen($value, 'utf-8') < $this->_length) {
            $this->_error(self::NOT_ENOUGH);
            return false;
        }
        if (mb_strlen(preg_replace('@[^\d]*@u', '', $value), 'utf-8') <
            $this->_digits
        ) {
            $this->_error(self::NOT_ENOUGH);
            return false;
        }
        if (mb_strlen(preg_replace('@[^A-Z]*@u', '', $value), 'utf-8') <
            $this->_uppercaseCount
        ) {
            $this->_error(self::NOT_ENOUGH);
            return false;
        }
        return true;
    }

    /**
     * gets password length
     *
     * @return int
     */
    public function getLength()
    {
        return $this->_length;
    }

    /**
     * sets password length
     *
     * @param int $length
     * @return wrxNext_Validate_PasswordComplexity
     */
    public function setLength($length)
    {
        $this->_length = $length;
        return $this;
    }

    /**
     * gets uppercase letters count
     *
     * @return int
     */
    public function getUppercaseCount()
    {
        return $this->_uppercaseCount;
    }

    /**
     * sets uppercase letters count
     *
     * @param int $count
     * @return wrxNext_Validate_PasswordComplexity
     */
    public function setUppercaseCount($count)
    {
        $this->_uppercaseCount = $count;
        return $this;
    }

    /**
     * sets digits count
     *
     * @param int $count
     * @return wrxNext_Validate_PasswordComplexity
     */
    public function setDigitsCount($count)
    {
        $this->_digits = $count;
        return $this;
    }

    /**
     * gets digits count
     *
     * @return int
     */
    public function getDigitsCount()
    {
        return $this->_digits;
    }
}
