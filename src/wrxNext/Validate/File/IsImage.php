<?php

/**
 * validation clas for checking if given file (path) is common image ie. gix,jpg,png
 *
 * @author bweres01
 *
 */
class wrxNext_Validate_File_IsImage extends Zend_Validate_Abstract
{

    const NOT_IMAGE = 'notImage';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IMAGE => "Given file is not image, expected files of type: gif, jpeg, png"
    );

    /*
     * (non-PHPdoc)
     * @see Zend_Validate_Interface::isValid()
     */
    public function isValid($value, $context = null)
    {
        $this->_setValue(basename($value));
        $data = @getimagesize($value);
        if (!in_array($data[2],
            array(
                IMAGETYPE_GIF,
                IMAGETYPE_JPEG,
                IMAGETYPE_PNG
            ))
        ) {
            $this->_error(self::NOT_IMAGE);
            return false;
        }
        return true;
    }
}
