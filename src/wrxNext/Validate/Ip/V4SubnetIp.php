<?php

/**
 * validator for checking if given ip is in given subnet
 *
 * @author bweres01
 *
 */
class wrxNext_Validate_Ip_V4SubnetIp extends Zend_Validate_Abstract
{

    const NOT_IN_NETWORK = 'notInNetwork';

    const IP_INVALID = 'ipInvalid';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::NOT_IN_NETWORK => "ip '%value%' does not belongs to subnet '%subnet%' is in invalid format",
        self::IP_INVALID => "ip address '%value%' is invalid"
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'subnet' => '_subnet'
    );

    /**
     * holds subnet definition
     *
     * @var string
     */
    protected $_subnet = null;

    /**
     * constructor, accepts subnet definition
     *
     * possible forms:
     * 192.168.1.0/24
     * 192.168.1.0/255.255.255.0
     * 192.168.1.0-192.168.1.255
     *
     * @param string $subnet
     */
    public function __construct($subnet = null)
    {
        if (!is_null($subnet)) {
            $this->setSubnet($subnet);
        }
    }

    /**
     * gets subnet
     *
     * @return string
     */
    public function getSubnet()
    {
        return $this->_subnet;
    }

    /**
     * sets subnet
     *
     * possible forms:
     * 192.168.1.0/24
     * 192.168.1.0/255.255.255.0
     * 192.168.1.0-192.168.1.255
     *
     * @param string $subnet
     * @return wrxNext_Validate_Ip_V4SubnetIp
     */
    public function setSubnet($subnet)
    {
        $this->_subnet = $subnet;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        $this->_setValue($value);
        if (empty($this->_subnet)) {
            throw new wrxNext_Validate_Ip_Exception('subnet not set');
        }
        if (strpos($this->_subnet, '/') !== false) {
            return $this->_validSubnet($value);
        } else {
            return $this->_validSubnet($value);
        }
    }

    /**
     * handles check if given ip is in subnet
     *
     * @param string $ipAddress
     * @return boolean
     */
    protected function _validSubnet($ipAddress)
    {
        $ip = ip2long($ipAddress);
        list ($subnet, $mask) = explode('/', $this->_subnet);
        $subnet = ip2long($subnet);
        if (ip2long($mask) !== false) {
            $mask = ip2long($mask);
        } else {
            $mask = -pow(2, 32 - $mask);
        }
        if (($ip & $mask) !== ($subnet & $mask)) {
            return false;
        }
        return true;
    }

    /**
     * handles checking if ip is in range
     *
     * @param string $ipAddress
     * @return boolean
     */
    protected function _validRange($ipAddress)
    {
        $range = explode('-', $this->_subnet);
        if ((ip2long($range[0]) > ip2long($ipAddress)) ||
            (ip2long($range[0]) < ip2long($ipAddress))
        ) {
            return false;
        }
        return true;
    }
}
