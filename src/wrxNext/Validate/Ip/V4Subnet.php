<?php

class wrxNext_Validate_Ip_V4Subnet extends Zend_Validate_Abstract
{

    const SUBNET_INVALID = 'subnetInvalid';

    const MASK_INVALID = 'maskInvalid';

    const RANGE_LOW = 'lowRangeInvalid';

    const RANGE_HIGH = 'highRangeInvalid';

    const RANGE_REVERSE = 'rangeReversed';

    /**
     *
     * @var array
     */
    protected $_messageTemplates = array(
        self::SUBNET_INVALID => "subnet '%value%' is in invalid format",
        self::MASK_INVALID => "subnet mask '%mask%' is in invalid format, ther can be only 0-32 or XXX.XXX.XXX.XXX",
        self::RANGE_LOW => "range start '%low%' is invalid",
        self::RANGE_HIGH => "range end '%high%' is invalid",
        self::RANGE_REVERSE => "start value '%low%' is higher than high value '%high%'"
    );

    /**
     *
     * @var array
     */
    protected $_messageVariables = array(
        'mask' => '_mask',
        'low' => '_low',
        'high' => '_high'
    );

    /**
     * holds mask value
     *
     * @var string
     */
    protected $_mask = null;

    /**
     * holds range lower ip
     *
     * @var string
     */
    protected $_low = null;

    /**
     * holds range high ip
     *
     * @var unknown
     */
    protected $_high = null;

    /**
     * @inheritdoc
     */
    public function isValid($value)
    {
        $this->_setValue($value);
        $validation = true;
        if (strpos($value, '/') !== false) {
            $validation = $this->_validateBitMask($value);
        } elseif (strpos($value, '-') !== false) {
            $validation = $this->_validateIpRange($value);
        } else {
            $this->_error(self::SUBNET_INVALID);
            $validation = false;
        }
        return $validation;
    }

    /**
     * validates range type subnet
     *
     * @param string $value
     * @return boolean
     */
    protected function _validateIpRange($value)
    {
        $parts = explode("/", $value, 2);
        $this->_low = $parts[0];
        $this->_high = $parts[1];
        $low = ip2long($this->_low);
        $high = ip2long($this->_high);
        if ($low === false) {
            $this->_error(self::RANGE_LOW);
            return false;
        }
        if ($high === false) {
            $this->_error(self::RANGE_HIGH);
            return false;
        }
        if ($low > $high) {
            $this->_error(self::RANGE_REVERSE);
            return false;
        }
        return true;
    }

    /**
     * validates mask type subnet
     *
     * @param string $value
     * @return boolean
     */
    protected function _validateMask($value)
    {
        $parts = explode("/", $value, 2);
        $this->_mask = $parts[1];
        $ipLong = ip2long($parts[0]);

        if ($ipLong === false) {
            $this->_error(self::SUBNET_INVALID);
            return false;
        }

        if (ip2long($this->_mask) === false) {
            if (empty($this->_mask) && $this->_mask != 0 &&
                preg_match('@[^\d]@', $this->_mask) &&
                (0 <= $this->_mask <= 32
            )
        ) {
                $this->_error(self::MASK_INVALID);
                return false;
            }
        }
        return true;
    }
}
