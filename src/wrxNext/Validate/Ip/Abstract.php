<?php

class wrxNext_Validate_Ip_Abstract extends Zend_Validate_Abstract
{

    /**
     * holds ip v4 address validator
     *
     * @var Zend_Validate_Ip
     */
    protected $_ipValidator = null;

    public function isValid($value)
    {
        // TODO Auto-generated method stub
        return false;
    }

    /*
     * (non-PHPdoc) @see Zend_Validate_Interface::isValid()
     */

    /**
     * returns instance of ip validator
     *
     * @return Zend_Validate_Ip
     */
    protected function _ipValidator()
    {
        if (!$this->_ipValidator instanceof Zend_Validate_Ip) {
            $this->_ipValidator = new Zend_Validate_Ip(false);
        }
        return $this->_ipValidator;
    }

    /**
     * normalises ip address
     *
     * for example: from 192.045.001.012 to 192.45.1.12
     *
     * @param string $ipAddress
     * @return string
     */
    protected function _normalizeIp($ipAddress)
    {
        $ipChunks = array();
        foreach (explode('.', $ipAddress) as $chunk) {
            $ipChunks[] = intval($chunk);
        }
        return implode('.', $ipChunks);
    }

    /**
     * converts ip to bit representation
     *
     * for example: from 192.168.0.1 to 11000000101010000000000000000001
     *
     * @param unknown $ipAddress
     * @return string
     */
    protected function _ipToBin($ipAddress)
    {
        return str_pad(decbin(ip2long($ipAddress)), 32, '0', STR_PAD_LEFT);
    }

    /**
     * returns bit representation of binary mask
     *
     * for example: /24 to 11111111111111111111111100000000
     *
     * @param unknown $bitCount
     * @return string
     */
    protected function _bitToBin($bitCount)
    {
        $zeroBit = 32 - $bitCount;
        return str_pad('', $bitCount, '1', STR_PAD_LEFT) . str_pad('', $zeroBit, '0', STR_PAD_RIGHT);
    }
}
