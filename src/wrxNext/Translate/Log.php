<?php

class wrxNext_Translate_Log extends Zend_Log
{
    use wrxNext_Trait_RegistryTranslate;

    /**
     * holds untranslated messages list table instance
     *
     * @var bd_Model_DbTable_UntranslatedMessagesList
     */
    private $_table = null;

    private $_logMessagePattern = null;

    /**
     * holds Zend_Log intance
     *
     * @var Zend_Log
     */
    private $_logInstance = null;

    /**
     * sets Zend_Log Instance
     *
     * @param Zend_Log $log
     * @return wrxNext_Translate_Log
     */
    public function setLog($log)
    {
        $this->_logInstance = $log;
        return $this;
    }

    public function log($message, $priority, $extras = null)
    {
        $this->checkForTranslateMessage($message);
        return $this->getLog()->log($message, $priority, $extras);
    }

    /**
     * checks if given message is log message for untranslated message
     *
     * @param string $message
     */
    public function checkForTranslateMessage($message)
    {
        if (is_null($this->_logMessagePattern)) {
            $logMessage = $this->_getTranslate()
                ->getAdapter()
                ->getOptions('logMessage');
            $substitute = array(
                '%locale%' => '(?P<locale>.*?)',
                '%message%' => '(?P<message>.*?)'
            );
            $this->_logMessagePattern = '@^' . strtr($logMessage, $substitute) .
                '$@';
        }
        if (preg_match($this->_logMessagePattern, $message, $matches)) {
            $this->_saveMessage($matches['locale'], $matches['message']);
        }
    }

    /**
     * saves given message/locale to db, with current date
     *
     * @param unknown $locale
     * @param unknown $message
     */
    protected function _saveMessage($locale, $message)
    {
        $row = $this->_getTable()->fetchRow(
            array(
                'locale = ?' => $locale,
                'message = ?' => $message
            ));
        if (!$row instanceof bd_Model_DbTableRow_UntranslatedMessagesList) {
            $row = $this->_getTable()->createRow();
            $row->locale = $locale;
            $row->message = $message;
        } else {
            $row->locale = $locale;
        }
        $row->save();
    }

    /**
     * returns table instance
     *
     * @return bd_Model_DbTable_UntranslatedMessagesList
     */
    protected function _getTable()
    {
        if (is_null($this->_table)) {
            $this->_table = new bd_Model_DbTable_UntranslatedMessagesList();
        }
        return $this->_table;
    }

    /**
     * gets Zend_Log Instance
     *
     * @return Zend_Log
     */
    public function getLog()
    {
        return $this->_logInstance;
    }
}
