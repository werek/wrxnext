<?php

/**
 * class for managing URL address
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Url
{

    /**
     * holds status of port recognition
     *
     * @var boolean
     */
    protected static $_disbalePortRecognition = false;

    /**
     * holding data from parse_url()
     *
     * @var array
     */
    protected $_urlParts = array();

    /**
     * constructor
     *
     * @param string $url
     */
    public function __construct($url = null)
    {
        $this->parseUrl($url);
    }

    /**
     * parses given url, if no url is given then current recognised url will be
     * taken from request data
     *
     * @param string $url
     * @return wrxNext_Url
     */
    public function parseUrl($url = null)
    {
        if (is_null($url)) {
            $url = $this->_recognisedUrl();
        }
        if ($url instanceof self) {
            $this->_urlParts = $url->_urlParts;
        } else {
            $this->_urlParts = parse_url((string)$url);
        }
        return $this;
    }

    /**
     * returns currently recognised url
     *
     * @return string
     */
    protected function _recognisedUrl()
    {
        $secure = wrxNext_SecureCheck::isSecure();
        $url = $secure ? 'https://' : 'http://';
        $url .= $_SERVER['HTTP_HOST'];
        if (!self::$_disbalePortRecognition) {
            if ((!$secure && $_SERVER['SERVER_PORT'] != 80) ||
                ($secure && $_SERVER['SERVER_PORT'] != 443 &&
                    (array_key_exists('HTTP_SSL', $_SERVER) &&
                        $_SERVER['HTTP_SSL'] == 1 && $_SERVER['SERVER_PORT'] != 80))
            ) {
                $url .= ':' . intval($_SERVER['SERVER_PORT']);
            }
        }
        $url .= $_SERVER['REQUEST_URI'];
        return $url;
    }

    /**
     * sets wheter to enable port recognition during reading current url
     *
     * @param boolean $disable
     */
    public static function setDisablePortRecognition($disable = false)
    {
        self::$_disbalePortRecognition = (boolean)$disable;
    }

    /**
     * gets flag wheter to enable port recognition during reading current url
     *
     * @return boolean
     */
    public static function getDisbalePortRecognition()
    {
        return self::$_disbalePortRecognition;
    }

    /**
     * sets scheme
     *
     * @param string $scheme
     * @return wrxNext_Url
     */
    public function setScheme($scheme)
    {
        $this->_urlParts['scheme'] = $scheme;
        return $this;
    }

    /**
     * gets scheme
     *
     * @return string
     */
    public function getScheme()
    {
        return array_key_exists('scheme', $this->_urlParts) ? $this->_urlParts['scheme'] : null;
    }

    /**
     * sets host
     *
     * @param string $host
     * @return wrxNext_Url
     */
    public function setHost($host)
    {
        $this->_urlParts['host'] = $host;
        return $this;
    }

    /**
     * gets host
     *
     * @return string
     */
    public function getHost()
    {
        return array_key_exists('host', $this->_urlParts) ? $this->_urlParts['host'] : null;
    }

    /**
     * sets port
     *
     * @param string $port
     * @return wrxNext_Url
     */
    public function setPort($port)
    {
        $this->_urlParts['port'] = $port;
        return $this;
    }

    /**
     * gets port
     *
     * @return string
     */
    public function getPort()
    {
        return array_key_exists('port', $this->_urlParts) ? intval(
            $this->_urlParts['port']) : null;
    }

    /**
     * sets user
     *
     * @param string $username
     * @return wrxNext_Url
     */
    public function setUser($username)
    {
        $this->_urlParts['user'] = $username;
        return $this;
    }

    /**
     * gets user
     *
     * @return string
     */
    public function getUser()
    {
        return array_key_exists('user', $this->_urlParts) ? $this->_urlParts['user'] : null;
    }

    /**
     * sets password
     *
     * @param string $password
     * @return wrxNext_Url
     */
    public function setPassword($password)
    {
        $this->_urlParts['pass'] = $password;
        return $this;
    }

    /**
     * gets password
     *
     * @return string
     */
    public function getPassword()
    {
        return array_key_exists('pass', $this->_urlParts) ? $this->_urlParts['pass'] : null;
    }

    /**
     * sets path
     *
     * @param string $path
     * @return wrxNext_Url
     */
    public function setPath($path)
    {
        $this->_urlParts['path'] = $path;
        return $this;
    }

    /**
     * gets path
     *
     * @return string
     */
    public function getPath()
    {
        return array_key_exists('path', $this->_urlParts) ? $this->_urlParts['path'] : null;
    }

    /**
     * sets fragment
     *
     * @param string $fragment
     * @return wrxNext_Url
     */
    public function setFragment($fragment)
    {
        $this->_urlParts['fragment'] = $fragment;
        return $this;
    }

    /**
     * gets fragment
     *
     * @return string
     */
    public function getFragment()
    {
        return array_key_exists('fragment', $this->_urlParts) ? $this->_urlParts['fragment'] : null;
    }

    /**
     * returns array representation of query data
     *
     * @return array
     */
    public function getQueryStringData()
    {
        $data = array();
        parse_str($this->getQueryString(), $data);
        return $data;
    }

    /**
     * gets query string
     *
     * @return string
     */
    public function getQueryString()
    {
        return array_key_exists('query', $this->_urlParts) ? $this->_urlParts['query'] : null;
    }

    /**
     * sets query string from given array
     *
     * @param array $data
     * @param string $separator
     * @param int $enctype
     * @return wrxNext_Url
     */
    public function setQueryStringData($data, $separator = '&',
                                       $enctype = PHP_QUERY_RFC1738)
    {
        return $this->setQueryString(
            http_build_query($data, null, $separator, $enctype));
    }

    /**
     * sets query string
     *
     * @param string $querystring
     * @return wrxNext_Url
     */
    public function setQueryString($querystring)
    {
        $this->_urlParts['query'] = $querystring;
        return $this;
    }

    /**
     * magic method
     *
     * @return string
     */
    public function __toString()
    {
        return $this->buildUrl();
    }

    /**
     * builds url from internal data, optionally it can use recognised request
     * data as defaults
     *
     * @param boolean $useRequestData
     * @return string
     */
    public function buildUrl($useRequestData = false)
    {
        $urlParts = $this->_urlParts;
        if ($useRequestData) {
            $urlParts = array_merge(parse_url($this->_recognisedUrl()),
                $urlParts);
        }

        $url = '';
        if (array_key_exists('host', $urlParts) && !empty($urlParts['host'])) {
            // host exists proceeding with scheme, if schem is empty then
            // prefixing with http://
            $url .= isset($urlParts['scheme']) ? $urlParts['scheme'] . '://' : 'http://';
            // checking for given user & password
            if (array_key_exists('user', $urlParts)) {
                $url .= $urlParts['user'];
                if (isset($urlParts['pass']) && !empty($urlParts['pass'])) {
                    $url .= ':' . $urlParts['pass'];
                }
                $url .= '@';
            }
            $url .= $urlParts['host'];
            // checking if port is needed
            if (array_key_exists('port', $urlParts)) {
                $url .= ':' . $urlParts['port'];
            }
        }
        $url .= isset($urlParts['path']) ? $urlParts['path'] : '/';
        $url .= isset($urlParts['query']) ? '?' . $urlParts['query'] : '';
        $url .= isset($urlParts['fragment']) ? '#' . $urlParts['fragment'] : '';

        return $url;
    }

    /**
     * send redirect command to browser
     *
     * there can be changed http status code
     *
     * @param int $httpStatusCode
     * @param bool $useRequestData
     * @param bool $exitOnHeaderSend
     */
    public function redirect($httpStatusCode = 302, $useRequestData = false,
                             $exitOnHeaderSend = true)
    {
        $url = $this->buildUrl($useRequestData);
        header('Location: ' . $url, true, intval($httpStatusCode));
        if ($exitOnHeaderSend) {
            exit();
        }
    }
}
