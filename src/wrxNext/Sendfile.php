<?php

/**
 * klasa umozliwiajaca wysylanie plikow, zapewnia obsluge przekazywania
 * informacji o wielkosci pliku i nazwy, oraz dodatkowych naglowkow
 *
 */
class wrxNext_Sendfile
{

    const CONTENT_DISPOSITION_ATTACHMENT = "attachment";

    const CONTENT_DISPOSITION_MESSAGE = "message";

    /**
     * przechowuje standardowe nag�owki dla wysy�anego strumienia danych
     *
     * @var array
     */
    private $_headers = array(
        'Content-Type' => 'application/octet-stream',
        'Content-Disposition' => 'attachment',
        'Content-Length' => 0
    );

    /**
     * przechowuje sciezke do pliku ktory ma zostac wyslany
     *
     * @var string
     */
    private $_filePath;

    /**
     * okresla czy wysylac naglowek content-disposition
     * warto ustawic na false dla obrazow
     *
     * @var bool
     */
    private $_dispatchContentDisposition = true;

    /**
     * przechowuje nazwe pliku ktora zostanie wyslana do przegladarki
     *
     * @var string
     */
    private $_fileName;

    /**
     * konstruktor
     * jako parametr podawana jest sciezka do pliku
     *
     * @param string $filePath
     */
    function __construct($filePath = null)
    {
        if (!is_null($filePath)) {
            $this->setFile2Send($filePath);
        }
    }

    /**
     * ustawia wszystkie dane zwiazane z plikiem
     *
     * @param string $filePath
     * @param string $fileName
     * @param string $mimeType
     */
    public function setFile2Send($filePath, $fileName = null, $mimeType = null)
    {
        if (file_exists($filePath)) {
            $this->_filePath = $filePath;
            if (is_null($fileName)) {
                $this->_fileName = basename($filePath);
            } else {
                $this->_fileName = $fileName;
            }
            if (!is_null($mimeType)) {
                $this->setMimeType($mimeType);
            }
            $this->_headers['Content-Length'] = filesize($this->_filePath);
        } else {
            throw new Exception(
                'Given path to file "' . $filePath . '" does not exist');
        }
    }

    /**
     * ustawia typ MIME pliku
     *
     * @link wrxNext_Mime
     *
     * @param string $mimeType
     */
    public function setMimeType($mimeType)
    {
        $this->_headers['Content-Type'] = $mimeType;
    }

    /**
     * ustawia nazwe pliku jaka zostanie zaprezentowana przegladarce
     *
     * @param string $fileName
     */
    public function setFileName($fileName)
    {
        $this->_fileName = $fileName;
    }

    /**
     * wysy�a naglowki i zawartosc do przegladarki
     *
     * @param bool $exitAfterComplete
     */
    public function send($exitAfterComplete = true)
    {
        if (!file_exists($this->_filePath)) {
            throw new Exception(
                "Given filepath: " . $this->_filePath . " does not exist");
        }
        if ($this->_dispatchContentDisposition) {
            header('Content-Type: ' . $this->_headers['Content-Type']);
            header(
                'Content-Disposition: ' .
                $this->_headers['Content-Disposition'] .
                '; filename=' . $this->_fileName);
            header('Content-Length: ' . filesize($this->_filePath));
        }
        readfile($this->_filePath);
        if ($exitAfterComplete) {
            exit();
        }
    }

    /**
     * zwraca wszystkie ustawione nag�owki
     *
     * @return array
     */
    public function getHeaders()
    {
        return $this->_headers;
    }

    /**
     * implementacja magicznej metody get
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->_headers[$name];
    }

    /**
     * implementacja magicznej metody set
     *
     * @param string $name
     * @param string $value
     */
    public function __set($name, $value)
    {
        $this->setHeader($name, $value);
    }

    /**
     * ustawia nowy nag�owek
     *
     * @param string $name
     * @param string $value
     */
    public function setHeader($name, $value)
    {
        $this->_headers[$name] = $value;
    }

    /**
     * okresla czy wysylac naglowek content-disposition
     * dla obrazow warto ustawic na false
     *
     * @param bool $status
     */
    public function setDispatchContentDisposition($status)
    {
        $this->_dispatchContentDisposition = $status;
    }
}
