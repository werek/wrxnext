<?php

/**
 * logic object for handling password and random hash generation
 *
 * @author Bartlomiej Wereszczynski <werexx@gmail.com>
 *
 */
class wrxNext_Authentication
{

    /**
     * hashes given plain password
     *
     * @param string $plainPassword
     * @return string
     */
    public function generatePassword($plainPassword)
    {
        $salt = Zend_Registry::get('options')->salt;
        return hash('sha512', $plainPassword . $salt);
    }

    /**
     * generates random password up to given length
     *
     * @param int $length
     * @return string
     */
    public function random($length = 8)
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $special = ".-+=_,!@$#*%<>[]{}";
        $chars = $alpha . $alpha_upper . $numeric . $special;
        $chars = str_shuffle($chars);
        $len = strlen($chars);
        $pw = '';
        for ($i = 0; $i < $length; $i++) {
            $pw .= substr($chars, rand(0, $len - 1), 1);
        }
        $pw = str_shuffle($pw);
        return $pw;
    }

    /**
     * generates random alpha numeric string up to given length
     *
     * @param int $length
     * @return string
     */
    public function randomAlphaNumeric($length = 8)
    {
        $alpha = "abcdefghijklmnopqrstuvwxyz";
        $alpha_upper = strtoupper($alpha);
        $numeric = "0123456789";
        $chars = $alpha . $alpha_upper . $numeric;
        $chars = str_shuffle($chars);
        $len = strlen($chars);
        $pw = '';
        for ($i = 0; $i < $length; $i++) {
            $pw .= substr($chars, rand(0, $len - 1), 1);
        }
        $pw = str_shuffle($pw);
        return $pw;
    }

    /**
     * generates GUID for uniquely identify resource
     *
     * @return string
     */
    public function generateGuid()
    {
        $pr_bits = "";
        for ($cnt = 0; $cnt < 16; $cnt++) {
            $pr_bits .= chr(mt_rand(0, 255));
        }
        $time_low = bin2hex(substr($pr_bits, 0, 4));
        $time_mid = bin2hex(substr($pr_bits, 4, 2));
        $time_hi_and_version = bin2hex(substr($pr_bits, 6, 2));
        $clock_seq_hi_and_reserved = bin2hex(substr($pr_bits, 8, 2));
        $node = bin2hex(substr($pr_bits, 10, 6));

        /**
         * Set the four most significant bits (bits 12 through 15) of the
         * time_hi_and_version field to the 4-bit version number from
         * Section 4.1.3.
         *
         * @see http://tools.ietf.org/html/rfc4122#section-4.1.3
         */
        $time_hi_and_version = hexdec($time_hi_and_version);
        $time_hi_and_version = $time_hi_and_version >> 4;
        $time_hi_and_version = $time_hi_and_version | 0x4000;

        /**
         * Set the two most significant bits (bits 6 and 7) of the
         * clock_seq_hi_and_reserved to zero and one, respectively.
         */
        $clock_seq_hi_and_reserved = hexdec($clock_seq_hi_and_reserved);
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved >> 2;
        $clock_seq_hi_and_reserved = $clock_seq_hi_and_reserved | 0x8000;

        return sprintf('%08s-%04s-%04x-%04x-%012s', $time_low, $time_mid,
            $time_hi_and_version, $clock_seq_hi_and_reserved, $node);
    }
}
