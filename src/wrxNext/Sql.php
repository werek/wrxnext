<?php

/**
 * klasa zawierajaca przydatne elementy do SQL
 *
 */
class wrxNext_Sql
{

    const DATEFUNC_FORMAT_DATETIME = 'Y-m-d H:i:s';

    const DATEFUNC_FORMAT_DATE = 'Y-m-d';

    const DATEFUNC_FORMAT_TIME = 'H:i:s';
}