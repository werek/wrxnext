<?php

/**
 * klasa obslugujaca parametry konkretnej strony
 *
 */
class wrxNext_Site
{

    /**
     * przechowuje instancje wrxNext_Site
     *
     * @var wrxNext_Site
     */
    private static $_instance;
    /**
     * tablica cache'ujaca parametry strony
     *
     * @var array
     */
    private $_site_param_cache = array();

    function __construct()
    {
    }

    /**
     * zwraca instancje wrxNext_site
     *
     * @return wrxNext_Site
     */
    public static function param()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * zwraca parametr konfiguracyjny strony zapisany w bazie danych
     *
     * @param string $paramName
     * @return mixed
     */
    public function get($paramName)
    {
        if (isset($this->_site_param_cache[$paramName])) {
            return $this->_site_param_cache[$paramName];
        }
        $sql = "SELECT `value` FROM site_config WHERE `param_name`='" .
            $paramName . "'";
        $this->_site_param_cache[$paramName] = Zend_Db_Table_Abstract::getDefaultAdapter()->fetchOne(
            $sql);
        return $this->_site_param_cache[$paramName];
    }

    /**
     * czysci cache'owane parametry
     */
    public function flush()
    {
        $this->_site_param_cache = array();
    }

    /**
     * zwraca wszystkie parametry strony, opcjonalnie zamieniajac dane na forme
     * <param_name>=><value>
     *
     * @param bool $flat
     * @return array
     */
    public function all($flat = false)
    {
        $sql = "SELECT * FROM site_config";
        if (!$flat) {
            return Zend_Db_Table_Abstract::getDefaultAdapter()->fetchAll($sql);
        } else {
            $t = array();
            $rs = Zend_Db_Table_Abstract::getDefaultAdapter()->query($sql);
            while ($row = $rs->fetch($sql)) {
                $t[$row['param_name']] = $row['value'];
            }
            return $t;
        }
    }

    /**
     * wstawia w naglowek html redirect z opoznieniem w sekundach (zalezne od
     * @link wrxNext_Template )
     *
     * @param string $url
     * @param int $timeout
     */
    public function redirectHTML($url, $timeout = 5)
    {
        $tag = '<meta http-equiv="refresh" content="%s; url=%s">';
        wrxNext_Template::instance()->assign('site_meta_redirect',
            sprintf($tag, $timeout, $url));
    }
}
