<?php

abstract class wrxNext_Collection_Abstract extends ArrayObject
{

    /**
     * sets collection data
     *
     * @param array $data
     * @return wrxNext_Collection_Abstract
     */
    public function setCollection($data)
    {
        $this->exchangeArray($data);
        return $this;
    }

    /**
     * gets collection data
     *
     * @return array
     */
    public function getCollection()
    {
        return $this->getArrayCopy();
    }

    /**
     * checks if given key exists
     *
     * @param mixed $key
     * @return mixed
     */
    public function hasKey($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * checks if collection have given value
     *
     * @param mixed $value
     * @return boolean
     */
    public function has($value)
    {
        return in_array($value, (array)$this);
    }

    /**
     * sets value at given key
     *
     * @param mixed $key
     * @param mixed $value
     * @return wrxNext_Collection_Abstract
     */
    public function set($key, $value)
    {
        $this->offsetSet($key, $value);
        return $this;
    }

    /**
     * gets value at given key
     *
     * @param mixed $key
     * @return mixed
     */
    public function get($key)
    {
        return $this->offsetGet($key);
    }

    /**
     * returns key of given value, if strict is set to true.
     * compares also type of variable. by default type is not checked
     *
     * @param mixed $value
     * @param boolean $strict
     * @return mixed null null on failure
     */
    public function search($value, $strict = false)
    {
        return array_search($value, (array)$this, $strict);
    }
}
