<?php

class wrxNext_Autocomplete
{

    /**
     * holds autocomplete adapter
     *
     * @var wrxNext_Autocomplete_Adapter_Interface
     */
    protected $_adapter;

    /**
     * holds cache object
     *
     * @var Zend_Cache_Core
     */
    protected $_cache;

    /**
     * limits number of entries
     *
     * @var int
     */
    protected $_limit = null;

    /**
     * creates autocomplete adapter
     *
     * @param wrxNext_Autocomplete_Adapter_Interface|Zend_Db_Table_Abstract|Zend_Db_Select $source
     * @param array $options
     * @return wrxNext_Autocomplete
     */
    public static function factory($source, array $options = null)
    {
        $adapter = null;
        if ($source instanceof wrxNext_Autocomplete_Adapter_Interface) {
            $adapter = $source;
        } elseif ($source instanceof Zend_Db_Table_Abstract) {
            $adapter = new wrxNext_Autocomplete_Adapter_DbTable($source);
        } elseif ($source instanceof Zend_Db_Select) {
            $adapter = new wrxNext_Autocomplete_Adapter_DbSelect($source);
        }

        // check if proper adapter has been found
        if (!$adapter instanceof wrxNext_Autocomplete_Adapter_Interface) {
            $type = (is_object($source)) ? get_class($source) : gettype($source);
            throw new InvalidArgumentException(
                'autocomplete factory could not find suitable adapter for ' .
                $type);
        }
        if (isset($options['adapterOptions']) &&
            $adapter instanceof wrxNext_Autocomplete_Adapter_OptionsInterface
        ) {
            $adapter->searchOptions($options['adapterOptions']);
        }
        $autocomplete = new wrxNext_Autocomplete();
        $autocomplete->setAdapter($adapter);
        if (isset($options['cache'])) {
            $autocomplete->setCache($options['cache']);
        }
        if (isset($options['limit'])) {
            $autocomplete->setLimit($options['limit']);
        }
        if (isset($options['filter'])) {
            $autocomplete->setFilter($options['filter']);
        }
        return $autocomplete;
    }

    /**
     * gets cache object
     *
     * @return Zend_Cache_Core
     */
    public function getCache()
    {
        return $this->_cache;
    }

    /**
     * sets cache object
     *
     * @param Zend_Cache_Core $cache
     * @return wrxNext_Autocomplete
     */
    public function setCache(Zend_Cache_Core $cache)
    {
        $this->_cache = $cache;
        return $this;
    }

    /**
     * gets limit for returned entries
     *
     * @return int
     */
    public function getLimit()
    {
        return $this->_limit;
    }

    /**
     * sets limit for returned entries
     *
     * @param int $limit
     * @return wrxNext_Autocomplete
     */
    public function setLimit($limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    /**
     * searches term in given adapter
     *
     * @param string $term
     * @param int $limit
     * @return wrxNext_Autocomplete_Result
     */
    public function search($term, $limit = null)
    {
        if (is_null($limit)) {
            $limit = $this->_limit;
        }

        if (!$this->_cacheEnabled()) {
            $result = $this->_adapter->search($term, $limit);
        } else {
            $id = $this->_getCacheId($term, $limit);
            if (($result = $this->_cache->load($id)) === false) {
                $result = $this->_adapter->search($term, $limit);
                $this->_cache->save($result, $id);
            }
        }

        return $result;
    }

    /**
     * returns status of caching results
     *
     * @return boolean
     */
    protected function _cacheEnabled()
    {
        return ($this->_cache instanceof Zend_Cache_Core);
    }

    /**
     * returns cache id for given adapter
     *
     * @param string $term
     * @param int $limit
     * @return string
     */
    protected function _getCacheId($term, $limit = null)
    {
        $adapter = $this->getAdapter();

        if (method_exists($adapter, 'getCacheIdentifier')) {
            return 'autocomplete_' . md5(
                serialize(
                    array(
                        $adapter->getCacheIdentifier(),
                        $term,
                        $limit
                    )));
        } else {
            return 'autocomplete_' . md5(
                serialize(
                    array(
                        $adapter,
                        $term,
                        $limit
                    )));
        }
    }

    /**
     * gets autocomplete adapter
     *
     * @return wrxNext_Autocomplete_Adapter_Interface
     */
    public function getAdapter()
    {
        return $this->_adapter;
    }

    /**
     * sets autocomplete adapter
     *
     * @param wrxNext_Autocomplete_Adapter_Interface $adapter
     * @return wrxNext_Autocomplete
     */
    public function setAdapter(wrxNext_Autocomplete_Adapter_Interface $adapter)
    {
        $this->_adapter = $adapter;
        return $this;
    }
}
