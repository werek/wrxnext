<?php

class wrxNext_Cookie
{

    /**
     * czas wygasniecia ciasteczka
     *
     * @var int
     */
    protected $_expire = 0;

    /**
     * sciezka zrodlowa w adresie dla ktorej ciastko bedzie wazne
     * np.
     * /test/ sprawi ze zapisywana wartosc bedzie widuczna
     * tylko dla tego katalogu w url
     *
     * @var string
     */
    protected $_path = "";

    /**
     * domena dla ktorej zapisywane jest ciastko, jezeli podana zostanie domena
     * z kropka na poczatku to ciastko bedzie dostepne rowniez dla subdomen
     *
     * @var string
     */
    protected $_domain = "";

    /**
     * sprawi ze ciastko zostanie zapisane tylko podczas polaczenia szyfrowanego
     *
     * @var bool
     */
    protected $_secure = false;

    /**
     * sprawi ze ciastko bedzie dostepne tylko poprzez request HTTP,
     * wykluczajac wszelkie jezyki skryptowe, zapobiegajac XSS
     *
     * @var bool
     */
    protected $_httponly = false;

    /**
     * magiczna funkcja
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->get($name);
    }

    /**
     * magiczna funkcja
     *
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    public function __set($name, $value)
    {
        return $this->set($name, $value);
    }

    /**
     * zwraca wartosc ciastka
     *
     * @param string $name
     * @return mixed
     */
    public function get($name)
    {
        return $_COOKIE[$name];
    }

    /**
     * ustala wartosc ciastka
     *
     * @param string $name
     * @param mixed $value
     */
    public function set($name, $value = null)
    {
        if (is_array($value)) {
            $this->setArray($name, $value);
        } else {
            setcookie($name, (string)$value, $this->_expire, $this->_path,
                $this->_domain, $this->_secure, $this->_httponly);
            $p = strpos($name, "[");
            if ($p !== false) {
                $_baseName = substr($name, 0, $p + 1);
                $_arrayName = substr($name, $p);
                $_arrayName = strtr($_arrayName, array(
                    "[" => "['",
                    "]" => "']"
                ));
                $t = '$_COOKIE[\'%s\']%s=\'%s\';';
                $command = sprintf($t, $_baseName, $_arrayName, strval($value));
                eval($command);
            } else {
                $_COOKIE[$name] = (string)$value;
            }
        }
    }

    /**
     * wywoluje przejscie przez tablice
     *
     * @param string $baseName
     * @param array $array
     */
    protected function setArray($baseName, $array)
    {
        foreach ($array as $k => $v) {
            $this->set($baseName . "[" . $k . "]", $v);
        }
    }

    /**
     * zwraca czas zycia ciasteczka
     *
     * @return int
     */
    public function getExpire()
    {
        return $this->_expire;
    }

    /**
     * ustala czas zycia ciasteczka
     *
     * @param int $timestamp
     */
    public function setExpire($timestamp)
    {
        $this->_expire = (int)$timestamp;
    }

    /**
     * zwraca sciezke w domenie dla ciasteczka
     *
     * @return string
     */
    public function getPath()
    {
        return $this->_path;
    }

    /**
     * ustala sciezke w domenie dla ciasteczka
     *
     * @param string $path
     */
    public function setPath($path)
    {
        $this->_path = (string)$path;
    }

    /**
     * zwraca domene dla ciasteczka
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->_domain;
    }

    /**
     * ustala domene dla ciasteczka
     *
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->_domain = (string)$domain;
    }

    /**
     * zwraca info czy p[rzesylac ciastko tylko po szyfrowanym polaczeniu
     *
     * @return bool
     */
    public function getSecure()
    {
        return $this->_secure;
    }

    /**
     * ustala czy przesylac tylko po szyfrowanym polaczeniu
     *
     * @param bool $secure
     */
    public function setSecure($secure)
    {
        $this->_secure = (bool)$secure;
    }

    /**
     * zwraca czy wysylac ciastka tylko po HTTP
     *
     * @return bool
     */
    public function getHttponly()
    {
        return $this->_httponly;
    }

    /**
     * ustala czy wysylac ciastka tylko po HTTP
     *
     * @param bool $httponly
     */
    public function setHttponly($httponly)
    {
        $this->_httponly = (bool)$httponly;
    }
}
